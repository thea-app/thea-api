<?php

// cli-config.php

use DI\Container;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Slim\App;
use Slim\Factory\AppFactory;

/** @var Container $container */
$container = require_once __DIR__ . '/bootstrap.php';
$app = AppFactory::create();
$container = $app->getContainer();
return ConsoleRunner::createHelperSet($container->get(\Doctrine\ORM\EntityManagerInterface::class));