
# Úvod
Tento projekt je součástí řešení softwarového projektu THEA složeného ze tří částí, a to webové aplikace pro majitele podniků (čajoven, kaváren nebo vináren), mobilní aplikace pro klienty majitele a aplikačního rozhraní API. Účelem této části projektu je poskytnout potřebné aplikační rozhraní pro mobilní a webovou aplikaci. Navíc je součástí řešení i ukládání dat do databáze a komunikace s ní.

## Instalační manuál

Aplikace očekává poměrně standardní konfiguraci webového serveru. K úspěšnému spuštění aplikace je potřeba mít k dispozici Apache server s PHP 7.4, s povolenou konfigurací hlaviček přes soubory .htaccess a s rozšířením [Imagick](https://www.php.net/manual/en/book.imagick.php) nakonfigurovaným tak, aby podporoval obrázky ve formátu Jpeg, Png a Webp.

### Příprava pro produkční použití

Aplikaci není potřeba kompilovat, ale je potřeba nainstalovat všechny potřebné závislosti. K tomu slouží následující příkaz:

    composer install --optimize-autoloader --prefer-dist --no-dev

### Nasazení na produkční server

Po nainstalování závislostí stačí aplikaci nasadit nahráním kompletního obsahu adresáře s aplikací na server.