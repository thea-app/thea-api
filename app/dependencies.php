<?php
declare(strict_types=1);

use Acelaya\Doctrine\Type\PhpEnumType;
use App\Application\Model\Enum\CurrencyEnum;
use App\Application\Model\Enum\AdminTypeEnum;
use App\Application\Model\Enum\CountryEnum;
use App\Application\Model\Enum\DayEnum;
use App\Application\Model\Enum\EventFilterEnum;
use App\Application\Model\Enum\EventTypeEnum;
use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\LocaleEnum;
use App\Application\Model\Enum\NotificationGroupEnum;
use App\Application\Model\Enum\ShopTypeEnum;
use App\Application\Model\Enum\SocialsTypeEnum;
use App\Application\Model\Enum\GlobalRightsEnum;
use App\Application\Model\Enum\UserTypeEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\UserHasEmailNotificationsEnabledEnum;
use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Kreait\Firebase\Factory;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;


return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $container) {
            $settings = $container->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },

        \Doctrine\DBAL\Connection::class => function (ContainerInterface $container) {
            $settings = $container->get('settings');

            $connectionParams = [
                'dbname'   => $settings['doctrine']['connection']['dbname'],
                'user'     => $settings['doctrine']['connection']['user'],
                'password' => $settings['doctrine']['connection']['password'],
                'host'     => $settings['doctrine']['connection']['host'],
                'driver'   => $settings['doctrine']['connection']['driver'],
            ];

            $connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);
            $connection->connect();

            # Set up SQL logging
            $connection->getConfiguration()->setSQLLogger(new \Doctrine\DBAL\Logging\DebugStack());

            return $connection;
        },

        \Kreait\Firebase\Auth::class => function (ContainerInterface $container) {
            //get firebase credentials filename from settings
            $settings = $container->get('settings');
            $firebaseCredentials = $settings['firebaseCredentials'];

            $factory = (new Factory)->withServiceAccount(dirname(__DIR__, 1) . $firebaseCredentials);

            return $factory->createAuth();

        },

        \Kreait\Firebase\Messaging::class => function (ContainerInterface $container) {
            //get firebase credentials filename from settings
            $settings = $container->get('settings');
            $firebaseCredentials = $settings['firebaseCredentials'];

            $factory = (new Factory)->withServiceAccount(dirname(__DIR__, 1) . $firebaseCredentials);

            return $factory->createMessaging();

        },

        EntityManagerInterface::class => function (ContainerInterface $c): EntityManager {
            $doctrineSettings = $c->get('settings')['doctrine'];

            $config = Setup::createAnnotationMetadataConfiguration(
                $doctrineSettings['metadata_dirs'],
                $doctrineSettings['dev_mode'],
                null,
                null,
                false
            );

            AnnotationReader::addGlobalIgnoredName('TheCodingMachine\GraphQLite\Annotations\Type');
            AnnotationReader::addGlobalIgnoredName('TheCodingMachine\GraphQLite\Annotations\Field');
            AnnotationReader::addGlobalIgnoredName('TheCodingMachine\GraphQLite\Annotations\Logged');
            AnnotationReader::addGlobalIgnoredName('TheCodingMachine\GraphQLite\Annotations\Security');

            $config->setMetadataDriverImpl(
                new AnnotationDriver(
                    new AnnotationReader,
                    $doctrineSettings['metadata_dirs']
                )
            );

            $config->setMetadataCacheImpl(
                new FilesystemCache($doctrineSettings['cache_dir'])
            );

            $config->addCustomStringFunction("COLLATE", "App\\DoctrineExtensions\\CollateFunction");

            $config->addCustomNumericFunction('SIN', 'DoctrineExtensions\Query\Mysql\Sin');
            $config->addCustomNumericFunction('ASIN', 'DoctrineExtensions\Query\Mysql\Asin');
            $config->addCustomNumericFunction('COS', 'DoctrineExtensions\Query\Mysql\Cos');
            $em = EntityManager::create($doctrineSettings['connection'], $config);

            PhpEnumType::registerEnumTypes([
                'shop_type_enum' => ShopTypeEnum::class,
                'day_enum' => DayEnum::class,
                'admin_type_enum' => AdminTypeEnum::class,
                'socials_type_enum' => SocialsTypeEnum::class,
                'user_type_enum' => UserTypeEnum::class,
                'gender_enum' => GenderEnum::class,
                'entity_state_enum' => EntityStateEnum::class,
                'user_has_email_notification_enabled_enum' => UserHasEmailNotificationsEnabledEnum::class,
                'locale_enum' => LocaleEnum::class,
                'currency_enum' => CurrencyEnum::class,
                'country_enum' => CountryEnum::class,
                'event_type_enum' => EventTypeEnum::class,
                'event_filter_enum' => EventFilterEnum::class,
                'notification_group_enum' => NotificationGroupEnum::class,
                'global_rights_enum' => GlobalRightsEnum::class
            ]);
            $platform = $em->getConnection()->getDatabasePlatform();
            $platform->registerDoctrineTypeMapping('enum', 'string');

            $platform->registerDoctrineTypeMapping('VARCHAR', 'shop_type_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'day_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'admin_type_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'socials_type_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'user_type_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'gender_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'entity_state_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'user_has_email_notification_enabled_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'locale_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'currency_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'country_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'event_type_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'event_filter_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'notification_group_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'global_rights_enum');

            return $em;
        }
    ]);
};