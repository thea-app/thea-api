<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'displayErrorDetails' => (bool)($_ENV['DEVELOPMENT_MODE'] ?? false), // Should be set to false in production
            'firebaseCredentials' => '/firebase_credentials.json',
            'doctrine' => [
                'dev_mode' => (bool)($_ENV['DEVELOPMENT_MODE'] ?? false),
                'cache_dir' => __DIR__.'/../var/cache/doctrine',
                'metadata_dirs' => [__DIR__.'/../src'],
                'connection' => [
                    'driver'   => $_ENV['DB_DRIVER'] ?? 'pdo_mysql',
                    'host'     => $_ENV['DB_HOST'] ?? 'localhost',
                    'dbname'   => $_ENV['DB_NAME'] ?? 'd248163_testhea',
                    'user'     => $_ENV['DB_USERNAME'] ?? 'root',
                    'password' => $_ENV['DB_PASSWORD'] ?? '',
                    'charset' => 'utf8',
                    'default_table_options'  => [
                        'charset' => 'utf8',
                        'collate' => 'utf8_general_ci'
                    ]
                 ]
            ],
            'logger' => [
                'name' => 'slim-app',
                'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                'level' => Logger::DEBUG,
            ]
        ]
    ]);
};
