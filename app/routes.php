<?php
declare(strict_types=1);

use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;
use App\Application\Controllers\GraphQLController;
use App\Application\Controllers\MediaController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('');
        return $response;
    });

    $app->group('/users', function (Group $group) {
        $group->get('', ListUsersAction::class);
        $group->get('/{id}', ViewUserAction::class);
    });

    $app->options('/graphql', function (Request $request, Response $response) {
        return $response;
    });
    $app->get('/graphql', GraphQLController::class . ':index');
    $app->post('/graphql', GraphQLController::class . ':index');

    $app->group('/media', function (Group $group) {
        $group->get('/{dimensions}/{name}', MediaController::class . ":getSizedImage");
    });
};
