<?php


namespace App\Application\Services\SecurityService;


use App\Application\ApplicationDefaults;
use App\Application\Model\Entities\User;
use App\Application\Model\Enum\EntityStateEnum;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Factory\AppFactory;
use TheCodingMachine\GraphQLite\Security\AuthenticationServiceInterface;

class AuthenticationService implements AuthenticationServiceInterface
{
    private $userRepository;

    public function isLogged(): bool
    {
        return isset($_SESSION) && isset($_SESSION['uid']) &&
               isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true;
    }

    public function getUser(): ?object
    {
        if (is_null($this->userRepository)) {
            $entityManager = AppFactory::create()->getContainer()->get(EntityManagerInterface::class);
            $this->userRepository = $entityManager->getRepository(ApplicationDefaults::$userTable);
        }

        /** @var User $foundUser */
        $foundUser = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));

        if (is_null($foundUser) || ($foundUser->getState() == EntityStateEnum::DELETED)) {
            return null;
        }

        return $foundUser;
    }
}