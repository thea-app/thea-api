<?php


namespace App\Application\Services\SecurityService;


use App\Application\Model\Entities\Shop;
use App\Application\Model\Entities\User;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\Enum\GlobalRightsEnum;
use App\Application\Model\Enum\UserTypeEnum;
use TheCodingMachine\GraphQLite\Security\AuthorizationServiceInterface;

class AuthorizationService implements AuthorizationServiceInterface
{
    private AuthenticationService $authenticationService;
    private static $instance;

    public static function getInstance(): AuthorizationService {
        if (is_null(self::$instance))
            self::$instance = new self();
        return self::$instance;
    }

    public function setAuthenticationService(AuthenticationService $authenticationService) {
        $this->authenticationService = $authenticationService;
    }

    private function __construct() {}

    public function isAllowed(string $right, $subject = null): bool
    {
        /** @var User $caller */
        $caller = $this->authenticationService->getUser();

        if (is_null($caller))                                           //if the caller is unknown
            return false;

        if ($caller->getType() == UserTypeEnum::SUPERUSER)              //if the caller is superuser
            return true;

        if (GlobalRightsEnum::isValid($right))                          //if the right is global
            return $caller->isAllowed(new GlobalRightsEnum($right));

        if (!ScopeRightsEnum::isValid($right))                          //if the right is not scope based, don't allow
            return false;

        if (is_null($subject))                                          //if the scope is null, allow - there should be something like no data in database
            return true;

        switch ($right) {                                               //the right is based on scope ($subject)
            case ScopeRightsEnum::CAN_MANAGE_USER:
                return !$subject instanceof User ? true
                    : $this->canManageUser($caller, $subject);

            case ScopeRightsEnum::CAN_GET_PRIVATE_USER_INFO:
                return !$subject instanceof User ? true
                    : $caller->getId() == $subject->getId();

            case ScopeRightsEnum::CAN_GET_PRIVATE_SHOP_INFO:
            case ScopeRightsEnum::CAN_MANAGE_SHOP:
            case ScopeRightsEnum::CAN_MANAGE_ACCESS_CODES:
            case ScopeRightsEnum::CAN_MANAGE_BONUSES:
            case ScopeRightsEnum::CAN_MANAGE_MENU:
            case ScopeRightsEnum::CAN_MANAGE_EVENTS:
            case ScopeRightsEnum::CAN_MANAGE_NOTIFICATIONS:
            case ScopeRightsEnum::CAN_MANAGE_VIP:
            case ScopeRightsEnum::CAN_MANAGE_SHOP_ADMINS:
                return !$subject instanceof Shop ? true
                    : $caller->isAdminOfShop($subject);

            default:
                return false;
        }
    }

    /**
     * @param User $caller
     * @param User $target
     * @return bool
     */
    private function canManageUser(User $caller, User $target): bool
    {
        if ($caller->getId() == $target->getId())                       //user can get himself
            return true;

        foreach ($caller->getAdminOfShops() as $callerAdminShop) {      //or any other admin of the same shop
            foreach ($target->getAdminOfShops() as $targetUserAdminShop) {
                if ($callerAdminShop->getId() == $targetUserAdminShop->getId())
                    return true;
            }
        }
        return false;
    }

    /**
     * @param User $caller
     * @param User $target
     * @return bool
     */
    private function canGetPrivateUserInfo(User $caller, User $target): bool
    {
        return $caller->getId() == $target->getId();                    //user can get himself
    }
}

