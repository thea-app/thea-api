<?php

namespace App\Application\Services;

use App\Application\Services\MediaService\MediaServiceException;
use Exception;
use Imagick;
use ImagickException;

class MediaService
{
    const FORMAT_WEBP = "webp";
    const FORMAT_JPEG = "jpg";
    const FORMAT_PNG = "png";

    const VALID_FORMATS = [
        self::FORMAT_WEBP,
        self::FORMAT_JPEG,
        self::FORMAT_PNG
    ];

    const BASE_IMAGE_FORMAT = self::FORMAT_WEBP;

    /**
     * Mime type overrides, where the key is the format and the value is the mimetype
     */
    const MIME_TYPES = [
        self::FORMAT_JPEG => "image/jpeg"
    ];

    /**
     * The compression parameter for saving JPEG images. A value between 1 and 100
     *
     * @var int
     */
    private int $jpegCompression;

    /**
     * Name of an existing directory, used to store and retrieve images
     *
     * @var string
     */
    private string $userFilesDirname;

    /**
     * The width of the base image
     *
     * @var int
     */
    private int $baseImageWidth;

    /**
     * The height of the base image
     *
     * @var int
     */
    private int $baseImageHeight;

    /**
     * Size of a breakpoint used when retrieving an image of a given size
     * The dimensions will be ceiled to a product of this number (while keeping the aspect ratio)
     * to avoid excess amounts of cached images.
     *
     * The only exception is when the ceiled value would exceed the size of the base dimensions,
     * in this case the base dimension will be used regardless of any ceiling.
     *
     * @var int
     */
    private int $breakPointStepSize;

    public function __construct()
    {
        $this->setJpegCompression(90);
        $this->setUserFilesDirname(dirname(__DIR__, 3) . "/userfiles");
        $this->setBaseImageWidth(1920);
        $this->setBaseImageHeight(1920);
        $this->setBreakPointStepSize(20);
    }

    // <editor-fold desc="Getters and setters of the configuration variables" defaultstate="collapsed">

    /**
     * @return int
     */
    public function getJpegCompression(): int
    {
        return $this->jpegCompression;
    }

    /**
     * @param int $jpegCompression
     */
    public function setJpegCompression(int $jpegCompression): void
    {
        if ($jpegCompression < 1 || $jpegCompression > 100) {
            throw new MediaServiceException(
                "JPEG compression must be an integer between 1 and 100.",
                MediaServiceException::CODE_CONFIG_ERROR
            );
        }
        $this->jpegCompression = $jpegCompression;
    }

    /**
     * @return string
     */
    public function getUserFilesDirname(): string
    {
        return $this->userFilesDirname;
    }

    /**
     * @param string $userFilesDirname
     */
    public function setUserFilesDirname(string $userFilesDirname): void
    {
        if (!is_dir($userFilesDirname)) {
            throw new MediaServiceException(
                "The directory '" . $userFilesDirname . "' does not exist or is not a directory.",
                MediaServiceException::CODE_CONFIG_ERROR
            );
        }
        $this->userFilesDirname = $userFilesDirname;
    }

    /**
     * @return int
     */
    public function getBaseImageWidth(): int
    {
        return $this->baseImageWidth;
    }

    /**
     * @param int $baseImageWidth
     */
    public function setBaseImageWidth(int $baseImageWidth): void
    {
        if ($baseImageWidth < 1) {
            throw new MediaServiceException(
                "Base image width must be a positive integer.",
                MediaServiceException::CODE_CONFIG_ERROR
            );
        }
        $this->baseImageWidth = $baseImageWidth;
    }

    /**
     * @return int
     */
    public function getBaseImageHeight(): int
    {
        return $this->baseImageHeight;
    }

    /**
     * @param int $baseImageHeight
     */
    public function setBaseImageHeight(int $baseImageHeight): void
    {
        if ($baseImageHeight < 1) {
            throw new MediaServiceException(
                "Base image height must be a positive integer.",
                MediaServiceException::CODE_CONFIG_ERROR
            );
        }
        $this->baseImageHeight = $baseImageHeight;
    }

    /**
     * @return int
     */
    public function getBreakPointStepSize(): int
    {
        return $this->breakPointStepSize;
    }

    /**
     * @param int $breakPointStepSize
     */
    public function setBreakPointStepSize(int $breakPointStepSize): void
    {
        $this->breakPointStepSize = $breakPointStepSize;
    }

    //</editor-fold>

    /**
     * Processes a user-uploaded image and saves it for further use. Returns an unique image identifier,
     * that can be used to retrieve sized versions of the image in the future.
     *
     * @param string $imageString The user-uploaded image represented by a Base64-encoded file
     * @return string The unique identifier of the newly processed image
     * @throws MediaServiceException
     */
    public function importFromBase64(string $imageString): string
    {
        return $this->generateBaseImage($this->base64ToBlob($imageString));
    }

    /**
     * A shorthand function for deleting and old image and importing a new one
     *
     * The newly imported image will have a new identifier (instead of using the identifier of the old image),
     * to enable simpler caching of existing images.
     *
     * @param string $newImageString The user-uploaded image represented by a Base64-encoded file
     * @param string $oldImageIdentifier Identifier of an image that should be deleted.
     * @return string The unique identifier of the newly processed image
     */
    public function replaceFromBase64(string $newImageString, string $oldImageIdentifier): string
    {
        $this->deleteImage($oldImageIdentifier);
        return $this->importFromBase64($newImageString);
    }

    /**
     * Returns an image based on the identifier, format and the provided dimensions
     *
     * The dimensions of the returned image will be likely not exactly the same as the requested values - the image
     * will be resized to fit a rectangle of dimensions $width x $height rounded up to the closest breakpoints.
     * The size of the rectangle is furthermore capped by the dimensions of the base image.
     *
     * Therefore at least one dimension of the image might be slightly larger than requested.
     *
     * @param string $identifier
     * @param string $format
     * @param int    $width
     * @param int    $height
     * @return string
     * @throws MediaServiceException
     */
    public function getImageByIdentifier(string $identifier, string $format, int $width, int $height): string
    {
        if (!in_array($format, self::VALID_FORMATS)) {
            throw new MediaServiceException(
                "Invalid image format.",
                MediaServiceException::CODE_INVALID_IMAGE_FORMAT
            );
        }

        $directory = $this->getDirnameFromIdentifier($identifier);

        if (!file_exists($directory)) {
            throw new MediaServiceException(
                "Image not found.",
                MediaServiceException::CODE_IMAGE_NOT_FOUND
            );
        }

        $ceiledWidth = $this->ceilWidthToStepSize($width);
        $ceiledHeight = $this->ceilHeightToStepSize($height);

        $path = $this->getSizedImagePath($identifier, $format, $ceiledWidth, $ceiledHeight);

        if (!file_exists($path)) {
            $this->generateSizedImage($identifier, $format, $ceiledWidth, $ceiledHeight);
        }

        $contents = file_get_contents($path);
        if ($contents === false) {
            throw new MediaServiceException(
                "Image cannot be opened.",
                MediaServiceException::CODE_GENERIC_SERVER_ERROR
            );
        }

        return $contents;
    }

    /**
     * Deletes an image and all its generated versions.
     *
     * @param string $identifier
     */
    public function deleteImage(string $identifier)
    {
        $dirname = $this->getDirnameFromIdentifier($identifier);
        if (file_exists($dirname)) {
            $this->deleteRecursive($dirname);
        }
    }

    /**
     * Converts a base64 string of an image and returns a binary blob string containing the image data
     *
     * @param string $imageString
     * @return false|string
     * @throws MediaServiceException
     */
    protected function base64ToBlob(string $imageString)
    {
        if ($binary = base64_decode($imageString, true)) {
            return $binary;
        } else {
            throw new MediaServiceException(
                "The input is not a valid Base64 string.",
                MediaServiceException::CODE_INVALID_INPUT_IMAGE
            );
        }
    }

    /**
     * Save the binary blob string in predefined image sizes and formats
     *
     * @param string $imageBlob
     * @return string The identifier of the new image
     * @throws MediaServiceException
     */
    protected function generateBaseImage(string $imageBlob): string
    {
        try {
            $image = new Imagick();

            try {
                $image->readImageBlob($imageBlob);
            } catch (ImagickException $e) {
                throw new MediaServiceException(
                    "The image cannot be imported.",
                    MediaServiceException::CODE_INVALID_INPUT_IMAGE
                );
            }

            $this->stripImageMetadata($image);

            if (abs($this->getBaseImageWidth() - $image->getImageWidth()) < 50
                || abs($this->getBaseImageHeight() - $image->getImageHeight()) < 50) {
                $image->adaptiveResizeImage($this->getBaseImageWidth(), $this->getBaseImageHeight(), true);
            } else {
                $image->resizeImage(
                    $this->getBaseImageWidth(),
                    $this->getBaseImageHeight(),
                    Imagick::FILTER_LANCZOS,
                    1,
                    true
                );
            }

            $identifier = $this->generateImageIdentifier();

            $this->saveImage($image, $this->getBaseImagePath($identifier), self::BASE_IMAGE_FORMAT);

            return $identifier;
        } catch (ImagickException $e) {
            throw new MediaServiceException(
                "The image cannot be imported.",
                MediaServiceException::CODE_GENERIC_SERVER_ERROR,
                $e
            );
        }
    }

    /**
     * Ceils the value to the closest bigger or equal value that is divisible by the break point size.
     *
     * @param int $value
     * @return int
     */
    protected function ceilToStepSize(int $value): int
    {
        return intval(ceil($value / $this->getBreakPointStepSize()) * $this->getBreakPointStepSize());
    }

    /**
     * Ceils the width to be divisible by the break point size. If the resulting value would be bigger
     * than the base image width, the base image width is used instead.
     *
     * @param int $width
     * @return int
     */
    protected function ceilWidthToStepSize(int $width): int
    {
        return min($this->getBaseImageWidth(), $this->ceilToStepSize($width));
    }

    /**
     * Ceils the height to be divisible by the break point size. If the resulting value would be bigger
     * than the base image height, the base image height is used instead.
     *
     * @param int $height
     * @return int
     */
    protected function ceilHeightToStepSize(int $height): int
    {
        return min($this->getBaseImageHeight(), $this->ceilToStepSize($height));
    }

    /**
     * Generates a sized image of the specified dimensions and format based on a based image specified by the identifier
     *
     * @param string $identifier
     * @param string $format
     * @param int    $width
     * @param int    $height
     * @throws MediaServiceException
     */
    protected function generateSizedImage(string $identifier, string $format, int $width, int $height)
    {
        try {
            $image = new Imagick();
            $image->readImage($this->getBaseImagePath($identifier));

            if ($this->getBaseImageWidth() - $width < 50 || $this->getBaseImageHeight() - $height < 50) {
                $image->adaptiveResizeImage($width, $height, true);
            } else {
                $image->resizeImage($width, $height, Imagick::FILTER_LANCZOS, 1, true);
            }

            $imagePath = $this->getSizedImagePath($identifier, $format, $width, $height);
            $this->saveImage($image, $imagePath, $format);
        } catch (ImagickException $e) {
            throw new MediaServiceException(
                "The image cannot be generated.",
                MediaServiceException::CODE_GENERIC_SERVER_ERROR,
                $e
            );
        }
    }

    /**
     * Strip image metadata while keeping the orientation and ICC color profile
     *
     * @param Imagick $image
     */
    protected function stripImageMetadata(Imagick $image)
    {
        $profiles = $image->getImageProfiles("icc", true);

        $this->fixImageRotation($image);

        $image->stripImage();

        if (!empty($profiles['icc'])) {
            $image->profileImage("icc", $profiles['icc']);
        }
    }

    /**
     * Convert the EXIF orientation constants to degrees
     *
     * @param int $orientation
     * @return int
     */
    protected function orientationToDegrees(int $orientation)
    {
        switch ($orientation) {
            case imagick::ORIENTATION_BOTTOMRIGHT:
                return 180;
                break;
            case imagick::ORIENTATION_RIGHTTOP:
                return 90;
                break;
            case imagick::ORIENTATION_LEFTBOTTOM:
                return -90;
                break;
        }

        return 0;
    }

    /**
     * Rotate the image based on its EXIF orientation data
     *
     * @param Imagick $image
     */
    protected function fixImageRotation(Imagick $image)
    {
        $degrees = $this->orientationToDegrees($image->getImageOrientation());
        $image->rotateimage("#000", $degrees);
        $image->setImageOrientation(imagick::ORIENTATION_TOPLEFT);
    }

    protected function getDirnameFromIdentifier(string $identifier): string
    {
        return $this->getUserFilesDirname() . "/" . $identifier;
    }

    protected function getBaseImagePath(string $identifier): string
    {
        return $this->getDirnameFromIdentifier($identifier) . "/base." . self::BASE_IMAGE_FORMAT;
    }

    protected function getSizedImagePath(string $identifier, string $format, int $width, int $height): string
    {
        return $this->getDirnameFromIdentifier($identifier) . "/" . $width . "_" . $height . "." . $format;
    }

    protected function generateImageIdentifier()
    {
        try {
            $limit = 100;
            for ($i = 0; $i < $limit; $i++) {
                $randomIdentifier = bin2hex(random_bytes(20));
                $dirname = $this->getDirnameFromIdentifier($randomIdentifier);
                if (!file_exists($dirname)) {
                    mkdir($dirname);
                    return $randomIdentifier;
                }
            }

            throw new MediaServiceException(
                "Random dirname cannot be generated.",
                MediaServiceException::CODE_GENERIC_SERVER_ERROR
            );
        } catch (Exception $e) {
            throw new MediaServiceException(
                "Random dirname cannot be generated.",
                MediaServiceException::CODE_GENERIC_SERVER_ERROR,
                $e
            );
        }
    }

    /**
     * Save the image to a file
     *
     * @param Imagick $image
     * @param string  $filepath
     * @param string  $format
     */
    protected function saveImage(Imagick $image, string $filepath, string $format)
    {
        // validate format
        if (!in_array($format, self::VALID_FORMATS)) {
            throw new MediaServiceException(
                "Invalid image format.",
                MediaServiceException::CODE_INVALID_IMAGE_FORMAT
            );
        }

        // format-specific settings
        if ($format === self::FORMAT_JPEG) {
            $image->setImageCompression(Imagick::COMPRESSION_JPEG);
            $image->setImageCompressionQuality($this->getJpegCompression());
        }

        // if the file already exists, delete it first
        if (file_exists($filepath)) {
            unlink($filepath);
        }

        // save the image
        $image->writeImage($filepath);
    }

    /**
     * Deletes a file or a directory on the given path.
     * If the directory is not empty, the contents of the directory are recursively deleted as well.
     *
     * @param string $path
     */
    protected function deleteRecursive(string $path) {
        if (is_dir($path)) {
            $contents = array_diff(scandir($path), [".", ".."]);
            foreach ($contents as $subpath) {
                $this->deleteRecursive($path . "/" . $subpath);
            }
            rmdir($path);
        } else {
            unlink($path);
        }
    }

    public static function getMimeType(string $format): string
    {
        if (!empty(self::MIME_TYPES[$format])) {
            return self::MIME_TYPES[$format];
        }

        return "image/" . $format;
    }
}