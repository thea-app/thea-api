<?php


namespace App\Application;


use App\Application\Model\Entities\Language;
use App\Application\Model\Enum\LocaleEnum;
use App\Application\Model\GraphQLTypes\Translation;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Factory\AppFactory;

class Utils
{

    /**
     * @param string|null $preferredLocale
     * @param string $getterFunc
     * @param array|object|null $translations
     * @return string|null
     */
    public static function getFirstAvailableTranslation(?string $preferredLocale, string $getterFunc, $translations) : ?string {
        //first try to get translated field by preferred locale
        $lang = Language::getLanguageByLocale($preferredLocale);
        if (isset($lang) && isset($translations[$lang->getLanguageId()])) {
            $value = $translations[$lang->getLanguageId()]->$getterFunc();
            if (!empty($value))
                return $value;
            else
                return "";
        }

        //then find in all locales based on enum position
        foreach (LocaleEnum::values() as $locale) {
            if  ($locale->getValue() == $preferredLocale) //no need to try get from preferred locale, there is nothing in it
                continue;
            $lang = Language::getLanguageByLocale($locale);
            if (isset($translations[$lang->getLanguageId()])) {
                $value = $translations[$lang->getLanguageId()]->$getterFunc();
                if (!empty($value))
                    return $value;
            }
        }
        return "";
    }

    /**
     * @param string $getterFunc
     * @param $translations
     * @return Translation[]
     */
    public static function getAllTranslations(string $getterFunc, $translations): array
    {
        $result = [];
        if (is_array($translations) || is_object($translations))
            foreach($translations as $translation) {
                $lang = $translation->getLanguage();
                $value = $translation->$getterFunc();
                if (is_null($value))
                    continue;
                $result[] = Translation::create(
                    $lang,
                    $translation->$getterFunc()
                );
            }
        return $result;
    }

    /**
     * @param string $imageIdentifier
     * @param string $width
     * @param string $height
     * @param string $format
     * @return string
     */
    public static function buildMediaDownloadURL(string $imageIdentifier, string $width, string $height, string $format): string {
        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/media/' . $width . ':' . $height . '/' . $imageIdentifier . '.' . $format;
    }

    public static function generateEntityUniqueCode($entity, $length = 8, $values = '0123456789', $codeColumnName = 'code') : string
    {
        $entityManager = AppFactory::create()->getContainer()->get(EntityManagerInterface::class);

        $result = '';
        for ($i = 0; $i < $length; $i++){
            $result .= $values[rand(0, strlen($values)-1)];
        }

        $foundEntity = $entityManager->getRepository(get_class($entity))->findOneBy([$codeColumnName => $result]);

        if (!is_null($foundEntity)) {
            $result = Utils::generateEntityUniqueCode($entity, $length, $values, $codeColumnName);
        }

        return $result;
    }

}