<?php


namespace App\Application;


class CustomQueryDefaults
{
    public static function selectSimilarShops
    (
        string $name,
        string $street,
        string $city,
        ?string $descriptiveNumber,
        ?string $routingNumber
    ) : string {
        if(is_null($descriptiveNumber))
            $descriptiveNumberSelectionPart = 'shop.descriptiveNumber IS NULL';
        else
            $descriptiveNumberSelectionPart = 'shop.descriptiveNumber=' . $descriptiveNumber;

        if(is_null($routingNumber))
            $routingNumberSelectionPart = 'shop.routingNumber IS NULL';
        else
            $routingNumberSelectionPart = 'shop.routingNumber=' . $routingNumber;


        return 'SELECT shop 
                FROM App\Application\Model\Entities\Shop shop 
                WHERE COLLATE(\'name\', utf8_general_ci, false) LIKE COLLATE(\'%' . $name . '%\', utf8_general_ci, true) 
                    AND COLLATE(\'street\', utf8_general_ci, false) LIKE COLLATE(\'%' . $street . '%\', utf8_general_ci, true) 
                    AND COLLATE(\'city\', utf8_general_ci, false) LIKE COLLATE(\'%' . $city . '%\', utf8_general_ci, true)
                    AND ' . $descriptiveNumberSelectionPart .' 
                    AND ' . $routingNumberSelectionPart;
    }
}