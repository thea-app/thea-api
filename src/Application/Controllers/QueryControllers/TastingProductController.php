<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\ApplicationDefaults;
use App\Application\Model\Entities\Product;
use App\Application\Model\Entities\Tasting;
use App\Application\Model\Entities\TastingProduct;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\Exceptions\InputNotValidException;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateTastingProductInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetEntityOrderingInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetTastingProductInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetTastingProductResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetTastingProductsResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class TastingProductController. Resolves queries and mutations connected with TastingProduct entity
 *
 * @package App\Application\Controllers\QueryControllers
 */
class TastingProductController extends BaseController
{

    /**
     * CreateTastingProduct mutation
     *
     * Creates tasting product from given product for given tasting.
     * Create is possible only if tasting has not started yet.
     *
     * @Mutation(name="createTastingProduct")
     * @Logged
     * @Security("this.canManageTastingProduct(null, inputData.getTastingId())")
     *
     * @UseInputType(for="$inputData", inputType="CreateTastingProductInput")
     *
     * @param CreateTastingProductInput $inputData
     * @return GetTastingProductResponse
     */
    public function createTastingProduct(CreateTastingProductInput $inputData) : GetTastingProductResponse {
        $result = new GetTastingProductResponse();

        try {
            /** @var Product $product */
            $product = $this->productRepository->findOneBy((array('id' => $inputData->getProductId())));

            if (is_null($product))
                throw new Exception("Create failed: Product does not exist in DB");

            /** @var Tasting $tasting */
            $tasting = $this->tastingRepository->findOneBy((array('id' => $inputData->getTastingId())));

            if (is_null($tasting))
                throw new Exception("Create failed: Tasting does not exist in DB");

            if ($tasting->started())
                throw new Exception("Create failed: Tasting has already started");

            if ($product->getShop()->getId() != $tasting->getShop()->getId())
                throw new Exception("Create failed: Product is not available for this shop");

            //Create new tasting product entity
            $newTastingProduct = TastingProduct::create(
                $product,
                $tasting,
                $inputData->getDescriptionTranslations(),
                $inputData->getOrdering()
            );

            $this->entityManager->persist($newTastingProduct);
            $this->entityManager->flush();

            $result->setData($newTastingProduct);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetTastingProduct query
     *
     * Gets tasting product based on id
     *
     * @Query(name="getTastingProduct")
     * @Logged
     *
     * @param int $tastingProductId
     *
     * @return GetTastingProductResponse
     */
    public function getTastingProduct(int $tastingProductId) : GetTastingProductResponse {
        $result = new GetTastingProductResponse();

        try {

            /** @var TastingProduct $foundTastingProduct */
            $foundTastingProduct = $this->tastingProductRepository->findOneBy((array('id' => $tastingProductId)));
            if (is_null($foundTastingProduct))
                throw new Exception("Tasting product does not exist in DB");

            $result->setData($foundTastingProduct);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /*** GetTastingProducts query - basic query resolver used for getting tasting products based on given filter parameters.
     * If the tastingId is set, returns only tasting products in given tasting (if it does exist), doesn't take into account the shopId.
     * If the shopId is set and tastingId is null, then returns all tasting products from given shop, if it does exist.
     * Either tastingId or shopId must be set.
     *
     * @Query(name="getTastingProducts")
     * @Logged
     *
     * @param int|null $limit number of records to select
     * @param int|null $offset position from which to start selecting
     * @param int|null $shopId shop
     * @param int|null $tastingId tasting
     * @param string $searchValue string to be used for filtering product names
     * @return GetTastingProductsResponse
     */
    public function getTastingProducts(?int $limit = 10, int $offset = null, int $shopId = null,
                                       int $tastingId = null, string $searchValue = "") : GetTastingProductsResponse {
        $result = new GetTastingProductsResponse();

        try {
            $data = $this->tastingProductRepository->createQueryBuilder('product')
                ->select(array('product'))
                ->join(ApplicationDefaults::$tastingProductTranslationTable, 't', 'WITH', 'product.id = t.product')
                ->andWhere('COLLATE(\'name\', utf8_general_ci, false) LIKE COLLATE(:name, utf8_general_ci, true)')
                ->setParameter('name', '%'.$searchValue.'%');

            //todo logika bude asi ina
            if (!is_null($tastingId))
                $data = $data->andWhere('product.tasting = ' . $tastingId);
            else if (!is_null($shopId))
                $data = $data->andWhere('product.shop = ' . $shopId);
            else
                throw new InputNotValidException("Either tastingId or shopId must not be null");

            $data = $data->orderBy('product.ordering')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();

            $result->setData($data);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetTastingProduct mutation
     *
     * Sets tasting product based on id.
     * Set is possible only if the tasting has not started yet.
     *
     * @Mutation(name="setTastingProduct")
     * @Logged
     * @Security("this.canManageTastingProduct(inputData.getTastingProductId())")
     *
     * @UseInputType(for="$inputData", inputType="SetTastingProductInput")
     *
     * @param SetTastingProductInput $inputData
     * @return GetTastingProductResponse
     */
    public function setTastingProduct(SetTastingProductInput $inputData) : GetTastingProductResponse {
        $result = new GetTastingProductResponse();

        try {
            /** @var TastingProduct $foundTastingProduct */
            $foundTastingProduct = $this->tastingProductRepository->findOneBy((array('id' => $inputData->getTastingProductId())));
            if (is_null($foundTastingProduct)) {
                $result->setData(null);
                throw new Exception("Set failed: Tasting product does not exist in database");
            }

            /** @var Product $product */
            $product = $this->productRepository->findOneBy((array('id' => $inputData->getProductId())));
            if (is_null($product)) {
                $result->setData(null);
                throw new Exception("Set failed: Product does not exist in database");
            }

            /** @var Tasting $tasting */
            $tasting = $this->tastingRepository->findOneBy((array('id' => $inputData->getTastingId())));
            if (is_null($tasting)) {
                $result->setData(null);
                throw new Exception("Set failed: Tasting does not exist in database");
            }

            if ($tasting->started())
                throw new Exception("Set failed: Tasting has already started");

            if ($foundTastingProduct->getTasting()->started())
                throw new Exception("Set failed: Tasting has already started");

            $foundTastingProduct->setProduct($product);
            $foundTastingProduct->setTasting($tasting);
            $foundTastingProduct->setDescriptionTranslations($inputData->getDescriptionTranslations());

            $this->entityManager->persist($foundTastingProduct);
            $this->entityManager->flush();

            $result->setData($foundTastingProduct);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteTastingProduct mutation
     *
     * Deletes tasting product based on id.
     * Delete is possible only if the tasting has not started yet.
     *
     * @Mutation(name="deleteTastingProduct")
     * @Logged
     * @Security("this.canManageTastingProduct(tastingProductId)")
     *
     * @param int $tastingProductId
     * @return RemoveItemResponse
     */
    public function deleteTastingProduct(int $tastingProductId) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            $foundTastingProduct = $this->tastingProductRepository->findOneBy((array('id' => $tastingProductId)));
            if (is_null($foundTastingProduct)) {
                throw new Exception("Delete failed: Tasting product does not exist in database");
            }

            if ($foundTastingProduct->getTasting()->started())
                throw new Exception("Delete failed: Tasting has already started");

            $this->entityManager->remove($foundTastingProduct);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetTastingProductsOrdering mutation
     *
     * Sets tasting products ordering.
     * Set is possible only if tasting has not started yet.
     *
     * @param SetEntityOrderingInput[] $inputData
     *
     * @Mutation(name="setTastingProductsOrdering")
     * @Logged
     * @Security("this.canManageTastingProducts(inputData)")
     *
     * @UseInputType(for="$inputData", inputType="[SetEntityOrderingInput!]!")
     *
     * @return GetTastingProductsResponse
     */
    public function setTastingProductsOrdering(array $inputData) : GetTastingProductsResponse {
        $result = new GetTastingProductsResponse();

        try {

            $tastingProductsData = [];

            foreach ($inputData as $orderingInput) {
                /** @var TastingProduct $foundTastingProduct */
                $foundTastingProduct = $this->tastingProductRepository->findOneBy((array('id' => $orderingInput->getEntityId())));
                if (is_null($foundTastingProduct)) {
                    $result->setData(null);
                    throw new Exception("Update failed: Tasting product with id " . $orderingInput->getEntityId() . " does not exist in database");
                }

                if ($foundTastingProduct->getTasting()->started())
                    throw new Exception("Update failed: Tasting has already started");

                $foundTastingProduct->setOrdering($orderingInput->getOrdering());
                $tastingProductsData[] = $foundTastingProduct;
            }

            $this->entityManager->flush();

            $result->setData($tastingProductsData);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param SetEntityOrderingInput[] $entityOrderingInput
     * @return bool
     */
    public function canManageTastingProducts(array $entityOrderingInput): bool {
        foreach ($entityOrderingInput as $item) {
            if (!$this->canManageTastingProduct($item->getEntityId()))
                return false;
        }
        return true;
    }

    /**
     * @param string|null $tastingProductId
     * @param string|null $tastingId
     * @return bool
     */
    public function canManageTastingProduct(?string $tastingProductId = null, ?string $tastingId = null): bool {
        $shop = null;

        if (!is_null($tastingProductId)) {
            $tastingProduct = $this->getTastingProductInternal($tastingProductId);
            if (!is_null($tastingProduct))
                $shop = $tastingProduct->getTasting()->getShop();
        } else if (!is_null($tastingId)) {
            $shop = $this->getTastingInternal($tastingId)->getShop();
        }

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_EVENTS), $shop);
    }

    /**
     * @param string|null $tastingProductId
     * @return TastingProduct|null
     */
    public function getTastingProductInternal(?string $tastingProductId): ?TastingProduct
    {
        /** @var TastingProduct|null $tastingProduct */
        $tastingProduct = $this->tastingProductRepository->findOneBy((array('id' => $tastingProductId)));
        return $tastingProduct;
    }

    /**
     * @param string|null $tastingId
     * @return Tasting|null
     */
    public function getTastingInternal(?string $tastingId): ?Tasting
    {
        /** @var Tasting|null $tasting */
        $tasting = $this->tastingRepository->findOneBy((array('id' => $tastingId)));
        return $tasting;
    }
}