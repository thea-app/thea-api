<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\ApplicationDefaults;
use App\Application\Model\Entities\Event;
use App\Application\Model\Enum\EventFilterEnum;
use App\Application\Model\Enum\EventTypeEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\Enum\TastingStateEnum;
use App\Application\Model\Exceptions\InputNotValidException;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetEventResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetEventsResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class EventController. Resolves queries and mutations connected with Events
 *
 * @package App\Application\Controllers\QueryControllers
 */
class EventController extends BaseController
{

    /**
     * GetEvents query
     *
     * There are two ways how to call this query
     * 1. To get events for shop - call with shopId. uid must be null
     * 2. To get events for user - call with uid or uid from session. shopId must be null
     *
     * How to filter with EventFilterEnum:
     * 1. way - events for shop:
     *  PAST - event has ended in past
     *  ONGOING - event has started in past and has not ended yet
     *  FUTURE - event has not started yet
     *
     * 2. way - events for user - based on value of eventState for this shop:
     *  PAST:
     *   - UNREGISTERED_ENDED
     *   - REGISTERED_STARTED_FINISHED
     *   - REGISTERED_ENDED_FINISHED
     *  ONGOING:
     *   - UNREGISTERED_STARTED
     *   - REGISTERED_STARTED_NOT_FINISHED
     *   - REGISTERED_ENDED_NOT_FINISHED
     *  FUTURE:
     *   - UNREGISTERED_NOT_STARTED
     *   - REGISTERED_NOT_STARTED
     *
     * @Query(name="getEvents")
     * @Logged
     *
     * @param int|null $limit
     * @param int|null $offset
     * @param EventFilterEnum[]|null $eventFilterEnum
     * @param EventTypeEnum[]|null $eventTypeEnum
     * @param string $searchValue
     * @param string|null $uid
     * @param int|null $shopId
     * @return GetEventsResponse
     */
    public function getEvents(?int $limit = 10, int $offset = null,
                              array $eventFilterEnum = null, array $eventTypeEnum = null, string $searchValue = "",
                              string $uid = null, int $shopId = null): GetEventsResponse {
        $result = new GetEventsResponse();

        try {
            $queryBuilder = $this->eventRepository->createQueryBuilder('event')
                ->select(array('event'))
                ->join(ApplicationDefaults::$eventTranslationTable, 't', 'WITH', 'event.id = t.event')
                ->andWhere('COLLATE(\'name\', utf8_general_ci, false) LIKE COLLATE(:name, utf8_general_ci, true)')
                ->setParameter('name', '%'.$searchValue.'%');

            if(!is_null($uid) && !is_null($shopId)) {
                throw new InputNotValidException("Invalid input: only uid or shopId must be set, not both");
            }

            if (!is_null($shopId)) {
                //shop
                $queryBuilder = $queryBuilder->andWhere('event.shop = :shopId')
                    ->setParameter('shopId', $shopId);
            } else {
                //user
                if (!is_null($uid)) {
                    $userId = $this->userRepository->findOneBy((array('uid' => $uid)))->getId();
                } else {
                    $userId = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])))->getId();
                }

                $expr = $this->entityManager->getExpressionBuilder();

                $queryBuilder = $queryBuilder->andWhere(
                    $expr->in(
                        'event.id',
                        $this->userEventRepository->createQueryBuilder('user_event')
                            ->select(array('user_event.eventId'))
                            ->where('user_event.user = ' . $userId)
                            ->getDQL()
                    )
                );
            }

            if (!is_null($eventTypeEnum) && !empty($eventTypeEnum)) {
                $orString = '';
                for ($i = 0; $i < count($eventTypeEnum); $i++) {
                    $orString .= 'event.event_type = \'' . $eventTypeEnum[$i] . '\'';
                    if ($i < count($eventTypeEnum) - 1)
                        $orString .= ' OR ';
                }
            }

            /** @var Event[] $data */
            $data = $queryBuilder
                ->orderBy('event.ordering')
                ->getQuery()
                ->getResult();

            $eventStateUid = null;
            if (is_null($shopId)) {       //this query is called in user's context
                if (is_null($uid))          //uid should be taken from session
                    $eventStateUid = $_SESSION['uid'];
                else
                    $eventStateUid = $uid;
            }

            if (!is_null($eventTypeEnum) && !empty($eventTypeEnum)) {
                $data = array_filter(
                    $data,
                    function ($event, $value) use ($eventFilterEnum, $eventStateUid) {
                        foreach ($eventFilterEnum as $eventFilter) {
                            if ($this->isEventStateValidForEventFilter($eventFilter, $event->getEventStateInternal($eventStateUid))) {
                                return true;
                            }
                        }
                        return false;
                    },
                    ARRAY_FILTER_USE_BOTH
                );
            }

            $data = array_slice($data, $offset, $limit);

            $result->setData($data);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param EventFilterEnum $eventFilter
     * @param TastingStateEnum $tastingState
     * @return bool
     * @throws Exception
     */
    function isEventStateValidForEventFilter(EventFilterEnum $eventFilter, TastingStateEnum $tastingState): bool {
        switch ($eventFilter) {
            case EventFilterEnum::FUTURE:
                return $tastingState == TastingStateEnum::UNREGISTERED_NOT_STARTED ||
                    $tastingState == TastingStateEnum::REGISTERED_NOT_STARTED;
            case EventFilterEnum::ONGOING:
                return $tastingState == TastingStateEnum::UNREGISTERED_STARTED ||
                    $tastingState == TastingStateEnum::REGISTERED_STARTED_NOT_FINISHED ||
                    $tastingState == TastingStateEnum::REGISTERED_ENDED_NOT_FINISHED;
            case EventFilterEnum::PAST:
                return $tastingState == TastingStateEnum::UNREGISTERED_ENDED ||
                    $tastingState == TastingStateEnum::REGISTERED_STARTED_FINISHED ||
                    $tastingState == TastingStateEnum::REGISTERED_ENDED_FINISHED;
            default:
                throw new Exception('Undefined EventFilterEnum value');
        }
    }

    /**
     * GetEvent query
     *
     * Gets event based on id.
     *
     * @Query(name="getEvent")
     * @Logged
     *
     * @param int $eventId
     *
     * @return GetEventResponse
     */
    public function getEvent(int $eventId) : GetEventResponse {
        $result = new GetEventResponse();

        try {

            /** @var Event $foundEvent */
            $foundEvent = $this->eventRepository->findOneBy((array('id' => $eventId)));
            if (is_null($foundEvent))
                throw new Exception("Event does not exist in DB");

            $result->setData($foundEvent);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteEvent mutation
     *
     * Deletes event based on id. Delete is possible only if event has not started yet.
     *
     * @Mutation(name="deleteEvent")
     * @Logged
     * @Security("this.canManageEvent(eventId)")
     *
     * @param int $eventId
     * @return RemoveItemResponse
     */
    public function deleteEvent(int $eventId) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            /** @var Event|null $foundEvent */
            $foundEvent = $this->eventRepository->findOneBy((array('id' => $eventId)));
            if (is_null($foundEvent)) {
                throw new Exception("Delete failed: Event does not exist in database");
            }

            if ($foundEvent->started())
                throw new Exception("Delete failed: Event has already started");

            $this->entityManager->remove($foundEvent);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param string|null $eventId
     * @return bool
     */
    public function canManageEvent(?string $eventId): bool {
        $event = $this->getEventInternal($eventId);
        $shop = null;
        if (!is_null($event))
            $shop = $event->getShop();

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_EVENTS), $shop);
    }

    /**
     * @param string|null $eventId
     * @return Event|null
     */
    public function getEventInternal(?string $eventId): ?Event
    {
        /** @var Event|null $event */
        $event = $this->eventRepository->findOneBy((array('id' => $eventId)));
        return $event;
    }
}