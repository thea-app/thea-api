<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\FcmToken;
use App\Application\Model\Entities\Notification;
use App\Application\Model\Entities\SentNotification;
use App\Application\Model\Entities\Shop;
use App\Application\Model\Entities\User;
use App\Application\Model\Enum\NotificationGroupEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\Enum\UserHasEmailNotificationsEnabledEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateNotificationInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetEntityImageInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetNotificationInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetFcmTokenResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetNotificationResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetNotificationsResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\MessagingException;
use Kreait\Firebase\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class NotificationController. Resolves queries and mutations connected with Notifications
 *
 * @package App\Application\Controllers\QueryControllers
 */
class NotificationController extends BaseController
{
    private $firebase;

    /**
     * NotificationController constructor. Sets main repository of class @see Notification
     */
    public function __construct()
    {
        parent::__construct();
        $this->firebase = $this->container->get(Messaging::class);
    }

    /**
     * SendFcmToken mutation
     *
     * There are two ways how to call this query
     * 1. fcmToken is given and oldFcmToken is null. In this case new FcmToken will be created in database.
     * 2. both fcmToken and oldFcmToken are given. In this case oldFcmToken will be replaced by fcmToken in database.
     *
     * @Mutation(name="sendFcmToken")
     * @Logged
     *
     * @param string $fcmToken
     * @param string|null $oldFcmToken
     * @return GetFcmTokenResponse
     */
    public function sendFcmToken(string $fcmToken, string $oldFcmToken = null) : GetFcmTokenResponse {
        $result = new GetFcmTokenResponse();

        try {
            /** @var User $user */
            $user = null;
            if (!isset($_SESSION['uid'])) {
                throw new Exception("No user in database with given parameters.");
            }
            $user = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));


            /** @var FcmToken $dbFcmToken */
            $dbFcmToken = null;
            if (!is_null($oldFcmToken)) {
                $dbFcmToken = $this->fcmTokenRepository->findOneBy((array('fcmToken' => $oldFcmToken, 'user' => $user)));

                if (is_null($dbFcmToken)) {
                    throw new Exception("FcmToken for this user does not exist in database.");
                }

                $dbFcmToken->setFcmToken($fcmToken);
            } else {
                $dbFcmToken = FcmToken::create(
                    $user,
                    $fcmToken,
                    true
                );
            }

            $this->entityManager->persist($dbFcmToken);
            $this->entityManager->flush();

            $result->setData($dbFcmToken);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }


    /**
     * CreateNotification mutation
     *
     * Creates notification for given shop.
     *
     * @Mutation(name="createNotification")
     * @Logged
     * @Security("this.canManageNotification(null, inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="CreateNotificationInput")
     *
     * @param CreateNotificationInput $inputData
     * @return GetNotificationResponse
     */
    public function createNotification(CreateNotificationInput $inputData) : GetNotificationResponse {
        $result = new GetNotificationResponse();

        try {

            /** @var Shop|null $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));

            if (is_null($shop))
                throw new Exception("Shop does not exist in DB");

            //Create new Notification entity
            $newNotification = Notification::create(
                $shop,
                $inputData->getTitle(),
                $inputData->getText(),
                !is_null($inputData->getImage())
                    ? $this->mediaService->importFromBase64($inputData->getImage())
                    : null,
                $inputData->getName(),
                $inputData->getGroups(),
                $this->entityManager
            );

            $this->entityManager->persist($newNotification);
            $this->entityManager->flush();

            $result->setData($newNotification);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetNotification mutation
     *
     * Sets given notification.
     * Set is possible only if the notification has not been sent yet.
     *
     * @Mutation(name="setNotification")
     * @Logged
     * @Security("this.canManageNotification(inputData.getNotificationId())")
     * 
     * @UseInputType(for="$inputData", inputType="SetNotificationInput")
     *
     * @param SetNotificationInput $inputData
     * @return GetNotificationResponse
     */
    public function setNotification(SetNotificationInput $inputData) : GetNotificationResponse {
        $result = new GetNotificationResponse();

        try {

            /** @var Shop|null $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));

            if (is_null($shop))
                throw new Exception("Shop does not exist in DB");

            /** @var Notification $foundNotification */
            $foundNotification = $this->notificationRepository->findOneBy((array('id' => $inputData->getNotificationId())));
            if (is_null($foundNotification))
                throw new Exception("Set failed: Notification does not exist in database");

            if (!is_null($foundNotification->getSendingTime()))
                throw new Exception("Set failed: Notification has been already sent and can not be modified.");

            $foundNotification->setShop($shop);
            $foundNotification->setTitle($inputData->getTitle());
            $foundNotification->setText($inputData->getText());
            $foundNotification->setName($inputData->getName());
            $foundNotification->setNotificationGroups($inputData->getGroups(), $this->entityManager);

            $this->entityManager->persist($foundNotification);
            $this->entityManager->flush();

            $result->setData($foundNotification);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetNotifications query
     *
     * There are two ways how to call this query
     * 1. To get notifications for shop - call with shopId. uid must be null
     * 2. To get notifications for user - call with uid or uid from session. shopId must be null
     *
     * @Query(name="getNotifications")
     * @Logged
     *
     * @param int $limit
     * @param int|null $offset
     * @param string|null $uid
     * @param int|null $shopId
     * @return GetNotificationsResponse
     */
    public function getNotifications(int $limit = 10, int $offset = null, string $uid = null, int $shopId = null) : GetNotificationsResponse {
        $result = new GetNotificationsResponse();

        try {

            if (!is_null($shopId)) {
                //shop
                /** @var Shop|null $shop */
                $shop = $this->shopRepository->findOneBy((array('id' => $shopId)));

                if (is_null($shop))
                    throw new Exception("Shop does not exist in DB");

                $result->setData($shop->getNotifications($limit, $offset));
            } else {
                //user
                /** @var User|null $user */
                $user = null;
                if (!is_null($uid)) {
                    $user = $this->userRepository->findOneBy((array('uid' => $uid)));
                } else {
                    $user = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
                }

                if (is_null($user))
                    throw new Exception("User does not exist in DB");

                $result->setData($user->getNotifications($limit, $offset));
            }

            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetNotification query
     *
     * Gets notification based on id.
     *
     * @Query(name="getNotification")
     * @Logged
     *
     * @param int $notificationId
     * @return GetNotificationResponse
     */
    public function getNotification(int $notificationId) : GetNotificationResponse {
        $result = new GetNotificationResponse();

        try {

            /** @var Notification $data */
            $data = $this->notificationRepository->findOneBy(array('id' => $notificationId));

            if (is_null($data))
                throw new Exception("No notification in database with given id");

            $result->setData($data);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * UpdateNotificationsEnabled mutation
     *
     * Enables or disables receiving notifications for given fcm token
     *
     * @Mutation(name="updateNotificationsEnabled")
     * @Logged
     *
     * @param string $fcmToken
     * @param bool $notificationsEnabled
     * @return GetFcmTokenResponse
     */
    public function updateNotificationsEnabled(string $fcmToken, bool $notificationsEnabled) : GetFcmTokenResponse {
        $result = new GetFcmTokenResponse();

        try {

            /** @var FcmToken|null $dbFcmToken **/
            $dbFcmToken = null;

            /** @var User $user */
            $user = null;
            if (!isset($_SESSION['uid'])) {
                throw new Exception("No user in database with given parameters.");
            }
            $user = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));

            $dbFcmToken = $this->fcmTokenRepository->findOneBy((array('fcmToken' => $fcmToken, 'user' => $user)));

            if (is_null($dbFcmToken)) {
                throw new Exception("FcmToken for this user does not exist in database.");
            }

            $dbFcmToken->setNotificationsEnabled($notificationsEnabled);

            $this->entityManager->persist($dbFcmToken);
            $this->entityManager->flush();

            $result->setData($dbFcmToken);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SendNotification mutation
     *
     * Sends notification to notification groups.
     * Notification is send to all devices in notification groups.
     * Send is possible only if the notification has not been sent yet.
     *
     * @Mutation(name="sendNotification")
     * @Logged
     * @Security("this.canManageNotification(notificationId)")
     *
     * @param int $notificationId
     * @return GetNotificationResponse
     * @throws FirebaseException
     * @throws MessagingException
     */
    public function sendNotification(int $notificationId) : GetNotificationResponse {
        $result = new GetNotificationResponse();

        try {

            /** @var Notification $notification */
            $notification = $this->notificationRepository->findOneBy((array('id' => $notificationId)));
            if (is_null($notification))
                throw new Exception("Notification for does not exist in DB");

            if (!is_null($notification->getSendingTime()))
                throw new Exception("This notification has been sent already");

            $notificationGroups = $notification->getNotificationGroups();

            if (is_null($notificationGroups) || empty($notificationGroups)) {
                throw new Exception("No target groups are set for this notification.");
            }

            /** @var string[] $targetFcmTokens */
            $targetFcmTokens = [];
            /** @var string[] $targetEmails */
            $targetEmails = [];
            foreach ($notificationGroups as $notificationGroup) {
                $targetUsers = [];
                switch ($notificationGroup->getNotificationGroup()) {
                    case NotificationGroupEnum::VIP:
                        $targetUsers = $notification->getShop()->getVipUsersInternal();
                        break;
                    case NotificationGroupEnum::MAIN:
                        $targetUsers = $notification->getShop()->getMainShopUsers();
                        break;
                    case NotificationGroupEnum::FAVOURITE:
                        $targetUsers = $notification->getShop()->getFavouriteUsers();
                        break;
                }
                foreach ($targetUsers as $targetUser) {
                    $userFcmTokens = $targetUser->getFcmTokens();
                    if ($targetUser->getHasEmailNotificationsEnabled() == UserHasEmailNotificationsEnabledEnum::ALLOWED_AND_ENABLED)
                        $targetEmails[] = $targetUser->getEmail();
                    foreach ($userFcmTokens as $userFcmToken) {
                        if ($userFcmToken->getNotificationsEnabled())
                            $targetFcmTokens[] = $userFcmToken->getFcmToken();
                    }
                }
            }

            $uniqueTargetFcmTokens = array_unique($targetFcmTokens);
            $uniqueTargetEmails = array_unique($targetEmails);

            if(!is_null($uniqueTargetFcmTokens) && !empty($uniqueTargetFcmTokens)) {
                $message = CloudMessage::new()->withNotification([
                    'title' => $notification->getTitle(),
                    'body' => $notification->getText(),
                    'image' => $notification->getImageURL()
                ]);
                $sendReport = $this->firebase->sendMulticast($message, $uniqueTargetFcmTokens);

                foreach ($sendReport->getItems() as $item) {
                    /** @var FcmToken $fcmToken */
                    $fcmToken = $this->fcmTokenRepository->findOneBy(array('fcmToken' => $item->target()->value()));

                    $errorMessage = null;
                    if (!is_null($item->error()))
                        $errorMessage = $item->error()->getMessage();

                    $dbSentNotification = SentNotification::create(
                        $fcmToken,
                        $notification,
                        $errorMessage
                    );

                    $this->entityManager->persist($dbSentNotification);
                }
            }
            $currentTime = new DateTimeImmutable('now', new DateTimeZone('Europe/Prague'));

            $notification->setSendingTime($currentTime);
            $this->entityManager->flush();

            $this->sendEmailNotifications($notification, $uniqueTargetEmails);

            $result->setData($notification);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteNotification mutation
     *
     * Deletes given notification.
     * Delete is possible only if the notification has not been sent yet.
     *
     * @Mutation(name="deleteNotification")
     * @Logged
     * @Security("this.canManageNotification(notificationId)")
     *
     * @param string $notificationId
     * @return RemoveItemResponse
     */
    public function deleteNotification(string $notificationId) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {

            /** @var Notification $notification */
            $notification = $this->notificationRepository->findOneBy((array('id' => $notificationId)));
            if (is_null($notification))
                throw new Exception("Notification does not exist in DB");

            if (!is_null($notification->getSendingTime()))
                throw new Exception("Notification has been sent already and can not be deleted");

            $this->entityManager->remove($notification);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetNotificationImage mutation
     *
     * Sets image in base64 format to given notification.
     * Set is possible only if the notification has not been sent yet.
     *
     * @Mutation(name="setNotificationImage")
     *
     * @UseInputType(for="$inputData", inputType="SetEntityImageInput")
     * @Logged
     * @Security("this.canManageNotification(inputData.getEntityId())")
     *
     * @param SetEntityImageInput $inputData
     * @return GetNotificationResponse
     */
    public function setNotificationImage(SetEntityImageInput $inputData): GetNotificationResponse
    {
        $result = new GetNotificationResponse();

        try {
            /** @var Notification $foundNotification */
            $foundNotification = $this->notificationRepository->findOneBy((array('id' => $inputData->getEntityId())));
            if (is_null($foundNotification))
                throw new Exception("Set failed: Notification does not exist in database");

            if (!is_null($foundNotification->getSendingTime()))
                throw new Exception("Notification has been sent already");

            $newImage = $inputData->getImage();
            $oldImageIdentifier = $foundNotification->getImageIdentifier();
            if (!is_null($newImage)) {
                if (!is_null($oldImageIdentifier))
                    $foundNotification->setImageIdentifier($this->mediaService->replaceFromBase64($newImage, $oldImageIdentifier));
                else
                    $foundNotification->setImageIdentifier($this->mediaService->importFromBase64($newImage));
            } else {
                $this->mediaService->deleteImage($oldImageIdentifier);
                $foundNotification->setImageIdentifier(null);
            }

            $this->entityManager->flush();

            $result->setData($foundNotification);
            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorCode($exception->getCode());
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SendEmailNotifications
     *
     * Sends given notification to all given emails.
     *
     * @param Notification $notification
     * @param array $targetEmails
     */
    private function sendEmailNotifications(Notification $notification, array $targetEmails) {
        $title = $notification->getTitle();
        $text = $notification->getText();
        $image = $notification->getImageURL();

        $mailSubject = 'Thea notification from shop ' . $notification->getShop()->getName();
        $mailBody = "
                <html lang=\"cs\">
                <head><title>Notification</title>
                <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet'>
                <link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
                <style>
                h1 {
                    font-family: 'Raleway',serif;font-size: 42px;
                }
                p {
                    font-family: 'Open Sans',serif;font-size: 22px;
                }
                </style>
                </head>
                <body>
                
                <h1>${title}</h1>
                <p><img src=${image} alt=\"notification image\"/></p>
                <p>${text}</p>
                
                </body>
                </html>
            ";

        $mailHeaders = "MIME-Version: 1.0" . "\r\n";
        $mailHeaders .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $mailHeaders .= 'From: Thea App <thea.customer.hello@gmail.com>' . "\r\n";

        foreach ($targetEmails as $targetEmail) {
            mail($targetEmail, $mailSubject, $mailBody, $mailHeaders);
        }
    }

    /**
     * @param string|null $notificationId
     * @param string|null $shopId
     * @return bool
     */
    public function canManageNotification(?string $notificationId, ?string $shopId = null): bool {
        $shop = null;
        if (!is_null($notificationId)) {
            $notification = $this->getNotificationInternal($notificationId);
            if (!is_null($notification))
                $shop = $notification->getShop();
        } else if (!is_null($shopId)) {
            $shop = $this->getShopInternal($shopId);
        }

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_NOTIFICATIONS), $shop);
    }

    /**
     * @param string|null $notificationId
     * @return Notification|null
     */
    public function getNotificationInternal(?string $notificationId): ?Notification
    {
        /** @var Notification|null $notification */
        $notification = null;

        if (!is_null($notificationId)) {
            $notification = $this->notificationRepository->findOneBy((array('id' => $notificationId)));
        }

        return $notification;
    }

    /**
     * @param string|null $shopId
     * @return Shop|null
     */
    public function getShopInternal(?string $shopId): ?Shop
    {
        /** @var Shop|null $shop */
        $shop = $this->shopRepository->findOneBy((array('id' => $shopId)));
        return $shop;
    }

}