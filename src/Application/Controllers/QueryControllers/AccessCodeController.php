<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\AccessCode;
use App\Application\Model\Entities\Event;
use App\Application\Model\Enum\AccessCodeAssignEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateAccessCodesInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetAccessCodeInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetAccessCodeResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetAccessCodesResponse;
use App\Application\Utils;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class AccessCodeController. Resolves queries and mutations connected with AccessCode
 *
 * @package App\Application\Controllers\QueryControllers
 */
class AccessCodeController extends BaseController
{
    /**
     * CreateAccessCodes mutation
     *
     * Creates access codes for given event. The number of codes must be specified in input data.
     * When no expire date is given, every code expires at the end of event.
     *
     * @Mutation(name="createAccessCodes")
     * @Logged
     * @Security("this.canManageAccessCode(null, inputData.getEventId())")
     *
     * @UseInputType(for="$inputData", inputType="CreateAccessCodesInput")
     *
     * @param CreateAccessCodesInput $inputData
     * @return GetAccessCodesResponse
     */
    public function createAccessCodes(CreateAccessCodesInput $inputData): GetAccessCodesResponse
    {
        $result = new GetAccessCodesResponse();

        try {
            /** @var Event $event */
            $event = $this->eventRepository->findOneBy((array('id' => $inputData->getEventId())));

            if (is_null($event))
                throw new Exception("Event does not exist in DB");

            /** @var  AccessCode[] $newAccessCodes */
            $newAccessCodes = [];

            for ($i = 0; $i < $inputData->getNumOfCodes(); $i++) {

                $expiryDate = $inputData->getExpiryDate();
                if (is_null($expiryDate)) {
                    $expiryDate = $event->getDateUntil();
                }

                $newAccessCode = AccessCode::create(
                    $event,
                    $expiryDate
                );

                $newAccessCode->setCode(Utils::generateEntityUniqueCode($newAccessCode, 8, '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'));

                $this->entityManager->persist($newAccessCode);
                $this->entityManager->flush();

                $newAccessCodes[] = $newAccessCode;
            }

            $result->setData($newAccessCodes);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetAccessCode mutation
     *
     * Sets data to given access code. When no expire date is given, every code expires at the end of event.
     *
     * @Mutation(name="setAccessCode")
     * @Logged
     * @Security("this.canManageAccessCode(inputData.getAccessCodeId())")
     *
     * @UseInputType(for="$inputData", inputType="SetAccessCodeInput")
     *
     * @param SetAccessCodeInput $inputData
     * @return GetAccessCodeResponse
     */
    public function setAccessCode(SetAccessCodeInput $inputData) : GetAccessCodeResponse
    {
        $result = new GetAccessCodeResponse();

        try {
            /** @var AccessCode $foundAccessCode */
            $foundAccessCode = $this->accessCodeRepository->findOneBy((array('id' => $inputData->getAccessCodeId())));
            if (is_null($foundAccessCode)) {
                throw new Exception("Set failed: Access Code does not exist in database");
            }

            $expiryDate = $inputData->getExpiryDate();
            if (is_null($expiryDate)) {
                $expiryDate = $foundAccessCode->getEvent()->getDateUntil();
            }

            $foundAccessCode->setNote($inputData->getNote());
            $foundAccessCode->setExpiryDate($expiryDate);

            $this->entityManager->persist($foundAccessCode);
            $this->entityManager->flush();

            $result->setData($foundAccessCode);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetAccessCodes Query
     *
     * Returns all access codes for given event.
     * There are three options on how to call this mutation:
     *  1. ALL - returns all access codes for given event
     *  2. ASSIGNED - returns only assigned access codes for given event
     *  3. NOT_ASSIGNED - returns only not assigned access codes for given event
     *
     * @Query(name="getAccessCodes")
     * @Logged
     * @Security("this.canManageAccessCode(null, eventId)")
     *
     * @param int $eventId
     * @param AccessCodeAssignEnum $assignEnum
     * @return GetAccessCodesResponse
     */
    public function getAccessCodes(int $eventId, AccessCodeAssignEnum $assignEnum): GetAccessCodesResponse
    {
        $result = new GetAccessCodesResponse();

        try {
            /** @var Event $event */
            $event = $this->eventRepository->findOneBy((array('id' => $eventId)));

            if (is_null($event))
                throw new Exception("Event does not exist in DB");

            $data = $this->accessCodeRepository->createQueryBuilder('access_code')
                ->select(array('access_code'))
                ->where('access_code.event = :eventId')
                ->setParameter('eventId', $eventId);

            if (!is_null($assignEnum) && !empty($assignEnum)) {
                if ($assignEnum == AccessCodeAssignEnum::ASSIGNED) {
                    $data = $data->andWhere('access_code.user IS NOT NULL');
                } elseif ($assignEnum == AccessCodeAssignEnum::NOT_ASSIGNED) {
                    $data = $data->andWhere('access_code.user IS NULL');
                }
            }

            $data = $data->getQuery()
                ->getResult();

            $result->setData($data);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param string|null $accessCodeId
     * @param string|null $eventId
     * @return bool
     */
    public function canManageAccessCode(?string $accessCodeId = null, ?string $eventId = null): bool {
        $shop = null;

        if (!is_null($accessCodeId)) {
            $accessCode = $this->getAccessCodeInternal($accessCodeId);
            if (!is_null($accessCode))
                $shop = $accessCode->getEvent()->getShop();

        } else if (!is_null($eventId)) {
            $shop = $this->getEventInternal($eventId)->getShop();
        }

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_ACCESS_CODES), $shop);
    }

    /**
     * @param string|null $accessCodeId
     * @return AccessCode|null
     */
    public function getAccessCodeInternal(?string $accessCodeId): ?AccessCode
    {
        /** @var AccessCode|null $accessCode */
        $accessCode = $this->accessCodeRepository->findOneBy((array('id' => $accessCodeId)));
        return $accessCode;
    }

    /**
     * @param string|null $eventId
     * @return Event|null
     */
    public function getEventInternal(?string $eventId): ?Event
    {
        /** @var Event|null $event */
        $event = $this->eventRepository->findOneBy((array('id' => $eventId)));
        return $event;
    }
}