<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\Product;
use App\Application\Model\Entities\User;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\UpdateFavouriteProductsInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetProductsResponse;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;

/**
 * Class UserProductRelationController. Resolves queries and mutations connected with relation between User and Product
 * @package App\Application\Controllers\QueryControllers
 */
class UserProductRelationController extends BaseController
{

    /**
     * UpdateFavouriteProducts mutation
     *
     * Update user favourite products.
     * If user marks product as favourite and the shop, which the product belongs to is not favourite for the user,
     * marks shop as favourite too.
     * Returns all favourite products of the user.
     *
     * @Mutation(name="updateFavouriteProducts")
     * @Logged
     *
     * @UseInputType(for="$inputData", inputType="[UpdateFavouriteProductsInput]")
     *
     * @param UpdateFavouriteProductsInput[] $inputData
     * @return GetProductsResponse
     */
    public function updateFavouriteProducts(array $inputData) : GetProductsResponse
    {
        $result = new GetProductsResponse();

        try {
            /** @var User $user */
            $user = null;
            if (isset($_SESSION['uid'])) {
                $user = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
                if(is_null($user))
                    throw new Exception("User with uid does not exist in database");
            } else
                throw new Exception("No user in database with given parameters");

            foreach ($inputData as $record) {
                /** @var Product $product */
                $product = $this->productRepository->findOneBy((array('id' => $record->getProductId())));
                if (is_null($product))
                    continue;
                if ($record->isFavourite()) {
                    $product->addFavouriteUser($user);

                    //If the shop is not in favourite, add
                    if ($product->getShop()->isFavourite() == false)
                        $product->getShop()->addFavouriteUser($user);

                } else {
                    $product->removeFavouriteUser($user, $this->entityManager);
                }
            }

            $this->entityManager->flush();

            $result->setData($user->getFavouriteProducts());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }
}