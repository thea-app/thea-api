<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\Bonus;
use App\Application\Model\Entities\BonusCode;
use App\Application\Model\Entities\Shop;
use App\Application\Model\Entities\ShopBonusUser;
use App\Application\Model\Entities\User;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\Exceptions\InputNotValidException;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateBonusInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetBonusInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetBonusProgramDescriptionInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetBonusCodeResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetBonusResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetShopResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class BonusController. Resolves queries and mutations connected with Bonus entity
 *
 * @package App\Application\Controllers\QueryControllers
 */
class BonusController extends BaseController
{
    /**
     * CreateBonus mutation
     *
     * Creates bonus for given shop, with given translations, expiry date and value of the bonus in points
     *
     * @Mutation(name="createBonus")
     * @Logged
     * @Security("this.canManageBonus(null, null, inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="CreateBonusInput")
     *
     * @param CreateBonusInput $inputData
     * @return GetBonusResponse
     */
    public function createBonus(CreateBonusInput $inputData) : GetBonusResponse
    {
        $result = new GetBonusResponse();

        try {
            /** @var Shop|null $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));

            if (is_null($shop))
                throw new Exception("Shop does not exist in DB");

            if ($inputData->getValue() < 0)
                throw new InputNotValidException("Value of bonus must not be negative integer");

            //Create new Bonus entity
            $newBonus = Bonus::create(
                $shop,
                $inputData->getNameTranslations(),
                $inputData->getExpiryDate(),
                $inputData->getValue()
            );

            $this->entityManager->persist($newBonus);
            $this->entityManager->flush();

            $shop->addBonus($newBonus);

            $this->entityManager->persist($shop);
            $this->entityManager->flush();

            $result->setData($newBonus);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetBonusProgramDescription mutation
     *
     * Sets description for bonus program of the shop
     *
     * @Mutation(name="setBonusProgramDescription")
     * @Logged
     * @Security("this.canManageBonus(null, null, inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="SetBonusProgramDescriptionInput")
     *
     * @param SetBonusProgramDescriptionInput $inputData
     * @return GetShopResponse
     */
    public function setBonusProgramDescription(SetBonusProgramDescriptionInput $inputData) : GetShopResponse
    {
        $result = new GetShopResponse();

        try {
            /** @var Shop|null $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));

            if (is_null($shop))
                throw new Exception("Shop does not exist in DB");

            $shop->setBonusProgramDescriptionTranslations($inputData->getDescriptionTranslations());

            $this->entityManager->persist($shop);
            $this->entityManager->flush();

            $result->setData($shop);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetBonus mutation
     *
     * Sets name translations, expiry date and value of the bonus in points to given bonus
     *
     * @Mutation(name="setBonus")
     * @Logged
     * @Security("this.canManageBonus(inputData.getBonusId())")
     *
     * @UseInputType(for="$inputData", inputType="SetBonusInput")
     *
     * @param SetBonusInput $inputData
     * @return GetBonusResponse
     */
    public function setBonus(SetBonusInput $inputData) : GetBonusResponse
    {
        $result = new GetBonusResponse();

        try {
            /** @var Bonus $foundBonus */
            $foundBonus = $this->bonusRepository->findOneBy((array('id' => $inputData->getBonusId())));
            if (is_null($foundBonus)) {
                $result->setData(null);
                throw new Exception("Update failed: Bonus does not exist in database");
            }

            if ($inputData->getValue() < 0)
                throw new InputNotValidException("Value of bonus must not be negative integer");

            $foundBonus->setNameTranslations($inputData->getNameTranslations());
            $foundBonus->setExpiryDate($inputData->getExpiryDate());
            $foundBonus->setValue($inputData->getValue());

            $this->entityManager->persist($foundBonus);
            $this->entityManager->flush();

            $result->setData($foundBonus);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetBonus query
     *
     * Get bonus either by id or code.
     * When caller gets bonus by code, the code will be marked as used, so no code can be used twice
     *
     * @Query(name="getBonus")
     * @Logged
     * @Security("this.canManageBonus(bonusId, bonusCode)")
     *
     * @param int|null $bonusId
     * @param string|null $bonusCode
     *
     * @return GetBonusResponse
     */
    public function getBonus(?int $bonusId, ?string $bonusCode) : GetBonusResponse {
        $result = new GetBonusResponse();

        try {
            /** @var BonusCode|null $foundBonusCode */
            $foundBonusCode = null;

            if (is_null($bonusCode) && !is_null($bonusId)) {
                $foundBonus = $this->bonusRepository->findOneBy((array('id' => $bonusId)));
                if (is_null($foundBonus))
                    throw new Exception("Bonus does not exist in DB");
                $data = $foundBonus;
            }
            else if (!is_null($bonusCode) && is_null($bonusId)) {
                $foundBonusCode = $this->bonusCodeRepository->findOneBy((array('code' => $bonusCode, 'state' => EntityStateEnum::ACTIVE)));
                if (is_null($foundBonusCode))
                    throw new Exception("Bonus code does not exist in DB");
                $data = $foundBonusCode->getBonus();
                $foundBonusCode->setState(new EntityStateEnum(EntityStateEnum::INACTIVE));      //mark it as used = INACTIVE
                $this->entityManager->persist($foundBonusCode);
                $this->entityManager->flush();
            } else
                throw new Exception("Exactly one of the parameters must be set");

            $result->setData($data);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteBonus mutation
     *
     * Deletes given bonus from shop
     *
     * @Mutation(name="deleteBonus")
     * @Logged
     * @Security("this.canManageBonus(inputData.getBonusId())")
     *
     * @param int $bonusId
     * @return RemoveItemResponse
     */
    public function deleteBonus(int $bonusId) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            /** @var Bonus|null $foundBonus */
            $foundBonus = $this->bonusRepository->findOneBy((array('id' => $bonusId)));
            if (is_null($foundBonus)) {
                throw new Exception("Delete failed: Bonus does not exist in database");
            }

            $foundBonus->getShop()->removeBonus($foundBonus, $this->entityManager);

            $this->entityManager->remove($foundBonus);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * RedeemBonus mutation - request by user identified by session for redeem bonus
     *
     * @Mutation(name="redeemBonus")
     * @Logged
     *
     * @param int $bonusId
     *
     * @return GetBonusCodeResponse
     */
    public function redeemBonus(int $bonusId) : GetBonusCodeResponse
    {
        $result = new GetBonusCodeResponse();

        try {
            /** @var Bonus $foundBonus */
            $foundBonus = $this->bonusRepository->findOneBy((array('id' => $bonusId)));
            if (is_null($foundBonus)) {
                throw new Exception("Bonus does not exist in database");
            }

            if ($foundBonus->isExpired()) {
                throw new Exception("Bonus is expired");
            }

            if (!isset($_SESSION['uid'])) {
                throw new Exception("User is not logged in");
            }

            /** @var User|null $user */
            $user = $this->userRepository->findOneBy(((array('uid' => $_SESSION['uid']))));
            if(is_null($user))
                throw new Exception("User with given uid from session does not exist in database");

            /** @var ShopBonusUser|null $shopBonusUser */
            $shopBonusUser = $this->shopBonusUserRepository->findOneBy(((array('bonusUserId' => $user->getId(), 'shopId' => $foundBonus->getShop()->getId()))));
            if (is_null($shopBonusUser))
                throw new Exception("Bonus user with given uid does not exist in database for given bonus");

            if ($shopBonusUser->getPoints() < $foundBonus->getValue())
                throw new Exception("Not enough points for given bonus");

            $newBonusCode = $shopBonusUser->redeemBonus($foundBonus);

            $this->entityManager->persist($foundBonus);
            $this->entityManager->persist($newBonusCode);
            $this->entityManager->flush();

            $result->setData($newBonusCode);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param string|null $bonusId
     * @param string|null $bonusCode
     * @param string|null $shopId
     * @return bool
     */
    public function canManageBonus(?string $bonusId, ?string $bonusCode = null, ?string $shopId = null): bool {
        $shop = null;
        if (!is_null($bonusId)) {
            $bonus = $this->getBonusInternal($bonusId);
            if (!is_null($bonus))
                $shop = $bonus->getShop();
        } else if (!is_null($bonusCode)) {
            $bonus = $this->getBonusInternal(null, $bonusCode);
            if (!is_null($bonus))
                $shop = $bonus->getShop();
        } else if (!is_null($shopId)) {
            $shop = $this->getShopInternal($shopId);
        }

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_BONUSES), $shop);
    }

    /**
     * @param string|null $bonusId
     * @param string|null $bonusCode
     * @return Bonus|null
     */
    public function getBonusInternal(?string $bonusId, ?string $bonusCode = null): ?Bonus
    {
        /** @var Bonus|null $bonus */
        $bonus = null;

        if (!is_null($bonusId)) {
            $bonus = $this->bonusRepository->findOneBy((array('id' => $bonusId)));
        } else if (!is_null($bonusCode)) {
            /** @var BonusCode|null $bonusCodeEntity */
            $bonusCodeEntity = $this->bonusCodeRepository->findOneBy((array('code' => $bonusCode)));
            if (!is_null($bonusCodeEntity))
                $bonus = $bonusCodeEntity->getBonus();
        }
        return $bonus;
    }

    /**
     * @param string|null $shopId
     * @return Shop|null
     */
    public function getShopInternal(?string $shopId): ?Shop
    {
        /** @var Shop|null $shop */
        $shop = $this->shopRepository->findOneBy((array('id' => $shopId)));
        return $shop;
    }

}
