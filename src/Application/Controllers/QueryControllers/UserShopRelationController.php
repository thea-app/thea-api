<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\Shop;
use App\Application\Model\Entities\ShopBonusUser;
use App\Application\Model\Entities\ShopVipUser;
use App\Application\Model\Entities\User;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\AddPointsToUserInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\UserShopRelationInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\UpdateFavouriteShopsInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetShopBonusUserResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetShopsResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetShopVipUserResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetUserResponse;
use DateTimeImmutable;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class UserShopRelationController. Resolves queries and mutations connected with relation between User and Shop
 *
 * @package App\Application\Controllers\QueryControllers
 */
class UserShopRelationController extends BaseController
{

    /**
     * UpdateFavouriteShops mutation
     *
     * Update user and its favourite shops binding
     *
     * Scenarios which can happen:
     * 1. Main shop is set to true
     *  1.1 on replacing main shop, always set shop as favourite
     *  1.2 on setting new main shop, always set shop as favourite
     *  1.3 nothing is changed, this shop was set as main before
     *      - favourite set to true - do nothing, nothing is changed
     *      - favourite set to false - remove it from favourite, remove main shop and favourite products connected with the shop
     *
     * 2. Main shop is set to false
     *  2.1 remove main shop if this shop was set as main before
     *      - favourite set to true - set shop as favourite
     *      - favourite set to false - remove it from favourite, remove favourite products connected with the shop
     *
     * Returns all user's favourite shops
     *
     * @Mutation(name="updateFavouriteShops")
     * @Logged
     *
     * @UseInputType(for="$inputData", inputType="[UpdateFavouriteShopsInput]")
     *
     * @param UpdateFavouriteShopsInput[] $inputData
     * @return GetShopsResponse
     */
    public function updateFavouriteShops(array $inputData) : GetShopsResponse
    {
        $result = new GetShopsResponse();

        try {
            /** @var User|null $user */
            $user = null;
            if (isset($_SESSION['uid'])) {
                $user = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
                if(is_null($user))
                    throw new Exception("User with uid does not exist in database");
            } else
                throw new Exception("No user in database with given parameters");

            foreach ($inputData as $record) {
                /** @var Shop $shop */
                $shop = $this->shopRepository->findOneBy((array('id' => $record->getShopId())));
                if (is_null($shop))
                    continue;
                if ($record->isMainShop()) {        //main shop is set to true - when setting new main shop, replacing main shop, or just leaving it unchanged
                    if (is_null($user->getMainShop())) {    //no shop is set as main. the shop is not set as main, set it as a favourite as well
                        $user->setMainShop($shop);
                        $shop->addFavouriteUser($user);     //set it as a favourite as well
                    } else if ($user->getMainShop()->getId() != $shop->getId()) {   //we are replacing the main shop
                        $user->setMainShop($shop);
                        $shop->addFavouriteUser($user);     //no matter what is set in favourite, we need to set it as a favourite
                    } else {        //nothing has been changed for the main shop
                        if (!$record->isFavourite()) {   //favourite set to false - user removed it from favourite, we need to remove the main
                            $shop->removeFavouriteUser($user, $this->entityManager);
                            $this->removeAllFavouriteProductsOfShops($user, $shop);
                            $user->setMainShop(null);
                        }
                    }
                } else {                                    //main shop is set to false - when removing it, or just leaving it unchanged
                    if (!is_null($user->getMainShop()) && $user->getMainShop()->getId() == $shop->getId()) {
                        $user->setMainShop(null);   //remove main shop
                    }
                    if ($record->isFavourite()) {
                        $shop->addFavouriteUser($user);
                    } else {
                        $shop->removeFavouriteUser($user, $this->entityManager);
                        $this->removeAllFavouriteProductsOfShops($user, $shop);
                    }
                }
            }

            $this->entityManager->flush();

            $result->setData($user->getFavouriteShops());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param User $user
     * @param Shop $shop
     * @throws Exception
     */
    private function removeAllFavouriteProductsOfShops(User $user, Shop $shop) {
        foreach ($user->getFavouriteProducts() as $favouriteProduct) {
            if ($favouriteProduct->getShop()->getId() == $shop->getId())
                $favouriteProduct->removeFavouriteUser($user, $this->entityManager);
        }
    }

    /**
     * AddUserToShopAdmins mutation
     *
     * Add user as admin to specific shop
     *
     * @Mutation(name="addUserToShopAdmins")
     * @Logged
     * @Security("this.canManageShopAdmins(inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="UserShopRelationInput")
     *
     * @param UserShopRelationInput $inputData
     * @return GetUserResponse
     */
    public function addUserToShopAdmins(UserShopRelationInput $inputData) : GetUserResponse
    {
        $result = new GetUserResponse();

        try {
            /** @var User|null $user */
            $user = $this->userRepository->findOneBy((array('uid' => $inputData->getUid())));
            if(is_null($user))
                throw new Exception("User with given uid does not exist in database");

            /** @var Shop|null $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));
            if (is_null($shop))
                throw new Exception("Shop with given id does not exist in database");

            $shop->addAdmin($user);

            $this->entityManager->flush();

            $result->setData($user);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * RemoveUserFromShopAdmins mutation
     *
     * Remove user as admin from specific shop
     *
     * @Mutation(name="removeUserFromShopAdmins")
     * @Logged
     * @Security("this.canManageShopAdmins(inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="UserShopRelationInput")
     *
     * @param UserShopRelationInput $inputData
     * @return GetUserResponse
     */
    public function removeUserFromShopAdmins(UserShopRelationInput $inputData) : GetUserResponse
    {
        $result = new GetUserResponse();

        try {
            /** @var User|null $user */
            $user = $this->userRepository->findOneBy(((array('uid' => $inputData->getUid()))));
            if(is_null($user))
                throw new Exception("User with given uid does not exist in database");

            /** @var Shop|null $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));
            if (is_null($shop))
                throw new Exception("Shop with given id does not exist in database");

            $shop->removeAdmin($user, $this->entityManager);

            $this->entityManager->flush();

            $result->setData($user);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * AddPointsToUser mutation
     *
     * Adds points to specific shop bonus user
     * If the bonusUserCode is set, add points to this specific shopBonusUser if the shopId matches the shopBonusUser shopId
     * Else if the uid is set, add points to this specific shopBonusUser for given shopId. If the shopBonusUser does not exist, create one
     * Else if the email is set, add points to this specific shopBonusUser for given shopId. If the shopBonusUser does not exist, create one
     *
     * @Mutation(name="addPointsToUser")
     * @Logged
     * @Security("this.canManageBonuses(inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="AddPointsToUserInput")
     * @param AddPointsToUserInput $inputData
     * @return GetShopBonusUserResponse
     */
    public function addPointsToUser(AddPointsToUserInput $inputData): GetShopBonusUserResponse
    {
        $result = new GetShopBonusUserResponse();

        try {

            if ($inputData->getPoints() <= 0)
                throw new Exception("Field points must not be lower than 1");

            /** @var ShopBonusUser $shopBonusUser */
            $shopBonusUser = null;

            /** @var Shop|null $shop */
            $shop = $this->shopRepository->findOneBy(((array('id' => $inputData->getShopId()))));
            if (is_null($shop))
                throw new Exception("Shop with given id does not exist in database");

            if (!is_null($inputData->getBonusUserCode())) {

                $shopBonusUser = $this->shopBonusUserRepository->findOneBy(((array('code' => $inputData->getBonusUserCode()))));
                if (is_null($shopBonusUser))
                    throw new Exception("Bonus user with given code does not exist in database");

                if ($shopBonusUser->getShop()->getId() != $inputData->getShopId())
                    throw new Exception("Bonus user with given code is not bonus user for given shop");

            } else if (!is_null($inputData->getUid()) || !is_null($inputData->getEmail())) {

                /** @var User|null $user */
                $user = null;

                if (!is_null($inputData->getUid())) {
                    $user = $this->userRepository->findOneBy(((array('uid' => $inputData->getUid()))));
                    if(is_null($user))
                        throw new Exception("User with given uid does not exist in database");
                } else {
                    $user = $this->userRepository->findOneBy(((array('email' => $inputData->getEmail()))));
                    if(is_null($user))
                        throw new Exception("User with given email does not exist in database");
                }

                $shopBonusUser = $this->shopBonusUserRepository->findOneBy(((array('bonusUserId' => $user->getId(), 'shopId' => $inputData->getShopId()))));

                if (is_null($shopBonusUser)) {
                    $shopBonusUser = ShopBonusUser::create(
                        $shop,
                        $user
                    );
                }

            } else {
                throw new Exception("Either bonusUserCode, uid or email must be set");
            }

            $shopBonusUser->addPoints($inputData->getPoints());

            $this->entityManager->persist($shopBonusUser);
            $this->entityManager->flush();

            $result->setData($shopBonusUser);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /** GetShopBonusUser mutation
     *
     * It is mutation because when the shopBonusUser does not exist, it creates one
     *
     * Gets or creates shopBonusUser based on uid from session and shopId parameter
     *
     * @Mutation(name="getShopBonusUser")
     * @Logged
     *
     * @param int $shopId
     * @return GetShopBonusUserResponse
     */
    public function getShopBonusUser(int $shopId) : GetShopBonusUserResponse {
        $result = new GetShopBonusUserResponse();

        try {
            if (!isset($_SESSION['uid'])) {
                throw new Exception("User is not logged in");
            }

            /** @var User|null $user */
            $user = $this->userRepository->findOneBy(((array('uid' => $_SESSION['uid']))));
            if(is_null($user))
                throw new Exception("User with given uid from session does not exist in database");


            /** @var Shop|null $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $shopId)));
            if (is_null($shop))
                throw new Exception("Shop with given id does not exist in database");

            $shopBonusUser = $this->shopBonusUserRepository->findOneBy(((array('bonusUserId' => $user->getId(), 'shopId' => $shopId))));

            if (is_null($shopBonusUser)) {
                $shopBonusUser = ShopBonusUser::create(
                    $shop,
                    $user
                );
            }

            $this->entityManager->persist($shopBonusUser);
            $this->entityManager->flush();

            $result->setData($shopBonusUser);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * AddVipUserToShop mutation
     *
     * Add user as VIP to specific shop. Caller can also set expiry date. If the expiry date is null, then the vip status has no expiration.
     * This mutation can be used for renewing vip memberships.
     *
     * @Mutation(name="addVipUserToShop")
     * @Logged
     * @Security("this.canManageVipUsers(inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="UserShopRelationInput")
     *
     * @param UserShopRelationInput $inputData
     * @param DateTimeImmutable|null $expiryDate
     * @return GetShopVipUserResponse
     */
    public function addVipUserToShop(UserShopRelationInput $inputData, ?DateTimeImmutable $expiryDate = null) : GetShopVipUserResponse
    {
        $result = new GetShopVipUserResponse();

        try {
            /** @var User|null $user */
            $user = $this->userRepository->findOneBy((array('uid' => $inputData->getUid())));
            if(is_null($user))
                throw new Exception("User with given uid does not exist in database");

            /** @var Shop|null $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));
            if (is_null($shop))
                throw new Exception("Shop with given id does not exist in database");

            /** @var ShopVipUser $vipUserBinding */
            $vipUserBinding = $shop->addVipUser($user, $expiryDate);

            $this->entityManager->persist($vipUserBinding);
            $this->entityManager->flush();

            $result->setData($vipUserBinding);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * RemoveVipUserFromShop mutation
     *
     * Removes user as VIP from specific shop
     *
     * @Mutation(name="removeVipUserFromShop")
     * @Logged
     * @Security("this.canManageVipUsers(inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="UserShopRelationInput")
     *
     * @param UserShopRelationInput $inputData
     * @return GetUserResponse
     */
    public function removeVipUserFromShop(UserShopRelationInput $inputData) : GetUserResponse
    {
        $result = new GetUserResponse();

        try {
            /** @var User|null $user */
            $user = $this->userRepository->findOneBy(((array('uid' => $inputData->getUid()))));
            if(is_null($user))
                throw new Exception("User with given uid does not exist in database");

            /** @var Shop|null $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));
            if (is_null($shop))
                throw new Exception("Shop with given id does not exist in database");

            $shop->removeVipUser($user, $this->entityManager);

            $this->entityManager->flush();

            $result->setData($user);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetShopVipUser query
     *
     * Gets user as VIP from specific shop.
     *
     * There are two ways how to call this query:
     * 1. To get vip user shop binding by vipCode.
     * 2. To get vip user shop binding by uid, or by session: User can be specified either by session, or uid.
     *    Setting uid overrides uid from session.
     *
     * @Query(name="getShopVipUser")
     * @Logged
     * @Security("this.canGetShopVipUser(user, vipCode, shopId)")
     *
     * @param string|null $vipCode
     * @param string|null $uid
     * @param int|null $shopId
     * @return GetShopVipUserResponse
     */
    public function getShopVipUser(?string $vipCode, ?string $uid, ?int $shopId) : GetShopVipUserResponse {
        $result = new GetShopVipUserResponse();

        try {
            /** @var ShopVipUser|null $shopVipUser */
            $shopVipUser = null;
            if (!is_null($vipCode)) {
                $shopVipUser = $this->shopVipUserRepository->findOneBy(array('code' => $vipCode));
                if (is_null($shopVipUser))
                    throw new Exception("Vip user with given VIP Code does not exist in database");
            } else {
                /** @var User|null $user */
                $user = null;
                if (!is_null($uid))
                    $user = $this->userRepository->findOneBy((array('uid' => $uid)));
                else if (isset($_SESSION['uid']))
                    $user = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
                else
                    throw new Exception("No user in database with given parameters");

                /** @var Shop|null $shop */
                $shop = $this->shopRepository->findOneBy((array('id' => $shopId)));
                if (is_null($shop))
                    throw new Exception("Shop with given id does not exist in database");

                /** @var ShopVipUser $shopVipUser */
                $shopVipUser = $shop->getVipUserBinding($user);
            }

            $result->setData($shopVipUser);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }


    /**
     * @param string|null $shopId
     * @return bool
     */
    public function canManageShopAdmins(string $shopId): bool {
        $shop = $this->getShopInternal($shopId);

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_SHOP_ADMINS), $shop);
    }

    /**
     * @param string|null $shopId
     * @return bool
     */
    public function canManageBonuses(string $shopId): bool {
        $shop = $this->getShopInternal($shopId);

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_BONUSES), $shop);
    }

    /**
     * @param string|null $shopId
     * @return bool
     */
    public function canManageVipUsers(string $shopId): bool {
        $shop = $this->getShopInternal($shopId);

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_VIP), $shop);
    }

    /**
     * @param User $user
     * @param string|null $vipCode
     * @param string|null $shopId
     * @return bool
     */
    public function canGetShopVipUser(User $user, ?string $vipCode, ?string $shopId): bool {
        if (!is_null($vipCode)) {
            return true;
        }

        if (!is_null($shopId)) {
            $shop = $this->getShopInternal($shopId);
            /** @var ShopVipUser $vipUser */
            $vipUser = $this->shopVipUserRepository->findOneBy((array('vipUser' => $user, 'shop' => $shop)));       //the caller is getting his own vip user status
            if (!is_null($vipUser))
                return true;
        }

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_VIP), $shop);
    }

    /**
     * @param string|null $shopId
     * @return Shop|null
     */
    public function getShopInternal(?string $shopId): ?Shop
    {
        /** @var Shop|null $shop */
        $shop = $this->shopRepository->findOneBy((array('id' => $shopId)));
        return $shop;
    }


}