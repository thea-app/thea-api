<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\Configuration;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\UpdateAvailableResponse;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Query;

/**
 * Class ConfigurationController. Resolves queries and mutations connected to entity Configuration
 *
 * @package App\Application\Controllers\QueryControllers
 */
class ConfigurationController extends BaseController
{
    /**
     * AndroidMandatoryUpdateAvailable query
     *
     * Returns true if mandatory update is available for the android app
     *
     * @Query(name="androidMandatoryUpdateAvailable")
     *
     * @param string $buildNumber
     * @return UpdateAvailableResponse
     */
    public function androidMandatoryUpdateAvailable(string $buildNumber) : UpdateAvailableResponse
    {
        $result = new UpdateAvailableResponse();
        try {
            /** @var Configuration $mandatoryBuildNumber */
            $mandatoryBuildNumber = $this->entityManager->getRepository(Configuration::class)->findOneBy(['code' => 'ANDROID_MANDATORY_BUILD_NUMBER']);

            if ($mandatoryBuildNumber != null) {
                $result->setData((int)$buildNumber <= (int)$mandatoryBuildNumber->getValue());
                $result->setSuccess(true);
            } else {
                $result->setSuccess(false);
            }
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

}
