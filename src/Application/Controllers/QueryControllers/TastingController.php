<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\Country;
use App\Application\Model\Entities\Shop;
use App\Application\Model\Entities\Tasting;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateTastingInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetEntityImageInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetTastingInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetTastingResponse;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class TastingController. Resolves queries and mutations connected with Tasting entity
 *
 * @package App\Application\Controllers\QueryControllers
 */
class TastingController extends BaseController
{

    /**
     * CreateTasting mutation
     *
     * Creates new tasting for given shop
     *
     * @Mutation(name="createTasting")
     * @Logged
     * @Security("this.canManageTasting(null, inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="CreateTastingInput")
     *
     * @param CreateTastingInput $inputData
     * @return GetTastingResponse
     */
    public function createTasting(CreateTastingInput $inputData): GetTastingResponse
    {
        $result = new GetTastingResponse();

        try {
            /** @var Shop $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));

            if (is_null($shop))
                throw new Exception("Shop does not exist in DB");

            /** @var Country $country */
            $country = $this->countryRepository->findOneBy((array('countryCode' => $inputData->getCountry())));

            if (is_null($country))
                throw new Exception("Country does not exist in DB");

            $state = new EntityStateEnum(EntityStateEnum::ACTIVE);
            if (!is_null($inputData->getState()))
                $state = $inputData->getState();

            //Create new Tasting entity
            $newTasting = Tasting::create(
                $shop,
                $inputData->getNameTranslations(),
                $inputData->getPublicDescriptionTranslations(),
                $inputData->getPrivateDescriptionTranslations(),
                $inputData->getDateFrom(),
                $inputData->getDateUntil(),
                $state,
                $inputData->getStreet(),
                $inputData->getDescriptiveNumber(),
                $inputData->getRoutingNumber(),
                $inputData->getPostcode(),
                $inputData->getCity(),
                $country,
                $inputData->getLongitude(),
                $inputData->getLatitude(),
                $inputData->isVipOnly()
            );

            $this->entityManager->persist($newTasting);
            $this->entityManager->flush();

            $result->setData($newTasting);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetTasting mutation
     *
     * Sets data to given tasting.
     * Set is possible only if tasting has not started yet.
     *
     * @Mutation(name="setTasting")
     * @Logged
     * @Security("this.canManageTasting(inputData.getTastingId())")
     *
     * @UseInputType(for="$inputData", inputType="SetTastingInput")
     *
     * @param SetTastingInput $inputData
     * @return GetTastingResponse
     */
    public function setTasting(SetTastingInput $inputData) : GetTastingResponse {
        $result = new GetTastingResponse();

        try {
            /** @var Tasting $foundTasting */
            $foundTasting = $this->tastingRepository->findOneBy((array('id' => $inputData->getTastingId())));
            if (is_null($foundTasting)) {
                $result->setData(null);
                throw new Exception("Set failed: Tasting does not exist in database");
            }

            if ($foundTasting->started())
                throw new Exception("Set failed: Tasting has already started");

            /** @var Shop $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));

            if (is_null($shop))
                throw new Exception("Set failed: Shop does not exist in DB");

            /** @var Country $country */
            $country = $this->countryRepository->findOneBy((array('countryCode' => $inputData->getCountry())));

            if (is_null($country))
                throw new Exception("Set failed: Country does not exist in DB");

            $foundTasting->setDateFrom($inputData->getDateFrom());
            $foundTasting->setDateUntil($inputData->getDateUntil());
            $foundTasting->setShop($shop);
            $foundTasting->setNameTranslations($inputData->getNameTranslations());
            $foundTasting->setPublicDescriptionTranslations($inputData->getPublicDescriptionTranslations());
            $foundTasting->setPrivateDescriptionTranslations($inputData->getPrivateDescriptionTranslations());
            $foundTasting->setCountry($country);
            $foundTasting->setLatitude($inputData->getLatitude());
            $foundTasting->setLongitude($inputData->getLongitude());
            $foundTasting->setCity($inputData->getCity());
            $foundTasting->setDescriptiveNumber($inputData->getDescriptiveNumber());
            $foundTasting->setRoutingNumber($inputData->getRoutingNumber());
            $foundTasting->setState(EntityStateEnum::ACTIVE);
            $foundTasting->setStreet($inputData->getStreet());
            $foundTasting->setPostcode($inputData->getPostcode());
            $foundTasting->setVipOnly($inputData->isVipOnly());

            $this->entityManager->persist($foundTasting);
            $this->entityManager->flush();

            $result->setData($foundTasting);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }


    /**
     * SetTastingImage mutation
     *
     * Sets image in base64 format to given tasting
     *
     * @Mutation(name="setTastingImage")
     * @Logged
     * @Security("this.canManageTasting(inputData.getEntityId())")
     *
     * @UseInputType(for="$inputData", inputType="SetEntityImageInput")
     *
     * @param SetEntityImageInput $inputData
     * @return GetTastingResponse
     */
    public function setTastingImage(SetEntityImageInput $inputData) : GetTastingResponse{
        $result = new GetTastingResponse();

        try {
            /** @var Tasting $foundTasting */
            $foundTasting = $this->tastingRepository->findOneBy((array('id' => $inputData->getEntityId())));
            if (is_null($foundTasting)) {
                $result->setData(null);
                throw new Exception("Set failed: Tasting does not exist in database");
            }

            if ($foundTasting->started())
                throw new Exception("Set failed: Tasting has already started");

            $newImage = $inputData->getImage();
            $oldImageIdentifier = $foundTasting->getImageIdentifier();
            if (!is_null($newImage)) {
                if (!is_null($oldImageIdentifier))
                    $foundTasting->setImageIdentifier($this->mediaService->replaceFromBase64($newImage, $oldImageIdentifier));
                else
                    $foundTasting->setImageIdentifier($this->mediaService->importFromBase64($newImage));
            } else {
                $this->mediaService->deleteImage($oldImageIdentifier);
                $foundTasting->setImageIdentifier(null);
            }

            $this->entityManager->flush();

            $result->setData($foundTasting);
            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorCode($exception->getCode());
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param string|null $tastingId
     * @param string|null $shopId
     * @return bool
     */
    public function canManageTasting(?string $tastingId = null, ?string $shopId = null): bool {
        $shop = null;

        if (!is_null($tastingId)) {
            $tasting = $this->getTastingInternal($tastingId);
            if (!is_null($tasting))
                $shop = $tasting->getShop();

        } else if (!is_null($shopId)) {
            $shop = $this->getShopInternal($shopId);
        }

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_EVENTS), $shop);

    }

    /**
     * @param string|null $tastingId
     * @return Tasting|null
     */
    public function getTastingInternal(?string $tastingId): ?Tasting
    {
        /** @var Tasting|null $tasting */
        $tasting = $this->tastingRepository->findOneBy((array('id' => $tastingId)));
        return $tasting;
    }

    /**
     * @param string|null $shopId
     * @return Shop|null
     */
    public function getShopInternal(?string $shopId): ?Shop
    {
        /** @var Shop|null $shop */
        $shop = $this->shopRepository->findOneBy((array('id' => $shopId)));
        return $shop;
    }
}