<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\AccessCode;
use App\Application\Model\Entities\Event;
use App\Application\Model\Entities\Flavour;
use App\Application\Model\Entities\Tasting;
use App\Application\Model\Entities\TastingProduct;
use App\Application\Model\Entities\User;
use App\Application\Model\Entities\UserEvent;
use App\Application\Model\Entities\UserTastingProductReview;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\AddUserToEventInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\RemoveUserFromEventInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\UserTastingProductReviewInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\UserTastingSessionReviewInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetTastingsResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetUserEventResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class UserEventController. Resolves queries and mutations connected with UserEvents
 *
 * @package App\Application\Controllers\QueryControllers
 */
class UserEventController extends BaseController
{

    /**
     * AddUserToEvent mutation
     * There are two ways how to use this mutation
     * 1. Using AccessCode, eventId and uid from session
     * 2. Using EventId and either uid or email. Setting uid overrides set email.
     * When the AccessCode is not null, try to process the first option, second otherwise.
     *
     * @Mutation(name="addUserToEvent")
     * @Logged
     * @Security("this.canManageUserEvent(inputData.getEventId(), inputData.getAccessCode())")
     *
     * @UseInputType(for="$inputData", inputType="AddUserToEventInput")
     *
     * @param AddUserToEventInput $inputData
     * @return GetUserEventResponse
     */
    public function addUserToEvent(AddUserToEventInput $inputData): GetUserEventResponse
    {
        $result = new GetUserEventResponse();

        try {
            /** @var Event $event */
            $event = $this->eventRepository->findOneBy((array('id' => $inputData->getEventId())));

            if (is_null($event))
                throw new Exception("Event does not exist in DB");

            /** @var User|null $user */
            $user = null;

            /** @var AccessCode|null $accessCode */
            $accessCode = null;

            if (!is_null($inputData->getAccessCode())) {
                /** @var User $user */
                $user = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));

                if (is_null($user))
                    throw new Exception("User does not exist in DB");

                $accessCode = $this->accessCodeRepository->findOneBy((array('code' => $inputData->getAccessCode())));

                if (is_null($accessCode))
                    throw new Exception("Access Code does not exist in DB");

                if ($accessCode->isExpired())
                    throw new Exception("Given Access Code is already expired");

                if ($accessCode->getEvent()->getId() != $event->getId())
                    throw new Exception("Given Access Code is not valid for given event");

                if (!is_null($accessCode->getUser()))
                    throw new Exception("Given Access Code is already used");

            } else {
                if (!is_null($inputData->getUid())) {
                    $user = $this->userRepository->findOneBy((array('uid' => $inputData->getUid())));

                    if (is_null($user))
                        throw new Exception("User does not exist in DB");

                } elseif (!is_null($inputData->getEmail())) {
                    /** @var User $user */
                    $user = $this->userRepository->createQueryBuilder('user')
                        ->select(array('user'))
                        ->where('COLLATE(\'email\', utf8_general_ci, false) LIKE COLLATE(:email, utf8_general_ci, true)')
                        ->setParameter('email', '%'.$inputData->getEmail().'%')
                        ->getQuery()
                        ->getSingleResult();

                    if (is_null($user))
                        throw new Exception("User does not exist in DB");
                } else {
                    throw new Exception("No user input data");
                }
            }

            if ($event->isVipOnly() && !$event->getShop()->isUserVip($user))
                throw new Exception("This event is for VIP users only");

            if (!is_null($accessCode)) {
                $accessCode->setUser($user);
                $this->entityManager->persist($accessCode);
            }

            //Create new UserEvent entity
            $newUserEvent = UserEvent::create(
                $user,
                $event,
                new EntityStateEnum(EntityStateEnum::ACTIVE),
                $accessCode
            );

            $this->entityManager->persist($newUserEvent);
            $this->entityManager->flush();

            $result->setData($newUserEvent);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }


    /**
     * RemoveUserFromEvent mutation
     *
     * Removes user based on UID from given event.
     * Remove is possible only if event has not started yet.
     *
     * @Mutation(name="removeUserFromEvent")
     * @Logged
     * @Security("this.canManageUserEvent(inputData.getEventId())")
     *
     * @UseInputType(for="$inputData", inputType="RemoveUserFromEventInput")
     *
     * @param RemoveUserFromEventInput $inputData
     * @return RemoveItemResponse
     */
    public function removeUserFromEvent(RemoveUserFromEventInput $inputData) :  RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            /** @var User $user */
            $user = $this->userRepository->findOneBy((array('uid' => $inputData->getUid())));

            if (is_null($user))
                throw new Exception("Remove failed: User does not exist in DB");

            /** @var Event $event */
            $event = $this->eventRepository->findOneBy((array('id' => $inputData->getEventId())));

            if (is_null($event))
                throw new Exception("Remove failed: Event does not exist in DB");

            if ($event->started())
                throw new Exception("Remove failed: Event has already started");

            $foundUserEvent = $this->userEventRepository->findOneBy((array('user' => $user, 'event' => $event)));

            if(is_null($foundUserEvent))
                throw new Exception("Remove failed: User is not in attendees for this event in DB");

            $this->entityManager->remove($foundUserEvent);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * FinishUserTastingSessions mutation
     *
     * Saves user tasting reviews and marks tasting as finished for the user
     *
     * @Mutation(name="finishUserTastingSessions")
     * @Logged
     *
     * @UseInputType(for="$inputData", inputType="[UserTastingSessionReviewInput!]")
     *
     * @param UserTastingSessionReviewInput[] $inputData
     * @return GetTastingsResponse
     */
    public function finishUserTastingSessions(array $inputData): GetTastingsResponse
    {
        $result = new GetTastingsResponse();

        try {

            /** @var User $user */
            $user = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));

            if (is_null($user))
                throw new Exception("User does not exist in DB");

            /** @var UserEvent[] $userEvents */
            $userEvents = [];
            /** @var Tasting[] $data */
            $data = [];

            foreach ($inputData as $userTastingSessionReviewInput) {
                /** @var Tasting $tasting */
                $tasting = $this->tastingRepository->findOneBy((array('id' => $userTastingSessionReviewInput->getTastingId())));

                if (is_null($tasting))
                    throw new Exception("Tasting does not exist in DB");

                $data[] = $tasting;

                /** @var UserEvent $userEvent */
                $userEvent = $this->userEventRepository->findOneBy(array('user' => $user, 'event' => $tasting));

                if (is_null($userEvent))
                    throw new Exception("User is not an attendee for this event");

                /** @var UserTastingProductReview[] $newProductReviews */
                $newProductReviews = [];

                /** @var UserTastingProductReviewInput $tastingProductReview */
                foreach ($userTastingSessionReviewInput->getProductReviews() as $tastingProductReview) {

                    /** @var TastingProduct $tastingProduct */
                    $tastingProduct = $this->tastingProductRepository->findOneBy((array('id' => $tastingProductReview->getTastingProductId())));

                    if (is_null($tastingProduct))
                        throw new Exception("Tasting product with id " . $tastingProductReview->getTastingProductId() . " does not exist in DB");

                    if ($tastingProduct->getTasting()->getId() != $tasting->getId())
                        throw new Exception("Tasting product with id " . $tastingProductReview->getTastingProductId() . " not found in tasting in DB");

                    /** @var Flavour[] $flavours */
                    $flavours = [];

                    if (is_array($tastingProductReview->getFlavours()) || is_object($tastingProductReview->getFlavours())) {
                        foreach ($tastingProductReview->getFlavours() as $flavourId) {
                            /** @var Flavour|null $flavour */
                            $flavour = $this->flavourRepository->findOneBy((array('id' => $flavourId)));

                            if (is_null($flavour))
                                throw new Exception("Flavour with id " . $flavourId . " does not exist in DB");

                            $flavours[] = $flavour;
                        }
                    }

                    $newUserTastingProductReview = UserTastingProductReview::create(
                        $userEvent,
                        $tastingProduct,
                        $tastingProductReview->getPrivateNote(),
                        $tastingProductReview->getPublicNote(),
                        $tastingProductReview->getRating(),
                        $flavours
                    );

                    $newProductReviews[] = $newUserTastingProductReview;
                }

                $userEvent->setPublicNote($userTastingSessionReviewInput->getPublicNote());
                $userEvent->setPrivateNote($userTastingSessionReviewInput->getPrivateNote());
                $userEvent->setRating($userTastingSessionReviewInput->getRating());
                $userEvent->setFinished(true);

                $userEvents[] = $userEvent;

                foreach ($newProductReviews as $newProductReview) {
                    $this->entityManager->persist($newProductReview);
                }
            }

            foreach ($userEvents as $entity) {
                $this->entityManager->persist($entity);
            }
            $this->entityManager->flush();

            $result->setData($data);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param string|null $accessCode
     * @param string|null $eventId
     * @return bool
     */
    public function canManageUserEvent(?string $eventId, ?string $accessCode = null): bool {
        if (!is_null($accessCode)) {            //anyone can add anyone to event, when he has accessCode
            return true;
        }

        $event = $this->getEventInternal($eventId);
        $shop = null;
        if ($event != null)
            $shop = $event->getShop();

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_EVENTS), $shop);
    }

    /**
     * @param string|null $eventId
     * @return Event|null
     */
    public function getEventInternal(?string $eventId): ?Event
    {
        /** @var Event|null $event */
        $event = $this->eventRepository->findOneBy((array('id' => $eventId)));
        return $event;
    }
}