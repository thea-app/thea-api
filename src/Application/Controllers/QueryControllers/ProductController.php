<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\ApplicationDefaults;
use App\Application\Model\Entities\Category;
use App\Application\Model\Entities\Product;
use App\Application\Model\Entities\Shop;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\ProductFilterEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\Exceptions\InputNotValidException;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateProductInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetEntityImageInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetEntityOrderingInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetProductCategoryInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetProductInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetProductResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetProductsResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class ProductController. Resolves queries and mutations connected with Product entity
 *
 * @package App\Application\Controllers\QueryControllers
 */
class ProductController extends BaseController
{

    /**
     * CreateProduct mutation
     *
     * Creates product for given shop and given category.
     *
     * @Mutation(name="createProduct")
     * @Logged
     * @Security("this.canManageProduct(null, inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="CreateProductInput")
     *
     * @param CreateProductInput $inputData
     * @return GetProductResponse
     */
    public function createProduct(CreateProductInput $inputData) : GetProductResponse
    {
        $result = new GetProductResponse();

        try {
            /** @var Shop $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));

            if (is_null($shop))
                throw new Exception("Shop does not exist in DB");

            /** @var Category $category */
            $category = $this->categoryRepository->findOneBy((array('id' => $inputData->getCategoryId())));

            if (is_null($category))
                throw new Exception("Category does not exist in DB");

            if ($category->getShop()->getId() != $shop->getId())
                throw new Exception("Given category is not available for given shop");

            //Create new Product entity
            $newProduct = Product::create(
                new EntityStateEnum(EntityStateEnum::ACTIVE),
                $shop,
                $category,
                $inputData->getPrice(),
                $inputData->getCurrency(),
                !is_null($inputData->getImage())
                    ? $this->mediaService->importFromBase64($inputData->getImage())
                    : null,
                $inputData->getNameTranslations(),
                $inputData->getDescriptionTranslations()
            );

            $this->entityManager->persist($newProduct);
            $this->entityManager->flush();

            $result->setData($newProduct);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetProduct mutation
     *
     * Sets product data.
     *
     * @Mutation(name="setProduct")
     * @Logged
     * @Security("this.canManageProduct(inputData.getProductId())")
     *
     * @UseInputType(for="$inputData", inputType="SetProductInput")
     *
     * @param SetProductInput $inputData
     * @return GetProductResponse
     */
    public function setProduct(SetProductInput $inputData) : GetProductResponse
    {
        $result = new GetProductResponse();

        try {
            /** @var Product $foundProduct */
            $foundProduct = $this->productRepository->findOneBy((array('id' => $inputData->getProductId())));
            if (is_null($foundProduct)) {
                $result->setData(null);
                throw new Exception("Set failed: Product does not exist in database");
            }

            /** @var Shop $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));
            if (is_null($shop)) {
                $result->setData(null);
                throw new Exception("Set failed: Shop does not exist in DB");
            }

            /** @var Category $category */
            $category = null;
            if (!is_null($inputData->getCategoryId())) {
                $category = $this->categoryRepository->findOneBy((array('id' => $inputData->getCategoryId())));
                if (is_null($category))
                    throw new Exception("Set failed: Category does not exist in DB");

                if ($category->getShop()->getId() != $shop->getId())
                    throw new Exception("Set failed: Given category is not available for given shop");
            }

            $foundProduct->setShop($shop);
            $foundProduct->setNameTranslations($inputData->getNameTranslations());
            $foundProduct->setDescriptionTranslations($inputData->getDescriptionTranslations());
            $foundProduct->setCategory($category);
            $foundProduct->setCurrency($inputData->getCurrency());
            $foundProduct->setPrice($inputData->getPrice());

            $this->entityManager->persist($foundProduct);
            $this->entityManager->flush();

            $result->setData($foundProduct);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetProductImage mutation
     *
     * Sets image in base64 format to given product
     *
     * @Mutation(name="setProductImage")
     * @Logged
     * @Security("this.canManageProduct(inputData.getEntityId())")
     *
     * @UseInputType(for="$inputData", inputType="SetEntityImageInput")
     *
     * @param SetEntityImageInput $inputData
     * @return GetProductResponse
     */
    public function setProductImage(SetEntityImageInput $inputData): GetProductResponse
    {
        $result = new GetProductResponse();

        try {
            /** @var Product $foundProduct */
            $foundProduct = $this->productRepository->findOneBy((array('id' => $inputData->getEntityId())));
            if (is_null($foundProduct)) {
                $result->setData(null);
                throw new Exception("Set failed: Product does not exist in database");
            }

            $newImage = $inputData->getImage();
            $oldImageIdentifier = $foundProduct->getImageIdentifier();
            if (!is_null($newImage)) {
                if (!is_null($oldImageIdentifier))
                    $foundProduct->setImageIdentifier($this->mediaService->replaceFromBase64($newImage, $oldImageIdentifier));
                else
                    $foundProduct->setImageIdentifier($this->mediaService->importFromBase64($newImage));
            } else {
                $this->mediaService->deleteImage($oldImageIdentifier);
                $foundProduct->setImageIdentifier(null);
            }

            $this->entityManager->flush();

            $result->setData($foundProduct);
            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorCode($exception->getCode());
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetProductsOrdering mutation
     *
     * Set products ordering.
     *
     * @param SetEntityOrderingInput[] $inputData
     *
     * @Mutation(name="setProductsOrdering")
     * @Logged
     * @Security("this.canManageProducts(inputData)")
     *
     * @UseInputType(for="$inputData", inputType="[SetEntityOrderingInput!]!")
     *
     * @return GetProductsResponse
     */
    public function setProductsOrdering(array $inputData) : GetProductsResponse {
        $result = new GetProductsResponse();

        try {

            $productData = [];

            foreach ($inputData as $orderingInput) {
                /** @var Product $foundProduct */
                $foundProduct = $this->productRepository->findOneBy((array('id' => $orderingInput->getEntityId())));
                if (is_null($foundProduct)) {
                    $result->setData(null);
                    throw new Exception("Set failed: Product with id " . $orderingInput->getEntityId() . " does not exist in database");
                }
                $foundProduct->setOrdering($orderingInput->getOrdering());
                $productData[] = $foundProduct;
            }

            $this->entityManager->flush();

            $result->setData($productData);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }


    /**
     * GetProduct query
     *
     * Gets product based on id.
     *
     * @Query(name="getProduct")
     * @Logged
     *
     * @param int $productId
     *
     * @return GetProductResponse
     */
    public function getProduct(int $productId) : GetProductResponse {
        $result = new GetProductResponse();

        try {

            /** @var Product $foundProduct */
            $foundProduct = $this->productRepository->findOneBy((array('id' => $productId)));
            if (is_null($foundProduct))
                throw new Exception("Product does not exist in DB");

            $result->setData($foundProduct);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetProducts query
     *
     * Basic query resolver used for getting products based on given pagination and filter parameters.
     * If the categoryId is set, it returns only products in given category (if it does exist), doesn't take into account the shopId.
     * If the shopId is set and categoryId is null, then returns all products from given shop, if it does exist.
     * Either categoryId or shopId must be set.
     *
     * @Query(name="getProducts")
     * @Logged
     *
     * @param ProductFilterEnum|null $filterValue filter to be applied
     * @param int|null $limit number of records to select
     * @param int|null $offset position from which to start selecting
     * @param int|null $shopId shop
     * @param int|null $categoryId category
     * @param string $searchValue string to be used for filtering product names
     * @return GetProductsResponse
     */
    public function getProducts(ProductFilterEnum $filterValue = null, ?int $limit = 10, int $offset = null,
                                int $shopId = null, int $categoryId = null, string $searchValue = "") : GetProductsResponse {

        $result = new GetProductsResponse();

        try {
            if (!isset($filterValue))
                $filterValue = new ProductFilterEnum(ProductFilterEnum::NO_FILTER);

            $data = $this->productRepository->createQueryBuilder('product')
                ->select(array('product'));

            if (!empty($searchValue)) {
                $data = $data->join(ApplicationDefaults::$productTranslationTable, 't', 'WITH', 'product.id = t.product')
                        ->andWhere('COLLATE(\'name\', utf8_general_ci, false) LIKE COLLATE(:name, utf8_general_ci, true)')
                        ->setParameter('name', '%'.$searchValue.'%');
            }

            if (!is_null($categoryId))
                $data = $data->andWhere('product.category = ' . $categoryId);
            else if (!is_null($shopId))
                $data = $data->andWhere('product.shop = ' . $shopId);
            else
                throw new InputNotValidException("Either categoryId or shopId must not be null");

            switch ($filterValue) {
                case ProductFilterEnum::MENU:
                    $data = $data->andWhere('product.category IS NOT NULL');
                    break;
                case ProductFilterEnum::NO_FILTER:
                default:
                    break;
            }

            $data = $data->orderBy('product.ordering')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();

            $result->setData($data);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }


    /**
     * DeleteProduct mutation - deletes subcategories
     *
     * Deletes given product.
     *
     * @Mutation(name="deleteProduct")
     * @Logged
     * @Security("this.canManageProduct(productId)")
     *
     * @param int $productId
     * @return RemoveItemResponse
     */
    public function deleteProduct(int $productId) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            $foundProduct = $this->productRepository->findOneBy((array('id' => $productId)));
            if (is_null($foundProduct)) {
                throw new Exception("Delete failed: Product does not exist in database");
            }

            $this->entityManager->remove($foundProduct);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetProductCategory mutation
     *
     * Sets product category.
     *
     * @Mutation(name="setProductCategory")
     * @Logged
     * @Security("this.canManageProduct(inputData.getProductId())")
     *
     * @UseInputType(for="$inputData", inputType="SetProductCategoryInput")
     *
     * @param SetProductCategoryInput $inputData
     * @return GetProductResponse
     */
    public function setProductCategory(SetProductCategoryInput $inputData): GetProductResponse
    {
        $result = new GetProductResponse();

        try {
            /** @var Product $foundProduct */
            $foundProduct = $this->productRepository->findOneBy((array('id' => $inputData->getProductId())));
            if (is_null($foundProduct)) {
                $result->setData(null);
                throw new Exception("Set failed: Product does not exist in database");
            }

            /** @var Category $category */
            $category = null;
            if (!is_null($inputData->getCategoryId())) {
                $category = $this->categoryRepository->findOneBy((array('id' => $inputData->getCategoryId())));
                if (is_null($category))
                    throw new Exception("Set failed: Category does not exist in DB");
            }

            if ($foundProduct->getShop()->getId() != $category->getShop()->getId())
                throw new Exception("Set failed: Given category is not available for shop of product");

            $foundProduct->setCategory($category);

            $this->entityManager->persist($foundProduct);
            $this->entityManager->flush();

            $result->setData($foundProduct);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param SetEntityOrderingInput[] $entityOrderingInput
     * @return bool
     */
    public function canManageProducts(array $entityOrderingInput): bool {
        foreach ($entityOrderingInput as $item) {
            if (!$this->canManageProduct($item->getEntityId()))
                return false;
        }
        return true;
    }

    /**
     * @param string|null $productId
     * @param string|null $shopId
     * @return bool
     */
    public function canManageProduct(?string $productId = null, ?string $shopId = null): bool {
        $shop = null;

        if (!is_null($productId)) {
            $product = $this->getProductInternal($productId);
            if (!is_null($product))
                $shop = $product->getShop();

        } else if (!is_null($shopId)) {
            $shop = $this->getShopInternal($shopId);
        }

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_MENU), $shop);
    }

    /**
     * @param string|null $productId
     * @return Product|null
     */
    public function getProductInternal(?string $productId): ?Product
    {
        /** @var Product|null $product */
        $product = $this->productRepository->findOneBy((array('id' => $productId)));
        return $product;
    }

    /**
     * @param string|null $shopId
     * @return Shop|null
     */
    public function getShopInternal(?string $shopId): ?Shop
    {
        /** @var Shop|null $shop */
        $shop = $this->shopRepository->findOneBy((array('id' => $shopId)));
        return $shop;
    }
}