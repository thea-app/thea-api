<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\Shop;
use App\Application\Model\Entities\User;
use App\Application\Model\Entities\VipBenefit;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateVipBenefitInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetVipBenefitInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\UserVipBenefitRelationInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetUserResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetVipBenefitResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class VipBenefitController. Resolves queries and mutations connected with VipBenefit entity
 *
 * @package App\Application\Controllers\QueryControllers
 */
class VipBenefitController extends BaseController
{

    /**
     * CreateVipBenefit mutation
     *
     * Creates vip benefit with given data for given shop
     *
     * @Mutation(name="createVipBenefit")
     * @Logged
     * @Security("this.canManageVipBenefit(null, inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="CreateVipBenefitInput")
     *
     * @param CreateVipBenefitInput $inputData
     * @return GetVipBenefitResponse
     */
    public function createVipBenefit(CreateVipBenefitInput $inputData) : GetVipBenefitResponse
    {
        $result = new GetVipBenefitResponse();

        try {
            /** @var Shop|null $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));

            if (is_null($shop))
                throw new Exception("Shop does not exist in DB");

            $newVipBenefit = VipBenefit::create(
                $shop,
                $inputData->getNameTranslations(),
                $inputData->getDescriptionTranslations()
            );

            $this->entityManager->persist($newVipBenefit);
            $this->entityManager->flush();

            $shop->addVipBenefit($newVipBenefit);

            $this->entityManager->persist($shop);
            $this->entityManager->flush();

            $result->setData($newVipBenefit);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetVipBenefit mutation
     *
     * Sets data for given vip benefit
     *
     * @Mutation(name="setVipBenefit")
     * @Logged
     * @Security("this.canManageVipBenefit(inputData.getVipBenefitId())")
     *
     * @UseInputType(for="$inputData", inputType="SetVipBenefitInput")
     *
     * @param SetVipBenefitInput $inputData
     * @return GetVipBenefitResponse
     */
    public function setVipBenefit(SetVipBenefitInput $inputData) : GetVipBenefitResponse
    {
        $result = new GetVipBenefitResponse();

        try {
            /** @var VipBenefit $foundVipBenefit */
            $foundVipBenefit = $this->vipBenefitRepository->findOneBy((array('id' => $inputData->getVipBenefitId())));
            if (is_null($foundVipBenefit)) {
                $result->setData(null);
                throw new Exception("Update failed: VIP Benefit does not exist in database");
            }

            $foundVipBenefit->setNameTranslations($inputData->getNameTranslations());
            $foundVipBenefit->setDescriptionTranslations($inputData->getDescriptionTranslations());

            $this->entityManager->persist($foundVipBenefit);
            $this->entityManager->flush();

            $result->setData($foundVipBenefit);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetVipBenefit query
     *
     * Gets vip benefit based on id
     *
     * @Query(name="getVipBenefit")
     * @Logged
     * @Security("this.canManageVipBenefit(vipBenefit)")
     *
     * @param int $vipBenefit
     *
     * @return GetVipBenefitResponse
     */
    public function getVipBenefit(int $vipBenefit) : GetVipBenefitResponse {
        $result = new GetVipBenefitResponse();

        try {

            /** @var VipBenefit $foundVipBenefit */
            $foundVipBenefit = $this->vipBenefitRepository->findOneBy((array('id' => $vipBenefit)));
            if (is_null($foundVipBenefit))
                throw new Exception("VIP benefit does not exist in DB");

            $result->setData($foundVipBenefit);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteVipBenefit mutation
     *
     * Deletes vip benefit based on id
     *
     * @Mutation(name="deleteVipBenefit")
     * @Logged
     * @Security("this.canManageVipBenefit(vipBenefitId)")
     *
     * @param int $vipBenefitId
     * @return RemoveItemResponse
     */
    public function deleteVipBenefit(int $vipBenefitId) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            $foundVipBenefit = $this->vipBenefitRepository->findOneBy((array('id' => $vipBenefitId)));
            if (is_null($foundVipBenefit)) {
                throw new Exception("Delete failed: VIP Benefit does not exist in database");
            }

            $this->entityManager->remove($foundVipBenefit);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * AddVipBenefitToVipUser mutation
     *
     * Adds given vip benefit to given vip user
     *
     * @Mutation(name="addVipBenefitToVipUser")
     *
     * @UseInputType(for="$inputData", inputType="UserVipBenefitRelationInput")
     * @Logged
     * @Security("this.canManageVipBenefit(inputData.getVipBenefitId())")
     *
     * @param UserVipBenefitRelationInput $inputData
     * @return GetUserResponse
     */
    public function addVipBenefitToVipUser(UserVipBenefitRelationInput $inputData) : GetUserResponse {
        $result = new GetUserResponse();

        try {
            /** @var User|null $user */
            $user = $this->userRepository->findOneBy(((array('uid' => $inputData->getUid()))));
            if(is_null($user))
                throw new Exception("User with given uid does not exist in database");

            /** @var VipBenefit|null $vipBenefit */
            $vipBenefit = $this->vipBenefitRepository->findOneBy((array('id' => $inputData->getVipBenefitId())));
            if (is_null($vipBenefit))
                throw new Exception("Vip benefit with given id does not exist in database");

            $vipUser = $vipBenefit->getShop()->getVipUserBinding($user);
            if (!is_null($vipUser))
                $vipBenefit->addVipUser($vipUser);
            else
                throw new Exception("User is not VIP for given shop");

            $this->entityManager->flush();

            $result->setData($user);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * RemoveVipBenefitFromVipUser mutation
     *
     * Removes given vip benefit from given vip user
     *
     * @Mutation(name="removeVipBenefitFromVipUser")
     * @Logged
     * @Security("this.canManageVipBenefit(inputData.getVipBenefitId())")
     *
     * @UseInputType(for="$inputData", inputType="UserVipBenefitRelationInput")
     *
     * @param UserVipBenefitRelationInput $inputData
     * @return GetUserResponse
     */
    public function removeVipBenefitFromVipUser(UserVipBenefitRelationInput $inputData) : GetUserResponse {
        $result = new GetUserResponse();

        try {
            /** @var User|null $user */
            $user = $this->userRepository->findOneBy(((array('uid' => $inputData->getUid()))));
            if(is_null($user))
                throw new Exception("User with given uid does not exist in database");

            /** @var VipBenefit|null $vipBenefit */
            $vipBenefit = $this->vipBenefitRepository->findOneBy((array('id' => $inputData->getVipBenefitId())));
            if (is_null($vipBenefit))
                throw new Exception("Vip benefit with given id does not exist in database");

            $vipUser = $vipBenefit->getShop()->getVipUserBinding($user);
            if (!is_null($vipUser))
                $vipBenefit->removeVipUser($vipUser, $this->entityManager);
            else
                throw new Exception("User is not VIP for given shop");

            $this->entityManager->flush();

            $result->setData($user);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param string|null $vipBenefitId
     * @param string|null $shopId
     * @return bool
     */
    public function canManageVipBenefit(?string $vipBenefitId, ?string $shopId = null): bool {
        $shop = null;
        if (!is_null($vipBenefitId)) {
            $vipBenefit = $this->getVipBenefitInternal($vipBenefitId);
            if (!is_null($vipBenefit))
                $shop = $vipBenefit->getShop();
        } else if (!is_null($shopId)) {
            $shop = $this->getShopInternal($shopId);
        }

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_VIP), $shop);
    }

    /**
     * @param string|null $vipBenefitId
     * @return VipBenefit|null
     */
    public function getVipBenefitInternal(?string $vipBenefitId): ?VipBenefit
    {
        /** @var VipBenefit|null $vipBenefit */
        $vipBenefit = null;

        if (!is_null($vipBenefitId)) {
            $vipBenefit = $this->vipBenefitRepository->findOneBy((array('id' => $vipBenefitId)));
        }

        return $vipBenefit;
    }

    /**
     * @param string|null $shopId
     * @return Shop|null
     */
    public function getShopInternal(?string $shopId): ?Shop
    {
        /** @var Shop|null $shop */
        $shop = $this->shopRepository->findOneBy((array('id' => $shopId)));
        return $shop;
    }

}