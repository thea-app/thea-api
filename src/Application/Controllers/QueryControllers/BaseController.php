<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\ApplicationDefaults;
use App\Application\Services\MediaService;
use App\Application\Services\SecurityService\AuthorizationService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Slim\Factory\AppFactory;

/**
 * Class BaseController. Abstract controller class for resolving queries and mutations
 *
 * @package App\Application\Controllers\QueryControllers
 */
abstract class BaseController
{
    private $app;

    /**
     * Dependency container used across entire App
     */
    protected $container;

    /**
     * @var EntityManager
     *
     * Entity manager used across entire App
     */
    protected $entityManager;

    /** @var EntityRepository */
    protected $shopRepository;
    /** @var EntityRepository */
    protected $shopTranslationRepository;
    /** @var EntityRepository */
    protected $userRepository;
    /** @var EntityRepository */
    protected $countryRepository;
    /** @var EntityRepository */
    protected $languageRepository;
    /** @var EntityRepository */
    protected $categoryRepository;
    /** @var EntityRepository */
    protected $productRepository;
    /** @var EntityRepository */
    protected $productTranslationRepository;
    /** @var EntityRepository */
    protected $tastingProductRepository;
    /** @var EntityRepository */
    protected $tastingRepository;
    /** @var EntityRepository */
    protected $tastingProductTranslationRepository;
    /** @var EntityRepository */
    protected $eventRepository;
    /** @var EntityRepository */
    protected $eventTranslationRepository;
    /** @var EntityRepository */
    protected $userEventRepository;
    /** @var EntityRepository */
    protected $accessCodeRepository;
    /** @var EntityRepository */
    protected $bonusRepository;
    /** @var EntityRepository */
    protected $bonusCodeRepository;
    /** @var EntityRepository */
    protected $shopBonusUserRepository;
    /** @var EntityRepository */
    protected $vipBenefitRepository;
    /** @var EntityRepository */
    protected $shopVipUserRepository;
    /** @var EntityRepository */
    protected $flavourRepository;
    /** @var EntityRepository */
    protected $flavourWheelRepository;
    /** @var EntityRepository */
    protected $notificationRepository;
    /** @var EntityRepository */
    protected $fcmTokenRepository;
    /** @var EntityRepository */
    protected $userFavouriteShopRepository;
    /** @var EntityRepository */
    protected $sentNotificationRepository;

    /**
     * @var MediaService Media service for handling medias
     */
    protected $mediaService;

    /**
     * @var AuthorizationService
     */
    protected $authorizationService;

    /**
     * BaseController constructor. Sets entity manager.
     */
    public function __construct()
    {
        $this->app = AppFactory::create();
        $this->container = $this->app->getContainer();
        $this->entityManager = $this->container->get(EntityManagerInterface::class);

        $this->shopRepository = $this->entityManager->getRepository(ApplicationDefaults::$shopTable);
        $this->shopTranslationRepository = $this->entityManager->getRepository(ApplicationDefaults::$shopTranslationTable);
        $this->userRepository = $this->entityManager->getRepository(ApplicationDefaults::$userTable);
        $this->countryRepository = $this->entityManager->getRepository(ApplicationDefaults::$countryTable);
        $this->languageRepository = $this->entityManager->getRepository(ApplicationDefaults::$languageTable);
        $this->categoryRepository = $this->entityManager->getRepository(ApplicationDefaults::$categoryTable);
        $this->productRepository = $this->entityManager->getRepository(ApplicationDefaults::$productTable);
        $this->productTranslationRepository = $this->entityManager->getRepository(ApplicationDefaults::$productTranslationTable);
        $this->tastingProductRepository = $this->entityManager->getRepository(ApplicationDefaults::$tastingProductTable);
        $this->tastingRepository = $this->entityManager->getRepository(ApplicationDefaults::$tastingTable);
        $this->tastingProductTranslationRepository = $this->entityManager->getRepository(ApplicationDefaults::$tastingProductTranslationTable);
        $this->eventRepository = $this->entityManager->getRepository(ApplicationDefaults::$eventTable);
        $this->eventTranslationRepository = $this->entityManager->getRepository(ApplicationDefaults::$eventTranslationTable);
        $this->userEventRepository = $this->entityManager->getRepository(ApplicationDefaults::$userEventTable);
        $this->accessCodeRepository = $this->entityManager->getRepository(ApplicationDefaults::$accessCodeTable);
        $this->bonusRepository = $this->entityManager->getRepository(ApplicationDefaults::$bonusTable);
        $this->bonusCodeRepository = $this->entityManager->getRepository(ApplicationDefaults::$bonusCodeTable);
        $this->shopBonusUserRepository = $this->entityManager->getRepository(ApplicationDefaults::$shopBonusUserTable);
        $this->vipBenefitRepository = $this->entityManager->getRepository(ApplicationDefaults::$vipBenefitTable);
        $this->shopVipUserRepository = $this->entityManager->getRepository(ApplicationDefaults::$shopVipUserTable);
        $this->flavourRepository = $this->entityManager->getRepository(ApplicationDefaults::$flavourTable);
        $this->flavourWheelRepository = $this->entityManager->getRepository(ApplicationDefaults::$flavourWheelTable);
        $this->notificationRepository = $this->entityManager->getRepository(ApplicationDefaults::$notificationTable);
        $this->fcmTokenRepository = $this->entityManager->getRepository(ApplicationDefaults::$fcmTokenTable);
        $this->userFavouriteShopRepository = $this->entityManager->getRepository(ApplicationDefaults::$userFavouriteShopTable);
        $this->sentNotificationRepository = $this->entityManager->getRepository(ApplicationDefaults::$sentNotificationTable);


        $this->mediaService = new MediaService();
        $this->authorizationService = AuthorizationService::getInstance();
    }
}