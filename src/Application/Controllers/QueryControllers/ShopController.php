<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\ApplicationDefaults;
use App\Application\Model\Entities\Country;
use App\Application\Model\Entities\Shop;
use App\Application\Model\Entities\User;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\Enum\SocialsTypeEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Exceptions\InputNotValidException;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateShopInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\Location;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetEntityImageInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetShopInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetShopStateInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetShopResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetShopsResponse;
use App\Application\Model\Enum\ShopFilterEnum;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetSocialTypesUrlsResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class ShopController. Resolves queries and mutations connected with Shop entity
 *
 * @package App\Application\Controllers\QueryControllers
 */
class ShopController extends BaseController
{
    private string $dqlDistanceString = '3956 * 2 * ASIN(SQRT( (SIN((:latitude - shop.latitude) * :pi/180 / 2) * SIN((:latitude - shop.latitude) * :pi/180 / 2)) 
                    + COS(:latitude * :pi/180) * COS(:latitude * :pi/180) 
                    * (SIN((:longitude - shop.longitude) * :pi/180 / 2) * SIN((:longitude - shop.longitude) * :pi/180 / 2)) )) AS HIDDEN distance';

    /**
     * GetShop query
     *
     * Basic query resolver used for getting shop based on given shopId
     *
     * @Query(name="getShop")
     * @Logged
     *
     * @param int $shopId unique id of shop
     * @return GetShopResponse
     */
    public function getShop(int $shopId) : GetShopResponse {
        $result = new GetShopResponse();

        try {
            /** @var Shop $data */
            $data = $this->shopRepository->findOneBy((array('id' => $shopId)));

            if (is_null($data)) {
                throw new Exception("No shop in database with given id");
            }

            $result->setData($data);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetShops query
     *
     * Basic query resolver used for getting shops based on given pagination and filter parameters
     *
     * @Query(name="getShops")
     * @Logged
     *
     * @param ShopFilterEnum $filterValue filter to be applied
     * @param int|null $limit number of records to select
     * @param int|null $offset position from which to start selecting
     * @param Location|null $userLocation geolocation of user
     * @param string $searchValue string to be used for filtering shop names
     * @param string[]|null $states array of shop states
     * @return GetShopsResponse
     */
    public function getShops(ShopFilterEnum $filterValue, int $limit = 10, int $offset = null,
                          Location $userLocation = null, string $searchValue = "", array $states = null) : GetShopsResponse {

        $result = new GetShopsResponse();

        try {
            if(empty($states) || is_null($states))
                $states = [EntityStateEnum::ACTIVE];

            $orString = '';
            for ($i = 0; $i < count($states); $i++) {
                $orString .= 'shop.state = \'' . $states[$i] . '\'';
                if($i < count($states) - 1)
                    $orString .= ' OR ';
            }

            $data = $this->shopRepository->createQueryBuilder('shop')
                ->select(array('shop'));

            switch ($filterValue) {
                case ShopFilterEnum::NEAR:
                    if (!is_null($userLocation)) {
                        $data = $data->addSelect($this->dqlDistanceString)
                            ->setParameter('longitude', $userLocation->getLongitude())
                            ->setParameter('latitude', $userLocation->getLatitude())
                            ->setParameter('pi', pi())
                            ->orderBy('distance', 'asc');
                    }
                    break;
                case ShopFilterEnum::NO_FILTER:
                default:
                    if (!empty($searchValue)) {
                        $data = $data->join(ApplicationDefaults::$shopTranslationTable, 't', 'WITH', 'shop.id = t.shop')
                            ->andWhere('COLLATE(\'name\', utf8_general_ci, false) LIKE COLLATE(:name, utf8_general_ci, true)')
                            ->setParameter('name', '%' . $searchValue . '%');
                    }
                    break;
            }

            $data = $data->andWhere($orString)
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();

            $result->setData($data);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetSocialTypesUrls query
     *
     * Query resolver used for getting all possible Social Types and static urls connected with them
     *
     * @Query("getSocialTypesUrls")
     * @Logged
     *
     * @return GetSocialTypesUrlsResponse
     */
    public function getSocialTypesUrls() : GetSocialTypesUrlsResponse {
        $result = new GetSocialTypesUrlsResponse();

        try {
            foreach (SocialsTypeEnum::values() as $socialType) {
                $result->addData($socialType, SocialsTypeEnum::getLinkUrl($socialType));
            }
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * CreateShop mutation
     *
     * Creates new shop
     *
     * @Mutation(name="createShop")
     * @Logged
     * @Security("is_granted('CAN_CREATE_SHOP')")
     *
     * @UseInputType(for="$inputData", inputType="CreateShopInput")
     *
     * @param CreateShopInput $inputData
     * @return GetShopResponse
     */
    public function createShop(CreateShopInput $inputData) : GetShopResponse {
        $result = new GetShopResponse();

        try {

            //validations
            if ($inputData->getStreet() == '')
                throw new InputNotValidException("Create failed: Street field is mandatory and can not be empty");
            if ($inputData->getPostcode() == '')
                throw new InputNotValidException("Create failed: Postcode field is mandatory and can not be empty");
            if ($inputData->getCity() == '')
                throw new InputNotValidException("Create failed: City field is mandatory and can not be empty");

            /** @var Country $country */
            $country = $this->countryRepository->findOneBy((array('countryCode' => $inputData->getCountry())));

            if(is_null($country))
                throw new Exception("Create failed: Country does not exist in DB");

            //Create new Shop entity
            $newShop = Shop::create(
                $inputData->getState(),
                $inputData->getType(),
                $inputData->getStreet(),
                $inputData->getDescriptiveNumber(),
                $inputData->getRoutingNumber(),
                $inputData->getPostcode(),
                $inputData->getCity(),
                $country,
                $inputData->getEmails(),
                $inputData->getPhoneNumbers(),
                $inputData->getWebpage(),
                $inputData->getNameTranslations(),
                $inputData->getDescriptionTranslations(),
                false,
                $inputData->getLongitude(),
                $inputData->getLatitude(),
                $inputData->getEshopURL(),
                $inputData->getExternalReservationURL(),
                !is_null($inputData->getMainImage())
                    ? $this->mediaService->importFromBase64($inputData->getMainImage())
                    : null,
                $inputData->getOpeningHours(),
                $inputData->getOpeningHoursExceptions(),
                $inputData->getSocialLinks(),
                $this->entityManager
            );

            $this->entityManager->persist($newShop);
            $this->entityManager->flush();

            if (isset($_SESSION['uid'])) {
                /** @var User $caller */
                $caller = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
                $newShop->addAdmin($caller);
                $this->entityManager->flush();
            }

            $result->setData($newShop);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorCode($exception->getCode());
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetShop mutation
     *
     * Sets data to given shop
     *
     * @Mutation(name="setShop")
     * @Logged
     * @Security("this.canManageShop(inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="SetShopInput")
     *
     * @param SetShopInput $inputData
     * @return GetShopResponse
     */
    public function setShop(SetShopInput $inputData) : GetShopResponse {
        $result = new GetShopResponse();

        try {

            //validations
            if ($inputData->getStreet() == '')
                throw new InputNotValidException("Set failed: Street field is mandatory and can not be empty");
            if ($inputData->getPostcode() == '')
                throw new InputNotValidException("Set failed: Postcode field is mandatory and can not be empty");
            if ($inputData->getCity() == '')
                throw new InputNotValidException("Set failed: City field is mandatory and can not be empty");

            /** @var Shop $foundShop */
            $foundShop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));
            if (is_null($foundShop)) {
                $result->setData(null);
                throw new Exception("Set failed: Shop does not exist in database");
            }

            $foundShop->setType($inputData->getType());
            $foundShop->setStreet($inputData->getStreet());
            $foundShop->setDescriptiveNumber($inputData->getDescriptiveNumber());
            $foundShop->setRoutingNumber($inputData->getRoutingNumber());
            $foundShop->setPostcode($inputData->getPostcode());
            $foundShop->setCity($inputData->getCity());
            $foundShop->setEmails($inputData->getEmails(), $this->entityManager);
            $foundShop->setPhoneNumbers($inputData->getPhoneNumbers(), $this->entityManager);
            $foundShop->setWebpage($inputData->getWebpage());
            $foundShop->setEshopURL($inputData->getEshopURL());
            $foundShop->setNameTranslations($inputData->getNameTranslations());
            $foundShop->setDescriptionTranslations($inputData->getDescriptionTranslations());
            $foundShop->setBookingAvailable($inputData->getBookingAvailable());
            $foundShop->setLatitude($inputData->getLatitude());
            $foundShop->setLongitude($inputData->getLongitude());
            $foundShop->setExternalReservationURL($inputData->getExternalReservationURL());
            $foundShop->setOpeningHours($inputData->getOpeningHours(), $this->entityManager);
            $foundShop->setOpeningHoursExceptions($inputData->getOpeningHoursExceptions(), $this->entityManager);
            $foundShop->setSocialLinks($inputData->getSocialLinks(), $this->entityManager);
            if(!is_null($inputData->getCountry()))
            {
                /** @var Country $country */
                $country = $this->countryRepository->findOneBy((array('countryCode' => $inputData->getCountry())));

                if(is_null($country))
                    throw new Exception("Country does not exist in DB");

                $foundShop->setCountry($country);
            } else {
                $foundShop->setCountry(null);
            }

            $this->entityManager->flush();

            $result->setData($foundShop);
            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetShopImage mutation
     *
     * Sets image in base64 format to given shop
     *
     * @Mutation(name="setShopImage")
     * @Logged
     * @Security("this.canManageShop(inputData.getEntityId())")
     *
     * @UseInputType(for="$inputData", inputType="SetEntityImageInput")
     *
     * @param SetEntityImageInput $inputData
     * @return GetShopResponse
     */
    public function setShopImage(SetEntityImageInput $inputData): GetShopResponse
    {
        $result = new GetShopResponse();

        try {
            /** @var Shop $foundShop */
            $foundShop = $this->shopRepository->findOneBy((array('id' => $inputData->getEntityId())));
            if (is_null($foundShop)) {
                $result->setData(null);
                throw new Exception("Set failed: Shop does not exist in database");
            }

            $newImage = $inputData->getImage();
            $oldImageIdentifier = $foundShop->getMainImageIdentifier();
            if (!is_null($newImage)) {
                if (!is_null($oldImageIdentifier))
                    $foundShop->setMainImageIdentifier($this->mediaService->replaceFromBase64($newImage, $oldImageIdentifier));
                else
                    $foundShop->setMainImageIdentifier($this->mediaService->importFromBase64($newImage));
            } else {
                $this->mediaService->deleteImage($oldImageIdentifier);
                $foundShop->setMainImageIdentifier(null);
            }


            $this->entityManager->flush();

            $result->setData($foundShop);
            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorCode($exception->getCode());
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetShopState mutation
     *
     * Sets shop state
     *
     * @Mutation(name="setShopState")
     * @Logged
     * @Security("is_granted('CAN_UPDATE_SHOP_STATE')")
     *
     * @UseInputType(for="$inputData", inputType="SetShopStateInput")
     *
     * @param SetShopStateInput $inputData
     * @return GetShopResponse
     */
    public function setShopState(SetShopStateInput $inputData) : GetShopResponse{
        $result = new GetShopResponse();

        try {

            /** @var Shop $foundShop */
            $foundShop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));
            if (is_null($foundShop)) {
                $result->setData(null);
                throw new Exception("Set failed: Shop does not exist in database");
            }

            $foundShop->setState($inputData->getState());
            $this->entityManager->persist($foundShop);
            $this->entityManager->flush();

            $result->setData($foundShop);
            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorCode($exception->getCode());
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteShop mutation
     *
     * Deletes given shop
     *
     * @Mutation(name="deleteShop")
     * @Logged
     * @Security("is_granted('CAN_DELETE_SHOP')")
     *
     * @param int $shopId
     * @return RemoveItemResponse
     */
    public function deleteShop(int $shopId) :  RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            $foundShop = $this->shopRepository->findOneBy((array('id' => $shopId)));
            if (is_null($foundShop)) {
                throw new Exception("Delete failed: Shop does not exist in database");
            }

            $this->entityManager->remove($foundShop);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param string|null $shopId
     * @return bool
     */
    public function canManageShop(?string $shopId): bool
    {
        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_SHOP), $this->getShopInternal($shopId));
    }

    /**
     * @param string|null $shopId
     * @return Shop|null
     */
    public function getShopInternal(?string $shopId): ?Shop
    {
        /** @var Shop|null $shop */
        $shop = $this->shopRepository->findOneBy((array('id' => $shopId)));
        return $shop;
    }

}