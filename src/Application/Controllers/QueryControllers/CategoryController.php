<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\Category;
use App\Application\Model\Entities\Shop;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateCategoryInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetCategoryInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetEntityOrderingInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetParentEntityInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetCategoriesResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetCategoryResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use Exception;

use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class CategoryController. Resolves queries and mutations connected with Category entity
 *
 * @package App\Application\Controllers\QueryControllers
 */
class CategoryController extends BaseController
{
    /**
     * CreateCategory mutation
     *
     * Creates new category for given shop with given parameters.
     *
     * @Mutation(name="createCategory")
     * @Logged
     * @Security("this.canManageCategory(null, inputData.getShopId())")
     *
     * @UseInputType(for="$inputData", inputType="CreateCategoryInput")
     *
     * @param CreateCategoryInput $inputData
     * @return GetCategoryResponse
     */
    public function createCategory(CreateCategoryInput $inputData) : GetCategoryResponse
    {
        $result = new GetCategoryResponse();

        try {
            /** @var Shop $shop */
            $shop = $this->shopRepository->findOneBy((array('id' => $inputData->getShopId())));

            if (is_null($shop))
                throw new Exception("Shop does not exist in DB");

            /** @var Category $parentCategory */
            $parentCategory = null;
            if (!is_null($inputData->getParentCategoryId())) {
                $parentCategory = $this->categoryRepository->findOneBy((array('id' => $inputData->getParentCategoryId())));
                if (is_null($parentCategory))
                    throw new Exception("Create failed: Parent category does not exist in DB");
            }

            //Create new Category entity
            $newCategory = Category::create(
                new EntityStateEnum(EntityStateEnum::ACTIVE),
                null,
                $shop,
                $parentCategory,
                $inputData->getNameTranslations(),
                $inputData->getDescriptionTranslations()
            );

            $this->entityManager->persist($newCategory);
            $this->entityManager->flush();

            $result->setData($newCategory);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetCategory mutation
     *
     * Sets given parameters to given category.
     *
     * @Mutation(name="setCategory")
     * @Logged
     * @Security("this.canManageCategory(inputData.getCategoryId())")
     *
     * @UseInputType(for="$inputData", inputType="SetCategoryInput")
     *
     * @param SetCategoryInput $inputData
     * @return GetCategoryResponse
     */
    public function setCategory(SetCategoryInput $inputData) : GetCategoryResponse
    {
        $result = new GetCategoryResponse();

        try {

            /** @var Category $foundCategory */
            $foundCategory = $this->categoryRepository->findOneBy((array('id' => $inputData->getCategoryId())));
            if (is_null($foundCategory)) {
                $result->setData(null);
                throw new Exception("Set failed: Category does not exist in database");
            }

            /** @var Category $parentCategory */
            $parentCategory = null;
            if (!is_null($inputData->getParentCategoryId())) {
                $parentCategory = $this->categoryRepository->findOneBy((array('id' => $inputData->getParentCategoryId())));
                if (is_null($parentCategory))
                    throw new Exception("Set failed: Parent category does not exist in DB");
            }

            $foundCategory->setNameTranslations($inputData->getNameTranslations());
            $foundCategory->setDescriptionTranslations($inputData->getDescriptionTranslations());
            $foundCategory->setParentCategory($parentCategory);

            $this->entityManager->persist($foundCategory);
            $this->entityManager->flush();

            $result->setData($foundCategory);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetCategoriesOrdering mutation
     *
     * Sets categories ordering.
     *
     * @param SetEntityOrderingInput[] $inputData
     *
     * @Mutation(name="setCategoriesOrdering")
     * @Logged
     * @Security("this.canManageCategories(inputData)")
     *
     * @UseInputType(for="$inputData", inputType="[SetEntityOrderingInput!]!")
     *
     * @return GetCategoriesResponse
     */
    public function setCategoriesOrdering(array $inputData) : GetCategoriesResponse {
        $result = new GetCategoriesResponse();

        try {
            $categoryData = [];

            foreach ($inputData as $orderingInput) {
                /** @var Category $foundCategory */
                $foundCategory = $this->categoryRepository->findOneBy((array('id' => $orderingInput->getEntityId())));
                if (is_null($foundCategory)) {
                    $result->setData(null);
                    throw new Exception("Set failed: Category with id " . $orderingInput->getEntityId() . " does not exist in database");
                }
                $foundCategory->setOrdering($orderingInput->getOrdering());
                $categoryData[] = $foundCategory;
            }

            $this->entityManager->flush();

            $result->setData($categoryData);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetCategory query
     *
     * Gets category based on id
     *
     * @Query(name="getCategory")
     * @Logged
     *
     * @param int $categoryId
     *
     * @return GetCategoryResponse
     */
    public function getCategory(int $categoryId) : GetCategoryResponse {
        $result = new GetCategoryResponse();

        try {

            /** @var Category $data */
            $data = $this->categoryRepository->findOneBy((array('id' => $categoryId)));

            if (is_null($data)) {
                throw new Exception("No category in database with given categoryId");
            }

            $result->setData($data);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetCategories query
     *
     * Gets categories for given shop.
     * If the topLevel parameter is true, returns only categories with no parent.
     * Categories are ordered by predefined ordering (in setCategoriesOrdering mutation).
     *
     * @Query(name="getCategories")
     * @Logged
     *
     * @param int $shopId
     * @param bool $topLevel get only top level categories - useful for hierarchies
     *
     * @return GetCategoriesResponse
     */
    public function getCategories(int $shopId, bool $topLevel) : GetCategoriesResponse {
        $result =  new GetCategoriesResponse();

        try  {
            $shop = $this->shopRepository->findOneBy((array('id' => $shopId)));

            if(is_null($shop)) {
                throw new Exception("No shop with given id found");
            }

            $qb = $this->categoryRepository->createQueryBuilder('c');
            $data = $qb->where('c.shop =' . $shopId);
            if ($topLevel)
                $data = $data->andWhere('c.parentCategory is null');
            $data = $data->orderBy('c.ordering')
                         ->getQuery()
                         ->getResult();

            $result->setData($data);
            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteCategory mutation
     *
     * Deletes given category.
     * All products in this category will be moved under parent category.
     * If there is no parent category, all products will have no category.
     *
     * @Mutation(name="deleteCategory")
     * @Logged
     * @Security("this.canManageCategory(categoryId)")
     *
     * @param int $categoryId
     * @return RemoveItemResponse
     */
    public function deleteCategory(int $categoryId) : RemoveItemResponse
    {
        $result = new RemoveItemResponse();

        try {
            /** @var Category $foundCategory */
            $foundCategory = $this->categoryRepository->findOneBy((array('id' => $categoryId)));
            if (is_null($foundCategory)) {
                throw new Exception("Delete failed: Category does not exist in database");
            }

            foreach ($foundCategory->getProducts() as $product) {
                $product->setCategory($foundCategory->getParentCategory());
            }

            $this->entityManager->remove($foundCategory);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }


    /**
     * SetParentCategory mutation
     *
     * Sets parent category to given category or move the category to top level if parentEntityId is null
     *
     * @Mutation(name="setParentCategory")
     * @Logged
     * @Security("this.canManageCategory(inputData.getEntityId())")
     *
     * @UseInputType(for="$inputData", inputType="SetParentEntityInput")
     *
     * @param SetParentEntityInput $inputData
     * @return GetCategoryResponse
     */
    public function setParentCategory(SetParentEntityInput $inputData): GetCategoryResponse
    {
        $result = new GetCategoryResponse();

        try {
            /** @var Category $foundCategory */
            $foundCategory = $this->categoryRepository->findOneBy((array('id' => $inputData->getEntityId())));
            if (is_null($foundCategory)) {
                $result->setData(null);
                throw new Exception("Update failed: Category does not exist in database");
            }

            /** @var Category $parentCategory */
            $parentCategory = null;
            if (!is_null($inputData->getParentEntityId())) {
                $parentCategory = $this->categoryRepository->findOneBy((array('id' => $inputData->getParentEntityId())));
                if (is_null($parentCategory))
                    throw new Exception("Update failed: Parent category does not exist in DB");
            }

            $foundCategory->setParentCategory($parentCategory);

            $this->entityManager->persist($foundCategory);
            $this->entityManager->flush();

            $result->setData($foundCategory);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param SetEntityOrderingInput[] $entityOrderingInput
     * @return bool
     */
    public function canManageCategories(array $entityOrderingInput): bool {
        foreach ($entityOrderingInput as $item) {
            if (!$this->canManageCategory($item->getEntityId()))
                return false;
        }
        return true;
    }

    /**
     * @param string|null $categoryId
     * @param string|null $shopId
     * @return bool
     */
    public function canManageCategory(?string $categoryId = null, ?string $shopId = null): bool {
        $shop = null;

        if (!is_null($categoryId)) {
            $category = $this->getCategoryInternal($categoryId);
            if (!is_null($category))
                $shop = $category->getShop();

        } else if (!is_null($shopId)) {
            $shop = $this->getShopInternal($shopId);
        }

        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_MENU), $shop);
    }

    /**
     * @param string|null $categoryId
     * @return Category|null
     */
    public function getCategoryInternal(?string $categoryId): ?Category
    {
        /** @var Category|null $category */
        $category = $this->categoryRepository->findOneBy((array('id' => $categoryId)));
        return $category;
    }

    /**
     * @param string|null $shopId
     * @return Shop|null
     */
    public function getShopInternal(?string $shopId): ?Shop
    {
        /** @var Shop|null $shop */
        $shop = $this->shopRepository->findOneBy((array('id' => $shopId)));
        return $shop;
    }
}
