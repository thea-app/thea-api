<?php


namespace App\Application\Controllers\QueryControllers;


use App\Application\Model\Entities\FcmToken;
use App\Application\Model\Entities\Shop;
use App\Application\Model\Entities\User;
use App\Application\Model\Entities\UserRights;
use App\Application\Model\Enum\GlobalRightsEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\Enum\UserHasEmailNotificationsEnabledEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\UserTypeEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateUserInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetUserImageInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\UpdateUserInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\UserAuthInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetUserResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetUsersResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\UserAuthResponse;
use Exception;
use InvalidArgumentException;
use Kreait\Firebase\Auth;
use Kreait\Firebase\Exception\Auth\UserNotFound;
use Kreait\Firebase\Exception\AuthException;
use Kreait\Firebase\Exception\FirebaseException;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class UserController. Resolves queries and mutations connected with User entity
 *
 * @package App\Application\Controllers\QueryControllers
 */
class UserController extends BaseController
{
    private $firebase;

    /**
     * UserController constructor. Sets main repository of class @see User
     */
    public function __construct()
    {
        parent::__construct();
        $this->firebase = $this->container->get(Auth::class);
    }

    /**
     * GetUser query
     *
     * Basic query resolver used for getting one user based on given email or uid in parameter or HTTP session
     *
     * @Query(name="getUser")
     * @Logged
     *
     * @param string|null $uid unique id of user taken from firebase
     * @param string|null $email unique email of user taken from firebase
     * @return GetUserResponse
     */
    public function getUser(string $uid = null, string $email = null) : GetUserResponse {
        $result = new GetUserResponse();

        try {

            /** @var User $data */
            $data = null;
            if(!is_null($uid))
                $data = $this->userRepository->findOneBy((array('uid' => $uid)));
            else if (!is_null($email))
                $data = $this->userRepository->findOneBy((array('email' => $email)));
            else if (isset($_SESSION['uid']))
                $data = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            else
                throw new Exception("At least one of the parameters must be set");

            if (is_null($data))
                throw new Exception("No user in database with given parameters");

            $result->setData($data);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetUsers query
     *
     * Basic query resolver used for getting users based on given pagination parameters
     *
     * @Query(name="getUsers")
     * @Logged
     * @Security("is_granted('CAN_GET_USERS')")
     *
     * @param int|null $limit number of records to select
     * @param int|null $offset position from which to start selecting
     * @param string[]|null $states
     * @return GetUsersResponse
     */
    public function getUsers(int $limit = 10, int $offset = null, array $states = null) : GetUsersResponse {
        $result = new GetUsersResponse();

        try {
            if(empty($states) || is_null($states))
                $states = [EntityStateEnum::ACTIVE];

            $data = $this->userRepository->createQueryBuilder('user');

            for ($i = 0; $i < count($states); $i++) {
                $data->orWhere('user.state = :stateValue' . $i)->setParameter('stateValue' . $i, $states[$i]);
            }

            //TODO: maybe better pagination example here: https://stackoverflow.com/questions/34463859/symfony-doctrine-pagination
            $data = $data->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();

            if (empty($data)) {
                throw new Exception("No user data in database");
            }

            $result->setData($data);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * CreateUser mutation
     *
     * Creates new user.
     * User must exists in firebase already.
     *
     * @Mutation(name="createUser")
     * @Logged
     *
     * @UseInputType(for="$inputData", inputType="CreateUserInput")
     *
     * @param CreateUserInput $inputData
     * @return GetUserResponse
     */
    public function createUser(CreateUserInput $inputData) : GetUserResponse
    {
        $result = new GetUserResponse();

        try {
            //Test, if user does exist in Firebase
            $firebaseUser = $this->firebase->getUser($inputData->getUid());
            $email = $firebaseUser->email;

            /** @var User $foundUser */
            $foundUser = $this->userRepository->findOneBy((array('uid' => $inputData->getUid())));

            if (!is_null($foundUser) && ($foundUser->getState() != EntityStateEnum::DELETED)) {
                $result->setData($foundUser);
                throw new Exception("User already exists in database");
            }

            //TODO: setting main shop has to be deleted in the real use, when there are more than one shop
            /** @var Shop $karelShop */
            $karelShop = $this->shopRepository->findOneBy((array('id' => 1)));

            //Create new User entity
            $newUser = User::create(
                $inputData->getUid(),
                is_null($inputData->getState())
                    ? new EntityStateEnum(EntityStateEnum::ACTIVE)
                    : $inputData->getState(),
                $inputData->getType(),
                $email,
                $inputData->getFirstName(),
                $inputData->getLastName(),
                $inputData->getBirthYear(),
                $inputData->getGender(),
                $karelShop,
                is_null($inputData->getHasEmailNotificationsEnabled())
                    ? new UserHasEmailNotificationsEnabledEnum(UserHasEmailNotificationsEnabledEnum::ALLOWED_AND_DISABLED)
                    : $inputData->getHasEmailNotificationsEnabled(),
                !is_null($inputData->getUserImage())
                    ? $this->mediaService->importFromBase64($inputData->getUserImage())
                    : null
            );

            $this->entityManager->persist($newUser);
            $this->entityManager->flush();

            $canCreateShop = UserRights::create($newUser, new GlobalRightsEnum(GlobalRightsEnum::CAN_CREATE_SHOP));
            $this->entityManager->persist($canCreateShop);
            $this->entityManager->flush();

            return $this->getUser($newUser->getUid());
        } catch (AuthException | FirebaseException $exception) {
            $result->setData(null);
            $result->setSuccess(false);

            if($exception instanceof UserNotFound) {
                $result->setErrorMessage("User does not exist in Firebase");
            } else {
                $result->setErrorMessage($exception->getMessage());
            }
        } catch (Exception $exception)  {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * UpdateUser mutation
     *
     * Basic query resolver used for updating users based on given uid or uid from session
     *
     * @Mutation(name="updateUser")
     * @Logged
     * @Security("this.canManageUser(inputData.getUid())")
     *
     * @UseInputType(for="$inputData", inputType="UpdateUserInput")
     *
     * @param UpdateUserInput $inputData
     * @return GetUserResponse
     */
    public function updateUser(UpdateUserInput $inputData) : GetUserResponse {

        $result = new GetUserResponse();

        try {
            if (is_null($inputData->getUid()) && isset($_SESSION['uid'])) {
                $inputData->setUid($_SESSION['uid']);
            }

            //Test, if user does exist in Firebase
            $this->firebase->getUser($inputData->getUid());

            /** @var User|null $foundUser */
            $foundUser = $this->userRepository->findOneBy((array('uid' => $inputData->getUid())));

            if (is_null($foundUser)) {
                $result->setData(null);
                throw new Exception("Update failed: User does not exist in database");
            }

            //update new values
            $newImage = $inputData->getUserImage();
            if (!is_null($newImage)) {
                $oldImageIdentifier = $foundUser->getUserImageIdentifier();
                if (!is_null($oldImageIdentifier))
                    $foundUser->setUserImageIdentifier($this->mediaService->replaceFromBase64($newImage, $oldImageIdentifier));
                else
                    $foundUser->setUserImageIdentifier($this->mediaService->importFromBase64($newImage));
            }

            if(!is_null($inputData->getState())) $foundUser->setState($inputData->getState());
            if(!is_null($inputData->getType())) $foundUser->setType($inputData->getType());
            if(!is_null($inputData->getEmail())) $foundUser->setEmail($inputData->getEmail());
            if(!is_null($inputData->getFirstName())) $foundUser->setFirstName($inputData->getFirstName());
            if(!is_null($inputData->getLastName())) $foundUser->setLastName($inputData->getLastName());
            if(!is_null($inputData->getBirthYear())) $foundUser->setBirthYear($inputData->getBirthYear());
            if(!is_null($inputData->getGender())) $foundUser->setGender($inputData->getGender());
            if(!is_null($inputData->getHasEmailNotificationsEnabled())) $foundUser->setHasEmailNotificationsEnabled($inputData->getHasEmailNotificationsEnabled());

            //Update user in DB
            $this->entityManager->flush();

            //Update user in firebase
            $this->firebase->updateUser($inputData->getUid(), [
                "email" => $foundUser->getEmail(),
                "displayName" => $foundUser->getFirstName() . ' ' . $foundUser->getLastName()
            ]);

            $result->setData($foundUser);
            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (AuthException | FirebaseException $exception) {
            $result->setData(null);
            $result->setSuccess(false);

            if($exception instanceof UserNotFound) {
                $result->setErrorMessage("Update failed: User does not exist in Firebase");
            } else {
                $result->setErrorMessage($exception->getMessage());
            }
        } catch (Exception $exception)  {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetUserImage mutation
     *
     * Sets image in base64 format to given user
     *
     * @Mutation(name="setUserImage")
     * @Logged
     * @Security("this.canManageUser(inputData.getUid())")
     *
     * @UseInputType(for="$inputData", inputType="SetUserImageInput")
     *
     * @param SetUserImageInput $inputData
     * @return GetUserResponse
     */
    public function setUserImage(SetUserImageInput $inputData): GetUserResponse
    {
        $result = new GetUserResponse();

        try {
            if (is_null($inputData->getUid()) && isset($_SESSION['uid'])) {
                $inputData->setUid($_SESSION['uid']);
            }

            //Test, if user does exist in Firebase
            $this->firebase->getUser($inputData->getUid());

            /** @var User $foundUser */
            $foundUser = $this->userRepository->findOneBy((array('uid' => $inputData->getUid())));
            if (is_null($foundUser)) {
                $result->setData(null);
                throw new Exception("Update failed: User does not exist in database");
            }

            $newImage = $inputData->getUserImage();
            $oldImageIdentifier = $foundUser->getUserImageIdentifier();
            if (!is_null($newImage)) {
                if (!is_null($oldImageIdentifier))
                    $foundUser->setUserImageIdentifier($this->mediaService->replaceFromBase64($newImage, $oldImageIdentifier));
                else
                    $foundUser->setUserImageIdentifier($this->mediaService->importFromBase64($newImage));
            } else {
                if (!is_null($oldImageIdentifier))
                    $this->mediaService->deleteImage($oldImageIdentifier);
                $foundUser->setUserImageIdentifier(null);
            }


            $this->entityManager->flush();

            $result->setData($foundUser);
            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (AuthException | FirebaseException $exception) {
            $result->setData(null);
            $result->setSuccess(false);

            if ($exception instanceof UserNotFound) {
                $result->setErrorMessage("Update failed: User does not exist in Firebase");
            } else {
                $result->setErrorMessage($exception->getMessage());
            }
        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * LoginUser mutation
     *
     * Logs in user.
     * Login information are set to session.
     *
     * @Mutation(name="loginUser")
     * @UseInputType(for="$inputData", inputType="UserAuthInput")
     *
     * @param UserAuthInput $inputData
     * @return GetUserResponse
     */
    public function loginUser(UserAuthInput $inputData) : GetUserResponse {
        $result = new GetUserResponse();

        try {
            //verify token get uid from firebase base on given token
            $uid = $this->firebase->verifyIdToken($inputData->getToken())->claims()->get("sub");

            /** @var User $foundUser */
            $foundUser = $this->userRepository->findOneBy((array('uid' => $uid)));

            if (is_null($foundUser) || ($foundUser->getState() == EntityStateEnum::DELETED)) {
                throw new Exception("User does not exist in database");
            }

            $_SESSION['loggedIn'] = true;
            $_SESSION['uid'] = $uid;
            setcookie('PHPSESSID', session_id(), time() + 31536000);

            $result->setSuccess(true);
            $result->setErrorMessage("");
            $result->setData($foundUser);
        } catch (InvalidArgumentException $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage('The token is invalid: ' . $exception->getMessage());
            $result->setData(null);
        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
            $result->setData(null);
        }

        return $result;
    }

    /**
     * LogoutUser mutation
     *
     * Logs out user.
     * Session is destroyed.
     *
     * @Mutation(name="logoutUser")
     *
     * @param string|null $fcmToken
     * @return UserAuthResponse
     */
    public function logoutUser(?string $fcmToken) : UserAuthResponse {
        $result = new UserAuthResponse();
        try {

            if (!is_null($fcmToken) && isset($_SESSION['uid'])) {
                /** @var User $user */
                $user = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));

                /** @var FcmToken $dbFcmToken */
                $dbFcmToken = $this->fcmTokenRepository->findOneBy((array('fcmToken' => $fcmToken, 'user' => $user)));

                if (!is_null($dbFcmToken)) {
                    $this->entityManager->remove($dbFcmToken);
                    $this->entityManager->flush();
                }
            }

            setcookie(session_name(), null, time() - 86400);
            session_destroy();
            $_SESSION = array();

            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * AuthorizeUser mutation
     *
     * AuthorizeUser mutation logs in the user, and if he does not already exist in the database, it creates him
     *
     * @Mutation(name="authorizeUser")
     * @UseInputType(for="$inputData", inputType="UserAuthInput")
     *
     * @param UserAuthInput $inputData
     * @return GetUserResponse
     */
    public function authorizeUser(UserAuthInput $inputData) : GetUserResponse {
        $result = new GetUserResponse();

        try {
            //verify token get uid from firebase base on given token
            $uid = $this->firebase->verifyIdToken($inputData->getToken())->claims()->get("sub");

            /** @var User $foundUser */
            $foundUser = $this->userRepository->findOneBy((array('uid' => $uid)));

            if (is_null($foundUser) || ($foundUser->getState() == EntityStateEnum::DELETED)) {
                $newUserData = CreateUserInput::create(
                    $uid,
                    null,
                    new UserTypeEnum(UserTypeEnum::CUSTOMER),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                );

                $createUserResult = $this->createUser($newUserData);

                $result->setSuccess($createUserResult->getSuccess());
                $result->setErrorMessage($createUserResult->getErrorMessage());
                $result->setData($createUserResult->getData());
            } else {
                $result->setSuccess(true);
                $result->setErrorMessage("");
                $result->setData($foundUser);
            }

            $_SESSION['loggedIn'] = true;
            $_SESSION['uid'] = $uid;
            setcookie('PHPSESSID', session_id(), time() + 31536000);


        } catch (InvalidArgumentException $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage('The token is invalid: ' . $exception->getMessage());
        } catch (Exception | AuthException | FirebaseException $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteUser mutation
     *
     * Deletes user based on userUID. User is not deleted from firebase.
     *
     * @Mutation(name="deleteUser")
     * @Logged
     * @Security("is_granted('CAN_DELETE_USER')")
     *
     * @param string $userUID
     * @return RemoveItemResponse
     */
    public function deleteUser(string $userUID) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            $foundUser = $this->userRepository->findOneBy((array('uid' => $userUID)));
            if (is_null($foundUser)) {
                throw new Exception("Delete failed: User does not exist in database");
            }

            $foundUser->setState(new EntityStateEnum(EntityStateEnum::DELETED));

            $this->entityManager->flush();

            //TODO: vymazat vo firebase

            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param string|null $uid
     * @param string|null $email
     * @return bool
     */
    public function canManageUser(?string $uid, ?string $email = null): bool
    {
        return $this->authorizationService->isAllowed(
            new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_USER), $this->getUserInternal($uid, $email));
    }

    /**
     * @param string|null $uid
     * @param string|null $email
     * @return User|null
     */
    public function getUserInternal(?string $uid, ?string $email): ?User
    {
        if (!is_null($uid) && !is_null($email)) {
            /** @var User $user */
            $user = $this->userRepository->findOneBy((array('uid' => $uid, 'email' => $email)));
            if (is_null($user))
                return null;
            return $user;
        }

        if (!is_null($uid)) {
            /** @var User $user */
            $user = $this->userRepository->findOneBy((array('uid' => $uid)));
            if (is_null($user))
                return null;
            return $user;
        }

        if (!is_null($email)) {
            /** @var User $user */
            $user = $this->userRepository->findOneBy((array('email' => $email)));
            if (is_null($user))
                return null;
            return $user;
        }

        return null;
    }
}
