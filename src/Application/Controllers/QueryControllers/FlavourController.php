<?php


namespace App\Application\Controllers\QueryControllers;


use App\Application\Model\Entities\Flavour;
use App\Application\Model\Entities\FlavourWheel;
use App\Application\Model\Entities\Tasting;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateFlavourInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateFlavourWheelInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetEntityOrderingInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetFlavourInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetParentEntityInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetFlavourResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetFlavoursResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetFlavourWheelResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * Class FlavourController. Resolves queries and mutations connected with Flavour and FlavourWheel entities
 *
 * @package App\Application\Controllers\QueryControllers
 */
class FlavourController extends BaseController
{
    /**
     * CreateFlavourWheel mutation
     *
     * Creates flavour wheel for given tasting.
     * Create is possible only if the tasting has not started yet.
     *
     * @Mutation(name="createFlavourWheel")
     * @Logged
     * @Security("this.canManageFlavour(null, null, inputData.getTastingId())")
     *
     * @UseInputType(for="$inputData", inputType="CreateFlavourWheelInput")
     *
     * @param CreateFlavourWheelInput $inputData
     * @return GetFlavourWheelResponse
     */
    public function createFlavourWheel(CreateFlavourWheelInput $inputData) : GetFlavourWheelResponse
    {
        $result = new GetFlavourWheelResponse();

        try {
            /** @var Tasting|null $tasting */
            $tasting = $this->eventRepository->findOneBy((array('id' => $inputData->getTastingId())));

            if (is_null($tasting))
                throw new Exception("Tasting does not exist in DB");

            if ($tasting->started())
                throw new Exception("Create failed: Tasting has already started");

            //Create new FlavourWheel entity
            $newFlavourWheel = FlavourWheel::create(
                new EntityStateEnum(EntityStateEnum::ACTIVE),
                $inputData->getName(),
                $tasting
            );

            $this->entityManager->persist($newFlavourWheel);

            $tasting->setFlavourWheel($newFlavourWheel);

            $this->entityManager->flush();

            $result->setData($newFlavourWheel);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * CreateFlavour mutation
     *
     * Creates flavour for given flavour wheel.
     * Create is possible only if the tasting has not started yet.
     *
     * @Mutation(name="createFlavour")
     * @Logged
     * @Security("this.canManageFlavour(null, inputData.getFlavourWheelId())")
     *
     * @UseInputType(for="$inputData", inputType="CreateFlavourInput")
     *
     * @param CreateFlavourInput $inputData
     * @return GetFlavourResponse
     */
    public function createFlavour(CreateFlavourInput $inputData) : GetFlavourResponse
    {
        $result = new GetFlavourResponse();

        try {
            /** @var FlavourWheel|null $flavourWheel */
            $flavourWheel = $this->flavourWheelRepository->findOneBy((array('id' => $inputData->getFlavourWheelId())));

            if (is_null($flavourWheel))
                throw new Exception("Create failed: Flavour wheel does not exist in DB");

            foreach ($flavourWheel->getTastings() as $tasting) {
                if ($tasting->started())
                    throw new Exception("Create failed: Tasting has already started");
            }

            /** @var Flavour $parentFlavour */
            $parentFlavour = null;
            if (!is_null($inputData->getParentFlavourId())) {
                $parentFlavour = $this->flavourRepository->findOneBy((array('id' => $inputData->getParentFlavourId())));
                if (is_null($parentFlavour))
                    throw new Exception("Create failed: Parent flavour does not exist in DB");
            }

            $color = $inputData->getColor();

            if (!is_null($parentFlavour))
                $color = $parentFlavour->getColor();

            //Create new Flavour entity
            $newFlavour = Flavour::create(
                new EntityStateEnum(EntityStateEnum::ACTIVE),
                $inputData->getOrdering(),
                $color,
                $flavourWheel,
                $parentFlavour,
                $inputData->getNameTranslations()
            );

            $this->entityManager->persist($newFlavour);
            $this->entityManager->flush();

            $result->setData($newFlavour);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetFlavour mutation
     *
     * Sets flavour based on id for given flavour wheel.
     * Set is possible only if the tasting has not started yet.
     *
     * @Mutation(name="setFlavour")
     * @Logged
     * @Security("this.canManageFlavour(inputData.getFlavourId())")
     *
     * @UseInputType(for="$inputData", inputType="SetFlavourInput")
     *
     * @param SetFlavourInput $inputData
     * @return GetFlavourResponse
     */
    public function setFlavour(SetFlavourInput $inputData) : GetFlavourResponse
    {
        $result = new GetFlavourResponse();

        try {
            /** @var Flavour|null $foundFlavour */
            $foundFlavour = $this->flavourRepository->findOneBy((array('id' => $inputData->getFlavourId())));
            if (is_null($foundFlavour)) {
                throw new Exception("Set failed: Flavour does not exist in database");
            }

            /** @var FlavourWheel|null $flavourWheel */
            $flavourWheel = $this->flavourWheelRepository->findOneBy((array('id' => $inputData->getFlavourWheelId())));

            if (is_null($flavourWheel))
                throw new Exception("Set failed: Flavour wheel does not exist in DB");

            foreach ($flavourWheel->getTastings() as $tasting) {
                if ($tasting->started())
                    throw new Exception("Set failed: Tasting has already started");
            }

            foreach ($foundFlavour->getFlavourWheel()->getTastings() as $tasting) {
                if ($tasting->started())
                    throw new Exception("Update failed: Tasting has already started");
            }

            $foundFlavour->setFlavourWheel($flavourWheel);
            $foundFlavour->setNameTranslations($inputData->getNameTranslations());
            $foundFlavour->setColor($inputData->getColor());

            $this->entityManager->persist($foundFlavour);
            $this->entityManager->flush();

            $result->setData($foundFlavour);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }


    /**
     * SetFlavoursOrdering mutation
     *
     * Sets flavours ordering.
     *
     * @param SetEntityOrderingInput[] $inputData
     *
     * @Mutation(name="setFlavoursOrdering")
     * @Logged
     * @Security("this.canManageFlavours(inputData)")
     *
     * @UseInputType(for="$inputData", inputType="[SetEntityOrderingInput!]!")
     *
     * @return GetFlavoursResponse
     */
    public function setFlavoursOrdering(array $inputData) : GetFlavoursResponse {
        $result = new GetFlavoursResponse();

        try {

            $flavoursData = [];

            foreach ($inputData as $orderingInput) {
                /** @var Flavour $foundFlavour */
                $foundFlavour = $this->flavourRepository->findOneBy((array('id' => $orderingInput->getEntityId())));
                if (is_null($foundFlavour)) {
                    $result->setData(null);
                    throw new Exception("Set failed: Flavour with id " . $orderingInput->getEntityId() . " does not exist in database");
                }
                foreach ($foundFlavour->getFlavourWheel()->getTastings() as $tasting) {
                    if ($tasting->started())
                        throw new Exception("Set failed: Tasting has already started");
                }
                $foundFlavour->setOrdering($orderingInput->getOrdering());
                $flavoursData[] = $foundFlavour;
            }

            $this->entityManager->flush();

            $result->setData($flavoursData);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetParentFlavour mutation
     *
     * Sets parent flavour to given flavour or move the flavour to top level if parentEntityId is null.
     * Flavour and parent flavour must be in the same flavour wheel.
     * Set is possible only if the tasting has not started yet.
     *
     * @Mutation(name="setParentFlavour")
     * @Logged
     * @Security("this.canManageFlavour(inputData.getEntityId())")
     *
     * @UseInputType(for="$inputData", inputType="SetParentEntityInput")
     *
     * @param SetParentEntityInput $inputData
     * @return GetFlavourResponse
     */
    public function setParentFlavour(SetParentEntityInput $inputData): GetFlavourResponse
    {
        $result = new GetFlavourResponse();

        try {
            /** @var Flavour $foundFlavour */
            $foundFlavour = $this->flavourRepository->findOneBy((array('id' => $inputData->getEntityId())));
            if (is_null($foundFlavour)) {
                $result->setData(null);
                throw new Exception("Set failed: Flavour does not exist in database");
            }

            foreach ($foundFlavour->getFlavourWheel()->getTastings() as $tasting) {
                if ($tasting->started())
                    throw new Exception("Set failed: Tasting has already started");
            }

            /** @var Flavour $parentFlavour */
            $parentFlavour = null;
            if (!is_null($inputData->getParentEntityId())) {
                $parentFlavour = $this->flavourRepository->findOneBy((array('id' => $inputData->getParentEntityId())));
                if (is_null($parentFlavour))
                    throw new Exception("Set failed: Parent flavour does not exist in DB");
            }

            if (!is_null($parentFlavour)) {
                $foundFlavour->setColor($parentFlavour->getColor());
                if ($foundFlavour->getFlavourWheel()->getId() != $parentFlavour->getFlavourWheel()->getId())
                    throw new Exception("Set failed: Both flavours must be in the same flavour wheel");
            }

            $foundFlavour->setParentFlavour($parentFlavour);

            $this->entityManager->persist($foundFlavour);
            $this->entityManager->flush();

            $result->setData($foundFlavour);
            $result->setErrorMessage("");
            $result->setSuccess(true);
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteFlavour mutation
     *
     * Deletes flavour based on Id.
     * Delete is possible only if the tasting has not started yet.
     *
     * @Mutation(name="deleteFlavour")
     * @Logged
     * @Security("this.canManageFlavour(flavourId)")
     *
     * @param int $flavourId
     * @return RemoveItemResponse
     */
    public function deleteFlavour(int $flavourId): RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            /** @var Flavour|null $foundFlavour */
            $foundFlavour = $this->flavourRepository->findOneBy((array('id' => $flavourId)));
            if (is_null($foundFlavour)) {
                throw new Exception("Delete failed: Flavour does not exist in database");
            }

            foreach ($foundFlavour->getFlavourWheel()->getTastings() as $tasting) {
                if ($tasting->started())
                    throw new Exception("Delete failed: Tasting has already started");
            }

            foreach ($foundFlavour->getSubFlavours() as $subFlavour) {
                $subFlavour->setParentFlavour($foundFlavour->getParentFlavour());
                $this->entityManager->persist($subFlavour);
            }

            $this->entityManager->remove($foundFlavour);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param SetEntityOrderingInput[] $entityOrderingInput
     * @return bool
     */
    public function canManageFlavours(array $entityOrderingInput): bool {
        foreach ($entityOrderingInput as $item) {
            if (!$this->canManageFlavour($item->getEntityId()))
                return false;
        }
        return true;
    }

    /**
     * @param string|null $flavourId
     * @param string|null $flavourWheelId
     * @param string|null $tastingId
     * @return bool
     */
    public function canManageFlavour(?string $flavourId = null, ?string $flavourWheelId = null, ?string $tastingId = null): bool {
        $shops = [];

        if (!is_null($flavourId)) {
            $flavour = $this->getFlavourInternal($flavourId);
            if (!is_null($flavour)) {
                foreach ($flavour->getFlavourWheel()->getTastings() as $tasting) {
                    $shops[] = $tasting->getShop();
                }
            }
        } else if (!is_null($flavourWheelId)) {
            $flavourWheel = $this->getFlavourWheelInternal($flavourWheelId);
            if (!is_null($flavourWheel)) {
                foreach ($flavourWheel->getTastings() as $tasting) {
                    $shops[] = $tasting->getShop();
                }
            }
        } else if (!is_null($tastingId)) {
            $shops[] = $this->getTastingInternal($tastingId)->getShop();
        }

        foreach ($shops as $shop) {
            if (!$this->authorizationService->isAllowed(new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_EVENTS), $shop))
                return false;
        }
        return true;
    }

    /**
     * @param string|null $flavourId
     * @return Flavour|null
     */
    public function getFlavourInternal(?string $flavourId): ?Flavour
    {
        /** @var Flavour|null $flavour */
        $flavour = $this->flavourRepository->findOneBy((array('id' => $flavourId)));
        return $flavour;
    }

    /**
     * @param string|null $flavourWheelId
     * @return FlavourWheel|null
     */
    public function getFlavourWheelInternal(?string $flavourWheelId): ?FlavourWheel
    {
        /** @var FlavourWheel|null $flavourWheel */
        $flavourWheel = $this->flavourWheelRepository->findOneBy((array('id' => $flavourWheelId)));
        return $flavourWheel;
    }

    /**
     * @param string|null $tastingId
     * @return Tasting|null
     */
    public function getTastingInternal(?string $tastingId): ?Tasting
    {
        /** @var Tasting|null $tasting */
        $tasting = $this->tastingRepository->findOneBy((array('id' => $tastingId)));
        return $tasting;
    }

}