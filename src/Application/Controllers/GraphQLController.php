<?php

namespace App\Application\Controllers;

use App\Application\Services\SecurityService\AuthenticationService;
use App\Application\Services\SecurityService\AuthorizationService;
use Doctrine\DBAL\Connection;
use GraphQL\GraphQL;
use Kreait\Firebase\Auth;
use Phoole\Cache\Cache;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use Slim\Factory\AppFactory;
use TheCodingMachine\GraphQLite\SchemaFactory;
use TheCodingMachine\GraphQLite\Types\AnyScalar\AnyScalarTypeMapperFactory;

class GraphQLController
{
    protected $db;
    protected $logger;
    protected $auth;

    //$connection and $logger are taken from dependency container declared in app/dependencies.php
    public function __construct(Connection $connection, LoggerInterface $logger, Auth $auth)
    {
        $this->db = $connection;
        $this->logger = $logger;
        $this->auth = $auth;
    }

    public function index(Request $request, Response $response) {

        $app = AppFactory::create();
        $cache = new Cache();
        $factory = new SchemaFactory($cache, $app->getContainer());
        $factory->addRootTypeMapperFactory(new AnyScalarTypeMapperFactory());
        $factory->addControllerNamespace('App\\Application\\Controllers\\QueryControllers')
            ->addTypeNamespace('App\\Application\\Model');
        // Configure an authentication service (to resolve the @Logged annotations).
        $authenticationService = new AuthenticationService();
        $factory->setAuthenticationService($authenticationService);
        // Configure an authorization service (to resolve the @Right annotations).
        $authorizationService = AuthorizationService::getInstance();
        $authorizationService->setAuthenticationService($authenticationService);
        $factory->setAuthorizationService($authorizationService);

        $schema = $factory->createSchema();

        $input = $request->getParsedBody();
        $this->logger->debug("GraphQL Request: " . json_encode($input));
        $this->logger->debug("Session details: " . json_encode([$request->getCookieParams(), $_SESSION]));

        $query = $input['query'] ?? '';

        $variables = isset($input['variables']) ? $input['variables'] : null;

        $context = [
            'db'      => $this->db,
            'logger'  => $this->logger,
            'auth'    => $this->auth
        ];

        $result = GraphQL::executeQuery($schema, $query, null, $context, $variables);

        # This line is not needed if you use StandardServer
        $response->getBody()->write(json_encode($result));

        $this->logger->debug("GraphQL Response: " . json_encode($result));
        if (!empty($result->errors))
            $this->logger->error("Error: " . json_encode($result->errors));

        $sqlQueryLogger = $this->db->getConfiguration()->getSQLLogger();
        if (!empty($sqlQueryLogger->queries))
            $this->logger->info(json_encode($sqlQueryLogger->queries));

        return $response->withHeader('Content-Type', 'application/json');
    }
}
