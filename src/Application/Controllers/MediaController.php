<?php


namespace App\Application\Controllers;


use App\Application\Services\MediaService;
use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;

class MediaController
{
    protected LoggerInterface $logger;
    protected MediaService $mediaService;

    //$connection and $logger are taken from dependency container declared in app/dependencies.php
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;

        $this->mediaService = new MediaService();
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @return Response
     * @throws Exception
     */
    public function getSizedImage(Request $request, Response $response) {
        $dimensions = explode(":", $request->getAttribute("dimensions"));
        $nameParts = explode(".", $request->getAttribute("name"));

        if (count($dimensions) !== 2) {
            throw new Exception("invalid dimension string");
        }
        
        list($width, $height) = array_map("intval", $dimensions);

        if ($width <= 0) {
            throw new Exception("The width has to be an integer greater than 0.");
        }

        if ($height <= 0) {
            throw new Exception("The height has to be an integer greater than 0.");
        }

        if (count($nameParts) !== 2) {
            throw new Exception("invalid image name");
        }

        list($name, $format) = $nameParts;

        $binary = $this->mediaService->getImageByIdentifier($name, $format, $width, $height);

        $response->getBody()->write($binary);

        return $response->withHeader('Content-Type', MediaService::getMimeType($format));
    }
}