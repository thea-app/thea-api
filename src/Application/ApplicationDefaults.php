<?php


namespace App\Application;


class ApplicationDefaults
{
    public static $shopTable = "App\\Application\\Model\\Entities\\Shop";
    public static $shopTranslationTable = "App\\Application\\Model\\Entities\\ShopTranslation";
    public static $userTable = "App\\Application\\Model\\Entities\\User";
    public static $countryTable = "App\\Application\\Model\\Entities\\Country";
    public static $languageTable = "App\\Application\\Model\\Entities\\Language";
    public static $categoryTable = "App\\Application\\Model\\Entities\\Category";
    public static $productTable = "App\\Application\\Model\\Entities\\Product";
    public static $productTranslationTable = "App\\Application\\Model\\Entities\\ProductTranslation";
    public static $tastingProductTable = "App\\Application\\Model\\Entities\\TastingProduct";
    public static $tastingTable = "App\\Application\\Model\\Entities\\Tasting";
    public static $tastingProductTranslationTable = "App\\Application\\Model\\Entities\\TastingProductTranslation";
    public static $eventTable = "App\\Application\\Model\\Entities\\Event";
    public static $eventTranslationTable = "App\\Application\\Model\\Entities\\EventTranslation";
    public static $userEventTable = "App\\Application\\Model\\Entities\\UserEvent";
    public static $accessCodeTable = "App\\Application\\Model\\Entities\\AccessCode";
    public static $userTastingProductReview = "App\\Application\\Model\\Entities\\UserTastingProductReview";
    public static $bonusTable = "App\\Application\\Model\\Entities\\Bonus";
    public static $bonusCodeTable = "App\\Application\\Model\\Entities\\BonusCode";
    public static $shopBonusUserTable = "App\\Application\\Model\\Entities\\ShopBonusUser";
    public static $vipBenefitTable = "App\\Application\\Model\\Entities\\VipBenefit";
    public static $vipBenefitTranslationTable = "App\\Application\\Model\\Entities\\VipBenefitTranslation";
    public static $shopVipUserTable = "App\\Application\\Model\\Entities\\ShopVipUser";
    public static $flavourTable = "App\\Application\\Model\\Entities\\Flavour";
    public static $flavourWheelTable = "App\\Application\\Model\\Entities\\FlavourWheel";
    public static $notificationTable = "App\\Application\\Model\\Entities\\Notification";
    public static $fcmTokenTable = "App\\Application\\Model\\Entities\\FcmToken";
    public static $userFavouriteShopTable = "App\\Application\\Model\\Entities\\UserFavouriteShop";
    public static $sentNotificationTable = "App\\Application\\Model\\Entities\\SentNotification";

}