<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Enum\LocaleEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\TranslationInput;
use App\Application\Model\GraphQLTypes\Translation;
use App\Application\Model\Timestampable;
use App\Application\Utils;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;
use Exception;


/** GraphQLite annotations:
 * @Type(name="TastingProduct")
 *
 * Shop
 *
 * ORM annotations:
 * @ORM\Table(name="tasting_product", options={"comment":"Tasting products of tasting"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TastingProduct
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product", cascade={"persist"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @var Tasting
     *
     * @ORM\ManyToOne(targetEntity="Tasting", cascade={"persist"})
     * @ORM\JoinColumn(name="tasting_id", referencedColumnName="id")
     */
    private $tasting;

    /**
     * @var TastingProductTranslation[]
     *
     * @ORM\OneToMany(targetEntity="TastingProductTranslation", mappedBy="tastingProduct", indexBy="language_id", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var UserTastingProductReview[]
     *
     * @ORM\OneToMany(targetEntity="UserTastingProductReview", mappedBy="tastingProduct", indexBy="user_id", cascade={"persist", "remove"})
     */
    private $userReviews;

    /**
     * @var int
     *
     * @ORM\Column(name="ordering", type="integer", nullable=false)
     */
    private $ordering;

    /**
     * @param Product $product
     * @param Tasting $tasting
     * @param array $descriptionTranslations
     * @param int $ordering
     * @return TastingProduct
     * @throws Exception
     */
    public static function create(
        Product $product,
        Tasting $tasting,
        array $descriptionTranslations,
        int $ordering
    ) : TastingProduct {
        $instance = new self();

        $instance->product = $product;
        $instance->tasting = $tasting;
        $instance->setDescriptionTranslations($descriptionTranslations);
        $instance->ordering = $ordering;

        return $instance;

    }


    /**
     * Gets translated description of product based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getDescription", $this->translations);
    }

    /**
     * Gets description translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getDescriptionTranslations(): array
    {
        return Utils::getAllTranslations("getDescription", $this->translations);
    }

    /**
     * @param TranslationInput[]|null $descriptionTranslations
     * @throws Exception
     */
    public function setDescriptionTranslations(?array $descriptionTranslations) {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setDescription(null);
            }

        foreach ($descriptionTranslations as $descriptionTranslation) {
            if (LocaleEnum::isValid($descriptionTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($descriptionTranslation->getLanguageCode());
                if (is_null($language))
                    continue;
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = TastingProductTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setDescription($descriptionTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * Gets base product of tasting product
     *
     * @Field()
     *
     * @return Product
     */
    public function getProduct() : Product {
        return $this->product;
    }

    /**
     * Gets tasting where the tasting product is
     *
     * @Field()
     *
     * @return Tasting
     */
    public function getTasting() : Tasting {
        return $this->tasting;
    }

    /**
     * Gets tasting product review of the logged in user.
     * Returns null if the user is not logged in.
     *
     * @Field()
     *
     * @return UserTastingProductReview|null
     */
    public function getMyReview() : ?UserTastingProductReview {
        if (isset($_SESSION['uid']) && (is_array($this->userReviews) || is_object($this->userReviews))) {
            /** @var UserTastingProductReview $userReview */
            foreach ($this->userReviews as $userReview) {
                if ($userReview->getUserEvent()->getUser()->getUid() == $_SESSION['uid']) {
                    return $userReview;
                }
            }
        }
        return null;
    }

    /**
     * Gets all user's reviews of tasting product
     *
     * @Field()
     *
     * @return UserTastingProductReview[]|null
     */
    public function getUsersReviews() : ?iterable {
        return $this->userReviews;
    }

    /**
     * Gets tasting product's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets tasting product's ordering number.
     * The tasting product with the lowest value should be shown first.
     *
     * @Field()
     *
     * @return int
     */
    public function getOrdering(): int
    {
        return $this->ordering;
    }

    /**
     * @param int $ordering
     */
    public function setOrdering(int $ordering): void
    {
        $this->ordering = $ordering;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @param Tasting $tasting
     */
    public function setTasting(Tasting $tasting): void
    {
        $this->tasting = $tasting;
    }
}