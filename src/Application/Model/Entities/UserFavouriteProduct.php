<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/** UserFavouriteProduct
 *
 * @ORM\Table(name="user_favourite_product", options={"comment":"User's favourite products"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class UserFavouriteProduct
{
    use Timestampable;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     */
    private $productId;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="favouriteProducts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="favouriteUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /** UserFavouriteProduct constructor
     *
     * @param Product $product
     * @param User $user
     * @return UserFavouriteProduct
     */
    public static function create(
        Product $product,
        User $user
    ): UserFavouriteProduct
    {
        $instance = new self();

        $instance->productId = $product->getId();
        $instance->userId = $user->getId();
        $instance->product = $product;
        $instance->user = $user;
        $instance->state = EntityStateEnum::ACTIVE;

        return $instance;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }


}