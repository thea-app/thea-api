<?php


namespace App\Application\Model\Entities;


use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\LocaleEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\TranslationInput;
use App\Application\Model\GraphQLTypes\Translation;
use App\Application\Model\Timestampable;
use App\Application\Utils;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** Flavour
 *
 * @Type(name="Flavour")
 *
 * @ORM\Table(name="flavour", options={"comment":"Table of flavours (in flavour wheel)"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Flavour
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var FlavourTranslation[]
     *
     * @ORM\OneToMany(targetEntity="FlavourTranslation", mappedBy="flavour", indexBy="language_id", cascade={"persist", "remove"})
     */
    private $translations;

    /** Subflavours of this Flavour
     *
     * @var Flavour[]|null
     *
     * @ORM\OneToMany(targetEntity="Flavour", mappedBy="parentFlavour", cascade={"persist", "remove"})
     */
    private $subflavours;

    /** Parent Flavour
     *
     * @var Flavour|null
     *
     * @ORM\ManyToOne(targetEntity="Flavour", inversedBy="subflavours")
     * @ORM\JoinColumn(name="parent_flavour_id", referencedColumnName="id")
     */
    private $parentFlavour;

    /**
     * @var FlavourWheel
     *
     * @ORM\ManyToOne(targetEntity="FlavourWheel", inversedBy="flavours", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="flavour_wheel_id", referencedColumnName="id")
     * })
     */
    private $flavourWheel;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true, options={"comment":"Color of the flavour"})
     */
    private $color;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /**
     * @var int
     *
     * @ORM\Column(name="ordering", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $ordering;

    /**
     * @param EntityStateEnum $state
     * @param int $ordering
     * @param string $color
     * @param FlavourWheel $flavourWheel
     * @param Flavour|null $parentFlavour
     * @param TranslationInput[] $nameTranslations
     * @return Flavour
     * @throws Exception
     */
    public static function create(
        EntityStateEnum $state,
        int $ordering,
        string $color,
        FlavourWheel $flavourWheel,
        ?Flavour $parentFlavour,
        array $nameTranslations
    ): Flavour
    {
        $instance = new self();

        $instance->state = $state;
        $instance->ordering = $ordering;
        $instance->color = $color;
        $instance->flavourWheel = $flavourWheel;
        $instance->parentFlavour = $parentFlavour;
        $instance->setNameTranslations($nameTranslations);

        return $instance;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     * @throws Exception
     */
    public function setNameTranslations(array $nameTranslations) {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setName(null);
            }

        foreach ($nameTranslations as $nameTranslation) {
            if (LocaleEnum::isValid($nameTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($nameTranslation->getLanguageCode());
                if (is_null($language))
                    throw new Exception("Given language is not supported");
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = FlavourTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setName($nameTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * Gets flavour's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /** Gets translated name of flavour based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string
     */
    public function getName(): string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getName", $this->translations);
    }

    /**
     * Gets flavour's name translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getNameTranslations(): array
    {
        return Utils::getAllTranslations("getName", $this->translations);
    }

    /**
     * Gets flavour's sub flavours
     *
     * @Field()
     *
     * @return Flavour[]|null
     */
    public function getSubFlavours(): ?array
    {
        if (is_array($this->subflavours) || is_object($this->subflavours)) {
            $result = $this->subflavours->toArray();
            usort($result,
                function(Flavour $c1, Flavour $c2) {
                    return $c1->ordering > $c2->ordering;
                });
            return $result;
        }
        return [];
    }

    /**
     * Gets flavour's parent flavour.
     * Returns null if the flavour is in top level.
     *
     * @Field()
     *
     * @return Flavour|null
     */
    public function getParentFlavour(): ?Flavour
    {
        return $this->parentFlavour;
    }

    /**
     * @param Flavour|null $parentFlavour
     */
    public function setParentFlavour(?Flavour $parentFlavour): void
    {
        $this->parentFlavour = $parentFlavour;
        if (!is_null($parentFlavour))
            $this->setColor($parentFlavour->getColor());
    }

    /**
     * Gets flavour's color in hex format
     *
     * @Field()
     *
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;

        /** @var Flavour $subFlavour */
        foreach ($this->getSubFlavours() as $subFlavour) {
            $subFlavour->setColor($color);
        }
    }

    /**
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * Gets flavour's ordering number.
     * The flavour with the lowest value should be shown first.
     *
     * @Field()
     *
     * @return int
     */
    public function getOrdering(): int
    {
        return $this->ordering;
    }

    /**
     * @param int $ordering
     */
    public function setOrdering(int $ordering): void
    {
        $this->ordering = $ordering;
    }

    /**
     * @return FlavourWheel
     */
    public function getFlavourWheel(): FlavourWheel
    {
        return $this->flavourWheel;
    }

    /**
     * @param FlavourWheel $flavourWheel
     */
    public function setFlavourWheel(FlavourWheel $flavourWheel): void
    {
        $this->flavourWheel = $flavourWheel;
    }
}
