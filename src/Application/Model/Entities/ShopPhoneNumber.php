<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * ShopPhoneNumber
 *
 * ORM annotations:
 * @ORM\Table(name="shop_phone_number", options={"comment":"Shop's phone numbers"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ShopPhoneNumber
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=15, nullable=false)
     */
    private $phoneNumber;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="phoneNumber", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /**
     * Gets shop's phone number value
     *
     * @Field()
     *
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param Shop $shop
     * @param string $phoneNumber
     *
     * @return ShopPhoneNumber
     * @throws Exception
     */
    public static function create(Shop $shop, string $phoneNumber) : ShopPhoneNumber {
        $instance = new self();

        $instance->shop = $shop;
        $instance->phoneNumber = $phoneNumber;

        return $instance;
    }

}
