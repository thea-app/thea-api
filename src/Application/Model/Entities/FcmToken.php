<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type(name="FcmToken")
 *
 * Shop
 *
 * ORM annotations:
 * @ORM\Table(name="fcm_token", options={"comment":"Firebase cloud messaging token table"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class FcmToken
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fcm_token", type="text", length=65535, nullable=false, options={"comment":"Firebase cloud messaging unique token that represents user's device"})
     */
    private $fcmToken;

    /**
     * @var int
     * @ORM\Column(name="user_id", type="integer", nullable=false, options={"comment":"Reference to user table"})
     */
    private $userId;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="fcmTokens", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /** Sent notifications
     *
     * @var SentNotification[]|null
     *
     * @ORM\OneToMany(targetEntity="SentNotification", mappedBy="fcmToken", cascade={"persist", "remove"})
     */
    private $sentNotifications;

    /**
     * @var bool
     *
     * @ORM\Column(name="notifications_enabled", type="boolean", nullable=false, options={"comment":"Has the user enabled notifications for the device?"})
     */
    private $notificationsEnabled;


    /**
     * FcmToken constructor.
     * @param User $user
     * @param string $fcmToken
     * @param bool $notificationsEnabled
     * @return FcmToken
     */
    public static function create(
        User $user,
        string $fcmToken,
        bool $notificationsEnabled
    ) : FcmToken {
        $instance = new self();

        $instance->user = $user;
        $instance->fcmToken = $fcmToken;
        $instance->userId = $user->getId();
        $instance->notificationsEnabled = $notificationsEnabled;

        return $instance;
    }

    /**
     * Gets fcm token's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets fcm token's value
     *
     * @Field()
     *
     * @return string
     */
    public function getFcmToken(): string
    {
        return $this->fcmToken;
    }

    /**
     * @param string $fcmToken
     */
    public function setFcmToken(string $fcmToken): void
    {
        $this->fcmToken = $fcmToken;
    }

    /**
     * Gets user to whom the fcm token belongs
     *
     * @Field()
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * Gets sent notifications to the device
     *
     * @Field()
     *
     * @return SentNotification[]|null
     */
    public function getSentNotifications(): ?iterable
    {
        return $this->sentNotifications;
    }

    /**
     * @return bool
     */
    public function getNotificationsEnabled(): bool
    {
        return $this->notificationsEnabled;
    }

    /**
     * @param bool $notificationsEnabled
     */
    public function setNotificationsEnabled(bool $notificationsEnabled): void
    {
        $this->notificationsEnabled = $notificationsEnabled;
    }
}