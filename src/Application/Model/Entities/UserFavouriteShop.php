<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserFavouriteShop
 *
 * @ORM\Table(name="user_favourite_shop", options={"comment":"User's favourite shops"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class UserFavouriteShop
{
    use Timestampable;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="shop_id", type="integer", nullable=false)
     */
    private $shopId;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="favouriteShops", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="favouriteUsers", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /** UserFavouriteShop constructor
     *
     * @param Shop $shop
     * @param User $user
     *
     * @return UserFavouriteShop
     */
    public static function create(
        Shop $shop,
        User $user
    ) {
        $instance = new self();

        $instance->shopId = $shop->getId();
        $instance->userId = $user->getId();
        $instance->shop = $shop;
        $instance->user = $user;
        $instance->state = EntityStateEnum::ACTIVE;

        return $instance;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop): void
    {
        $this->shop = $shop;
    }
}
