<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/** 
 * OpeningHoursExceptionInterval
 *
 * @ORM\Table(name="opening_hours_exception_interval", options={"comment":"Interval of opening hour exception"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class OpeningHoursExceptionInterval
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="from_hour", type="integer", nullable=false, options={"comment":"Hour in day, when exception starts"})
     */
    private $fromHour;

    /**
     * @var int
     *
     * @ORM\Column(name="until_hour", type="integer", nullable=false, options={"comment":"Hour in day, when exception ends"})
     */
    private $untilHour;

    /**
     * @var int
     *
     * @ORM\Column(name="from_minute", type="integer", nullable=false, options={"comment":"Minute in hour, when exception starts"})
     */
    private $fromMinute;

    /**
     * @var int
     *
     * @ORM\Column(name="until_minute", type="integer", nullable=false, options={"comment":"Minute in hour, when exception ends"})
     */
    private $untilMinute;

    /**
     * @var OpeningHoursException
     *
     * @ORM\ManyToOne(targetEntity="OpeningHoursException", inversedBy="openingHoursExceptionsIntervals", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="opening_hours_exception_id", referencedColumnName="id")
     * })
     */
    private $openingHoursException;

    /**
     * @param int $fromHour
     * @param int $fromMinute
     * @param int $untilHour
     * @param int $untilMinute
     * @param OpeningHoursException $exception
     * @return OpeningHoursExceptionInterval
     * @throws Exception
     */
    public static function create(
        int $fromHour,
        int $fromMinute,
        int $untilHour,
        int $untilMinute,
        OpeningHoursException $exception
    ) : OpeningHoursExceptionInterval {
        $instance = new self();

        $instance->fromHour = $fromHour;
        $instance->fromMinute = $fromMinute;
        $instance->untilHour = $untilHour;
        $instance->untilMinute = $untilMinute;
        $instance->openingHoursException = $exception;

        return $instance;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getFromHour(): int
    {
        return $this->fromHour;
    }

    /**
     * @param int $fromHour
     */
    public function setFromHour(int $fromHour): void
    {
        $this->fromHour = $fromHour;
    }

    /**
     * @return int
     */
    public function getUntilHour(): int
    {
        return $this->untilHour;
    }

    /**
     * @param int $untilHour
     */
    public function setUntilHour(int $untilHour): void
    {
        $this->untilHour = $untilHour;
    }

    /**
     * @return int
     */
    public function getFromMinute(): int
    {
        return $this->fromMinute;
    }

    /**
     * @param int $fromMinute
     */
    public function setFromMinute(int $fromMinute): void
    {
        $this->fromMinute = $fromMinute;
    }

    /**
     * @return int
     */
    public function getUntilMinute(): int
    {
        return $this->untilMinute;
    }

    /**
     * @param int $untilMinute
     */
    public function setUntilMinute(int $untilMinute): void
    {
        $this->untilMinute = $untilMinute;
    }
}
