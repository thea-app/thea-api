<?php

namespace App\Application\Model\Entities;

use App\Application\ApplicationDefaults;
use App\Application\Model\Enum\LocaleEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Slim\Factory\AppFactory;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** Language
 *
 * @Type(name="Language")
 *
 * @ORM\Table(name="language", options={"comment":"Language table"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Language
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var LocaleEnum
     *
     * @ORM\Column(name="locale_code", type="locale_enum", nullable=false, options={"comment":"Locale code in ISO 639-1 format"})
     */
    private $localeCode;

    private function __construct() {
        //Private constructor
    }

    /**
     * Gets ISO 639-1 code of the language
     *
     * @Field()
     *
     * @return LocaleEnum
     */
    public function getLocaleCode(): LocaleEnum
    {
        return $this->localeCode;
    }

    /**
     * @return int
     */
    public function getLanguageId(): int
    {
        return $this->id;
    }

    public static function getLanguageByLocale(?string $localeCode) : ?Language
    {
        $app = AppFactory::create();
        $container = $app->getContainer();
        $entityManager = $container->get(EntityManagerInterface::class);
        $repo = $entityManager->getRepository(ApplicationDefaults::$languageTable);
        /** @var Language $result */
        $result = $repo->findOneBy(array("localeCode" => $localeCode));
        return $result;
    }
}
