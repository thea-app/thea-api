<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Timestampable;
use App\Application\Utils;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** BonusCode table - used for generated codes for redeeming coupons
 * @Type(name="BonusCode")
 *
 * @ORM\Table(name="bonus_code", options={"comment":"Unique one-time codes for bonuses."})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class BonusCode
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Bonus
     *
     * @ORM\ManyToOne(targetEntity="Bonus", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bonus_id", referencedColumnName="id")
     * })
     */
    private $bonus;

    /**
     * @var ShopBonusUser
     *
     * @ORM\ManyToOne(targetEntity="ShopBonusUser", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_bonus_user_id", referencedColumnName="id")
     * })
     */
    private $shopBonusUser;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="integer", nullable=false, unique=true)
     */
    private $code;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /**
     * @param Bonus $bonus
     * @param ShopBonusUser $shopBonusUser
     *
     * @return BonusCode
     * @throws Exception
     */
    public static function create(
        Bonus $bonus,
        ShopBonusUser $shopBonusUser
    ): BonusCode
    {
        $instance = new self();

        $instance->bonus = $bonus;
        $instance->shopBonusUser = $shopBonusUser;
        $instance->code = Utils::generateEntityUniqueCode($instance);
        $instance->state = new EntityStateEnum(EntityStateEnum::ACTIVE);

        return $instance;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Gets the bonus of the bonus code
     *
     * @Field()
     *
     * @return Bonus
     */
    public function getBonus(): Bonus
    {
        return $this->bonus;
    }

    /**
     * @param Bonus $bonus
     */
    public function setBonus(Bonus $bonus): void
    {
        $this->bonus = $bonus;
    }

    /**
     * Gets bonus user to whom bonus code belongs
     *
     * @Field()
     *
     * @return ShopBonusUser
     */
    public function getShopBonusUser(): ShopBonusUser
    {
        return $this->shopBonusUser;
    }

    /**
     * @param ShopBonusUser $shopBonusUser
     */
    public function setShopBonusUser(ShopBonusUser $shopBonusUser): void
    {
        $this->shopBonusUser = $shopBonusUser;
    }

    /**
     * Gets bonus code
     *
     * @Field()
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

}