<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * BonusTranslation
 *
 * @ORM\Table(name="bonus_translation", options={"comment":"Bonus entity translations"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class BonusTranslation
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /** Many translations to one language
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @var int
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false, options={"comment":"Reference to language table"})
     */
    private $languageId;

    /** Many translations to one bonus
     * @var Bonus
     *
     * @ORM\ManyToOne(targetEntity="Bonus", inversedBy="translations")
     * @ORM\JoinColumn(name="bonus_id", referencedColumnName="id")
     */
    private $bonus;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true)
     */
    private $name;

    /**
     * @param Language $language
     * @param Bonus $bonus
     *
     * @return BonusTranslation
     */
    public static function create(
        Language $language,
        Bonus $bonus
    )
    {
        $instance = new self();

        $instance->language = $language;
        $instance->languageId = $language->getLanguageId();
        $instance->bonus = $bonus;

        return $instance;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void {
        $this->name = $name;
    }

    /**
     * @return Language
     */
    public function getLanguage(): Language
    {
        return $this->language;
    }

}
