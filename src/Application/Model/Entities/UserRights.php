<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Enum\GlobalRightsEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserRights
 *
 * @ORM\Table(name="user_rights", options={"comment":"User's rights to access fields in GraphQL"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class UserRights
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="rights", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var int
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var GlobalRightsEnum
     *
     * @ORM\Column(name="rights", type="global_rights_enum", nullable=false)
     */
    private $rights;

    /** UserRights constructor
     *
     * @param User $user
     * @param GlobalRightsEnum $rights
     * @return UserRights
     */
    public static function create(
        User $user,
        GlobalRightsEnum $rights
    ): UserRights
    {
        $instance = new self();

        $instance->userId = $user->getId();
        $instance->user = $user;
        $instance->rights = $rights;

        return $instance;
    }

    /**
     * @return GlobalRightsEnum
     */
    public function getRights(): GlobalRightsEnum
    {
        return $this->rights;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

}