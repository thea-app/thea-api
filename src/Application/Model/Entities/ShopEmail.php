<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * ShopEmail
 *
 * ORM annotations:
 * @ORM\Table(name="shop_email", options={"comment":"Shop's emails"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ShopEmail
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="text", length=65535, nullable=false)
     */
    private $email;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="emails", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /**
     * Gets shop's email value
     *
     * @Field()
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param Shop $shop
     * @param string $email
     *
     * @return ShopEmail
     * @throws Exception
     */
    public static function create(Shop $shop, string $email) : ShopEmail {
        $instance = new self();

        $instance->shop = $shop;
        $instance->email = $email;

        return $instance;
    }


}
