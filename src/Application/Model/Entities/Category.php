<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\LocaleEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\TranslationInput;
use App\Application\Model\GraphQLTypes\Translation;
use App\Application\Model\Timestampable;
use App\Application\Utils;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** Category
 * @Type(name="Category")
 *
 * @ORM\Table(name="category", options={"comment":"Product categories in menu"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Category
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="text", length=65535, nullable=true)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="ordering", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $ordering;

    /** Subcategories of this Category
     *
     * @var Category[]|null
     *
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parentCategory", cascade={"persist", "remove"})
     */
    private $subcategories;

    /** Parent Category
     *
     * @var Category|null
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="subcategories")
     * @ORM\JoinColumn(name="parent_category_id", referencedColumnName="id")
     */
    private $parentCategory;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="categories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /**
     * @var CategoryTranslation[]
     *
     * @ORM\OneToMany(targetEntity="CategoryTranslation", mappedBy="category", indexBy="language_id", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var Product[]
     *
     * @ORM\OneToMany(targetEntity="Product", mappedBy="category", cascade={"persist"})
     */
    private $products;

    /**
     * @param EntityStateEnum $state
     * @param string|null $type
     * @param Shop $shop
     * @param Category|null $parentCategory
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $descriptionTranslations
     *
     * @return Category
     * @throws Exception
     */
    public static function create(
        EntityStateEnum $state,
        ?string $type,
        Shop $shop,
        ?Category $parentCategory,
        array $nameTranslations,
        ?array $descriptionTranslations
    ): Category
    {
        $instance = new self();

        $instance->state = $state;
        $instance->type = $type;
        $instance->ordering = 0;
        $instance->shop = $shop;
        $instance->parentCategory = $parentCategory;
        $instance->setNameTranslations($nameTranslations);
        $instance->setDescriptionTranslations($descriptionTranslations);

        return $instance;
    }


    /**
     * Gets category id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Gets translated name of category based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string
     */
    public function getName(): string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getName", $this->translations);
    }

    /**
     * Gets category name translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getNameTranslations(): array
    {
        return Utils::getAllTranslations("getName", $this->translations);
    }

    /**
     * Gets translated description of category based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getDescription", $this->translations);
    }

    /**
     * Gets category description translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getDescriptionTranslations(): array
    {
        return Utils::getAllTranslations("getDescription", $this->translations);
    }

    /**
     * @param TranslationInput[] $nameTranslations
     * @throws Exception
     */
    public function setNameTranslations(array $nameTranslations) {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setName(null);
            }

        if (!isset($nameTranslations))
            return;

        foreach ($nameTranslations as $nameTranslation) {
            if (LocaleEnum::isValid($nameTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($nameTranslation->getLanguageCode());
                if (is_null($language))
                    throw new Exception("Given language is not supported");
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = CategoryTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setName($nameTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * @param TranslationInput[]|null $descriptionTranslations
     * @throws Exception
     */
    public function setDescriptionTranslations(?array $descriptionTranslations) {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setDescription(null);
            }

        if (!isset($descriptionTranslations))
            return;

        foreach ($descriptionTranslations as $descriptionTranslation) {
            if (LocaleEnum::isValid($descriptionTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($descriptionTranslation->getLanguageCode());
                if (is_null($language))
                    continue;
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = CategoryTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setDescription($descriptionTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * Gets category's subcategories
     *
     * @Field()
     *
     * @return Category[]|null
     */
    public function getSubcategories(): ?array
    {
        if (is_array($this->subcategories) || is_object($this->subcategories)) {
            $result = $this->subcategories->toArray();
            usort($result,
                function(Category $c1, Category $c2) {
                    return $c1->ordering > $c2->ordering;
                });
            return $result;
        }
        return [];
    }

    /**
     * Gets category's parent category.
     * Returns null if the category is in top level.
     *
     * @Field()
     *
     * @return Category|null
     */
    public function getParentCategory(): ?Category
    {
        return $this->parentCategory;
    }

    /**
     * Gets category's ordering number.
     * The category with the lowest value should be shown first.
     *
     * @Field()
     *
     * @return int
     */
    public function getOrdering(): int
    {
        return $this->ordering;
    }

    /**
     * @param Category $subcategory
     */
    public function addSubcategory(Category $subcategory): void
    {
        $this->subcategories[] = $subcategory;
    }

    /**
     * Gets products belonging to category
     *
     * @Field()
     *
     * @return Product[]
     */
    public function getProducts(): array
    {
        if (is_array($this->products) || is_object($this->products))
            return $this->products->toArray();
        return [];
    }

    /**
     * @param Category|null $parentCategory
     */
    public function setParentCategory(?Category $parentCategory): void
    {
        $this->parentCategory = $parentCategory;
    }

    /**
     * @param int|null $ordering
     */
    public function setOrdering(?int $ordering): void
    {
        $this->ordering = $ordering;
    }

    /**
     * Gets shop to whom the category belongs
     *
     * @Field()
     *
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

}