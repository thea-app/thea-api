<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\SocialsTypeEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Field;

/** GraphQLite annotations:
 * @Type(name="SocialLink")
 * SocialLink
 *
 * @ORM\Table(name="social_link", options={"comment":"Social links of shop"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SocialLink
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="user_string", type="string", length=255, nullable=true, options={"comment":"User defined string that is appended to the base link defined by the type column"})
     */
    private $userString;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true, options={"comment":"Entire URL link when the type is OTHER"})
     */
    private $url;

    /**
     * @var SocialsTypeEnum
     *
     * @ORM\Column(name="type", type="socials_type_enum", length=255, nullable=false, options={"comment":"Type of the social network"})
     */
    private $type;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="socialLinks", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    public static function create (
        ?string $userString,
        ?string $url,
        SocialsTypeEnum $type
    ) : SocialLink {
        $instance = new self();

        $instance->userString = $userString;
        $instance->url = $url;
        $instance->type = $type;

        return $instance;
    }

    /**
     * Gets user defined string that is appended to the base link defined by the type
     *
     * @Field()
     *
     * @return string|null
     */
    public function getUserString(): ?string
    {
        return $this->userString;
    }

    /**
     * Gets entire social link's URL
     *
     * @Field()
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->type == SocialsTypeEnum::OTHER
            ? $this->url
            : SocialsTypeEnum::getLinkUrl($this->type) . $this->userString;
    }

    /**
     * Gets type of the social network
     *
     * @Field()
     *
     * @return SocialsTypeEnum
     */
    public function getType(): SocialsTypeEnum
    {
        return $this->type;
    }

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop): void
    {
        $this->shop = $shop;
    }

}
