<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\CurrencyEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\LocaleEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\TranslationInput;
use App\Application\Model\GraphQLTypes\Translation;
use App\Application\Model\Timestampable;
use App\Application\Utils;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;
use Exception;

/** Product
 * @Type(name="Product")
 *
 * @ORM\Table(name="product", options={"comment":"Shop products table"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Product
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var CurrencyEnum
     *
     * @ORM\Column(name="currency", type="currency_enum", nullable=false)
     */
    private $currency;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image_identifier", type="text", length=65535, nullable=true)
     */
    private $imageIdentifier;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /**
     * @var int
     *
     * @ORM\Column(name="ordering", type="integer", nullable=false)
     */
    private $ordering;

    /**
     * @var Category|null
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var ProductTranslation[]
     *
     * @ORM\OneToMany(targetEntity="ProductTranslation", mappedBy="product", indexBy="language_id", cascade={"persist", "remove"})
     */
    private $translations;

    /** Users that have this product as favourite
     *
     * @var UserFavouriteProduct[]
     *
     * @ORM\OneToMany(targetEntity="UserFavouriteProduct", mappedBy="product", indexBy="user_id", cascade={"persist", "remove"})
     *
     */
    private $favouriteUsers;

    /**
     * @param EntityStateEnum $state
     * @param Shop $shop
     * @param Category $category
     * @param float $price
     * @param CurrencyEnum $currency
     * @param string|null $imageIdentifier
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $descriptionTranslations
     *
     * @return Product
     * @throws Exception
     */
    public static function create(
        EntityStateEnum $state,
        Shop $shop,
        Category $category,
        float $price,
        CurrencyEnum $currency,
        ?string $imageIdentifier,
        array $nameTranslations,
        ?array $descriptionTranslations
    ): Product
    {
        $instance = new self();

        $instance->state = $state;
        $instance->ordering = 0;
        $instance->shop = $shop;
        $instance->category = $category;
        $instance->price = $price;
        $instance->currency = $currency;
        $instance->imageIdentifier = $imageIdentifier;
        $instance->setNameTranslations($nameTranslations);
        $instance->setDescriptionTranslations($descriptionTranslations);


        return $instance;
    }

    /**
     * Gets product's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets shop to whom the product belongs
     *
     * @Field()
     *
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop): void
    {
        $this->shop = $shop;
    }

    /**
     * Gets category to whom the product belongs.
     * Returns null if the product has no category.
     *
     * @Field()
     *
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category|null $category
     */
    public function setCategory(?Category $category): void
    {
        $this->category = $category;
    }

    /**
     * Gets product's price
     *
     * @Field()
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * Gets product's currency
     *
     * @Field()
     *
     * @return CurrencyEnum
     */
    public function getCurrency(): CurrencyEnum
    {
        return $this->currency;
    }

    /**
     * @param CurrencyEnum $currency
     */
    public function setCurrency(CurrencyEnum $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * Gets product's image URL
     *
     * @Field()
     *
     * @param string $width
     * @param string $height
     * @param string $format
     * @return string|null
     */
    public function getImageURL(string $width = '500', string $height = '500', string $format = 'webp'): ?string
    {
        if (is_null($this->imageIdentifier))
            return "";
        return Utils::buildMediaDownloadURL($this->imageIdentifier, $width, $height, $format);
    }

    /**
     * @return string|null
     */
    public function getImageIdentifier(): ?string
    {
        return $this->imageIdentifier;
    }

    /**
     * Gets translated name of product based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string
     */
    public function getName(): string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getName", $this->translations);
    }

    /**
     * Gets name translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getNameTranslations(): array
    {
        return Utils::getAllTranslations("getName", $this->translations);
    }

    /**
     * Gets translated description of product based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getDescription", $this->translations);
    }

    /**
     * Gets description translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getDescriptionTranslations(): array
    {
        return Utils::getAllTranslations("getDescription", $this->translations);
    }

    /**
     * @param TranslationInput[] $nameTranslations
     * @throws Exception
     */
    public function setNameTranslations(array $nameTranslations) {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setName(null);
            }

        if (!isset($nameTranslations))
            return;

        foreach ($nameTranslations as $nameTranslation) {
            if (LocaleEnum::isValid($nameTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($nameTranslation->getLanguageCode());
                if (is_null($language))
                    throw new Exception("Given language is not supported");
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = ProductTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setName($nameTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * @param TranslationInput[]|null $descriptionTranslations
     * @throws Exception
     */
    public function setDescriptionTranslations(?array $descriptionTranslations) {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setDescription(null);
            }

        if (!isset($descriptionTranslations))
            return;

        foreach ($descriptionTranslations as $descriptionTranslation) {
            if (LocaleEnum::isValid($descriptionTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($descriptionTranslation->getLanguageCode());
                if (is_null($language))
                    continue;
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = ProductTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setDescription($descriptionTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * Is the product favourite for logged in user?
     * Returns null if the user is not logged in.
     *
     * @Field()
     *
     * @return bool|null
     */
    public function isFavourite(): ?bool
    {
        if (isset($_SESSION['uid']) && (is_array($this->favouriteUsers) || is_object($this->favouriteUsers))) {
            foreach ($this->favouriteUsers as $userBinding) {
                if ($userBinding->getUser()->getUid() == $_SESSION['uid'])
                    return true;
            }
            return false;
        }
        return null;
    }

    /**
     * Gets count of users, that have this product marked as favourite
     *
     * @Field()
     *
     * @return int
     */
    public function getFavouriteUsersCount(): int
    {
        if (is_array($this->favouriteUsers) || is_object($this->favouriteUsers)) {
            return count($this->favouriteUsers);
        }
        return 0;
    }

    public function addFavouriteUser(User $user)
    {
        if (!isset($this->favouriteUsers[$user->getId()])) {
            $binding = UserFavouriteProduct::create($this, $user);
            $this->favouriteUsers[$user->getId()] = $binding;
            $user->addFavouriteProduct($this, $binding);
        }
    }

    /**
     * @param User $user
     * @param EntityManager $em
     * @throws Exception
     */
    public function removeFavouriteUser(User $user, EntityManager $em)
    {
        if (isset($this->favouriteUsers[$user->getId()])) {
            $user->removeFavouriteProduct($this);
            $em->remove($this->favouriteUsers[$user->getId()]);
            unset($this->favouriteUsers[$user->getId()]);
        }
    }

    /**
     * @param int|null $ordering
     */
    public function setOrdering(?int $ordering): void
    {
        $this->ordering = $ordering;
    }

    /**
     * @param string|null $imageIdentifier
     */
    public function setImageIdentifier(?string $imageIdentifier): void
    {
        $this->imageIdentifier = $imageIdentifier;
    }

    /**
     * Gets product's ordering number.
     * The product with the lowest value should be shown first.
     *
     * @Field()
     *
     * @return int
     */
    public function getOrdering(): int
    {
        return $this->ordering;
    }
}