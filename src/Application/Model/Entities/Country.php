<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\CountryEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 *
 * ORM annotations:
 * @ORM\Table(name="country", options={"comment":"Table represents list of allowed countries over application"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Country
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var CountryEnum
     *
     * @ORM\Column(name="country_code", type="country_enum", nullable=false,  options={"comment":"Contry code in ISO 3166-1 alpha-2 format"})
     */
    private $countryCode;

    /**
     * @return CountryEnum
     */
    public function getCountryCode(): CountryEnum
    {
        return $this->countryCode;
    }

}
