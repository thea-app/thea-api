<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * ShopTranslation
 *
 * @ORM\Table(name="shop_translation", options={"comment":"Shop translations"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ShopTranslation
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /** Many translations to one language
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @var int
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     */
    private $languageId;

    /** Many translations to one shop
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="translations")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    private $shop;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bonus_program_description", type="text", length=65535, nullable=true)
     */
    private $bonusProgramDescription;

    /**
     * @param Language $language
     * @param Shop $shop
     *
     * @return ShopTranslation
     */
    public static function create(
        Language $language,
        Shop $shop
    )
    {
        $instance = new self();

        $instance->language = $language;
        $instance->languageId = $language->getLanguageId();
        $instance->shop = $shop;

        return $instance;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Language
     */
    public function getLanguage(): Language
    {
        return $this->language;
    }

    /**
     * @return string|null
     */
    public function getBonusProgramDescription(): ?string
    {
        return $this->bonusProgramDescription;
    }

    /**
     * @param string|null $bonusProgramDescription
     */
    public function setBonusProgramDescription(?string $bonusProgramDescription): void
    {
        $this->bonusProgramDescription = $bonusProgramDescription;
    }

}
