<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\Timestampable;
use App\Application\Services\SecurityService\AuthorizationService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Security;
use Exception;

/** GraphQLite annotations:
 * @Type(name="UserEvent", options={"comment":"Information about user as an attendee in the event"})
 *
 *
 * Shop
 *
 * ORM annotations:
 * @ORM\Table(name="user_event", uniqueConstraints={@UniqueConstraint(name="unambiguity_idx", columns={"user_id", "event_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class UserEvent
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     */
    private $eventId;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="events", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var Event
     *
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="attendees", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     * })
     */
    private $event;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /**
     * @var string|null
     *
     * @ORM\Column(name="public_note", type="text", length=65535, nullable=true, options={"comment":"Public note visible to author and admin of the shop of the event"})
     */
    private $publicNote;

    /**
     * @var string|null
     *
     * @ORM\Column(name="private_note", type="text", length=65535, nullable=true, options={"comment":"Private note visible only to author"})
     */
    private $privateNote;

    /**
     * @var float|null
     *
     * @ORM\Column(name="rating", type="decimal", precision=4, scale=2, nullable=true, options={"comment":"Rating of event in 1-5 range"})
     */
    private $rating;

    /**
     * @var AccessCode|null
     *
     * @ORM\OneToOne(targetEntity="AccessCode")
     * @ORM\JoinColumn(name="access_code_id", referencedColumnName="id")
     */
    private $accessCode;

    /**
     * @var bool
     *
     * @ORM\Column(name="finished", type="boolean", nullable=false, options={"comment":"Has user finished the event already?"})
     */
    private $finished;

    /**
     * UserEvent constructor.
     * @param User $user
     * @param Event $event
     * @param EntityStateEnum $state
     * @param AccessCode|null $accessCode
     * @return UserEvent
     * @throws Exception
     */
    public static function create(
        User $user,
        Event $event,
        EntityStateEnum $state,
        ?AccessCode $accessCode = null
    ) : UserEvent {
        $instance = new self();

        $instance->user = $user;
        $instance->event = $event;
        $instance->userId = $user->getId();
        $instance->eventId = $event->getId();
        $instance->state = $state;
        $instance->accessCode = $accessCode;
        $instance->finished = false;

        return $instance;
    }

    /**
     * Gets user in event
     *
     * @Field()
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * Gets event
     *
     * @Field()
     *
     * @return Event
     */
    public function getEvent(): Event
    {
        return $this->event;
    }

    /**
     * @param Event $event
     */
    public function setEvent(Event $event): void
    {
        $this->event = $event;
    }

    /**
     * @Field()
     *
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * Gets public note visible to author and admin of the shop of the event
     *
     * @Field()
     *
     * @return string|null
     */
    public function getPublicNote(): ?string
    {
        return $this->publicNote;
    }

    /**
     * @param string|null $publicNote
     */
    public function setPublicNote(?string $publicNote): void
    {
        $this->publicNote = $publicNote;
    }

    /**
     * Gets private note visible only to author
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @return string|null
     */
    public function getPrivateNote(): ?string
    {
        return $this->privateNote;
    }

    /**
     * @param string|null $privateNote
     */
    public function setPrivateNote(?string $privateNote): void
    {
        $this->privateNote = $privateNote;
    }

    /**
     * Gets rating of event in 1-5 range
     *
     * @Field()
     *
     * @return float|null
     */
    public function getRating(): ?float
    {
        return $this->rating;
    }

    /**
     * @param float|null $rating
     */
    public function setRating(?float $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * Gets access code that has been used by the user to access the event.
     * Returns null if the user was added to the event manually by the shop admin.
     *
     * @Field()
     *
     * @return AccessCode|null
     */
    public function getAccessCode(): ?AccessCode
    {
        return $this->accessCode;
    }

    /**
     * @param AccessCode|null $accessCode
     */
    public function setAccessCode(?AccessCode $accessCode): void
    {
        $this->accessCode = $accessCode;
    }

    /**
     * Has user finished the event already?
     *
     * @Field()
     *
     * @return bool
     */
    public function isFinished(): bool
    {
        return $this->finished;
    }

    /**
     * @param bool $finished
     */
    public function setFinished(bool $finished): void
    {
        $this->finished = $finished;
    }

    /**
     * @return bool
     */
    public function canGetPrivateUserInfo(): bool
    {
        $authorizationService = AuthorizationService::getInstance();
        return $authorizationService->isAllowed(new ScopeRightsEnum(ScopeRightsEnum::CAN_GET_PRIVATE_USER_INFO), $this->user);
    }
}