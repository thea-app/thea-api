<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\TranslationInput;
use App\Application\Model\Timestampable;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * Tasting
 *
 * @Type(name="Tasting")
 *
 * @ORM\Table(name="tasting", options={"comment":"Shop's tasting event"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Tasting extends Event
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @var TastingProduct[]|null
     *
     * @ORM\OneToMany(targetEntity="TastingProduct", mappedBy="tasting", indexBy="tasting_product_id", cascade={"persist", "remove"})
     */
    private $tastingProducts;

    /**
     * @var FlavourWheel|null
     *
     * @ORM\ManyToOne(targetEntity="FlavourWheel", inversedBy="tastings", cascade={"persist"})
     * @ORM\JoinColumn(name="flavour_wheel_id", referencedColumnName="id")
     */
    private $flavourWheel;

    /**
     * Tasting constructor
     * @param Shop $shop
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $publicDescriptionTranslations
     * @param TranslationInput[]|null $privateDescriptionTranslations
     * @param DateTimeImmutable $dateFrom
     * @param DateTimeImmutable $dateUntil
     * @param EntityStateEnum $state
     * @param string $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string $postcode
     * @param string $city
     * @param Country $country
     * @param float $longitude
     * @param float $latitude
     * @param bool $vipOnly
     * @return Tasting
     * @throws Exception
     */
    public static function create(
        Shop $shop,
        array $nameTranslations,
        ?array $publicDescriptionTranslations,
        ?array $privateDescriptionTranslations,
        DateTimeImmutable $dateFrom,
        DateTimeImmutable $dateUntil,
        EntityStateEnum $state,
        string $street,
        ?string $descriptiveNumber,
        ?string $routingNumber,
        string $postcode,
        string $city,
        Country $country,
        float $longitude,
        float $latitude,
        bool $vipOnly
    ) : Tasting {
        return new self(
            $shop,
            $nameTranslations,
            $publicDescriptionTranslations,
            $privateDescriptionTranslations,
            $dateFrom,
            $dateUntil,
            $state,
            $street,
            $descriptiveNumber,
            $routingNumber,
            $postcode,
            $city,
            $country,
            $longitude,
            $latitude,
            $vipOnly
        );
    }

    /**
     * Gets tasting products
     *
     * @Field()
     *
     * @return TastingProduct[]|null
     */
    public function getTastingProducts(): ?iterable
    {
        /** @var TastingProduct[] $result */
        $result = [];
        if (is_array($this->tastingProducts) || is_object($this->tastingProducts)) {
            foreach ($this->tastingProducts as $tastingProduct) {
                $result[] = $tastingProduct;
            }
            usort($result,
                function(TastingProduct $c1, TastingProduct $c2) {
                    return $c1->getOrdering() > $c2->getOrdering();
                });
        }
        return $result;
    }

    /**
     * Gets tasting's flavour wheel
     *
     * @Field()
     *
     * @return FlavourWheel|null
     */
    public function getFlavourWheel(): ?FlavourWheel
    {
        return $this->flavourWheel;
    }

    /**
     * @param FlavourWheel $flavourWheel
     */
    public function setFlavourWheel(FlavourWheel $flavourWheel): void
    {
        $this->flavourWheel = $flavourWheel;
    }
}
