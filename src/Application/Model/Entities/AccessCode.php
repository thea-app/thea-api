<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** AccessCode
 *
 * @Type(name="AccessCode")
 *
 * @ORM\Table(name="access_code", options={"comment":"Access codes for access event."})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class AccessCode
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Event
     *
     * @ORM\ManyToOne(targetEntity="Event", cascade={"persist"})
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     */
    private $event;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $user;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(name="expiry_date", type="datetime", nullable=false,
     *     options={"comment":"The date and time, until it is possible to use the access code."
     * })
     */
    private $expiryDate;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false, options={"comment":"Access code"})
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="note", type="text", length=65535, nullable=true)
     */
    private $note;

    /**
     * Event constructor
     * @param Event $event
     * @param DateTimeImmutable $expiryDate
     * @return AccessCode
     * @throws Exception
     */
    public static function create(
        Event $event,
        DateTimeImmutable $expiryDate
    ): AccessCode
    {
        $instance = new self();

        $instance->event = $event;
        $instance->expiryDate = $expiryDate;

        return $instance;
    }

    /**
     * Gets access code's event
     *
     * @Field()
     *
     * @return Event
     */
    public function getEvent(): Event
    {
        return $this->event;
    }

    /**
     * @param Event $event
     */
    public function setEvent(Event $event): void
    {
        $this->event = $event;
    }

    /**
     * Gets date and time when access code expires
     *
     * @Field()
     *
     * @return DateTimeImmutable
     * @throws Exception
     */
    public function getExpiryDate(): DateTimeImmutable
    {
        return new DateTimeImmutable($this->expiryDate->format(DateTimeImmutable::ATOM));
    }

    /**
     * @param DateTimeImmutable $expiryDate
     */
    public function setExpiryDate(DateTimeImmutable $expiryDate): void
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isExpired(): bool {
        $currentTime = new DateTimeImmutable('now', new DateTimeZone('Europe/Prague'));
        return $currentTime > $this->expiryDate;
    }

    /**
     * Gets access code's value
     *
     * @Field()
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * Gets user who used the access code.
     * Returns null if the code has not been used yet
     *
     * @Field()
     *
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * Gets access code's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets access code's note
     *
     * @Field()
     *
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param string|null $note
     */
    public function setNote(?string $note): void
    {
        $this->note = $note;
    }

}