<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * VipBenefitTranslation
 *
 * @ORM\Table(name="vip_benefit_translation", options={"comment":"VIP benefit translations"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class VipBenefitTranslation
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /** Many translations to one language
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @var int
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     */
    private $languageId;

    /** Many translations to one product
     * @var VipBenefit
     *
     * @ORM\ManyToOne(targetEntity="VipBenefit", inversedBy="translations")
     * @ORM\JoinColumn(name="vip_benefit_id", referencedColumnName="id")
     */
    private $vipBenefit;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @param Language $language
     * @param VipBenefit $vipBenefit
     *
     * @return VipBenefitTranslation
     */
    public static function create(
        Language $language,
        VipBenefit $vipBenefit
    ): VipBenefitTranslation
    {
        $instance = new self();

        $instance->language = $language;
        $instance->languageId = $language->getLanguageId();
        $instance->vipBenefit = $vipBenefit;

        return $instance;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Language
     */
    public function getLanguage(): Language
    {
        return $this->language;
    }

}
