<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Enum\LocaleEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\TranslationInput;
use App\Application\Model\GraphQLTypes\Translation;
use App\Application\Model\Timestampable;
use App\Application\Utils;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** Bonus
 * @Type(name="Bonus")
 *
 * @ORM\Table(name="bonus", options={"comment":"Bonuses defined by shops"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Bonus
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var BonusTranslation[]
     *
     * @ORM\OneToMany(targetEntity="BonusTranslation", mappedBy="bonus", indexBy="language_id", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var DateTimeImmutable|null
     *
     * @ORM\Column(name="expiry_date", type="datetime", nullable=true, options={"comment":"The date and time, until it is possible to redeem the bonus."})
     */
    private $expiryDate;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="bonuses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer", nullable=false, options={"comment":"Value of bonus in points"})
     */
    private $value;

    /**
     * @param Shop $shop
     * @param TranslationInput[] $nameTranslations
     * @param DateTimeImmutable|null $expiryDate
     * @param int $value
     *
     * @return Bonus
     * @throws Exception
     */
    public static function create(
        Shop $shop,
        array $nameTranslations,
        ?DateTimeImmutable $expiryDate,
        int $value
    ): Bonus
    {
        $instance = new self();

        $instance->setNameTranslations($nameTranslations);
        $instance->expiryDate = $expiryDate;
        $instance->shop = $shop;
        $instance->value = $value;

        return $instance;
    }

    /**
     * Gets bonus's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     * @throws Exception
     */
    public function setNameTranslations(array $nameTranslations) {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setName(null);
            }

        foreach ($nameTranslations as $nameTranslation) {
            if (LocaleEnum::isValid($nameTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($nameTranslation->getLanguageCode());
                if (is_null($language))
                    throw new Exception("Given language is not supported");
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = BonusTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setName($nameTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }


    /**
     * Gets translated name of category based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string
     */
    public function getName(): string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getName", $this->translations);
    }

    /**
     * Gets name translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getNameTranslations(): array
    {
        return Utils::getAllTranslations("getName", $this->translations);
    }

    /**
     * Gets date and time when bonus expires.
     * Returns null if the bonus does not expire.
     *
     * @Field()
     *
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function getExpiryDate(): ?DateTimeImmutable
    {
        if (is_null($this->expiryDate))
            return $this->expiryDate;
        return new DateTimeImmutable($this->expiryDate->format(DateTimeImmutable::ATOM));
    }

    /**
     * @param DateTimeImmutable|null $expiryDate
     */
    public function setExpiryDate(?DateTimeImmutable $expiryDate): void
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * Gets bonus's shop
     *
     * @Field()
     *
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop): void
    {
        $this->shop = $shop;
    }

    /**
     * Gets value of bonus in points
     *
     * @Field()
     *
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue(int $value): void
    {
        $this->value = $value;
    }
    
    /**
     * Has the bonus expired yet?
     *
     * @Field()
     *
     * @return bool
     * @throws Exception
     */
    public function isExpired(): bool
    {
        if (is_null($this->expiryDate))
            return false;

        $currentTime = new DateTimeImmutable('now', new DateTimeZone('Europe/Prague'));
        return $currentTime > $this->expiryDate;
    }


}
