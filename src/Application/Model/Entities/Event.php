<?php


namespace App\Application\Model\Entities;


use App\Application\Model\Enum\CountryEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\LocaleEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\Enum\TastingStateEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\TranslationInput;
use App\Application\Model\GraphQLTypes\Translation;
use App\Application\Model\Timestampable;
use App\Application\Services\SecurityService\AuthorizationService;
use App\Application\Utils;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Security;

/**
 * @Type(name="Event")
 *
 * @Entity
 * @ORM\Table(name="event", options={"comment":"Event table"})
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="event_type", type="string")
 * @ORM\DiscriminatorMap({"event"="Event", "tasting"="Tasting"})
 */
abstract class Event
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(name="date_from", type="datetime", nullable=false, options={"comment":"Datetime when the event starts"})
     */
    private $dateFrom;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(name="date_until", type="datetime", nullable=false, options={"comment":"Datetime when the event ends"})
     */
    private $dateUntil;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="events", cascade={"persist"})
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    private $shop;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $latitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image_identifier", type="text", length=65535, nullable=true)
     */
    private $imageIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255, nullable=false)
     */
    private $state;

    /**
     * @var int
     *
     * @ORM\Column(name="ordering", type="integer", nullable=false)
     */
    private $ordering;

    /**
     * @var EventTranslation[]
     *
     * @ORM\OneToMany(targetEntity="EventTranslation", mappedBy="event", indexBy="language_id", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="text", length=65535, nullable=false)
     */
    private $street;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descriptive_number", type="string", length=255, nullable=true)
     */
    private $descriptiveNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="routing_number", type="string", length=255, nullable=true)
     */
    private $routingNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=5, nullable=false)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country", cascade={"persist"})
     * @ORM\JoinColumn(name="country", referencedColumnName="id", nullable=false)
     */
    private $country;

    /**
     * @var bool
     *
     * @ORM\Column(name="vip_only", type="boolean", nullable=false, options={"comment":"Is the event for VIP users only?"})
     */
    private $vipOnly;

    /** Users that are attendees at this event
     *
     * @var UserEvent[]|null
     *
     * @ORM\OneToMany(targetEntity="UserEvent", mappedBy="event", indexBy="user_id", cascade={"persist", "remove"})
     */
    private $attendees;

    /**
     * Event constructor
     * @param Shop $shop
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $publicDescriptionTranslations
     * @param TranslationInput[]|null $privateDescriptionTranslations
     * @param DateTimeImmutable $dateFrom
     * @param DateTimeImmutable $dateUntil
     * @param EntityStateEnum $state
     * @param string $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string $postcode
     * @param string $city
     * @param Country $country
     * @param float $longitude
     * @param float $latitude
     * @param bool $vipOnly
     * @throws Exception
     */
    public function __construct(
        Shop $shop,
        array $nameTranslations,
        ?array $publicDescriptionTranslations,
        ?array $privateDescriptionTranslations,
        DateTimeImmutable $dateFrom,
        DateTimeImmutable $dateUntil,
        EntityStateEnum $state,
        string $street,
        ?string $descriptiveNumber,
        ?string $routingNumber,
        string $postcode,
        string $city,
        Country $country,
        float $longitude,
        float $latitude,
        bool $vipOnly
    ) {
        $this->shop = $shop;
        $this->setNameTranslations($nameTranslations);
        $this->setPublicDescriptionTranslations($publicDescriptionTranslations);
        $this->setPrivateDescriptionTranslations($privateDescriptionTranslations);
        $this->dateFrom = $dateFrom;
        $this->dateUntil = $dateUntil;
        $this->state = $state;
        $this->street = $street;
        $this->descriptiveNumber = $descriptiveNumber;
        $this->routingNumber = $routingNumber;
        $this->postcode = $postcode;
        $this->city = $city;
        $this->country = $country;
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        $this->ordering = 0;
        $this->vipOnly = $vipOnly;
    }

    /**
     * Gets date and time when the event starts
     *
     * @Field()
     *
     * @return DateTimeImmutable
     * @throws Exception
     */
    public function getDateFrom(): DateTimeImmutable
    {
        return new DateTimeImmutable($this->dateFrom->format(DateTimeImmutable::ATOM));
    }

    /**
     * @param DateTimeImmutable $dateFrom
     */
    public function setDateFrom(DateTimeImmutable $dateFrom): void
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * Gets date and time when the event ends
     *
     * @Field()
     *
     * @return DateTimeImmutable
     * @throws Exception
     */
    public function getDateUntil(): DateTimeImmutable
    {
        return new DateTimeImmutable($this->dateUntil->format(DateTimeImmutable::ATOM));
    }

    /**
     * @param DateTimeImmutable $dateUntil
     */
    public function setDateUntil(DateTimeImmutable $dateUntil): void
    {
        $this->dateUntil = $dateUntil;
    }

    /**
     * Gets shop to whom event belongs
     *
     * @Field()
     *
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop): void
    {
        $this->shop = $shop;
    }

    /**
     * Gets translated name of event based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string
     */
    public function getName(): string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getName", $this->translations);
    }

    /**
     * Gets event's name translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getNameTranslations(): iterable
    {
        return Utils::getAllTranslations("getName", $this->translations);
    }

    /**
     * Gets translated public description of event based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string|null
     */
    public function getPublicDescription(): ?string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getPublicDescription", $this->translations);
    }

    /**
     * Gets event's public description translations
     *
     * @Field()
     *
     * @return Translation[]|null
     */
    public function getPublicDescriptionTranslations(): ?iterable
    {
        return Utils::getAllTranslations("getPublicDescription", $this->translations);
    }

    /**
     * Gets translated private description of event based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string|null
     */
    public function getPrivateDescription(): ?string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getPrivateDescription", $this->translations);
    }

    /**
     * Gets event's private description translations
     *
     * @Field()
     *
     * @return Translation[]|null
     */
    public function getPrivateDescriptionTranslations(): ?iterable
    {
        return Utils::getAllTranslations("getPrivateDescription", $this->translations);
    }

    /**
     * Gets event's ordering number.
     * The event with the lowest value should be shown first.
     *
     * @Field()
     *
     * @return int
     */
    public function getOrdering(): int
    {
        return $this->ordering;
    }

    /**
     * @param int $ordering
     */
    public function setOrdering(int $ordering): void
    {
        $this->ordering = $ordering;
    }

    /**
     * Gets event's longitude
     *
     * @Field()
     *
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * Gets event's latitude
     *
     * @Field()
     *
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @Field()
     *
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * Gets event's country
     *
     * @Field()
     *
     * @return CountryEnum
     */
    public function getCountry(): CountryEnum
    {
        return $this->country->getCountryCode();
    }

    /**
     * @param Country $country
     */
    public function setCountry(Country $country): void
    {
        $this->country = $country;
    }

    /**
     * Gets event's street
     *
     * @Field()
     *
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * Gets event's descriptive number
     *
     * @Field()
     *
     * @return string|null
     */
    public function getDescriptiveNumber(): ?string
    {
        return $this->descriptiveNumber;
    }

    /**
     * @param string|null $descriptiveNumber
     */
    public function setDescriptiveNumber(?string $descriptiveNumber): void
    {
        $this->descriptiveNumber = $descriptiveNumber;
    }

    /**
     * Gets event's routing number
     *
     * @Field()
     *
     * @return string|null
     */
    public function getRoutingNumber(): ?string
    {
        return $this->routingNumber;
    }

    /**
     * @param string|null $routingNumber
     */
    public function setRoutingNumber(?string $routingNumber): void
    {
        $this->routingNumber = $routingNumber;
    }

    /**
     * Gets event's postcode
     *
     * @Field()
     *
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode(string $postcode): void
    {
        $this->postcode = $postcode;
    }

    /**
     * Gets event's city
     *
     * @Field()
     *
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * Gets event's image url
     *
     * @Field()
     *
     * @param string $width
     * @param string $height
     * @param string $format
     * @return string|null
     */
    public function getImageURL(string $width = '500', string $height = '500', string $format = 'png'): ?string
    {
        if (is_null($this->imageIdentifier))
            return "";
        return Utils::buildMediaDownloadURL($this->imageIdentifier, $width, $height, $format);
    }

    /**
     * @return string|null
     */
    public function getImageIdentifier(): ?string
    {
        return $this->imageIdentifier;
    }

    /**
     * @param string|null $imageIdentifier
     */
    public function setImageIdentifier(?string $imageIdentifier): void
    {
        $this->imageIdentifier = $imageIdentifier;
    }

    /**
     * Gets event's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     * @throws Exception
     */
    public function setNameTranslations(array $nameTranslations)
    {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setName(null);
            }

        if (!isset($nameTranslations))
            return;

        foreach ($nameTranslations as $nameTranslation) {
            if (LocaleEnum::isValid($nameTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($nameTranslation->getLanguageCode());
                if (is_null($language))
                    throw new Exception("Given language is not supported");
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = EventTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setName($nameTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * @param TranslationInput[]|null $privateDescriptionTranslations
     * @throws Exception
     */
    public function setPrivateDescriptionTranslations(?array $privateDescriptionTranslations)
    {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setPrivateDescription(null);
            }

        if (!isset($privateDescriptionTranslations))
            return;

        foreach ($privateDescriptionTranslations as $privateDescriptionTranslation) {
            if (LocaleEnum::isValid($privateDescriptionTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($privateDescriptionTranslation->getLanguageCode());
                if (is_null($language))
                    continue;
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = EventTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setPrivateDescription($privateDescriptionTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * @param TranslationInput[]|null $publicDescriptionTranslations
     * @throws Exception
     */
    public function setPublicDescriptionTranslations(?array $publicDescriptionTranslations)
    {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setPublicDescription(null);
            }

        if (!isset($publicDescriptionTranslations))
            return;

        foreach ($publicDescriptionTranslations as $publicDescriptionTranslation) {
            if (LocaleEnum::isValid($publicDescriptionTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($publicDescriptionTranslation->getLanguageCode());
                if (is_null($language))
                    continue;
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = EventTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setPublicDescription($publicDescriptionTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * Is event only for VIP users?
     *
     * @Field()
     *
     * @return bool
     */
    public function isVipOnly(): bool
    {
        return $this->vipOnly;
    }

    /**
     * @param bool $vipOnly
     */
    public function setVipOnly(bool $vipOnly): void
    {
        $this->vipOnly = $vipOnly;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function started(): bool
    {
        $currentTime = new DateTimeImmutable('now', new DateTimeZone('Europe/Prague'));

        return $this->dateFrom <= $currentTime;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function ended(): bool
    {
        $currentTime = new DateTimeImmutable('now', new DateTimeZone('Europe/Prague'));

        return $this->dateUntil <= $currentTime;
    }

    /**
     * Gets information about logged in user as an attendee in the event.
     * Returns null if the user is not logged in.
     *
     * @Field()
     *
     * @return UserEvent|null
     */
    public function getMyReview(): ?UserEvent {
        if (isset($_SESSION['uid']) && (is_array($this->attendees) || is_object($this->attendees))) {
            /** @var UserEvent $userBinding */
            foreach ($this->attendees as $userBinding) {
                if ($userBinding->getUser()->getUid() == $_SESSION['uid']) {        //user is registered for this event
                    return $userBinding;
                }
            }
        }
        return null;
    }

    /**
     * Gets event's attendees
     *
     * @Field()
     * @Security("this.canManageEvent()")
     *
     * @return UserEvent[]|null
     */
    public function getAttendees(): ?iterable
    {
        return $this->attendees;
    }

    /**
     * Gets the state of event based on the logged in user.
     * Returns null if the user is not logged in.
     *
     * @Field()
     *
     * @return TastingStateEnum|null
     * @throws Exception
     */
    public function getEventState(): ?TastingStateEnum
    {
        if (!isset($_SESSION['uid'])) {
            return null;
        }

        return $this->getEventStateInternal($_SESSION['uid']);
    }

    /**
     * @param string|null $uid
     * @return TastingStateEnum|null
     * @throws Exception
     */
    public function getEventStateInternal(?string $uid): ?TastingStateEnum {
        if (!is_null($uid)) {
            if (!is_array($this->attendees) && !is_object($this->attendees)) {
                return null;
            }

            /** @var UserEvent $userBinding */
            foreach ($this->attendees as $userBinding) {
                if ($userBinding->getUser()->getUid() == $uid) {        //user is registered for this event
                    if (!$this->started()) {
                        return new TastingStateEnum(TastingStateEnum::REGISTERED_NOT_STARTED);
                    } else if ($this->ended()) {
                        if ($userBinding->isFinished())
                            return new TastingStateEnum(TastingStateEnum::REGISTERED_ENDED_FINISHED);
                        else
                            return new TastingStateEnum(TastingStateEnum::REGISTERED_ENDED_NOT_FINISHED);
                    } else {
                        if ($userBinding->isFinished())
                            return new TastingStateEnum(TastingStateEnum::REGISTERED_STARTED_FINISHED);
                        else
                            return new TastingStateEnum(TastingStateEnum::REGISTERED_STARTED_NOT_FINISHED);
                    }
                }
            }
        }

        //user is not registered for this event, or uid is null
        if (!$this->started()) {
            return new TastingStateEnum(TastingStateEnum::UNREGISTERED_NOT_STARTED);
        } else if ($this->ended()) {
            return new TastingStateEnum(TastingStateEnum::UNREGISTERED_ENDED);
        } else {
            return new TastingStateEnum(TastingStateEnum::UNREGISTERED_STARTED);
        }

    }

    /**
     * @return bool
     */
    public function canManageEvent(): bool
    {
        $authorizationService = AuthorizationService::getInstance();
        return $authorizationService->isAllowed(new ScopeRightsEnum(ScopeRightsEnum::CAN_MANAGE_EVENTS), $this->shop);
    }

}

