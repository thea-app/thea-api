<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Enum\NotificationGroupEnum;
use App\Application\Model\Timestampable;
use App\Application\Utils;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\ORMException;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type(name="Notification")
 *
 * Notification
 *
 * ORM annotations:
 * @ORM\Table(name="notification", options={"comment":"Notification table"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Notification
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", length=65535, nullable=false, options={"comment":"Notification title"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=false, options={"comment":"Notification text"})
     */
    private $text;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image_identifier", type="text", length=65535, nullable=true)
     */
    private $imageIdentifier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true, options={"comment":"Notification name for firebase"})
     */
    private $name;

    /**
     * @var DateTimeImmutable|null
     *
     * @ORM\Column(name="sending_time", type="datetime", nullable=true, options={"comment":"Datetime when was the notification sent"})
     */
    private $sendingTime;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="notifications", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /** Shop notifications
     *
     * @var NotificationGroup[]|null
     *
     * @ORM\OneToMany(targetEntity="NotificationGroup", mappedBy="notification", cascade={"persist", "remove"})
     */
    private $notificationGroups;

    /** Sent notifications
     *
     * @var SentNotification[]|null
     *
     * @ORM\OneToMany(targetEntity="SentNotification", mappedBy="notification", cascade={"persist", "remove"})
     */
    private $sentNotifications;

    /**
     * Notification constructor.
     * @param Shop $shop
     * @param string $title
     * @param string $text
     * @param string|null $imageIdentifier
     * @param string|null $name
     * @param NotificationGroupEnum[] $notificationGroups
     * @param EntityManager $em
     * @return Notification
     * @throws ORMException
     */
    public static function create(
        Shop $shop,
        string $title,
        string $text,
        ?string $imageIdentifier,
        ?string $name,
        array $notificationGroups,
        EntityManager $em
    ) : Notification {
        $instance = new self();

        $instance->shop = $shop;
        $instance->title = $title;
        $instance->text = $text;
        $instance->imageIdentifier = $imageIdentifier;
        $instance->name = $name;
        $instance->setNotificationGroups($notificationGroups, $em);

        return $instance;
    }

    /**
     * Gets notification's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets notification's title
     *
     * @Field()
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * Gets notification's text
     *
     * @Field()
     *
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }


    /**
     * Gets shop to whom the notification belongs
     *
     * @Field()
     *
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * Gets groups of users to which the notification should be or was send
     *
     * @Field()
     *
     * @return NotificationGroup[]|null
     */
    public function getNotificationGroups(): ?iterable
    {
        return $this->notificationGroups;
    }

    /**
     * @param NotificationGroupEnum[] $newNotificationGroupEnums
     * @param EntityManager $em
     * @throws ORMException
     */
    public function setNotificationGroups(array $newNotificationGroupEnums, EntityManager $em): void
    {
        if (is_array($this->notificationGroups) || is_object($this->notificationGroups)) {
            foreach ($this->notificationGroups as $oldNotificationGroup) {
                $foundItem = array_search($oldNotificationGroup->getNotificationGroup(), $newNotificationGroupEnums);
                if (!is_bool($foundItem)) {     //if the new notification group is in the list of old notification groups
                    unset($newNotificationGroupEnums[$foundItem]);
                    continue;
                }
                $em->remove($oldNotificationGroup);
                $em->flush();
                unset($oldNotificationGroup);
            }
        }
        if (is_array($newNotificationGroupEnums) || is_object($newNotificationGroupEnums)) {
            foreach ($newNotificationGroupEnums as $newNotificationGroup) {
                $newBinding = NotificationGroup::create($this, new NotificationGroupEnum($newNotificationGroup));
                $this->notificationGroups[] = $newBinding;
            }
        }
    }

    /**
     * Gets sending time of notification.
     * Returns null if the notification has not been sent yet.
     *
     * @Field()
     *
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function getSendingTime(): ?DateTimeImmutable
    {

        if ($this->sendingTime == null)
            return null;
        return new DateTimeImmutable($this->sendingTime->format(DateTimeImmutable::ATOM));
    }

    /**
     * @param DateTimeImmutable|null $sendingTime
     */
    public function setSendingTime(?DateTimeImmutable $sendingTime): void
    {
        $this->sendingTime = $sendingTime;
    }

    /**
     * Gets notification's name
     *
     * @Field()
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop): void
    {
        $this->shop = $shop;
    }

    /**
     * @return string|null
     */
    public function getImageIdentifier(): ?string
    {
        return $this->imageIdentifier;
    }

    /**
     * @param string|null $imageIdentifier
     */
    public function setImageIdentifier(?string $imageIdentifier): void
    {
        $this->imageIdentifier = $imageIdentifier;
    }

    /**
     * Gets notification's image URL
     *
     * @Field()
     *
     * @param string $width
     * @param string $height
     * @param string $format
     * @return string|null
     */
    public function getImageURL(string $width = '500', string $height = '500', string $format = 'webp'): ?string
    {
        if (is_null($this->imageIdentifier))
            return "";
        return Utils::buildMediaDownloadURL($this->imageIdentifier, $width, $height, $format);
    }

}