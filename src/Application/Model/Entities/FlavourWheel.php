<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Timestampable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Field;

/** Category
 * @Type(name="FlavourWheel")
 *
 * @ORM\Table(name="flavour_wheel", options={"comment":"Flavour wheel table"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class FlavourWheel
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true, options={"comment":"Name of flavour wheel"})
     */
    private $name;

    /**
     * @var Tasting[]|null
     *
     * @ORM\OneToMany(targetEntity="Tasting", mappedBy="flavourWheel", cascade={"persist"})
     */
    private $tastings;

    /**
     * @var Flavour[]|null
     *
     * @ORM\OneToMany(targetEntity="Flavour", mappedBy="flavourWheel", cascade={"persist", "remove"})
     */
    private $flavours;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /**
     * @param EntityStateEnum $state
     * @param string|null $name
     * @param Tasting $tasting
     * @return FlavourWheel
     * @throws Exception
     */
    public static function create(
        EntityStateEnum $state,
        ?string $name,
        Tasting $tasting
    ): FlavourWheel
    {
        $instance = new self();

        $instance->state = $state;
        $instance->name = $name;
        $instance->tastings = new ArrayCollection();
        $instance->tastings[] = $tasting;

        return $instance;
    }

    /**
     * Gets flavour wheel's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets flavour wheel's name
     *
     * @Field()
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * Gets flavour wheel's flavours.
     * The flavours are sorted by ordering in ascending order.
     *
     * @Field()
     *
     * @return Flavour[]|null
     */
    public function getFlavours(): ?iterable
    {
        /** @var Flavour[] $result */
        $result = [];
        if (is_array($this->flavours) || is_object($this->flavours)) {
            foreach ($this->flavours as $flavour) {
                if (is_null($flavour->getParentFlavour()))
                    $result[] = $flavour;
            }
            usort($result,
                function(Flavour $c1, Flavour $c2) {            //sort by ordering, ascending
                    return $c1->getOrdering() > $c2->getOrdering();
                });
        }
        return $result;
    }

    /**
     * @param Flavour[]|null $flavours
     */
    public function setFlavours(?array $flavours): void
    {
        $this->flavours = $flavours;
    }

    /**
     * @Field()
     *
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * Gets tastings that use the flavour wheel
     *
     * @Field()
     *
     * @return Tasting[]|null
     */
    public function getTastings(): ?iterable
    {
        return $this->tastings;
    }

    /**
     * @param Tasting[]|null $tastings
     */
    public function setTastings(?array $tastings): void
    {
        $this->tastings = $tastings;
    }
}