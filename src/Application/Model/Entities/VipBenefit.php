<?php


namespace App\Application\Model\Entities;


use App\Application\Model\Enum\LocaleEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\TranslationInput;
use App\Application\Model\GraphQLTypes\Translation;
use App\Application\Model\Timestampable;
use App\Application\Utils;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Exception;

use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class VipBenefit
 * @package App\Application\Model\Entities
 *
 * @Type()
 *
 * @ORM\Table(name="vip_benefit", options={"comment":"Benefits for VIP users"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class VipBenefit
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="vipBenefits", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /**
     * @var VipBenefitTranslation[]
     *
     * @ORM\OneToMany(targetEntity="VipBenefitTranslation", mappedBy="vipBenefit", indexBy="language_id", cascade={"persist", "remove"})
     */
    private $translations;

    /** Vip users, that have this benefit
     *
     * @var VipUserBenefit[]
     *
     * @ORM\OneToMany(targetEntity="VipUserBenefit", mappedBy="vipBenefit", indexBy="shop_vip_user_id", cascade={"persist", "remove"})
     */
    private $vipUsers;

    /**
     * @param Shop $shop
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $descriptionTranslations
     *
     * @return VipBenefit
     * @throws Exception
     */
    public static function create(
        Shop $shop,
        array $nameTranslations,
        ?array $descriptionTranslations
    ): VipBenefit
    {
        $instance = new self();

        $instance->shop = $shop;
        $instance->setNameTranslations($nameTranslations);
        $instance->setDescriptionTranslations($descriptionTranslations);

        return $instance;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     * @throws Exception
     */
    public function setNameTranslations(array $nameTranslations)
    {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setName(null);
            }

        if (!isset($nameTranslations))
            return;

        foreach ($nameTranslations as $nameTranslation) {
            if (LocaleEnum::isValid($nameTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($nameTranslation->getLanguageCode());
                if (is_null($language))
                    throw new Exception("Given language is not supported");
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = VipBenefitTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setName($nameTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * @param TranslationInput[]|null $descriptionTranslations
     * @throws Exception
     */
    public function setDescriptionTranslations(?array $descriptionTranslations)
    {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setDescription(null);
            }

        if (!isset($descriptionTranslations))
            return;

        foreach ($descriptionTranslations as $descriptionTranslation) {
            if (LocaleEnum::isValid($descriptionTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($descriptionTranslation->getLanguageCode());
                if (is_null($language))
                    continue;
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = VipBenefitTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setDescription($descriptionTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * Gets vip benefit's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function addVipUser(ShopVipUser $user)
    {
        if (!isset($this->vipUsers[$user->getId()])) {
            $binding = VipUserBenefit::create($this, $user);
            $this->vipUsers[$user->getId()] = $binding;
            $user->addVipBenefit($this, $binding);
        }
    }

    /**
     * @param ShopVipUser $user
     * @param EntityManager $em
     * @throws ORMException
     */
    public function removeVipUser(ShopVipUser $user, EntityManager $em)
    {
        if (isset($this->vipUsers[$user->getId()])) {
            $user->removeVipBenefit($this);
            $em->remove($this->vipUsers[$user->getId()]);
            unset($this->vipUsers[$user->getId()]);
        }
    }

    /**
     * Gets shop which the vip benefit belongs to
     *
     * @Field()
     *
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * Gets translated name of vip benefit based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string
     */
    public function getName(): string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getName", $this->translations);
    }

    /**
     * Gets name translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getNameTranslations(): array
    {
        return Utils::getAllTranslations("getName", $this->translations);
    }

    /**
     * Gets translated description of vip benefit based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getDescription", $this->translations);
    }

    /**
     * Gets description translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getDescriptionTranslations(): array
    {
        return Utils::getAllTranslations("getDescription", $this->translations);
    }

}