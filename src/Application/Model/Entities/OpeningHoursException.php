<?php

namespace App\Application\Model\Entities;

use App\Application\Model\GraphQLTypes\OpeningHoursTimeInterval;
use App\Application\Model\GraphQLTypes\OpeningHoursTime;
use App\Application\Model\Timestampable;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * OpeningHoursException
 *
 * @ORM\Table(name="opening_hours_exception", options={"comment":"Exception from opening hours of shops"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class OpeningHoursException
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var string|null
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=true, options={"comment":"Description text"})
     */
    private $text;

    /**
     * @var OpeningHoursExceptionInterval[]|null
     *
     * @ORM\OneToMany(targetEntity="OpeningHoursExceptionInterval", mappedBy="openingHoursException", cascade={"persist", "remove"})
     */
    private $openingHoursExceptionIntervals;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="openingHoursExceptions", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /**
     * @param $date
     * @param $text
     * @param OpeningHoursTimeInterval[]|null $openingHours
     * @return OpeningHoursException
     * @throws Exception
     */
    public static function create(
        $date,
        $text,
        ?array $openingHours
    ): OpeningHoursException
    {
        $instance = new self();

        $instance->date = $date;
        $instance->text = $text;

        foreach ($openingHours as $openingHour) {
            $instance->openingHoursExceptionIntervals[] = OpeningHoursExceptionInterval::create(
                $openingHour->getFrom()->getHours(),
                $openingHour->getFrom()->getMinutes(),
                $openingHour->getUntil()->getHours(),
                $openingHour->getUntil()->getMinutes(),
                $instance
            );
        }

        return $instance;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets opening hour exception's date
     *
     * @Field(outputType="DateTime")
     *
     * @return DateTimeImmutable
     * @throws Exception
     */
    public function getDate(): DateTimeImmutable
    {
        return new DateTimeImmutable($this->date->format(DateTimeImmutable::ATOM));
    }

    /**
     * @param DateTimeImmutable $date
     */
    public function setDate(DateTimeImmutable $date): void
    {
        $this->date = $date;
    }

    /**
     * Gets opening hour exception's text
     *
     * @Field()
     *
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * Gets opening hour exception's time intervals
     *
     * @Field()
     *
     * @return null|OpeningHoursTimeInterval[]
     */
    public function getOpeningHours(): ?iterable
    {
        //TODO: sort time intervals ascending
        $result = [];
        foreach ($this->openingHoursExceptionIntervals as $exceptionsInterval) {
            $timeFrom = OpeningHoursTime::create($exceptionsInterval->getFromHour(), $exceptionsInterval->getFromMinute());
            $timeUntil = OpeningHoursTime::create($exceptionsInterval->getUntilHour(), $exceptionsInterval->getUntilMinute());
            $timeInterval = OpeningHoursTimeInterval::create($timeFrom, $timeUntil);

            $result[] = $timeInterval;
        }
        return $result;
    }

    /**
     * @return mixed
     */
    public function getOpeningHoursExceptionIntervals()
    {
        return $this->openingHoursExceptionIntervals;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop): void
    {
        $this->shop = $shop;
    }

}
