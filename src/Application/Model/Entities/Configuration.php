<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/** Configuration
 *
 * @ORM\Table(name="configuration", options={"comment":"Configuration"})
 * @ORM\Entity
 */
class Configuration
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false, options={"comment":"Configuration code"})
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", length=65535, nullable=false, options={"comment":"Configuration value"})
     */
    private $value;

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

}