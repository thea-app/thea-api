<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Enum\NotificationGroupEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type(name="NotificationGroup")
 *
 * Shop
 *
 * ORM annotations:
 * @ORM\Table(name="notification_group", options={"comment":"Group of users, which we can send the notification"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class NotificationGroup
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Notification
     *
     * @ORM\ManyToOne(targetEntity="Notification", inversedBy="notificationGroups", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="notification_id", referencedColumnName="id")
     * })
     */
    private $notification;

    /**
     * @var NotificationGroupEnum
     *
     * @ORM\Column(name="notification_group", type="notification_group_enum", nullable=false)
     */
    private $notificationGroup;

    /**
     * NotificationGroup constructor.
     * @param Notification $notification
     * @param NotificationGroupEnum $notificationGroup
     * @return NotificationGroup
     */
    public static function create(
        Notification $notification,
        NotificationGroupEnum $notificationGroup
    ) : NotificationGroup {
        $instance = new self();

        $instance->notification = $notification;
        $instance->notificationGroup = $notificationGroup;

        return $instance;
    }

    /**
     * Gets notification group's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Gets notification that should or was sent to the notification group
     *
     * @Field()
     *
     * @return Notification
     */
    public function getNotification(): Notification
    {
        return $this->notification;
    }

    /**
     * @param Notification $notification
     */
    public function setNotification(Notification $notification): void
    {
        $this->notification = $notification;
    }

    /**
     * Gets notification group value
     *
     * @Field()
     *
     * @return NotificationGroupEnum
     */
    public function getNotificationGroup(): NotificationGroupEnum
    {
        return $this->notificationGroup;
    }

    /**
     * @param NotificationGroupEnum $notificationGroup
     */
    public function setNotificationGroup(NotificationGroupEnum $notificationGroup): void
    {
        $this->notificationGroup = $notificationGroup;
    }

}