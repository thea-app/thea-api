<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use App\Application\Utils;
use Exception;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * ShopBonusUser
 *
 * @Type()
 *
 * @ORM\Table(name="shop_bonus_user", options={"comment":"User, who has bonus program in shop"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ShopBonusUser
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="code", type="string", length=255, nullable=false, unique=true, options={"comment":"Unique code that identifies the user"})
     */
    private $code;

    /**
     * @var int
     * @ORM\Column(name="points", type="integer", nullable=false, options={"comment":"Total balance in points"})
     */
    private $points;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="bonusShops", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bonus_user_id", referencedColumnName="id")
     * })
     */
    private $bonusUser;

    /**
     * @var int
     * @ORM\Column(name="bonus_user_id", type="integer", nullable=false)
     */
    private $bonusUserId;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="bonusUsers", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /**
     * @var int
     * @ORM\Column(name="shop_id", type="integer", nullable=false)
     */
    private $shopId;

    /** ShopBonusUser constructor
     *
     * @param Shop $shop
     * @param User $bonusUser
     * @return ShopBonusUser
     */
    public static function create(
        Shop $shop,
        User $bonusUser
    ): ShopBonusUser
    {
        $instance = new self();

        $instance->shop = $shop;
        $instance->shopId = $shop->getId();
        $instance->bonusUser = $bonusUser;
        $instance->bonusUserId = $bonusUser->getId();
        $instance->code = Utils::generateEntityUniqueCode($instance);
        $instance->points = 0;

        return $instance;
    }

    /**
     * Gets shop's bonus user
     *
     * @Field()
     *
     * @return User
     */
    public function getBonusUser(): User
    {
        return $this->bonusUser;
    }

    /**
     * Gets shop of the bonus user
     *
     * @Field()
     *
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * Gets unique code that identifies the user
     *
     * @Field()
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * Gets shop bonus user's id
     *
     * @Field()
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Gets points
     *
     * @Field()
     *
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * @param int $points
     */
    public function addPoints(int $points): void
    {
        $this->points += $points;
    }

    /**
     * @param Bonus $bonus
     * @return BonusCode
     * @throws Exception
     */
    public function redeemBonus(Bonus $bonus): BonusCode {
        $this->points -= $bonus->getValue();

        return BonusCode::create(
            $bonus,
            $this
        );
    }

}
