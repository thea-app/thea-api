<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * FlavourTranslation
 *
 * @ORM\Table(name="flavour_translation", options={"comment":"Flavour translations"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class FlavourTranslation
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /** Many translations to one language
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @var int
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false, options={"comment":"Reference to language table"})
     */
    private $languageId;

    /** Many translations to one flavour
     * @var Flavour
     *
     * @ORM\ManyToOne(targetEntity="Flavour", inversedBy="translations")
     * @ORM\JoinColumn(name="flavour_id", referencedColumnName="id")
     */
    private $flavour;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true)
     */
    private $name;


    /**
     * @param Language $language
     * @param Flavour $flavour
     *
     * @return FlavourTranslation
     */
    public static function create(
        Language $language,
        Flavour $flavour
    ): FlavourTranslation
    {
        $instance = new self();

        $instance->language = $language;
        $instance->languageId = $language->getLanguageId();
        $instance->flavour = $flavour;

        return $instance;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Language
     */
    public function getLanguage(): Language
    {
        return $this->language;
    }
}
