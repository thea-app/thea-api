<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * EventTranslation
 *
 * @ORM\Table(name="event_translation")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class EventTranslation
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /** Many translations to one language
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @var int
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false, options={"comment":"Reference to language table"})
     */
    private $languageId;

    /** Many translations to one product
     * @var Event
     *
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="translations")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     */
    private $event;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="public_description", type="text", length=65535, nullable=true, options={"comment":"Public description for all users"})
     */
    private $publicDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="private_description", type="text", length=65535, nullable=true, options={"comment":"Private description only for users in event"})
     */
    private $privateDescription;

    /**
     * @param Language $language
     * @param Event $event
     *
     * @return EventTranslation
     */
    public static function create(
        Language $language,
        Event $event
    ): EventTranslation
    {
        $instance = new self();

        $instance->language = $language;
        $instance->languageId = $language->getLanguageId();
        $instance->event = $event;

        return $instance;
    }

    /**
     * @return Language
     */
    public function getLanguage(): Language
    {
        return $this->language;
    }

    /**
     * @return Event
     */
    public function getEvent(): Event
    {
        return $this->event;
    }

    /**
     * @param Event $event
     */
    public function setEvent(Event $event): void
    {
        $this->event = $event;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getPublicDescription(): ?string
    {
        return $this->publicDescription;
    }

    /**
     * @param string|null $publicDescription
     */
    public function setPublicDescription(?string $publicDescription): void
    {
        $this->publicDescription = $publicDescription;
    }

    /**
     * @return string|null
     */
    public function getPrivateDescription(): ?string
    {
        return $this->privateDescription;
    }

    /**
     * @param string|null $privateDescription
     */
    public function setPrivateDescription(?string $privateDescription): void
    {
        $this->privateDescription = $privateDescription;
    }

}
