<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type(name="UserTastingProductReview")
 *
 * Shop
 *
 * ORM annotations:
 * @ORM\Table(name="user_tasting_product_review", uniqueConstraints={@ORM\UniqueConstraint(name="unambiguity_idx", columns={"user_event_id", "tasting_product_id"})}, options={"comment":"User's review of tasting product"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class UserTastingProductReview
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var UserEvent
     *
     * @ORM\ManyToOne(targetEntity="UserEvent", cascade={"persist"})
     * @ORM\JoinColumn(name="user_event_id", referencedColumnName="id")
     */
    private $userEvent;

    /**
     * @var TastingProduct
     *
     * @ORM\ManyToOne(targetEntity="TastingProduct", inversedBy="userReviews", cascade={"persist"})
     * @ORM\JoinColumn(name="tasting_product_id", referencedColumnName="id")
     */
    private $tastingProduct;

    /**
     * @var string|null
     *
     * @ORM\Column(name="public_note", type="text", length=65535, nullable=true, options={"comment":"Public note visible to author and admin of the shop of the event"})
     */
    private $publicNote;

    /**
     * @var string|null
     *
     * @ORM\Column(name="private_note", type="text", length=65535, nullable=true, options={"comment":"Private note visible only to author"})
     */
    private $privateNote;

    /**
     * @var float|null
     *
     * @ORM\Column(name="rating", type="decimal", precision=4, scale=2, nullable=true, options={"comment":"Rating of tasting product in 1-5 range"})
     */
    private $rating;

    /**
     * @var Flavour[]|null
     *
     * Many UserTastingProductReviews have many Flavours
     * @ORM\ManyToMany(targetEntity="Flavour")
     * @ORM\JoinTable(name="user_tasting_product_review_flavour",
     *      joinColumns={@ORM\JoinColumn(name="user_tasting_product_review_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="flavour_id", referencedColumnName="id")}
     * )
     */
    private $flavours;

    /**
     * UserTastingProductReview constructor.
     * @param UserEvent $userEvent
     * @param TastingProduct $tastingProduct
     * @param string|null $privateNote
     * @param string|null $publicNote
     * @param float|null $rating
     * @param Flavour[]|null $flavours
     * @return UserTastingProductReview
     * @throws Exception
     */
    public static function create(
        UserEvent $userEvent,
        TastingProduct $tastingProduct,
        ?string $privateNote,
        ?string $publicNote,
        ?float $rating,
        ?array $flavours
    ) : UserTastingProductReview {
        $instance = new self();

        $instance->userEvent = $userEvent;
        $instance->tastingProduct = $tastingProduct;
        $instance->privateNote = $privateNote;
        $instance->publicNote = $publicNote;
        $instance->rating = $rating;
        $instance->flavours = $flavours;

        return $instance;
    }

    /**
     * Gets user as an attendee in the event of the tasting product
     *
     * @Field()
     *
     * @return UserEvent
     */
    public function getUserEvent(): UserEvent
    {
        return $this->userEvent;
    }

    /**
     * @param UserEvent $userEvent
     */
    public function setUserEvent(UserEvent $userEvent): void
    {
        $this->userEvent = $userEvent;
    }

    /**
     * Gets tasting product
     *
     * @Field()
     *
     * @return TastingProduct
     */
    public function getTastingProduct(): TastingProduct
    {
        return $this->tastingProduct;
    }

    /**
     * @param TastingProduct $tastingProduct
     */
    public function setTastingProduct(TastingProduct $tastingProduct): void
    {
        $this->tastingProduct = $tastingProduct;
    }

    /**
     * Gets public note visible to author and admin of the shop of the event
     *
     * @Field()
     *
     * @return string|null
     */
    public function getPublicNote(): ?string
    {
        return $this->publicNote;
    }

    /**
     * @param string|null $publicNote
     */
    public function setPublicNote(?string $publicNote): void
    {
        $this->publicNote = $publicNote;
    }

    /**
     * Gets private note visible only to the author
     *
     * @Field()
     *
     * @return string|null
     */
    public function getPrivateNote(): ?string
    {
        return $this->privateNote;
    }

    /**
     * @param string|null $privateNote
     */
    public function setPrivateNote(?string $privateNote): void
    {
        $this->privateNote = $privateNote;
    }

    /**
     * Gets rating of tasting product in 1-5 range
     *
     * @Field()
     *
     * @return float|null
     */
    public function getRating(): ?float
    {
        return $this->rating;
    }

    /**
     * @param float|null $rating
     */
    public function setRating(?float $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * Gets product review flavours
     *
     * @Field()
     *
     * @return Flavour[]|null
     */
    public function getFlavours(): ?iterable
    {
        return $this->flavours;
    }

}