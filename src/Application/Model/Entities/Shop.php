<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\CountryEnum;
use App\Application\Model\Enum\DayEnum;
use App\Application\Model\Enum\LocaleEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\Enum\ShopTypeEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\TranslationInput;
use App\Application\Model\GraphQLTypes\OpeningHoursTimeInterval;
use App\Application\Model\GraphQLTypes\OpeningHoursTime;
use App\Application\Model\GraphQLTypes\RegularOpeningHours;
use App\Application\Model\GraphQLTypes\ShopStats;
use App\Application\Model\GraphQLTypes\Translation;
use App\Application\Model\Timestampable;
use App\Application\Services\SecurityService\AuthorizationService;
use App\Application\Utils;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\ORMException;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Security;

/** GraphQLite annotations:
 * @Type(name="Shop")
 *
 * Shop
 *
 * ORM annotations:
 * @ORM\Table(name="shop", options={"comment":"Shop table"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Shop
{
    use Timestampable;

    #region private fields
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /**
     * @var ShopTypeEnum
     *
     * @ORM\Column(name="type", type="shop_type_enum", nullable=false)
     */
    private $type;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ordering", type="integer", nullable=true)
     */
    private $ordering;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="text", length=65535, nullable=false)
     */
    private $street;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descriptive_number", type="string", length=255, nullable=true)
     */
    private $descriptiveNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="routing_number", type="string", length=255, nullable=true)
     */
    private $routingNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=5, nullable=false)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country", cascade={"persist"})
     * @ORM\JoinColumn(name="country", referencedColumnName="id", nullable=false)
     */
    private $country;

    /**
     * @var ShopEmail[]
     *
     * @ORM\OneToMany(targetEntity="ShopEmail", mappedBy="shop", cascade={"persist", "remove"})
     */
    private $emails;

    /**
     * @var ShopPhoneNumber[]
     *
     * @ORM\OneToMany(targetEntity="ShopPhoneNumber", mappedBy="shop", cascade={"persist", "remove"})
     */
    private $phoneNumbers;

    /**
     * @var Category[]
     *
     * @ORM\OneToMany(targetEntity="Category", mappedBy="shop", cascade={"persist"})
     */
    private $categories;

    /**
     * @var string|null
     *
     * @ORM\Column(name="webpage", type="text", length=65535, nullable=true)
     */
    private $webpage;

    /**
     * @var ShopTranslation[]
     *
     * @ORM\OneToMany(targetEntity="ShopTranslation", mappedBy="shop", indexBy="language_id", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var bool
     *
     * @ORM\Column(name="booking_available", type="boolean", nullable=false, options={"comment":"Is the online booking available in the shop?"})
     */
    private $bookingAvailable;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $latitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="external_reservation_url", type="text", length=65535, nullable=true, options={"comment":"Link to the external online reservation system"})
     */
    private $externalReservationURL;

    /**
     * @var string|null
     *
     * @ORM\Column(name="eshop_url", type="text", length=65535, nullable=true)
     */
    private $eshopURL;

    /**
     * @var string|null
     *
     * @ORM\Column(name="main_image_identifier", type="text", length=65535, nullable=true)
     */
    private $mainImageIdentifier;

    /**
     * @var OpeningHours[]|null
     *
     * @ORM\OneToMany(targetEntity="OpeningHours", mappedBy="shop", cascade={"persist", "remove"})
     */
    private $openingHours;

    /**
     * @var OpeningHoursException[]|null
     *
     * @ORM\OneToMany(targetEntity="OpeningHoursException", mappedBy="shop", cascade={"persist", "remove"})
     */
    private $openingHoursExceptions;

    /**
     * @var SocialLink[]|null
     *
     * @ORM\OneToMany(targetEntity="SocialLink", mappedBy="shop", cascade={"persist", "remove"})
     */
    private $socialLinks;

    /** Users that have this shop as main
     *
     * @var User[]
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="mainShop", cascade={"persist"})
     */
    private $mainShopUsers;

    /** Users that have this shop as favourite
     *
     * @var UserFavouriteShop[]
     *
     * @ORM\OneToMany(targetEntity="UserFavouriteShop", mappedBy="shop", indexBy="user_id", cascade={"persist", "remove"})
     *
     */
    private $favouriteUsers;

    /** Bonuses of this shop
     *
     * @var Bonus[]
     *
     * @ORM\OneToMany(targetEntity="Bonus", mappedBy="shop", indexBy="id", cascade={"persist", "remove"})
     *
     */
    private $bonuses;

    /** Users that are admins for this shop
     *
     * @var ShopAdmin[]
     *
     * @ORM\OneToMany(targetEntity="ShopAdmin", mappedBy="shop", indexBy="admin_id", cascade={"persist", "remove"})
     *
     */
    private $admins;

    /** Users that are vip users for this shop
     *
     * @var ShopVipUser[]
     *
     * @ORM\OneToMany(targetEntity="ShopVipUser", mappedBy="shop", indexBy="vip_user_id", cascade={"persist", "remove"})
     */
    private $vipUsers;

    /** Users that are bonus users for this shop
     *
     * @var ShopBonusUser[]
     *
     * @ORM\OneToMany(targetEntity="ShopBonusUser", mappedBy="shop", indexBy="bonus_user_id", cascade={"persist", "remove"})
     */
    private $bonusUsers;

    /** Vip benefits, that this shop offers
     *
     * @var VipBenefit[]|null
     *
     * @ORM\OneToMany(targetEntity="VipBenefit", mappedBy="shop", cascade={"persist", "remove"})
     */
    private $vipBenefits;

    /** Shop notifications
     *
     * @var Notification[]|null
     *
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="shop", cascade={"persist", "remove"})
     */
    private $notifications;

    /** Shop events
     *
     * @var Event[]|null
     *
     * @ORM\OneToMany(targetEntity="Event", mappedBy="shop", cascade={"persist", "remove"})
     */
    private $events;

    #endregion


    /**
     * Shop constructor.
     * @param EntityStateEnum $state
     * @param ShopTypeEnum $type
     * @param string $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string $postcode
     * @param string $city
     * @param Country $country
     * @param array|null $emails
     * @param array|null $phoneNumbers
     * @param string|null $webpage
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $descriptionTranslations
     * @param bool $bookingAvailable
     * @param float $longitude
     * @param float $latitude
     * @param string|null $eshopURL
     * @param string|null $externalReservationURL
     * @param string|null $mainImageIdentifier
     * @param RegularOpeningHours|null $openingHours
     * @param OpeningHoursException[]|null $openingHoursExceptions
     * @param SocialLink[]|null $socialLinks
     * @param EntityManager $em
     * @return Shop
     * @throws Exception
     */
    public static function create(
        EntityStateEnum $state,
        ShopTypeEnum $type,
        string $street,
        ?string $descriptiveNumber,
        ?string $routingNumber,
        string $postcode,
        string $city,
        Country $country,
        ?array $emails,
        ?array $phoneNumbers,
        ?string $webpage,
        array $nameTranslations,
        ?array $descriptionTranslations,
        bool $bookingAvailable,
        float $longitude,
        float $latitude,
        ?string $eshopURL,
        ?string $externalReservationURL,
        ?string $mainImageIdentifier,
        ?RegularOpeningHours $openingHours,
        ?array $openingHoursExceptions,
        ?array $socialLinks,
        EntityManager $em
    ) : Shop {
        $instance = new self();

        $instance->state = $state;
        $instance->type = $type;
        $instance->street = $street;
        $instance->descriptiveNumber = $descriptiveNumber;
        $instance->routingNumber = $routingNumber;
        $instance->postcode = $postcode;
        $instance->city = $city;
        $instance->country = $country;
        $instance->setEmails($emails, $em);
        $instance->setPhoneNumbers($phoneNumbers, $em);
        $instance->webpage = $webpage;
        $instance->setNameTranslations($nameTranslations);
        $instance->setDescriptionTranslations($descriptionTranslations);
        $instance->bookingAvailable = $bookingAvailable;
        $instance->longitude = $longitude;
        $instance->latitude = $latitude;
        $instance->eshopURL = $eshopURL;
        $instance->externalReservationURL = $externalReservationURL;
        $instance->mainImageIdentifier = $mainImageIdentifier;
        $instance->setOpeningHours($openingHours, $em);
        $instance->setOpeningHoursExceptions($openingHoursExceptions, $em);
        $instance->setSocialLinks($socialLinks, $em);

        return $instance;
    }

    #region getters and setters
    /**
     * Gets shop's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @Field()
     * @Security("this.canGetPrivateShopInfo()")
     *
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @Field()
     *
     * @return ShopTypeEnum
     */
    public function getType(): ShopTypeEnum
    {
        return $this->type;
    }

    /**
     * Gets shop's ordering number.
     *
     * @Field()
     *
     * @return int|null
     */
    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    /**
     * Gets shop's street
     *
     * @Field()
     *
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * Gets shop's descriptive number
     *
     * @Field()
     *
     * @return string|null
     */
    public function getDescriptiveNumber(): ?string
    {
        return $this->descriptiveNumber;
    }

    /**
     * Gets shop's routing number
     *
     * @Field()
     *
     * @return string|null
     */
    public function getRoutingNumber(): ?string
    {
        return $this->routingNumber;
    }

    /**
     * Gets shop's postcode
     *
     * @Field()
     *
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * Gets shop's city
     *
     * @Field()
     *
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * Gets shop's country
     *
     * @Field()
     *
     * @return CountryEnum
     */
    public function getCountry(): CountryEnum
    {
        return $this->country->getCountryCode();
    }

    /**
     * Gets shop's emails
     *
     * @Field()
     *
     * @return ShopEmail[]|null
     */
    public function getEmails(): ?iterable
    {
        return $this->emails;
    }

    /**
     * Gets shop's phone numbers
     *
     * @Field()
     *
     * @return ShopPhoneNumber[]|null
     */
    public function getPhoneNumbers(): ?iterable
    {
        return $this->phoneNumbers;
    }

    /**
     * Gets shop's web page
     *
     * @Field()
     *
     * @return string|null
     */
    public function getWebpage(): ?string
    {
        return $this->webpage;
    }

    /**
     * Gets translated name of shop based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string
     */
    public function getName(): string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getName", $this->translations);
    }

    /**
     * Gets shop's name translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getNameTranslations(): array
    {
        return Utils::getAllTranslations("getName", $this->translations);
    }

    /**
     * Gets translated description of shop based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getDescription", $this->translations);
    }

    /**
     * Gets shop's bonus program's description translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getBonusProgramDescriptionTranslations(): array
    {
        return Utils::getAllTranslations("getBonusProgramDescription", $this->translations);
    }

    /**
     * Gets translated description of bonus program of shop based on header Accept-Language. If the given language is not provided,
     * then it returns first available translated value in this order: 1. Czech, 2. Slovak, 3. English
     *
     * @Field()
     *
     * @return string|null
     */
    public function getBonusProgramDescription(): ?string
    {
        $preferredLocale = "";
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $preferredLocale = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

        return Utils::getFirstAvailableTranslation($preferredLocale,"getBonusProgramDescription", $this->translations);
    }

    /**
     * Gets shop's description translations
     *
     * @Field()
     *
     * @return Translation[]
     */
    public function getDescriptionTranslations(): array
    {
        return Utils::getAllTranslations("getDescription", $this->translations);
    }

    /**
     * Is online booking available in the shop?
     *
     * @Field()
     *
     * @return bool
     */
    public function isBookingAvailable(): bool
    {
        return $this->bookingAvailable;
    }

    /**
     * Gets shop's longitude
     *
     * @Field()
     *
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * Gets shop's latitude
     *
     * @Field()
     *
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * Gets shop's eshop URL
     *
     * @Field()
     *
     * @return string|null
     */
    public function getEshopURL(): ?string
    {
        return $this->eshopURL;
    }

    /**
     * Gets shop's external reservation URL
     *
     * @Field()
     *
     * @return string|null
     */
    public function getExternalReservationURL(): ?string
    {
        return $this->externalReservationURL;
    }

    /**
     * Gets shop's main image URL
     *
     * @Field()
     *
     * @param string $width
     * @param string $height
     * @param string $format
     * @return string|null
     */
    public function getMainImageURL(string $width = '500', string $height = '500', string $format = 'webp'): ?string
    {
        if (is_null($this->mainImageIdentifier))
            return "";
        return Utils::buildMediaDownloadURL($this->mainImageIdentifier, $width, $height, $format);
    }

    /**
     * @return string|null
     */
    public function getMainImageIdentifier(): ?string
    {
        return $this->mainImageIdentifier;
    }

    /**
     * Is the shop open now?
     *
     * @Field()
     *
     * @return bool
     * @throws Exception
     */
    public function getIsOpen(): bool
    {
        date_default_timezone_set('Europe/Prague');
        $currentTimestamp = time();
        $currentDay = date('l', $currentTimestamp);
        $currentTime = strtotime(date('H:i', $currentTimestamp));
        $currentDate = date('Y-m-d', $currentTimestamp);

        $result = false;
        //first take a look at the regular opening hours
        if (is_array($this->openingHours) || is_object($this->openingHours)) {
            foreach ($this->openingHours as $interval) {
                if (strtolower($interval->getDay()) == strtolower($currentDay)) {
                    //if current time is inside any interval, set to true
                    if ($this->isInInterval($currentTime, $interval)) {
                        $result = true;
                    }
                }
            }
        }
        //then take a look at the exceptions in regular opening hours
        if (is_array($this->openingHoursExceptions) || is_object($this->openingHoursExceptions)) {
            foreach ($this->openingHoursExceptions as $exception) {
                if (date('Y-m-d', $exception->getDate()->getTimestamp()) == $currentDate) {
                    //there is an exception in current date, so set $result implicitly to false
                    $result = false;
                    //if current time is inside any interval, set to true
                    foreach ($exception->getOpeningHoursExceptionIntervals() as $interval) {
                        if ($this->isInInterval($currentTime, $interval)) {
                            $result = true;
                        }
                    }
                }
            }
        }
        return $result;
    }

    private function isInInterval($currentTime, $interval) : bool {
        $fromTime = strtotime($interval->getFromHour() . ':' . $interval->getFromMinute());
        $untilTime = strtotime($interval->getUntilHour() . ':' . $interval->getUntilMinute());
        return $fromTime <= $currentTime && $currentTime <= $untilTime;
    }

    /**
     * Gets shop's opening hours
     *
     * @Field()
     *
     * @return RegularOpeningHours
     */
    public function getOpeningHours(): RegularOpeningHours
    {
        $result = new RegularOpeningHours();

        if(is_null($this->openingHours)) return $result;

        foreach ($this->openingHours as $openingHour) {
            $timeFrom = OpeningHoursTime::create($openingHour->getFromHour(), $openingHour->getFromMinute());
            $timeUntil = OpeningHoursTime::create($openingHour->getUntilHour(), $openingHour->getUntilMinute());
            $timeInterval = OpeningHoursTimeInterval::create($timeFrom, $timeUntil);
            switch ($openingHour->getDay()) {
                case DayEnum::MONDAY:
                    $result->addMondayInterval($timeInterval);
                    break;
                case DayEnum::TUESDAY:
                    $result->addTuesdayInterval($timeInterval);
                    break;
                case DayEnum::WEDNESDAY:
                    $result->addWednesdayInterval($timeInterval);
                    break;
                case DayEnum::THURSDAY:
                    $result->addThursdayInterval($timeInterval);
                    break;
                case DayEnum::FRIDAY:
                    $result->addFridayInterval($timeInterval);
                    break;
                case DayEnum::SATURDAY:
                    $result->addSaturdayInterval($timeInterval);
                    break;
                case DayEnum::SUNDAY:
                    $result->addSundayInterval($timeInterval);
                    break;
            }
        }

        return $result;
    }

    /**
     * Gets shop's opening hours exceptions
     *
     * @Field()
     *
     * @return OpeningHoursException[]|null
     */
    public function getOpeningHoursExceptions(): ?iterable
    {
        return $this->openingHoursExceptions;
    }

    /**
     * Gets shop's opening hours exceptions that start in future
     *
     * @Field()
     *
     * @return OpeningHoursException[]|null
     * @throws Exception
     */
    public function getFutureOpeningHoursExceptions(): ?iterable
    {
        $futureOpeningHoursExceptions = [];

        $currentDate = (new DateTimeImmutable())->setTimestamp(strtotime('today midnight'));

        if (is_array($this->openingHoursExceptions) || is_object($this->openingHoursExceptions))
            foreach($this->openingHoursExceptions as $openingHoursException) {
                if($openingHoursException->getDate() >= $currentDate) {
                    $futureOpeningHoursExceptions[] = $openingHoursException;
                }
            }

        return $futureOpeningHoursExceptions;
    }

    /**
     * Gets shop's social links
     *
     * @Field()
     *
     * @return null|SocialLink[]
     */
    public function getSocialLinks(): ?iterable
    {
        return $this->socialLinks;
    }

    /**
     * Is shop favourite for logged in user?
     * Returns null if the user is not logged in.
     *
     * @Field()
     *
     * @return bool|null
     */
    public function isFavourite(): ?bool
    {
        if (isset($_SESSION['uid']) && (is_array($this->favouriteUsers) || is_object($this->favouriteUsers))) {
            foreach ($this->favouriteUsers as $userBinding) {
                if ($userBinding->getUser()->getUid() == $_SESSION['uid'])
                    return true;
            }
            return false;
        }
        return null;
    }

    /**
     * Is the shop VIP for logged in user?
     * Returns null if the user is not logged in.
     *
     * @Field()
     *
     * @return bool|null
     * @throws Exception
     */
    public function isVip(): ?bool
    {
        if (isset($_SESSION['uid']) && (is_array($this->vipUsers) || is_object($this->vipUsers))) {
            /** @var ShopVipUser $userBinding */
            foreach ($this->vipUsers as $userBinding) {
                if ($userBinding->getVipUser()->getUid() == $_SESSION['uid'])
                    return !$userBinding->isExpired();
            }
            return false;
        }
        return null;
    }

    /**
     * @param User $user
     * @return bool|null
     */
    public function isUserVip(User $user): ?bool
    {
        if (is_array($this->vipUsers) || is_object($this->vipUsers)) {
            /** @var ShopVipUser $userBinding */
            foreach ($this->vipUsers as $userBinding) {
                if ($userBinding->getVipUser()->getUid() == $user->getUid())
                    return true;
            }
            return false;
        }
        return null;
    }

    /**
     * Gets shop's admins
     *
     * @Field()
     * @Security("this.canGetPrivateShopInfo()")
     *
     * @return User[]
     */
    public function getAdmins(): iterable {
        $result = [];
        if (is_array($this->admins) || is_object($this->admins)) {
            foreach ($this->admins as $adminBinding) {
                $result[] = $adminBinding->getAdmin();
            }
        }
        return $result;
    }

    /**
     * Gets shop's VIP users
     *
     * @Field()
     * @Security("this.canGetPrivateShopInfo()")
     *
     * @return ShopVipUser[]
     * @throws Exception
     */
    public function getVipUsers(): iterable {
        $result = [];
        if (is_array($this->vipUsers) || is_object($this->vipUsers)) {
            foreach ($this->vipUsers as $vipUser) {
                if (!$vipUser->isExpired())
                    $result[] = $vipUser;
            }
        }
        return $result;
    }

    /** Get Vip User information based on current user in session
     *
     * @Field()
     *
     * @return ShopVipUser|null
     * @throws Exception
     */
    public function getMyVipStatus(): ?ShopVipUser {
        if (isset($_SESSION['uid']) && (is_array($this->vipUsers) || is_object($this->vipUsers))) {
            /** @var ShopVipUser $userBinding */
            foreach ($this->vipUsers as $userBinding) {
                if ($userBinding->getVipUser()->getUid() == $_SESSION['uid'])
                    return $userBinding;
            }
        }
        return null;
    }

    /** VIP users of this shop
     *
     * @return User[]
     * @throws Exception
     */
    public function getVipUsersInternal(): iterable {
        $result = [];
        if (is_array($this->vipUsers) || is_object($this->vipUsers)) {
            /** @var ShopVipUser $vipUser */
            foreach ($this->vipUsers as $vipUser) {
                if (!$vipUser->isExpired())
                    $result[] = $vipUser->getVipUser();
            }
        }
        return $result;
    }

    /** Bonus users of this shop
     *
     * @return User[]
     */
    public function getBonusUsersInternal(): iterable {
        $result = [];
        if (is_array($this->bonusUsers) || is_object($this->bonusUsers)) {
            /** @var ShopBonusUser $bonusUser */
            foreach ($this->bonusUsers as $bonusUser) {
                $result[] = $bonusUser->getBonusUser();
            }
        }
        return $result;
    }

    /**
     * Gets users which have the shop set as main
     *
     * @Field()
     * @Security("this.canGetPrivateShopInfo()")
     *
     * @return User[]
     */
    public function getMainShopUsers(): iterable {
        $result = [];
        if (is_array($this->mainShopUsers) || is_object($this->mainShopUsers)) {
            foreach ($this->mainShopUsers as $mainShopUser) {
                $result[] = $mainShopUser;
            }
        }
        return $result;
    }


    /**
     * Gets users which have the shop set as favourite
     *
     * @Field()
     * @Security("this.canGetPrivateShopInfo()")
     *
     * @return User[]
     */
    public function getFavouriteUsers(): iterable {
        $result = [];
        if (is_array($this->favouriteUsers) || is_object($this->favouriteUsers)) {
            foreach ($this->favouriteUsers as $favouriteUser) {
                $result[] = $favouriteUser->getUser();
            }
        }
        return $result;
    }

    /**
     * @param User $user
     * @return ShopVipUser|null
     */
    public function getVipUserBinding(User $user): ?ShopVipUser {
        $result = null;

        if (is_array($this->vipUsers) || is_object($this->vipUsers)) {
            foreach ($this->vipUsers as $vipUser) {
                if ($vipUser->getVipUser()->getId() == $user->getId())
                    $result = $vipUser;
            }
        }

        return $result;
    }

    /**
     * Gets shop's VIP benefits
     *
     * @Field()
     * @Security("this.canGetPrivateShopInfo()")
     *
     * @return VipBenefit[]|null
     */
    public function getVipBenefits(): ?iterable {
        return $this->vipBenefits;
    }

    /**
     * Does the shop have menu available?
     *
     * @Field()
     *
     * @return bool
     */
    public function isMenuAvailable(): bool {
        return ((is_array($this->categories) || is_object($this->categories)) && count($this->categories) > 0);
    }

    /**
     * Does the shop have bonus program available?
     *
     * @Field()
     *
     * @return bool
     */
    public function isBonusProgramAvailable(): bool {
        return ((is_array($this->bonuses) || is_object($this->bonuses)) && count($this->bonuses) > 0);
    }

    /**
     * Does the shop have events available?
     *
     * @Field()
     *
     * @return bool
     */
    public function isEventsAvailable(): bool {
        return ((is_array($this->events) || is_object($this->events)) && count($this->events) > 0);
    }

    /**
     * Does the shop have tastings available?
     *
     * @Field()
     *
     * @return bool
     */
    public function isTastingsAvailable(): bool {
        if (is_array($this->events) || is_object($this->events)) {
            foreach ($this->events as $event) {
                if ($event instanceof Tasting)
                    return true;
            }
        }
        return false;
    }

    public function addFavouriteUser(User $user)
    {
        if (!isset($this->favouriteUsers[$user->getId()])) {
            $binding = UserFavouriteShop::create($this, $user);
            $this->favouriteUsers[$user->getId()] = $binding;
            $user->addFavouriteShop($this, $binding);
        }
    }

    /**
     * @param User $user
     * @param EntityManager $em
     * @throws ORMException
     */
    public function removeFavouriteUser(User $user, EntityManager $em)
    {
        if (isset($this->favouriteUsers[$user->getId()])) {
            $user->removeFavouriteShop($this);
            $em->remove($this->favouriteUsers[$user->getId()]);
            unset($this->favouriteUsers[$user->getId()]);
        }
    }

    public function addAdmin(User $user)
    {
        if (!isset($this->admins[$user->getId()])) {
            $binding = ShopAdmin::create($this, $user);
            $this->admins[$user->getId()] = $binding;
            $user->addAdminShop($this, $binding);
        }
    }

    /**
     * @param User $user
     * @param EntityManager $em
     * @throws ORMException
     */
    public function removeAdmin(User $user, EntityManager $em)
    {
        if (isset($this->admins[$user->getId()])) {
            $user->removeAdminShop($this);
            $em->remove($this->admins[$user->getId()]);
            unset($this->admins[$user->getId()]);
        }
    }

    /**
     * @param User $user
     * @param DateTimeImmutable|null $expiryDate
     * @return ShopVipUser|null
     */
    public function addVipUser(User $user, ?DateTimeImmutable $expiryDate): ?ShopVipUser
    {
        if (!isset($this->vipUsers[$user->getId()])) {
            $binding = ShopVipUser::create($this, $user, $expiryDate);
            $this->vipUsers[$user->getId()] = $binding;
            $user->addVipShop($this, $binding);
        }
        $this->vipUsers[$user->getId()]->setExpiryDate($expiryDate);        //change expiry date
        return $this->vipUsers[$user->getId()];
    }

    /**
     * @param User $user
     * @param EntityManager $em
     * @throws ORMException
     */
    public function removeVipUser(User $user, EntityManager $em)
    {
        if (isset($this->vipUsers[$user->getId()])) {
            $user->removeVipShop($this);
            $em->remove($this->vipUsers[$user->getId()]);
            unset($this->vipUsers[$user->getId()]);
        }
    }

    public function addVipBenefit(VipBenefit $vipBenefit)
    {
        if (is_array($this->vipBenefits) || is_object($this->vipBenefits)) {
            foreach ($this->vipBenefits as $currentBenefit) {
                if ($currentBenefit->getId() == $vipBenefit->getId())
                    return;
            }
        }
        $this->vipBenefits[] = $vipBenefit;
    }

    /**
     * @param VipBenefit $vipBenefit
     * @param EntityManager $em
     * @throws ORMException
     */
    public function removeVipBenefit(VipBenefit $vipBenefit, EntityManager $em)
    {
        if (is_array($this->vipBenefits) || is_object($this->vipBenefits)) {
            foreach ($this->vipBenefits as $currentBenefit) {
                if ($currentBenefit->getId() == $vipBenefit->getId()) {
                    $em->remove($currentBenefit);
                    unset($currentBenefit);
                    return;
                }
            }
        }
    }

    /**
     * Is this shop set as main for the logged in user?
     * Returns null if the user is not logged in.
     *
     * @Field()
     *
     * @return bool|null
     */
    public function isMainShop(): ?bool
    {
        if (isset($_SESSION['uid']) && (is_array($this->mainShopUsers) || is_object($this->mainShopUsers))) {
            foreach ($this->mainShopUsers as $user) {
                if ($user->getUid() == $_SESSION['uid'])
                    return true;
            }
            return false;
        }
        return null;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @param ShopTypeEnum $type
     */
    public function setType(ShopTypeEnum $type): void
    {
        $this->type = $type;
    }

    /**
     * @param int|null $ordering
     */
    public function setOrdering(?int $ordering): void
    {
        $this->ordering = $ordering;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @param string|null $descriptiveNumber
     */
    public function setDescriptiveNumber(?string $descriptiveNumber): void
    {
        $this->descriptiveNumber = $descriptiveNumber;
    }

    /**
     * @param string|null $routingNumber
     */
    public function setRoutingNumber(?string $routingNumber): void
    {
        $this->routingNumber = $routingNumber;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode(string $postcode): void
    {
        $this->postcode = $postcode;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @param Country $country
     */
    public function setCountry(Country $country): void
    {
        $this->country = $country;
    }

    /**
     * @param ShopEmail[]|null $emails
     * @param EntityManager $em
     * @throws ORMException
     */
    public function setEmails(?array $emails, EntityManager $em): void
    {
        if (is_array($this->emails) || is_object($this->emails)) {
            foreach ($this->emails as $oldEmail) {
                $foundItem = array_search($oldEmail->getEmail(), $emails);
                if (!is_bool($foundItem)) {     //if the new email is in the list of old emails
                    unset($emails[$foundItem]);
                    continue;
                }
                $em->remove($oldEmail);
                unset($oldEmail);
            }
        }
        if (is_array($emails) || is_object($emails)) {
            foreach ($emails as $newEmail) {
                $newShopsEmail = ShopEmail::create($this, $newEmail);
                $this->emails[] = $newShopsEmail;
            }
        }
    }

    /**
     * @param ShopPhoneNumber[]|null $phoneNumbers
     * @param EntityManager $em
     * @throws ORMException
     */
    public function setPhoneNumbers(?array $phoneNumbers, EntityManager $em): void
    {
        if (is_array($this->phoneNumbers) || is_object($this->phoneNumbers)) {
            foreach ($this->phoneNumbers as $oldPhoneNumber) {
                $foundItem = array_search($oldPhoneNumber->getPhoneNumber(), $phoneNumbers);
                if (!is_bool($foundItem)) {
                    unset($phoneNumbers[$foundItem]);
                    continue;
                }
                $em->remove($oldPhoneNumber);
                unset($oldPhoneNumber);
            }
        }
        if (is_array($phoneNumbers) || is_object($phoneNumbers)) {
            foreach ($phoneNumbers as $newPhoneNumber) {
                $newShopsPhoneNumber = ShopPhoneNumber::create($this, $newPhoneNumber);
                $this->phoneNumbers[] = $newShopsPhoneNumber;
            }
        }
    }

    /**
     * @param string|null $webpage
     */
    public function setWebpage(?string $webpage): void
    {
        $this->webpage = $webpage;
    }

    /**
     * @param bool $bookingAvailable
     */
    public function setBookingAvailable(bool $bookingAvailable): void
    {
        $this->bookingAvailable = $bookingAvailable;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @param string|null $eshopURL
     */
    public function setEshopURL(?string $eshopURL): void
    {
        $this->eshopURL = $eshopURL;
    }

    /**
     * @param string|null $externalReservationURL
     */
    public function setExternalReservationURL(?string $externalReservationURL): void
    {
        $this->externalReservationURL = $externalReservationURL;
    }

    /**
     * @param string|null $mainImageIdentifier
     */
    public function setMainImageIdentifier(?string $mainImageIdentifier): void
    {
        $this->mainImageIdentifier = $mainImageIdentifier;
    }

    /**
     * @param RegularOpeningHours|null $openingHours
     * @param EntityManager $em
     * @throws ORMException
     * @throws Exception
     */
    public function setOpeningHours(?RegularOpeningHours $openingHours, EntityManager $em): void
    {
        if (is_array($this->openingHours) || is_object($this->openingHours)) {
            foreach ($this->openingHours as $oldOpeningHours) {
                $em->remove($oldOpeningHours);
                unset($oldOpeningHours);
            }
        }

        if (!is_null($openingHours)) {
            $this->createOpeningHours($openingHours->getMonday(), new DayEnum(DayEnum::MONDAY));
            $this->createOpeningHours($openingHours->getTuesday(), new DayEnum(DayEnum::TUESDAY));
            $this->createOpeningHours($openingHours->getWednesday(), new DayEnum(DayEnum::WEDNESDAY));
            $this->createOpeningHours($openingHours->getThursday(), new DayEnum(DayEnum::THURSDAY));
            $this->createOpeningHours($openingHours->getFriday(), new DayEnum(DayEnum::FRIDAY));
            $this->createOpeningHours($openingHours->getSaturday(), new DayEnum(DayEnum::SATURDAY));
            $this->createOpeningHours($openingHours->getSunday(), new DayEnum(DayEnum::SUNDAY));
        }
    }

    /**
     * @param Bonus $bonus
     */
    public function addBonus(Bonus $bonus)
    {
        $this->bonuses[$bonus->getId()] = $bonus;
    }

    /**
     * @param Bonus $bonus
     * @param EntityManager $em
     * @throws Exception
     */
    public function removeBonus(Bonus $bonus, EntityManager $em)
    {
        $foundBonus = $this->bonuses[$bonus->getId()];
        $em->remove($foundBonus);
        unset($foundBonus);
    }

    /**
     * Gets shop's bonuses
     *
     * @Field()
     *
     * @return Bonus[]|null
     */
    public function getBonuses(): ?iterable
    {
        return $this->bonuses;
    }

    /**
     * Gets shop's notifications
     *
     * @Field()
     *
     * @param int $limit
     * @param int|null $offset
     * @return Notification[]|null
     * @throws Exception
     */
    public function getNotifications(int $limit = 10, int $offset = null): ?iterable
    {
        if (is_array($this->notifications) || is_object($this->notifications)) {
            $result = $this->notifications->toArray();
            usort($result,
                function(Notification $c1, Notification $c2) {
                    return $c1->getCreated() < $c2->getCreated();
                });
            return array_slice($result, $offset, $limit);
        }
        return null;
    }

    /**
     * @param Notification[]|null $notifications
     */
    public function setNotifications(?array $notifications): void
    {
        $this->notifications = $notifications;
    }

    /**
     * @param OpeningHoursTimeInterval[]|null $intervals
     * @param DayEnum $day
     * @throws Exception
     */
    private function createOpeningHours(?array $intervals, DayEnum $day) {
        if (!is_null($intervals)) {
            foreach ($intervals as $interval) {
                $this->openingHours[] = OpeningHours::create(
                    $day,
                    $interval->getFrom()->getHours(),
                    $interval->getFrom()->getMinutes(),
                    $interval->getUntil()->getHours(),
                    $interval->getUntil()->getMinutes(),
                    $this
                );
            }
        }
    }

    /**
     * @param OpeningHoursException[]|null $openingHoursExceptions
     * @param EntityManager $em
     * @throws ORMException
     */
    public function setOpeningHoursExceptions(?array $openingHoursExceptions, EntityManager $em): void
    {
        if (is_array($this->openingHoursExceptions) || is_object($this->openingHoursExceptions)) {
            foreach ($this->openingHoursExceptions as $oldOpeningHoursExceptions) {
                $em->remove($oldOpeningHoursExceptions);
                unset($oldOpeningHoursExceptions);
            }
        }
        if (is_array($openingHoursExceptions) || is_object($openingHoursExceptions)) {
            foreach ($openingHoursExceptions as $openingHoursException) {
                $openingHoursException->setShop($this);
            }
            $this->openingHoursExceptions = $openingHoursExceptions;
        }
    }

    /**
     * @param SocialLink[]|null $socialLinks
     * @param EntityManager $em
     * @throws ORMException
     */
    public function setSocialLinks(?array $socialLinks, EntityManager $em): void
    {
        if (is_array($this->socialLinks) || is_object($this->socialLinks))
            foreach ($this->socialLinks as $socialLink) {
                $em->remove($socialLink);
                unset($socialLink);
            }
        if (is_array($socialLinks) || is_object($socialLinks))
            foreach ($socialLinks as $socialLink) {
                $socialLink->setShop($this);
                $this->socialLinks[] = $socialLink;
            }
    }

    /**
     * @param TranslationInput[] $nameTranslations
     * @throws Exception
     */
    public function setNameTranslations(array $nameTranslations)
    {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setName(null);
            }

        if (!isset($nameTranslations))
            return;

        foreach ($nameTranslations as $nameTranslation) {
            if (LocaleEnum::isValid($nameTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($nameTranslation->getLanguageCode());
                if (is_null($language))
                    throw new Exception("Given language is not supported");
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = ShopTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setName($nameTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * @param TranslationInput[]|null $descriptionTranslations
     * @throws Exception
     */
    public function setDescriptionTranslations(?array $descriptionTranslations)
    {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setDescription(null);
            }

        if (!isset($descriptionTranslations))
            return;

        foreach ($descriptionTranslations as $descriptionTranslation) {
            if (LocaleEnum::isValid($descriptionTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($descriptionTranslation->getLanguageCode());
                if (is_null($language))
                    continue;
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = ShopTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setDescription($descriptionTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * @param TranslationInput[]|null $descriptionTranslations
     * @throws Exception
     */
    public function setBonusProgramDescriptionTranslations(?array $descriptionTranslations)
    {
        if (is_array($this->translations) || is_object($this->translations))
            foreach ($this->translations as $translation) {
                $translation->setBonusProgramDescription(null);
            }

        if (!isset($descriptionTranslations))
            return;

        foreach ($descriptionTranslations as $descriptionTranslation) {
            if (LocaleEnum::isValid($descriptionTranslation->getLanguageCode())) {
                $language = Language::getLanguageByLocale($descriptionTranslation->getLanguageCode());
                if (is_null($language))
                    continue;
                if (!isset($this->translations[$language->getLanguageId()])) {
                    $translation = ShopTranslation::create($language, $this);
                } else {
                    $translation = $this->translations[$language->getLanguageId()];
                }
                $translation->setBonusProgramDescription($descriptionTranslation->getValue());
                $this->translations[$language->getLanguageId()] = $translation;
            } else {
                throw new Exception("Given language is not supported");
            }
        }
    }

    /**
     * Gets shop's stats
     *
     * @Field()
     * @Security("this.canGetPrivateShopInfo()")
     *
     * @return ShopStats
     * @throws Exception
     */
    public function getShopStats(): ShopStats {
        return ShopStats::create(
            $this->getFavouriteUsers(),
            $this->getMainShopUsers(),
            $this->getVipUsersInternal(),
            $this->getBonusUsersInternal()
        );
    }

    #endregion

    /**
     * @return bool
     */
    public function canGetPrivateShopInfo(): bool
    {
        $authorizationService = AuthorizationService::getInstance();
        return $authorizationService->isAllowed(new ScopeRightsEnum(ScopeRightsEnum::CAN_GET_PRIVATE_SHOP_INFO), $this);
    }
}
