<?php


namespace App\Application\Model\Entities;


use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * VipUserBenefit
 *
 * @ORM\Table(name="vip_user_benefit", options={"comment":"VIP benefit assigned to VIP user"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class VipUserBenefit
{
    use Timestampable;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="vip_benefit_id", type="integer", nullable=false)
     */
    private $vipBenefitId;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="shop_vip_user_id", type="integer", nullable=false)
     */
    private $shopVipUserId;

    /**
     * @var VipBenefit
     *
     * @ORM\ManyToOne(targetEntity="VipBenefit", inversedBy="vipUsers", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vip_benefit_id", referencedColumnName="id")
     * })
     */
    private $vipBenefit;

    /**
     * @var ShopVipUser
     *
     * @ORM\ManyToOne(targetEntity="ShopVipUser", inversedBy="vipBenefits", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_vip_user_id", referencedColumnName="id")
     * })
     */
    private $shopVipUser;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /** VipUserBenefit constructor
     *
     * @param VipBenefit $vipBenefit
     * @param ShopVipUser $shopVipUser
     * @return VipUserBenefit
     */
    public static function create(
        VipBenefit $vipBenefit,
        ShopVipUser $shopVipUser
    ): VipUserBenefit
    {
        $instance = new self();

        $instance->vipBenefitId = $vipBenefit->getId();
        $instance->shopVipUserId = $shopVipUser->getId();
        $instance->vipBenefit = $vipBenefit;
        $instance->shopVipUser = $shopVipUser;
        $instance->state = EntityStateEnum::ACTIVE;

        return $instance;
    }

    /**
     * @return VipBenefit
     */
    public function getVipBenefit(): VipBenefit
    {
        return $this->vipBenefit;
    }

    /**
     * @return int
     */
    public function getVipBenefitId(): int
    {
        return $this->vipBenefitId;
    }

    /**
     * @return ShopVipUser
     */
    public function getShopVipUser(): ShopVipUser
    {
        return $this->shopVipUser;
    }


}