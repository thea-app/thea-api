<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * TastingProductTranslation
 *
 * @ORM\Table(name="tasting_product_translation", options={"comment":"Tasting product translations"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TastingProductTranslation
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /** Many translations to one language
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @var int
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     */
    private $languageId;

    /** Many translations to one tasting  product
     * @var TastingProduct
     *
     * @ORM\ManyToOne(targetEntity="TastingProduct", inversedBy="translations")
     * @ORM\JoinColumn(name="tasting_product_id", referencedColumnName="id")
     */
    private $tastingProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @param Language $language
     * @param TastingProduct $tastingProduct
     *
     * @return TastingProductTranslation
     */
    public static function create(
        Language $language,
        TastingProduct $tastingProduct
    ): TastingProductTranslation
    {
        $instance = new self();

        $instance->language = $language;
        $instance->languageId = $language->getLanguageId();
        $instance->tastingProduct = $tastingProduct;

        return $instance;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Language
     */
    public function getLanguage(): Language
    {
        return $this->language;
    }

}
