<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\DayEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * OpeningHours
 *
 * @ORM\Table(name="opening_hours", options={"comment":"Opening hours of shops"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class OpeningHours
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DayEnum
     *
     * @ORM\Column(name="day", type="day_enum", length=255, nullable=false)
     */
    private $day;

    /**
     * @var int
     *
     * @ORM\Column(name="from_hour", type="integer", nullable=false, options={"comment":"Hour in day, when shop opens"})
     */
    private $fromHour;

    /**
     * @var int
     *
     * @ORM\Column(name="until_hour", type="integer", nullable=false, options={"comment":"Hour in day, when shop closes"})
     */
    private $untilHour;

    /**
     * @var int
     *
     * @ORM\Column(name="from_minute", type="integer", nullable=false, options={"comment":"Minute in hour, when shop opens"})
     */
    private $fromMinute;

    /**
     * @var int
     *
     * @ORM\Column(name="until_minute", type="integer", nullable=false, options={"comment":"Minute in hour, when shop closes"})
     */
    private $untilMinute;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="openingHours", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /**
     * @param DayEnum $day
     * @param int $fromHour
     * @param int $fromMinute
     * @param int $untilHour
     * @param int $untilMinute
     * @param Shop $shop
     * @return OpeningHours
     * @throws Exception
     */
    public static function create(
        DayEnum $day,
        int $fromHour,
        int $fromMinute,
        int $untilHour,
        int $untilMinute,
        Shop $shop
    ): OpeningHours {
        $instance = new self();

        $instance->shop = $shop;
        $instance->day = $day;
        $instance->fromHour = $fromHour;
        $instance->fromMinute = $fromMinute;
        $instance->untilHour = $untilHour;
        $instance->untilMinute = $untilMinute;

        return $instance;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDay(): string
    {
        return $this->day;
    }

    /**
     * @param string $day
     */
    public function setDay(string $day): void
    {
        $this->day = $day;
    }

    /**
     * @return int
     */
    public function getFromHour(): int
    {
        return $this->fromHour;
    }

    /**
     * @param int $fromHour
     */
    public function setFromHour(int $fromHour): void
    {
        $this->fromHour = $fromHour;
    }

    /**
     * @return int
     */
    public function getUntilHour(): int
    {
        return $this->untilHour;
    }

    /**
     * @param int $untilHour
     */
    public function setUntilHour(int $untilHour): void
    {
        $this->untilHour = $untilHour;
    }

    /**
     * @return int
     */
    public function getFromMinute(): int
    {
        return $this->fromMinute;
    }

    /**
     * @param int $fromMinute
     */
    public function setFromMinute(int $fromMinute): void
    {
        $this->fromMinute = $fromMinute;
    }

    /**
     * @return int
     */
    public function getUntilMinute(): int
    {
        return $this->untilMinute;
    }

    /**
     * @param int $untilMinute
     */
    public function setUntilMinute(int $untilMinute): void
    {
        $this->untilMinute = $untilMinute;
    }

}
