<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Timestampable;
use App\Application\Utils;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * ShopVipUser
 *
 * @Type()
 *
 * @ORM\Table(name="shop_vip_user", uniqueConstraints={@ORM\UniqueConstraint(name="shopVipUser_idx", columns={"vip_user_id", "shop_id"})}, options={"comment":"Shop's VIP users"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ShopVipUser
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="code", type="string", length=255, nullable=false, unique=true, options={"comment":"Unique code that identifies the VIP user"})
     */
    private $code;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="vipShops", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vip_user_id", referencedColumnName="id")
     * })
     */
    private $vipUser;

    /**
     * @var int
     * @ORM\Column(name="vip_user_id", type="integer", nullable=false)
     */
    private $vipUserId;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="vipUsers", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /**
     * @var int
     * @ORM\Column(name="shop_id", type="integer", nullable=false)
     */
    private $shopId;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /** Vip benefits for this user
     *
     * @var VipUserBenefit[]
     *
     * @ORM\OneToMany(targetEntity="VipUserBenefit", mappedBy="shopVipUser", indexBy="vip_benefit_id", cascade={"persist", "remove"})
     */
    private $vipBenefits;

    /**
     * @var DateTimeImmutable|null
     *
     * @ORM\Column(name="expiry_date", type="datetime", nullable=true)
     */
    private $expiryDate;

    /** ShopVipUser constructor
     *
     * @param Shop $shop
     * @param User $vipUser
     * @param DateTimeImmutable|null $expiryDate
     * @return ShopVipUser
     */
    public static function create(
        Shop $shop,
        User $vipUser,
        ?DateTimeImmutable $expiryDate
    ): ShopVipUser
    {
        $instance = new self();

        $instance->shop = $shop;
        $instance->shopId = $shop->getId();
        $instance->vipUser = $vipUser;
        $instance->vipUserId = $vipUser->getId();
        $instance->state = EntityStateEnum::ACTIVE;
        $instance->code = Utils::generateEntityUniqueCode($instance);
        $instance->expiryDate = $expiryDate;

        return $instance;
    }

    /**
     * Gets vip user
     *
     * @Field()
     *
     * @return User
     */
    public function getVipUser(): User
    {
        return $this->vipUser;
    }

    /**
     * Gets shop where the user is vip
     *
     * @Field()
     *
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * Gets unique code that identifies the VIP user
     *
     * @Field()
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    public function addVipBenefit(VipBenefit $vipBenefit, VipUserBenefit $vipUserBenefit) {
        $this->vipBenefits[$vipBenefit->getId()] = $vipUserBenefit;
    }

    public function removeVipBenefit(VipBenefit $vipBenefit) {
        unset($this->vipBenefits[$vipBenefit->getId()]);
    }

    /**
     * Gets VIP benefits assigned to this user
     *
     * @Field()
     *
     * @return VipBenefit[]
     */
    public function getVipBenefits(): iterable
    {
        $result = [];
        if (is_array($this->vipBenefits) || is_object($this->vipBenefits)) {
            /** @var VipUserBenefit $vipUserBenefit */
            foreach ($this->vipBenefits as $vipUserBenefit) {
                $result[] = $vipUserBenefit->getVipBenefit();
            }
        }
        return $result;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Gets date and time when VIP membership expires.
     * Returns null if the VIP membership does not expire.
     *
     * @Field()
     *
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function getExpiryDate(): ?DateTimeImmutable
    {
        if (is_null($this->expiryDate))
            return null;

        return new DateTimeImmutable($this->expiryDate->format(DateTimeImmutable::ATOM));
    }

    /**
     * @param DateTimeImmutable|null $expiryDate
     */
    public function setExpiryDate(?DateTimeImmutable $expiryDate): void
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isExpired(): bool {
        if (is_null($this->expiryDate))
            return false;

        $currentTime = new DateTimeImmutable('now', new DateTimeZone('Europe/Prague'));
        return $currentTime > $this->expiryDate;
    }

}