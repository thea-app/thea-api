<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\GlobalRightsEnum;
use App\Application\Model\Enum\ScopeRightsEnum;
use App\Application\Model\Enum\UserTypeEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\UserHasEmailNotificationsEnabledEnum;
use App\Application\Model\Timestampable;
use App\Application\Services\SecurityService\AuthorizationService;
use App\Application\Utils;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Security;

/** GraphQLite annotations
 * @Type(name="User")
 *
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="uid", columns={"uid"}), @ORM\UniqueConstraint(name="email", columns={"email"})}, options={"comment":"User table"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class User
{
    use Timestampable;

    /**
     * User constructor.
     * @param string $uid
     * @param EntityStateEnum $state
     * @param UserTypeEnum $type
     * @param string $email
     * @param string|null $firstName
     * @param string|null $lastName
     * @param int|null $birthYear
     * @param GenderEnum|null $gender
     * @param Shop|null $mainShop
     * @param UserHasEmailNotificationsEnabledEnum $hasEmailNotificationsEnabled
     * @param string|null $userImageIdentifier
     * @return User
     * @throws Exception
     */
    public static function create(string $uid,
                                  EntityStateEnum $state,
                                  UserTypeEnum $type,
                                  string $email,
                                  string $firstName = null,
                                  string $lastName = null,
                                  int $birthYear = null,
                                  GenderEnum $gender = null,
                                  Shop $mainShop = null,
                                  UserHasEmailNotificationsEnabledEnum $hasEmailNotificationsEnabled,
                                  string $userImageIdentifier = null
    )
    {
        $instance = new self();

        $instance->uid = $uid;
        $instance->state = $state;
        $instance->type = $type;
        $instance->email = $email;
        $instance->firstName = $firstName;
        $instance->lastName = $lastName;
        $instance->birthYear = $birthYear;
        $instance->gender = $gender;
        $instance->mainShop = $mainShop;
        $instance->hasEmailNotificationsEnabled = $hasEmailNotificationsEnabled;
        $instance->userImageIdentifier = $userImageIdentifier;

        return $instance;
    }

    #region private fields
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uid", type="string", length=255, unique=true, nullable=false, options={"comment":"Uid from firebase"})
     */
    private $uid;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /**
     * @var UserTypeEnum
     *
     * @ORM\Column(name="type", type="user_type_enum", nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var int
     *
     * @ORM\Column(name="birth_year", type="integer", nullable=true)
     */
    private $birthYear;

    /**
     * @var GenderEnum
     *
     * @ORM\Column(name="gender", type="gender_enum", nullable=true)
     */
    private $gender;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="mainShopUsers", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="main_shop_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $mainShop;

    /** Shops that this user has as favourite
     *
     * @var UserFavouriteShop[]
     *
     * @ORM\OneToMany(targetEntity="UserFavouriteShop", mappedBy="user", indexBy="shop_id", cascade={"persist", "remove"})
     */
    private $favouriteShops;

    /** Products that this user has as favourite
     *
     * @var UserFavouriteProduct[]
     *
     * @ORM\OneToMany(targetEntity="UserFavouriteProduct", mappedBy="user", indexBy="product_id", cascade={"persist", "remove"})
     */
    private $favouriteProducts;

    /** Shops that this user is admin of
     *
     * @var ShopAdmin[]
     *
     * @ORM\OneToMany(targetEntity="ShopAdmin", mappedBy="admin", indexBy="shop_id", cascade={"persist", "remove"})
     */
    private $adminShops;

    /** Shops that this user is vip of
     *
     * @var ShopVipUser[]
     *
     * @ORM\OneToMany(targetEntity="ShopVipUser", mappedBy="vipUser", indexBy="shop_id", cascade={"persist", "remove"})
     */
    private $vipShops;

    /**
     * @var UserHasEmailNotificationsEnabledEnum
     *
     * @ORM\Column(name="has_email_notifications_enabled", type="user_has_email_notification_enabled_enum", nullable=false)
     */
    private $hasEmailNotificationsEnabled;

    /**
     * @var string|null
     *
     * @ORM\Column(name="userImageIdentifier", type="text", length=65535, nullable=true)
     */
    private $userImageIdentifier;

    /** Events that are this user is attendee
     *
     * @var UserEvent[]|null
     *
     * @ORM\OneToMany(targetEntity="UserEvent", mappedBy="user", indexBy="event_id", cascade={"persist", "remove"})
     */
    private $events;

    /** Fcm tokens belong to user
     *
     * @var FcmToken[]|null
     *
     * @ORM\OneToMany(targetEntity="FcmToken", mappedBy="user", cascade={"persist", "remove"})
     */
    private $fcmTokens;

    /** User rights
     *
     * @var UserRights[]
     *
     * @ORM\OneToMany(targetEntity="UserRights", mappedBy="user", indexBy="rightsString", cascade={"persist", "remove"}, fetch="EAGER")
     */
    private $rights;

    #endregion
    #region getters and setters
    /**
     * Gets user's id
     *
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $userId
     */
    public function setId(int $userId): void
    {
        $this->id = $userId;
    }

    /**
     * Gets user's firebase UID
     *
     * @Field()
     *
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @Field()
     *
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @Field()
     *
     * @return UserTypeEnum
     */
    public function getType(): UserTypeEnum
    {
        return $this->type;
    }

    /**
     * @param UserTypeEnum $type
     */
    public function setType(UserTypeEnum $type): void
    {
        $this->type = $type;
    }

    /**
     * Gets user's email
     *
     * @Field()
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * Gets user's first name
     *
     * @Field()
     *
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * Gets user's last name
     *
     * @Field()
     *
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * Gets user's main shop
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @return Shop|null
     */
    public function getMainShop(): ?Shop
    {
        return $this->mainShop;
    }

    /**
     * @param Shop|null $mainShop
     */
    public function setMainShop(?Shop $mainShop): void
    {
        $this->mainShop = $mainShop;
    }

    /**
     * Does the user have email notifications enabled?
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @return UserHasEmailNotificationsEnabledEnum
     */
    public function getHasEmailNotificationsEnabled(): UserHasEmailNotificationsEnabledEnum
    {
        return $this->hasEmailNotificationsEnabled;
    }

    /**
     * @param UserHasEmailNotificationsEnabledEnum $hasEmailNotificationsEnabled
     */
    public function setHasEmailNotificationsEnabled(UserHasEmailNotificationsEnabledEnum $hasEmailNotificationsEnabled): void
    {
        $this->hasEmailNotificationsEnabled = $hasEmailNotificationsEnabled;
    }

    /**
     *
     * @return string|null
     */
    public function getUserImageIdentifier(): ?string
    {
        return $this->userImageIdentifier;
    }

    /**
     * Gets user's image URL
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @param string $width
     * @param string $height
     * @param string $format
     * @return string|null
     */
    public function getUserImageURL(string $width = '500', string $height = '500', string $format = 'webp'): ?string
    {
        if (is_null($this->userImageIdentifier))
            return "";
        return Utils::buildMediaDownloadURL($this->userImageIdentifier, $width, $height, $format);
    }

    /**
     * @param string|null $userImageIdentifier
     */
    public function setUserImageIdentifier(?string $userImageIdentifier): void
    {
        $this->userImageIdentifier = $userImageIdentifier;
    }

    /**
     * Is the user fully registered?
     *
     * @Field()
     *
     * @return bool
     */
    public function getIsFullyRegistered(): bool
    {
        $result = false;
        if ($this->firstName !== null && $this->firstName != "" &&
            $this->lastName  !== null && $this->lastName  != ""
        ) {
            $result = true;
        }
        return $result;
    }

    /**
     * Gets user's birth year
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @return int|null
     */
    public function getBirthYear(): ?int
    {
        return $this->birthYear;
    }

    /**
     * Gets user's age
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @return int|null
     * @throws Exception
     */
    public function getAge(): ?int
    {
        if (is_null($this->birthYear))
            return null;

        $currentTime = new DateTimeImmutable('now', new DateTimeZone('Europe/Prague'));
        return ((int) $currentTime->format('Y')) - $this->birthYear;

    }

    /**
     * @param int|null $birthYear
     */
    public function setBirthYear(?int $birthYear): void
    {
        $this->birthYear = $birthYear;
    }

    /**
     * Gets user's gender
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @return GenderEnum|null
     */
    public function getGender(): ?GenderEnum
    {
        return $this->gender;
    }

    /**
     * @param GenderEnum|null $gender
     */
    public function setGender(?GenderEnum $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * Gets shops where the user has administrative access
     *
     * @Field()
     *
     * @return Shop[]
     */
    public function getAdminOfShops(): array {
        $result = [];
        if (is_array($this->adminShops) || is_object($this->adminShops)) {
            foreach ($this->adminShops as $shopBinding) {
                $result[] = $shopBinding->getShop();
            }
        }
        return $result;
    }

    /**
     * Gets user's favourite shops
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @return Shop[]
     */
    public function getFavouriteShops(): array {
        $result = [];
        if (is_array($this->favouriteShops) || is_object($this->favouriteShops)) {
            foreach ($this->favouriteShops as $shopBinding) {
                $result[] = $shopBinding->getShop();
            }
        }
        return $result;
    }

    /**
     * Gets user's favourite products
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @return Product[]
     */
    public function getFavouriteProducts(): array {
        $result = [];
        if (is_array($this->favouriteProducts) || is_object($this->favouriteProducts)) {
            foreach ($this->favouriteProducts as $productBinding) {
                $result[] = $productBinding->getProduct();
            }
        }
        return $result;
    }

    #endregion

    public function addFavouriteShop(Shop $shop, UserFavouriteShop $usersFavouriteShops) {
        $this->favouriteShops[$shop->getId()] = $usersFavouriteShops;
    }

    public function removeFavouriteShop(Shop $shop) {
        unset($this->favouriteShops[$shop->getId()]);
    }

    public function addFavouriteProduct(Product $product, UserFavouriteProduct $userFavouriteProduct) {
        $this->favouriteProducts[$product->getId()] = $userFavouriteProduct;
    }

    public function removeFavouriteProduct(Product $product) {
        unset($this->favouriteProducts[$product->getId()]);
    }

    public function addAdminShop(Shop $shop, ShopAdmin $shopAdmins) {
        $this->adminShops[$shop->getId()] = $shopAdmins;
    }

    public function removeAdminShop(Shop $shop) {
        unset($this->adminShops[$shop->getId()]);
    }

    public function addVipShop(Shop $shop, ShopVipUser $shopVipUser) {
        $this->vipShops[$shop->getId()] = $shopVipUser;
    }

    public function removeVipShop(Shop $shop) {
        unset($this->vipShops[$shop->getId()]);
    }

    /**
     * Gets user's VIP shops
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @return ShopVipUser[]|null
     */
    public function getVipShops(): ?iterable
    {
        return array_filter(
            $this->vipShops->toArray(),
            fn($vipShop, $value) => !$vipShop->isExpired(),
            ARRAY_FILTER_USE_BOTH
        );
    }

    /**
     * Gets user's events
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @return UserEvent[]|null
     */
    public function getEvents(): ?iterable
    {
        return $this->events;
    }

    /**
     * Gets user's notifications
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @param int $limit
     * @param int|null $offset
     * @return Notification[]|null
     * @throws Exception
     */
    public function getNotifications(int $limit = 10, int $offset = null): ?iterable
    {
        $result = [];

        if (is_array($this->fcmTokens) || is_object($this->fcmTokens)) {
            foreach ($this->fcmTokens as $fcmToken) {
                if (is_array($fcmToken->getSentNotifications()) || is_object($fcmToken->getSentNotifications())) {
                    foreach ($fcmToken->getSentNotifications() as $sentNotification) {
                        //add to result only if it is not there yet
                        if (!isset($result[$sentNotification->getNotification()->getId()])) {
                            $result[$sentNotification->getNotification()->getId()] = $sentNotification->getNotification();
                        }
                    }
                }
            }
        }
        usort($result,
            function(Notification $c1, Notification $c2) {
                return $c1->getCreated() < $c2->getCreated();
            });

        return array_slice($result, $offset, $limit);
    }

    /**
     * @return FcmToken[]|null
     */
    public function getFcmTokens(): ?iterable
    {
        return $this->fcmTokens;
    }

    /**
     * Does the user have notifications enabled on the device identified by fcm token?
     * If the given fcmToken is not in the list of user's fcm tokens, returns null.
     *
     * @Field()
     * @Security("this.canGetPrivateUserInfo()")
     *
     * @param string $fcmToken
     * @return bool|null
     */
    public function isNotificationsEnabled(string $fcmToken): ?bool {
        if (is_array($this->fcmTokens) || is_object($this->fcmTokens)) {
            foreach ($this->fcmTokens as $fcmTokenEntity) {
                if ($fcmTokenEntity->getFcmToken() == $fcmToken)
                    return $fcmTokenEntity->getNotificationsEnabled();
            }
        }
        return null;
    }

    /**
     * @param UserRights $rights
     */
    public function addRights(UserRights $rights) {
        $userRights = $this->rights->toArray();
        if (!in_array($rights, $userRights)) {
            $this->rights[] = $rights;
        }
    }

    /**
     * @param UserRights $rights
     */
    public function removeRights(UserRights $rights) {
        $userRights = $this->rights->toArray();
        if (in_array($rights, $userRights)) {
            $foundRights = array_search($rights, $userRights);
            unset($foundRights);
        }
    }

    /**
     * @param GlobalRightsEnum $rights
     * @return bool
     */
    public function isAllowed(GlobalRightsEnum $rights): bool
    {
        /** @var UserRights $userRights */
        foreach ($this->rights->toArray() as $userRights) {
            if ($userRights->getRights()->getValue() == $rights->getValue())
                return true;
        }
        return false;
    }

    public function isAdminOfShop(Shop $shop): bool
    {
        if (is_array($this->adminShops) || is_object($this->adminShops)) {
            foreach ($this->adminShops as $shopBinding) {
                if ($shopBinding->getShop()->getId() == $shop->getId())
                    return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function canGetPrivateUserInfo(): bool
    {
        $authorizationService = AuthorizationService::getInstance();
        return $authorizationService->isAllowed(new ScopeRightsEnum(ScopeRightsEnum::CAN_GET_PRIVATE_USER_INFO), $this);
    }
}
