<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\AdminTypeEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * ShopAdmin
 *
 * @ORM\Table(name="shop_admin", options={"comment":"Admin of shop"})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ShopAdmin
{
    use Timestampable;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     */
    private $adminId;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="shop_id", type="integer", nullable=false)
     */
    private $shopId;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="adminShops", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="admin_id", referencedColumnName="id")
     * })
     */
    private $admin;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="admins", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /**
     * @var AdminTypeEnum
     *
     * @ORM\Column(name="type", type="admin_type_enum", nullable=false)
     */
    private $type;

    /** ShopAdmins constructor
     *
     * @param Shop $shop
     * @param User $admin
     * @return ShopAdmin
     */
    public static function create(
        Shop $shop,
        User $admin
    ) {
        $instance = new self();

        $instance->shopId = $shop->getId();
        $instance->adminId = $admin->getId();
        $instance->shop = $shop;
        $instance->admin = $admin;
        $instance->type = new AdminTypeEnum(AdminTypeEnum::ADMIN);  //TODO: new roles in the future
        $instance->state = EntityStateEnum::ACTIVE;

        return $instance;
    }

    /**
     * @return User
     */
    public function getAdmin(): User
    {
        return $this->admin;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }


}
