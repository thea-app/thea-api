<?php


namespace App\Application\Model\GraphQLTypes;

use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Field;

/** GraphQLite annotations:
 * @Type()
 *
 * Class RegularOpeningHoursType
 * @package App\Application\Model\GraphQLTypes
 */
class RegularOpeningHours
{
    /**
     * @var OpeningHoursTimeInterval[]
     */
    private $mondayIntervals = [];

    /**
     * @var OpeningHoursTimeInterval[]
     */
    private $tuesdayIntervals = [];

    /**
     * @var OpeningHoursTimeInterval[]
     */
    private $wednesdayIntervals = [];

    /**
     * @var OpeningHoursTimeInterval[]
     */
    private $thursdayIntervals = [];

    /**
     * @var OpeningHoursTimeInterval[]
     */
    private $fridayIntervals = [];

    /**
     * @var OpeningHoursTimeInterval[]
     */
    private $saturdayIntervals = [];

    /**
     * @var OpeningHoursTimeInterval[]
     */
    private $sundayIntervals = [];

    public static function create(
        $mondayIntervals,
        $thursdayIntervals,
        $wednesdayIntervals,
        $tuesdayIntervals,
        $fridayIntervals,
        $saturdayIntervals,
        $sundayIntervals
    ): RegularOpeningHours
    {
        $instance = new self();
        $instance->mondayIntervals = $mondayIntervals;
        $instance->tuesdayIntervals = $tuesdayIntervals;
        $instance->wednesdayIntervals = $wednesdayIntervals;
        $instance->thursdayIntervals = $thursdayIntervals;
        $instance->fridayIntervals = $fridayIntervals;
        $instance->saturdayIntervals = $saturdayIntervals;
        $instance->sundayIntervals = $sundayIntervals;

        return $instance;
    }

    /**
     * @Field
     *
     * @return null|OpeningHoursTimeInterval[]
     */
    public function getMonday() : ?iterable {
        return $this->mondayIntervals;
    }

    /**
     * @Field
     *
     * @return null|OpeningHoursTimeInterval[]
     */
    public function getTuesday() : ?iterable {
        return $this->tuesdayIntervals;
    }

    /**
     * @Field
     *
     * @return null|OpeningHoursTimeInterval[]
     */
    public function getWednesday() : ?iterable {
        return $this->wednesdayIntervals;
    }

    /**
     * @Field
     *
     * @return null|OpeningHoursTimeInterval[]
     */
    public function getThursday() : ?iterable {
        return $this->thursdayIntervals;
    }

    /**
     * @Field
     *
     * @return null|OpeningHoursTimeInterval[]
     */
    public function getFriday() : ?iterable {
        return $this->fridayIntervals;
    }

    /**
     * @Field
     *
     * @return null|OpeningHoursTimeInterval[]
     */
    public function getSaturday() : ?iterable {
        return $this->saturdayIntervals;
    }

    /**
     * @Field
     *
     * @return null|OpeningHoursTimeInterval[]
     */
    public function getSunday() : ?iterable {
        return $this->sundayIntervals;
    }

    /**
     * @param OpeningHoursTimeInterval $mondayInterval
     */
    public function addMondayInterval(OpeningHoursTimeInterval $mondayInterval): void
    {
        //TODO: sort time intervals ascending
        $this->mondayIntervals[] = $mondayInterval;
    }

    /**
     * @param OpeningHoursTimeInterval $tuesdayInterval
     */
    public function addTuesdayInterval(OpeningHoursTimeInterval $tuesdayInterval): void
    {
        //TODO: sort time intervals ascending
        $this->tuesdayIntervals[] = $tuesdayInterval;
    }

    /**
     * @param OpeningHoursTimeInterval $wednesdayInterval
     */
    public function addWednesdayInterval(OpeningHoursTimeInterval $wednesdayInterval): void
    {
        //TODO: sort time intervals ascending
        $this->wednesdayIntervals[] = $wednesdayInterval;
    }

    /**
     * @param OpeningHoursTimeInterval $thursdayInterval
     */
    public function addThursdayInterval(OpeningHoursTimeInterval $thursdayInterval): void
    {
        //TODO: sort time intervals ascending
        $this->thursdayIntervals[] = $thursdayInterval;
    }

    /**
     * @param OpeningHoursTimeInterval $fridayInterval
     */
    public function addFridayInterval(OpeningHoursTimeInterval $fridayInterval): void
    {
        //TODO: sort time intervals ascending
        $this->fridayIntervals[] = $fridayInterval;
    }

    /**
     * @param OpeningHoursTimeInterval $saturdayInterval
     */
    public function addSaturdayInterval(OpeningHoursTimeInterval $saturdayInterval): void
    {
        //TODO: sort time intervals ascending
        $this->saturdayIntervals[] = $saturdayInterval;
    }

    /**
     * @param OpeningHoursTimeInterval $sundayInterval
     */
    public function addSundayInterval(OpeningHoursTimeInterval $sundayInterval): void
    {
        //TODO: sort time intervals ascending
        $this->sundayIntervals[] = $sundayInterval;
    }

}