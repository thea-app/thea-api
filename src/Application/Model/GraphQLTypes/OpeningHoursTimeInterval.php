<?php


namespace App\Application\Model\GraphQLTypes;

use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Field;

/** GraphQLite annotations:
 * @Type()
 *
 * Class OpeningHoursTimeIntervalType
 * @package App\Application\Model\GraphQLTypes
 */
class OpeningHoursTimeInterval
{
    /**
     * @var OpeningHoursTime
     */
    private $fromTime;

    /**
     * @var OpeningHoursTime
     */
    private $untilTime;

    public static function create($fromTime, $untilTime): OpeningHoursTimeInterval
    {
        $instance = new self();

        $instance->fromTime = $fromTime;
        $instance->untilTime = $untilTime;

        return $instance;
    }

    /**
     * @Field()
     *
     * @return OpeningHoursTime
     */
    public function getFrom() : OpeningHoursTime {
        return $this->fromTime;
    }

    /**
     * @param OpeningHoursTime $fromTime
     */
    public function setFrom(OpeningHoursTime $fromTime): void
    {
        $this->fromTime = $fromTime;
    }

    /**
     * @Field()
     *
     * @return OpeningHoursTime
     */
    public function getUntil(): OpeningHoursTime
    {
        return $this->untilTime;
    }

    /**
     * @param OpeningHoursTime $untilTime
     */
    public function setUntil(OpeningHoursTime $untilTime): void
    {
        $this->untilTime = $untilTime;
    }
}

