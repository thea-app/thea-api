<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Enum\SocialsTypeEnum;
use App\Application\Model\GraphQLTypes\SocialTypeStaticURL;
use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Field;

/** GraphQLite annotations
 * @Type()
 *
 * Class GetSocialTypesUrlsResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetSocialTypesUrlsResponse extends GraphQLResponseModel
{
    /**
     * @var SocialTypeStaticURL[]|null
     */
    protected $data = [];

    /**
     * @Field()
     * @return SocialTypeStaticURL[]|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param SocialTypeStaticURL[]|null $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }

    /**
     * @param SocialsTypeEnum $key
     * @param string $value
     */
    public function addData(SocialsTypeEnum $key, string $value): void
    {
        $this->data[] = SocialTypeStaticURL::create($key, $value);
    }

}

