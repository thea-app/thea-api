<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;


use App\Application\Model\Entities\Event;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;


/** GraphQLite annotations:
 * @Type
 *
 * Class GetEventResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetEventResponse extends GraphQLResponseModel
{
    /**
     * @var Event|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Event|null
     */
    public function getData(): ?Event
    {
        return $this->data;
    }

    /**
     * @param Event|null $data
     */
    public function setData(?Event $data): void
    {
        $this->data = $data;
    }
}