<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;


use App\Application\Model\Entities\Notification;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetNotificationResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetNotificationResponse extends GraphQLResponseModel
{
    /**
     * @var Notification|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Notification|null
     */
    public function getData(): ?Notification
    {
        return $this->data;
    }

    /**
     * @param Notification|null $data
     */
    public function setData(?Notification $data): void
    {
        $this->data = $data;
    }
}