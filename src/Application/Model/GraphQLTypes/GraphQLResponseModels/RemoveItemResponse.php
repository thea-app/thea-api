<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;


use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class RemoveItemResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class RemoveItemResponse extends GraphQLResponseModel
{

}