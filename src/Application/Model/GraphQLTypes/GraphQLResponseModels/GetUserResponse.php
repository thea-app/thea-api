<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\User;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetUserResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetUserResponse extends GraphQLResponseModel
{
    /**
     * @var User|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return User|null
     */
    public function getData(): ?User
    {
        return $this->data;
    }

    /**
     * @param User|null $data
     */
    public function setData(?User $data): void
    {
        $this->data = $data;
    }
}