<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\ShopVipUser;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetShopVipUserResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetShopVipUserResponse extends GraphQLResponseModel
{
    /**
     * @var ShopVipUser|null
     */
    protected ?ShopVipUser $data;

    /**
     * @Field()
     *
     * @return ShopVipUser|null
     */
    public function getData(): ?ShopVipUser
    {
        return $this->data;
    }

    /**
     * @param ShopVipUser|null $data
     */
    public function setData(?ShopVipUser $data): void
    {
        $this->data = $data;
    }
}