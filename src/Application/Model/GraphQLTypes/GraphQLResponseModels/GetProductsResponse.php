<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Product;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetProductsResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetProductsResponse extends GraphQLResponseModel
{
    /**
     * @var Product[]|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Product[]|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param Product[]|null $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }
}