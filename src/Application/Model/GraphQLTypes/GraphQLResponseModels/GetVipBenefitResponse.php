<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\VipBenefit;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetVipBenefitResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetVipBenefitResponse extends GraphQLResponseModel
{
    /**
     * @var VipBenefit|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return VipBenefit|null
     */
    public function getData(): ?VipBenefit
    {
        return $this->data;
    }

    /**
     * @param VipBenefit|null $data
     */
    public function setData(?VipBenefit $data): void
    {
        $this->data = $data;
    }
}