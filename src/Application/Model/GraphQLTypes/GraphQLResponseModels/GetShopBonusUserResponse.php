<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\ShopBonusUser;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetShopBonusUserResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetShopBonusUserResponse extends GraphQLResponseModel
{
    /**
     * @var ShopBonusUser|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return ShopBonusUser|null
     */
    public function getData(): ?ShopBonusUser
    {
        return $this->data;
    }

    /**
     * @param ShopBonusUser|null $data
     */
    public function setData(?ShopBonusUser $data): void
    {
        $this->data = $data;
    }
}