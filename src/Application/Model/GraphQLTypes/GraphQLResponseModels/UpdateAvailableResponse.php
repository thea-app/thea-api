<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;


use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Field;

/** GraphQLite annotations:
 * @Type()
 *
 * Class UpdateAvailableResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class UpdateAvailableResponse extends GraphQLResponseModel
{
    /**
     * @var bool|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return bool|null
     */
    public function getData(): ?bool
    {
        return $this->data;
    }

    /**
     * @param bool|null $data
     */
    public function setData(?bool $data): void
    {
        $this->data = $data;
    }
}