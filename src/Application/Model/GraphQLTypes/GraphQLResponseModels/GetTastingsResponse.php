<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;


use App\Application\Model\Entities\Tasting;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type
 *
 * Class GetTastingsResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetTastingsResponse extends GraphQLResponseModel
{
    /**
     * @var Tasting[]|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Tasting[]|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param Tasting[]|null $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }
}