<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Category;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type()
 *
 * Class GetCategoryResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetCategoryResponse extends GraphQLResponseModel
{
    /**
     * @var Category|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Category|null
     */
    public function getData(): ?Category
    {
        return $this->data;
    }

    /**
     * @param Category|null $data
     */
    public function setData(?Category $data): void
    {
        $this->data = $data;
    }

}