<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Shop;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type
 *
 * Class GetShopResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetShopResponse extends GraphQLResponseModel
{
    /**
     * @var Shop|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Shop|null
     */
    public function getData(): ?Shop
    {
        return $this->data;
    }

    /**
     * @param Shop|null $data
     */
    public function setData(?Shop $data): void
    {
        $this->data = $data;
    }

}