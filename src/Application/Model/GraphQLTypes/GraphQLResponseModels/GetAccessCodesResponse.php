<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;


use App\Application\Model\Entities\AccessCode;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type
 *
 * Class GetAccessCodesResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetAccessCodesResponse extends GraphQLResponseModel
{
    /**
     * @var AccessCode[]|null
     */
    protected $data;

    /**
     * @Field()
     * @return AccessCode[]|null
     */
    public function getData() : ?array
    {
        return $this->data;
    }

    /**
     * @param AccessCode[]|null $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }
}