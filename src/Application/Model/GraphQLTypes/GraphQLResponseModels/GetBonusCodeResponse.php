<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\BonusCode;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetBonusCodeResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetBonusCodeResponse extends GraphQLResponseModel
{
    /**
     * @var BonusCode|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return BonusCode|null
     */
    public function getData(): ?BonusCode
    {
        return $this->data;
    }

    /**
     * @param BonusCode|null $data
     */
    public function setData(?BonusCode $data): void
    {
        $this->data = $data;
    }
}