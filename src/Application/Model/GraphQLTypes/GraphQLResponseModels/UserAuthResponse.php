<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class UserAuthResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class UserAuthResponse extends GraphQLResponseModel
{

}