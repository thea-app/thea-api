<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Shop;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type
 *
 * Class GetShopsResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetShopsResponse extends GraphQLResponseModel
{
    /**
     * @var Shop[]|null
     */
    protected $data;

    /**
     * @Field()
     * @return Shop[]|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param array|null $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }

}