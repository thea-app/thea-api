<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Notification;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetNotificationsResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetNotificationsResponse extends GraphQLResponseModel
{
    /**
     * @var Notification[]|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Notification[]|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param Notification[]|null $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }
}