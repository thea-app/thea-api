<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Product;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetProductResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetProductResponse extends GraphQLResponseModel
{
    /**
     * @var Product|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Product|null
     */
    public function getData(): ?Product
    {
        return $this->data;
    }

    /**
     * @param Product|null $data
     */
    public function setData(?Product $data): void
    {
        $this->data = $data;
    }
}