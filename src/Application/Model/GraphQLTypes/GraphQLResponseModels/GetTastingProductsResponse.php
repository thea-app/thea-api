<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\TastingProduct;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetTastingProductsResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetTastingProductsResponse extends GraphQLResponseModel
{
    /**
     * @var TastingProduct[]|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return TastingProduct[]|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param TastingProduct[]|null $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }
}