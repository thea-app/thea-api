<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Event;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type
 *
 * Class GetEventsResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetEventsResponse extends GraphQLResponseModel
{
    /**
     * @var Event[]|null
     */
    protected $data;

    /**
     * @Field()
     * @return Event[]|null
     */
    public function getData() : ?array
    {
        return $this->data;
    }

    /**
     * @param Event[]|null $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }
}