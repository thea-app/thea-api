<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Flavour;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type()
 *
 * Class GetFlavourResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetFlavourResponse extends GraphQLResponseModel
{
    /**
     * @var Flavour|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Flavour|null
     */
    public function getData(): ?Flavour
    {
        return $this->data;
    }

    /**
     * @param Flavour|null $data
     */
    public function setData(?Flavour $data): void
    {
        $this->data = $data;
    }

}