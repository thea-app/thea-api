<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use DateTimeImmutable;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

date_default_timezone_set('Europe/Prague');

/** GraphQLite annotations:
 * @Type()
 */
abstract class GraphQLResponseModel
{
    /**
     * @var bool
     */
    protected $success;
    /**
     * @var string
     */
    protected $errorMessage;
    /**
     * @var null|int
     */
    protected $errorCode;

    /**
     * @Field()
     *
     * @return bool
     */
    public function getSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }

    /**
     * @Field()
     *
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage(string $errorMessage): void
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @Field(outputType="DateTime")
     *
     * @return DateTimeImmutable
     */
    public function getTimestamp() : DateTimeImmutable
    {
        return new DateTimeImmutable();
    }

    /**
     * @Field()
     *
     * @return null|int
     */
    public function getErrorCode(): ?int
    {
        return $this->errorCode;
    }

    /**
     * @param int $errorCode
     */
    public function setErrorCode(int $errorCode): void
    {
        $this->errorCode = $errorCode;
    }


}
