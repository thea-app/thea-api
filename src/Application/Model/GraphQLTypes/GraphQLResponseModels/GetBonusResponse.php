<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Bonus;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetBonusResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetBonusResponse extends GraphQLResponseModel
{
    /**
     * @var Bonus|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Bonus|null
     */
    public function getData(): ?Bonus
    {
        return $this->data;
    }

    /**
     * @param Bonus|null $data
     */
    public function setData(?Bonus $data): void
    {
        $this->data = $data;
    }
}