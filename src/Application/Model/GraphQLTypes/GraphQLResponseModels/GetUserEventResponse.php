<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;


use App\Application\Model\Entities\UserEvent;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetUserEventResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetUserEventResponse extends GraphQLResponseModel
{
    /**
     * @var UserEvent|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return UserEvent|null
     */
    public function getData(): ?UserEvent
    {
        return $this->data;
    }

    /**
     * @param UserEvent|null $data
     */
    public function setData(?UserEvent $data): void
    {
        $this->data = $data;
    }
}