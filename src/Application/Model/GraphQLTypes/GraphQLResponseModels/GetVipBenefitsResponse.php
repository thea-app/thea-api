<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\VipBenefit;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetVipBenefitsResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetVipBenefitsResponse extends GraphQLResponseModel
{
    /**
     * @var VipBenefit[]|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return VipBenefit[]|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param VipBenefit[]|null $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }
}