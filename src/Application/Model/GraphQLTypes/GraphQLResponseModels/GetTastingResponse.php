<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;


use App\Application\Model\Entities\Tasting;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type
 *
 * Class GetTastingResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetTastingResponse extends GraphQLResponseModel
{
    /**
     * @var Tasting|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Tasting|null
     */
    public function getData(): ?Tasting
    {
        return $this->data;
    }

    /**
     * @param Tasting|null $data
     */
    public function setData(?Tasting $data): void
    {
        $this->data = $data;
    }
}