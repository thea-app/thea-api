<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\FlavourWheel;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetFlavourWheelResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetFlavourWheelResponse extends GraphQLResponseModel
{
    /**
     * @var FlavourWheel|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return FlavourWheel|null
     */
    public function getData(): ?FlavourWheel
    {
        return $this->data;
    }

    /**
     * @param FlavourWheel|null $data
     */
    public function setData(?FlavourWheel $data): void
    {
        $this->data = $data;
    }
}