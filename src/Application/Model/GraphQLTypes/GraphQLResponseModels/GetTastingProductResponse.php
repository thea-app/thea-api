<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\TastingProduct;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetTastingProductResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetTastingProductResponse extends GraphQLResponseModel
{
    /**
     * @var TastingProduct|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return TastingProduct|null
     */
    public function getData(): ?TastingProduct
    {
        return $this->data;
    }

    /**
     * @param TastingProduct|null $data
     */
    public function setData(?TastingProduct $data): void
    {
        $this->data = $data;
    }
}