<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Flavour;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type()
 *
 * Class GetFlavoursResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetFlavoursResponse extends GraphQLResponseModel
{
    /**
     * @var Flavour[]|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Flavour[]|null
     */
    public function getData(): ?iterable
    {
        return $this->data;
    }

    /**
     * @param Flavour[]|null $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }

}