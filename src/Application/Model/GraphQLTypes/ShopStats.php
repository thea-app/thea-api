<?php

namespace App\Application\Model\GraphQLTypes;

use App\Application\Model\Entities\User;
use App\Application\Model\Enum\GenderEnum;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Field;

/** ShopStats GraphQL type
 * @Type()
 *
 * Class ShopStats
 * @package App\Application\Model\GraphQLTypes
 */
class ShopStats
{
    /**
     * @var User[]
     */
    protected $favouriteUsers;

    /**
     * @var User[]
     */
    protected $mainShopUsers;

    /**
     * @var User[]
     */
    protected $vipUsers;

    /**
     * @var User[]
     */
    protected $bonusUsers;

    /**
     * @param User[] $favouriteUsers
     * @param User[] $mainShopUsers
     * @param User[] $vipUsers
     * @param User[] $bonusUsers
     * @return ShopStats
     * @throws Exception
     */
    public static function create(array $favouriteUsers, array $mainShopUsers,
                                  array $vipUsers, array $bonusUsers): ShopStats
    {
        $instance = new self();

        $instance->favouriteUsers = $favouriteUsers;
        $instance->mainShopUsers = $mainShopUsers;
        $instance->vipUsers = $vipUsers;
        $instance->bonusUsers = $bonusUsers;

        return $instance;
    }

    /**
     * @Field()
     *
     * @return ShopUsersDistribution
     * @throws Exception
     */
    public function getFavouriteUsers(): ShopUsersDistribution
    {
        return ShopStats::countStats($this->favouriteUsers);
    }

    /**
     * @Field()
     *
     * @return ShopUsersDistribution
     * @throws Exception
     */
    public function getMainShopUsers(): ShopUsersDistribution
    {
        return ShopStats::countStats($this->mainShopUsers);
    }

    /**
     * @Field()
     *
     * @return ShopUsersDistribution
     * @throws Exception
     */
    public function getVipUsers(): ShopUsersDistribution
    {
        return ShopStats::countStats($this->vipUsers);
    }

    /**
     * @Field()
     *
     * @return ShopUsersDistribution
     * @throws Exception
     */
    public function getBonusUsers(): ShopUsersDistribution
    {
        return ShopStats::countStats($this->bonusUsers);
    }

    /**
     * @param User[] $users
     * @return ShopUsersDistribution
     * @throws Exception
     */
    private static function countStats(array $users) : ShopUsersDistribution {
        $statResult = array_reduce($users,
            function(&$statCarry, User $user)
            {
                if (empty($statCarry)) {
                    $statCarry['age'] = [];
                    $statCarry['gender'] = [];
                    $statCarry['gender']['female'] = 0;
                    $statCarry['gender']['male'] = 0;
                }
                $userAge = $user->getAge();
                $userGender = $user->getGender();
                if (is_null($userAge) || is_null($userGender))      //user is not fully registered. This shouldn't happen, but just in case
                    return $statCarry;

                $statCarry['age'][] = $userAge;
                if ($userGender == GenderEnum::FEMALE)
                    $statCarry['gender']['female'] += 1;
                else if ($userGender == GenderEnum::MALE)
                    $statCarry['gender']['male'] += 1;
                return $statCarry;
            });

        if (is_null($statResult))
            return ShopUsersDistribution::create(0, 0, 0, 0, 0, 0);

        $minAge = min($statResult['age']);
        $maxAge = max($statResult['age']);
        $totalCount = count($statResult['age']);
        $avgAge = round(array_sum($statResult['age']) / $totalCount, 1);
        $femaleCount = (int) $statResult['gender']['female'];
        $maleCount = (int) $statResult['gender']['male'];

        return ShopUsersDistribution::create($minAge, $maxAge, $avgAge, $femaleCount, $maleCount, $totalCount);
    }

}