<?php

namespace App\Application\Model\GraphQLTypes;

use App\Application\Model\Entities\Language;
use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Field;

/** Translation GraphQL type
 * @Type()
 *
 * Class Translation
 * @package App\Application\Model\GraphQLTypes
 */
class Translation
{
    /**
     * @var Language
     */
    protected $language;

    /**
     * @var string
     */
    protected $value;

    public static function create(Language $language, string $value): Translation
    {
        $instance = new self();

        $instance->language = $language;
        $instance->value = $value;

        return $instance;
    }

    /** what language is this translation in
     * @Field()
     *
     * @return Language
     */
    public function getLanguage(): Language
    {
        return $this->language;
    }

    /** the translated string, like "Last name" or "Příjmení"
     * @Field()
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }


}