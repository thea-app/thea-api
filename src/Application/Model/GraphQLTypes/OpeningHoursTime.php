<?php

namespace App\Application\Model\GraphQLTypes;

use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Field;

/** GraphQLite annotations:
 * @Type()
 *
 * Class OpeningHoursTimeType
 * @package App\Application\Model\GraphQLTypes
 */
class OpeningHoursTime
{
    /**
     * @var int
     */
    private $hours;

    /**
     * @var int
     */
    private $minutes;

    public static function create($hours, $minutes): OpeningHoursTime
    {
        $instance = new self();

        $instance->hours = $hours;
        $instance->minutes = $minutes;

        return $instance;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getHours(): int
    {
        return $this->hours;
    }

    /**
     * @param int $hours
     */
    public function setHours(int $hours): void
    {
        $this->hours = $hours;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getMinutes(): int
    {
        return $this->minutes;
    }

    /**
     * @param int $minutes
     */
    public function setMinutes(int $minutes): void
    {
        $this->minutes = $minutes;
    }
}