<?php

namespace App\Application\Model\GraphQLTypes;

use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Field;

/** ShopUsersDistribution GraphQL type
 * @Type()
 *
 * Class ShopUsersDistribution
 * @package App\Application\Model\GraphQLTypes
 */
class ShopUsersDistribution
{
    /**
     * @var int
     */
    protected $minAge;

    /**
     * @var int
     */
    protected $maxAge;

    /**
     * @var float
     */
    protected $avgAge;

    /**
     * @var int
     */
    protected $femaleCount;

    /**
     * @var int
     */
    protected $maleCount;

    /**
     * @var int
     */
    protected $totalCount;

    public static function create(int $minAge, int $maxAge, float $avgAge,
                                  int $femaleCount, int $maleCount, int $totalCount): ShopUsersDistribution
    {
        $instance = new self();

        $instance->minAge = $minAge;
        $instance->maxAge = $maxAge;
        $instance->avgAge = $avgAge;
        $instance->femaleCount = $femaleCount;
        $instance->maleCount = $maleCount;
        $instance->totalCount = $totalCount;

        return $instance;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getMinAge(): int
    {
        return $this->minAge;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getMaxAge(): int
    {
        return $this->maxAge;
    }

    /**
     * @Field()
     *
     * @return float
     */
    public function getAvgAge(): float
    {
        return $this->avgAge;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getFemaleCount(): int
    {
        return $this->femaleCount;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getMaleCount(): int
    {
        return $this->maleCount;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

}