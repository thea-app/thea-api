<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class CreateTastingProductInput
{
    /**
     * @var int
     */
    private $productId;

    /**
     * @var int
     */
    private $tastingId;

    /**
     * @var TranslationInput[]|null
     */
    private $descriptionTranslations;

    /**
     * @var int
     */
    private $ordering;

    public static function create(
        int $productId,
        int $tastingId,
        array $descriptionTranslations,
        int $ordering
    ) : CreateTastingProductInput {
        $instance = new self();

        $instance->productId = $productId;
        $instance->tastingId = $tastingId;
        $instance->descriptionTranslations = $descriptionTranslations;
        $instance->ordering = $ordering;

        return $instance;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getTastingId(): int
    {
        return $this->tastingId;
    }

    /**
     * @return TranslationInput[]|null
     */
    public function getDescriptionTranslations(): ?array
    {
        return $this->descriptionTranslations;
    }

    /**
     * @return int
     */
    public function getOrdering(): int
    {
        return $this->ordering;
    }

    /**
     * @param int $ordering
     */
    public function setOrdering(int $ordering): void
    {
        $this->ordering = $ordering;
    }
}