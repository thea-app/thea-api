<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class SetTastingProductInput
{
    /**
     * @var int
     */
    private $tastingProductId;

    /**
     * @var int
     */
    private $productId;

    /**
     * @var int
     */
    private $tastingId;

    /**
     * @var TranslationInput[]|null
     */
    private $descriptionTranslations;

    public static function create(
        int $tastingProductId,
        int $productId,
        int $tastingId,
        array $descriptionTranslations
    ) : SetTastingProductInput {
        $instance = new self();

        $instance->tastingProductId = $tastingProductId;
        $instance->productId = $productId;
        $instance->tastingId = $tastingId;
        $instance->descriptionTranslations = $descriptionTranslations;

        return $instance;
    }

    /**
     * @return int
     */
    public function getTastingProductId(): int
    {
        return $this->tastingProductId;
    }

    /**
     * @return int
     */
    public function getTastingId(): int
    {
        return $this->tastingId;
    }

    /**
     * @return TranslationInput[]|null
     */
    public function getDescriptionTranslations(): ?array
    {
        return $this->descriptionTranslations;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }
}