<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class CreateFlavourWheelInput
{
    /**
     * @var int
     */
    private $tastingId;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @param int $tastingId
     * @param string|null $name
     * @return CreateFlavourWheelInput
     */
    public static function create(
        int $tastingId,
        ?string $name
    ): CreateFlavourWheelInput
    {
        $instance = new self();

        $instance->tastingId = $tastingId;
        $instance->name = $name;

        return $instance;
    }

    /**
     * @return int
     */
    public function getTastingId(): int
    {
        return $this->tastingId;
    }

    /**
     * @param int $tastingId
     */
    public function setTastingId(int $tastingId): void
    {
        $this->tastingId = $tastingId;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

}