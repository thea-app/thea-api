<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class UserShopRelationInput
{
    /**
     * @var string
     */
    private $uid;

    /**
     * @var int
     */
    private $shopId;

    public static function create(string $uid, int $shopId)
    {
        $instance = new self();

        $instance->uid = $uid;
        $instance->shopId = $shopId;

        return $instance;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }
}