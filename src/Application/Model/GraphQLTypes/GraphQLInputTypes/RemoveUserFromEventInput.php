<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class RemoveUserFromEventInput
{
    /**
     * @var int
     */
    private $eventId;

    /**
     * @var string
     */
    private $uid;

    /**
     * @param string $uid
     * @param int $eventId
     * @return RemoveUserFromEventInput
     */
    public static function create(
        string $uid,
        int $eventId
    ): RemoveUserFromEventInput
    {
        $instance = new self();

        $instance->uid = $uid;
        $instance->eventId = $eventId;

        return $instance;
    }

    /**
     * @return int
     */
    public function getEventId(): int
    {
        return $this->eventId;
    }

    /**
     * @param int $eventId
     */
    public function setEventId(int $eventId): void
    {
        $this->eventId = $eventId;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid): void
    {
        $this->uid = $uid;
    }
}