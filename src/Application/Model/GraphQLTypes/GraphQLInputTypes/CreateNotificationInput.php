<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Entities\NotificationGroup;
use App\Application\Model\Entities\Shop;
use App\Application\Model\Enum\NotificationGroupEnum;

class CreateNotificationInput
{
    /**
     * @var int
     */
    private $shopId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string|null
     */
    private $image;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var NotificationGroupEnum[]
     */
    private $groups;

    /**
     * @param int $shopId
     * @param string $title
     * @param string $text
     * @param string|null $image
     * @param string|null $name
     * @param NotificationGroupEnum[] $groups
     * @return CreateNotificationInput
     */
    public static function create(
        int $shopId,
        string $title,
        string $text,
        ?string $image,
        ?string $name,
        array $groups
    )
    {
        $instance = new self();

        $instance->shopId = $shopId;
        $instance->title = $title;
        $instance->text = $text;
        $instance->image = $image;
        $instance->name = $name;
        $instance->groups = $groups;

        return $instance;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string|null $image
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     */
    public function setShopId(int $shopId): void
    {
        $this->shopId = $shopId;
    }

    /**
     * @return NotificationGroupEnum[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param NotificationGroupEnum[] $groups
     */
    public function setGroups(array $groups): void
    {
        $this->groups = $groups;
    }
}