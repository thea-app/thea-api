<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class UpdateFavouriteProductsInput
{
    /**
     * @var int
     */
    private $productId;

    /**
     * @var bool
     */
    private $favourite;

    public static function create(int $productId, bool $favourite): UpdateFavouriteProductsInput
    {
        $instance = new self();

        $instance->productId = $productId;
        $instance->favourite = $favourite;

        return $instance;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @return bool
     */
    public function isFavourite(): bool
    {
        return $this->favourite;
    }
}