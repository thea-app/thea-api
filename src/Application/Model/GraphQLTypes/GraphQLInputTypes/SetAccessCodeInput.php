<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use DateTimeImmutable;

class SetAccessCodeInput
{
    /**
     * @var int
     */
    private $accessCodeId;

    /**
     * @var DateTimeImmutable|null
     */
    private $expiryDate;

    /**
     * @var string|null
     */
    private $note;

    /**
     * @param int $accessCodeId
     * @param DateTimeImmutable|null $expiryDate
     * @param string|null $note
     * @return SetAccessCodeInput
     */
    public static function create(
        int $accessCodeId,
        ?DateTimeImmutable $expiryDate,
        ?string $note
    ): SetAccessCodeInput
    {
        $instance = new self();

        $instance->accessCodeId = $accessCodeId;
        $instance->expiryDate = $expiryDate;
        $instance->note = $note;

        return $instance;
    }

    /**
     * @return int
     */
    public function getAccessCodeId(): int
    {
        return $this->accessCodeId;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getExpiryDate(): ?DateTimeImmutable
    {
        return $this->expiryDate;
    }

    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

}