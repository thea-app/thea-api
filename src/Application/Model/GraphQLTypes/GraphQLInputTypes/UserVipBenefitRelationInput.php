<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class UserVipBenefitRelationInput
{
    /**
     * @var string
     */
    private $uid;

    /**
     * @var int
     */
    private $vipBenefitId;

    public static function create(string $uid, int $vipBenefitId): UserVipBenefitRelationInput
    {
        $instance = new self();

        $instance->uid = $uid;
        $instance->vipBenefitId = $vipBenefitId;

        return $instance;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @return int
     */
    public function getVipBenefitId(): int
    {
        return $this->vipBenefitId;
    }


}