<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class SetProductCategoryInput
{
    /**
     * @var int
     */
    private $productId;

    /**
     * @var int|null
     */
    private $categoryId;

    /**
     * @param int $productId
     * @param int|null $categoryId
     *
     * @return SetProductCategoryInput
     */
    public static function create(
        int $productId,
        ?int $categoryId
    ): SetProductCategoryInput
    {
        $instance = new self();

        $instance->productId = $productId;
        $instance->categoryId = $categoryId;

        return $instance;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId(int $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    /**
     * @param int|null $categoryId
     */
    public function setCategoryId(?int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }


}