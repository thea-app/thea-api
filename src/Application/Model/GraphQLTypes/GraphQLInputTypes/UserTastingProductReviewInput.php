<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class UserTastingProductReviewInput
{
    /**
     * @var int
     */
    private $tastingProductId;

    /**
     * @var string|null
     */
    private $publicNote;

    /**
     * @var string|null
     */
    private $privateNote;

    /**
     * @var float|null
     */
    private $rating;

    /**
     * @var int[]|null
     */
    private $flavours;

    /**
     * @param int $tastingProductId
     * @param string|null $publicNote
     * @param string|null $privateNote
     * @param float|null $rating
     * @param int[]|null $flavours
     *
     * @return UserTastingProductReviewInput
     */
    public static function create(
        int $tastingProductId,
        ?string $publicNote,
        ?string $privateNote,
        ?float $rating,
        ?array $flavours
    ): UserTastingProductReviewInput
    {
        $instance = new self();

        $instance->tastingProductId = $tastingProductId;
        $instance->publicNote = $publicNote;
        $instance->privateNote = $privateNote;
        $instance->rating = $rating;
        $instance->flavours = $flavours;

        return $instance;
    }

    /**
     * @return int
     */
    public function getTastingProductId(): int
    {
        return $this->tastingProductId;
    }

    /**
     * @param int $tastingProductId
     */
    public function setTastingProductId(int $tastingProductId): void
    {
        $this->tastingProductId = $tastingProductId;
    }

    /**
     * @return string|null
     */
    public function getPublicNote(): ?string
    {
        return $this->publicNote;
    }

    /**
     * @param string|null $publicNote
     */
    public function setPublicNote(?string $publicNote): void
    {
        $this->publicNote = $publicNote;
    }

    /**
     * @return string|null
     */
    public function getPrivateNote(): ?string
    {
        return $this->privateNote;
    }

    /**
     * @param string|null $privateNote
     */
    public function setPrivateNote(?string $privateNote): void
    {
        $this->privateNote = $privateNote;
    }

    /**
     * @return float|null
     */
    public function getRating(): ?float
    {
        return $this->rating;
    }

    /**
     * @param float|null $rating
     */
    public function setRating(?float $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @return array|null
     */
    public function getFlavours(): ?array
    {
        return $this->flavours;
    }

    /**
     * @param array|null $flavours
     */
    public function setFlavours(?array $flavours): void
    {
        $this->flavours = $flavours;
    }
}