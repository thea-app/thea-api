<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class AddPointsToUserInput
{
    /**
     * @var string|null
     */
    private $uid;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var string|null
     */
    private $bonusUserCode;

    /**
     * @var int
     */
    private $shopId;

    /**
     * @var int
     */
    private $points;

    /**
     * @param string|null $uid
     * @param string|null $email
     * @param string|null $bonusUserCode
     * @param int $shopId
     * @param int $points
     *
     * @return AddPointsToUserInput
     */
    public static function create(
        ?string $uid,
        ?string $email,
        ?string $bonusUserCode,
        int $shopId,
        int $points
    ): AddPointsToUserInput
    {
        $instance = new self();

        $instance->uid = $uid;
        $instance->email = $email;
        $instance->bonusUserCode = $bonusUserCode;
        $instance->shopId = $shopId;
        $instance->points = $points;

        return $instance;
    }

    /**
     * @return string|null
     */
    public function getUid(): ?string
    {
        return $this->uid;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getBonusUserCode(): ?string
    {
        return $this->bonusUserCode;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }


}