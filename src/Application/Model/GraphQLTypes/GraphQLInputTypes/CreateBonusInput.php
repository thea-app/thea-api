<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use DateTimeImmutable;

class CreateBonusInput
{
    /**
     * @var int
     */
    private $shopId;

    /**
     * @var TranslationInput[]
     */
    private $nameTranslations;

    /**
     * @var DateTimeImmutable|null
     */
    private $expiryDate;

    /**
     * @var int
     */
    private $value;

    /**
     * @param int $shopId
     * @param TranslationInput[] $nameTranslations
     * @param DateTimeImmutable|null $expiryDate
     * @param int $value
     * @return CreateBonusInput
     */
    public static function create(
        int $shopId,
        array $nameTranslations,
        ?DateTimeImmutable $expiryDate,
        int $value
    ): CreateBonusInput
    {
        $instance = new self();

        $instance->shopId = $shopId;
        $instance->nameTranslations = $nameTranslations;
        $instance->expiryDate = $expiryDate;
        $instance->value = $value;

        return $instance;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @return TranslationInput[]
     */
    public function getNameTranslations(): array
    {
        return $this->nameTranslations;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getExpiryDate(): ?DateTimeImmutable
    {
        return $this->expiryDate;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

}