<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Enum\CountryEnum;
use App\Application\Model\Enum\EntityStateEnum;
use DateTimeImmutable;

class CreateTastingInput
{
    /**
     * @var int
     */
    private $shopId;

    /**
     * @var TranslationInput[]
     */
    private $nameTranslations;

    /**
     * @var TranslationInput[]|null
     */
    private $publicDescriptionTranslations;

    /**
     * @var TranslationInput[]|null
     */
    private $privateDescriptionTranslations;

    /**
     * @var DateTimeImmutable
     */
    private $dateFrom;

    /**
     * @var DateTimeImmutable
     */
    private $dateUntil;

    /**
     * @var EntityStateEnum
     */
    private $state;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string|null
     */
    private $descriptiveNumber;

    /**
     * @var string|null
     */
    private $routingNumber;

    /**
     * @var string
     */
    private $postcode;

    /**
     * @var string
     */
    private $city;

    /**
     * @var CountryEnum
     */
    private $country;

    /**
     * @var float
     */
    private $longitude;

    /**
     * @var float
     */
    private $latitude;

    /**
     * @var bool
     */
    private $vipOnly;

    /**
     * @param int $shopId
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $publicDescriptionTranslations
     * @param TranslationInput[]|null $privateDescriptionTranslations
     * @param DateTimeImmutable $dateFrom
     * @param DateTimeImmutable $dateUntil
     * @param EntityStateEnum|null $state
     * @param string $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string $postcode
     * @param string $city
     * @param CountryEnum $country
     * @param float $longitude
     * @param float $latitude
     * @param bool $vipOnly
     * @return CreateTastingInput
     */
    public static function create(
        int $shopId,
        array $nameTranslations,
        ?array $publicDescriptionTranslations,
        ?array $privateDescriptionTranslations,
        DateTimeImmutable $dateFrom,
        DateTimeImmutable $dateUntil,
        ?EntityStateEnum $state,
        string $street,
        ?string $descriptiveNumber,
        ?string $routingNumber,
        string $postcode,
        string $city,
        CountryEnum $country,
        float $longitude,
        float $latitude,
        bool $vipOnly
    ): CreateTastingInput
    {
        $instance = new self();

        $instance->shopId = $shopId;
        $instance->nameTranslations = $nameTranslations;
        $instance->publicDescriptionTranslations = $publicDescriptionTranslations;
        $instance->privateDescriptionTranslations = $privateDescriptionTranslations;
        $instance->dateFrom = $dateFrom;
        $instance->dateUntil = $dateUntil;
        $instance->state = $state;
        $instance->street = $street;
        $instance->descriptiveNumber = $descriptiveNumber;
        $instance->routingNumber = $routingNumber;
        $instance->postcode = $postcode;
        $instance->city = $city;
        $instance->country = $country;
        $instance->longitude = $longitude;
        $instance->latitude = $latitude;
        $instance->vipOnly = $vipOnly;

        return $instance;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     */
    public function setShopId(int $shopId): void
    {
        $this->shopId = $shopId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDateFrom(): DateTimeImmutable
    {
        return $this->dateFrom;
    }

    /**
     * @param DateTimeImmutable $dateFrom
     */
    public function setDateFrom(DateTimeImmutable $dateFrom): void
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDateUntil(): DateTimeImmutable
    {
        return $this->dateUntil;
    }

    /**
     * @param DateTimeImmutable $dateUntil
     */
    public function setDateUntil(DateTimeImmutable $dateUntil): void
    {
        $this->dateUntil = $dateUntil;
    }

    /**
     * @return EntityStateEnum|null
     */
    public function getState(): ?EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum|null $state
     */
    public function setState(?EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return string|null
     */
    public function getDescriptiveNumber(): ?string
    {
        return $this->descriptiveNumber;
    }

    /**
     * @param string|null $descriptiveNumber
     */
    public function setDescriptiveNumber(?string $descriptiveNumber): void
    {
        $this->descriptiveNumber = $descriptiveNumber;
    }

    /**
     * @return string|null
     */
    public function getRoutingNumber(): ?string
    {
        return $this->routingNumber;
    }

    /**
     * @param string|null $routingNumber
     */
    public function setRoutingNumber(?string $routingNumber): void
    {
        $this->routingNumber = $routingNumber;
    }

    /**
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode(string $postcode): void
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return CountryEnum
     */
    public function getCountry(): CountryEnum
    {
        return $this->country;
    }

    /**
     * @param CountryEnum $country
     */
    public function setCountry(CountryEnum $country): void
    {
        $this->country = $country;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return TranslationInput[]
     */
    public function getNameTranslations(): array
    {
        return $this->nameTranslations;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     */
    public function setNameTranslations(array $nameTranslations): void
    {
        $this->nameTranslations = $nameTranslations;
    }

    /**
     * @return TranslationInput[]|null
     */
    public function getPublicDescriptionTranslations(): ?array
    {
        return $this->publicDescriptionTranslations;
    }

    /**
     * @param TranslationInput[]|null $publicDescriptionTranslations
     */
    public function setPublicDescriptionTranslations(?array $publicDescriptionTranslations): void
    {
        $this->publicDescriptionTranslations = $publicDescriptionTranslations;
    }

    /**
     * @return TranslationInput[]|null
     */
    public function getPrivateDescriptionTranslations(): ?array
    {
        return $this->privateDescriptionTranslations;
    }

    /**
     * @param TranslationInput[]|null $privateDescriptionTranslations
     */
    public function setPrivateDescriptionTranslations(?array $privateDescriptionTranslations): void
    {
        $this->privateDescriptionTranslations = $privateDescriptionTranslations;
    }

    /**
     * @return bool
     */
    public function isVipOnly(): bool
    {
        return $this->vipOnly;
    }

    /**
     * @param bool $vipOnly
     */
    public function setVipOnly(bool $vipOnly): void
    {
        $this->vipOnly = $vipOnly;
    }
}