<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Enum\CurrencyEnum;

class SetProductInput
{
    /**
     * @var int
     */
    private $productId;

    /**
     * @var int
     */
    private $shopId;

    /**
     * @var TranslationInput[]
     */
    private $nameTranslations;

    /**
     * @var TranslationInput[]|null
     */
    private $descriptionTranslations;

    /**
     * @var ?int
     */
    private $categoryId;

    /**
     * @var CurrencyEnum
     */
    private $currency;

    /**
     * @var float
     */
    private $price;

    /**
     * @param int $productId
     * @param int $shopId
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $descriptionTranslations
     * @param int|null $categoryId
     * @param CurrencyEnum $currency
     * @param float $price
     *
     * @return SetProductInput
     */
    public static function create(
        int $productId,
        int $shopId,
        array $nameTranslations,
        ?array $descriptionTranslations,
        ?int $categoryId,
        CurrencyEnum $currency,
        float $price
    ): SetProductInput
    {
        $instance = new self();

        $instance->productId = $productId;
        $instance->shopId = $shopId;
        $instance->nameTranslations = $nameTranslations;
        $instance->descriptionTranslations = $descriptionTranslations;
        $instance->categoryId = $categoryId;
        $instance->currency = $currency;
        $instance->price = $price;

        return $instance;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     */
    public function setShopId(int $shopId): void
    {
        $this->shopId = $shopId;
    }

    /**
     * @return TranslationInput[]
     */
    public function getNameTranslations(): array
    {
        return $this->nameTranslations;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     */
    public function setNameTranslations(array $nameTranslations): void
    {
        $this->nameTranslations = $nameTranslations;
    }

    /**
     * @return TranslationInput[]|null
     */
    public function getDescriptionTranslations(): ?array
    {
        return $this->descriptionTranslations;
    }

    /**
     * @param TranslationInput[]|null $descriptionTranslations
     */
    public function setDescriptionTranslations(?array $descriptionTranslations): void
    {
        $this->descriptionTranslations = $descriptionTranslations;
    }

    /**
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    /**
     * @param int|null $categoryId
     */
    public function setCategoryId(?int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return CurrencyEnum
     */
    public function getCurrency(): CurrencyEnum
    {
        return $this->currency;
    }

    /**
     * @param CurrencyEnum $currency
     */
    public function setCurrency(CurrencyEnum $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId(int $productId): void
    {
        $this->productId = $productId;
    }


}