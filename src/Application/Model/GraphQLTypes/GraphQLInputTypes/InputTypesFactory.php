<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;

use App\Application\Model\Entities\OpeningHoursException;
use App\Application\Model\Entities\SocialLink;
use App\Application\Model\Enum\CurrencyEnum;
use App\Application\Model\Enum\CountryEnum;
use App\Application\Model\Enum\EventTypeEnum;
use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\NotificationGroupEnum;
use App\Application\Model\Enum\ShopTypeEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\SocialsTypeEnum;
use App\Application\Model\Enum\UserTypeEnum;
use App\Application\Model\Enum\UserHasEmailNotificationsEnabledEnum;
use App\Application\Model\GraphQLTypes\OpeningHoursTime;
use App\Application\Model\GraphQLTypes\OpeningHoursTimeInterval;
use App\Application\Model\GraphQLTypes\RegularOpeningHours;
use DateTimeImmutable;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Factory;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;

/**
 * Class InputTypesFactory
 * @package App\Application\Model\GraphQLInputTypes
 */
class InputTypesFactory
{
    /**
     * @Factory()
     *
     * @param float $latitude
     * @param float $longitude
     * @return Location
     */
    public function createLocation(float $latitude, float $longitude): Location
    {
        return Location::create($latitude, $longitude);
    }


    /**
     * @Factory(name="CreateUserInput")
     *
     * @param string $uid
     * @param UserTypeEnum $type
     * @param EntityStateEnum|null $state
     * @param string|null $firstName
     * @param string|null $lastName
     * @param int|null $birthYear
     * @param GenderEnum|null $gender
     * @param int|null $mainShop
     * @param UserHasEmailNotificationsEnabledEnum|null $hasEmailNotificationsEnabled
     * @param string|null $userImage
     *
     * @return CreateUserInput
     */
    public function createUser(
        string $uid,
        UserTypeEnum $type,
        EntityStateEnum $state = null,
        string $firstName = null,
        string $lastName = null,
        int $birthYear = null,
        GenderEnum $gender = null,
        int $mainShop = null,
        UserHasEmailNotificationsEnabledEnum $hasEmailNotificationsEnabled = null,
        string $userImage = null
    ) : CreateUserInput
    {
        return CreateUserInput::create(
        $uid,
        $state,
        $type,
        $firstName,
        $lastName,
        $birthYear,
        $gender,
        $mainShop,
        $hasEmailNotificationsEnabled,
        $userImage);
    }

    /**
     * @Factory(name="UpdateUserInput")
     *
     * @param string|null $uid
     * @param UserTypeEnum|null $type
     * @param EntityStateEnum|null $state
     * @param string|null $email
     * @param string|null $firstName
     * @param string|null $lastName
     * @param int|null $birthYear
     * @param GenderEnum|null $gender
     * @param UserHasEmailNotificationsEnabledEnum|null $hasEmailNotificationsEnabled
     * @param string|null $userImage
     * @return UpdateUserInput
     */
    public function updateUser(
        string $uid = null,
        UserTypeEnum $type = null,
        EntityStateEnum $state = null,
        string $email = null,
        string $firstName = null,
        string $lastName = null,
        int $birthYear = null,
        GenderEnum $gender = null,
        UserHasEmailNotificationsEnabledEnum $hasEmailNotificationsEnabled = null,
        ?string $userImage
    ) : UpdateUserInput
    {
        return UpdateUserInput::create(
            $uid,
            $state,
            $type,
            $email,
            $firstName,
            $lastName,
            $birthYear,
            $gender,
            $hasEmailNotificationsEnabled,
            $userImage
        );
    }

    /**
     * @Factory(name="SetUserImageInput")
     *
     * @param string|null $uid
     * @param string|null $userImage
     * @return SetUserImageInput
     */
    public function setUserImage(?string $uid, ?string $userImage) : SetUserImageInput {
        return SetUserImageInput::create($uid, $userImage);
    }

    /**
     * @Factory(name="UserAuthInput")
     *
     * @param string $token
     * @return UserAuthInput
     */
    public function loginUser(string $token) : UserAuthInput {
        return UserAuthInput::create($token);
    }

    /**
     * @Factory(name="CreateShopInput")
     *
     * @param EntityStateEnum $state
     * @param ShopTypeEnum $type
     * @param string $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string $postcode
     * @param string $city
     * @param CountryEnum $country
     * @param string[]|null $emails
     * @param string[]|null $phoneNumbers
     * @param string|null $webpage
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $descriptionTranslations
     * @param bool $bookingAvailable
     * @param float $longitude
     * @param float $latitude
     * @param string|null $eshopURL
     * @param string|null $externalReservationURL
     * @param string|null $mainImage
     * @param RegularOpeningHours|null $openingHours
     * @param OpeningHoursException[]|null $openingHoursExceptions
     * @param SocialLink[]|null $socialLinks
     *
     * @return CreateShopInput
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     * @UseInputType(for="$descriptionTranslations", inputType="[TranslationInput!]")
     * @UseInputType(for="$openingHours", inputType="RegularOpeningHoursInput")
     * @UseInputType(for="$openingHoursExceptions", inputType="[OpeningHoursExceptionsInput!]")
     * @UseInputType(for="$socialLinks", inputType="[SocialLinkInput!]")
     *
     */
    public function createShop(
        EntityStateEnum $state,
        ShopTypeEnum $type,
        string $street,
        string $descriptiveNumber = null,
        string $routingNumber = null,
        string $postcode,
        string $city,
        CountryEnum $country,
        array $emails = null,
        array $phoneNumbers = null,
        string $webpage = null,
        array $nameTranslations,
        array $descriptionTranslations = null,
        bool $bookingAvailable,
        float $longitude,
        float $latitude,
        string $eshopURL = null,
        string $externalReservationURL = null,
        string $mainImage = null,
        RegularOpeningHours $openingHours = null,
        array $openingHoursExceptions = null,
        array $socialLinks = null
    ) : CreateShopInput
    {
        return CreateShopInput::create(
            $state,
            $type,
            $street,
            $descriptiveNumber,
            $routingNumber,
            $postcode,
            $city,
            $country,
            $emails,
            $phoneNumbers,
            $webpage,
            $nameTranslations,
            $descriptionTranslations,
            $bookingAvailable,
            $longitude,
            $latitude,
            $eshopURL,
            $externalReservationURL,
            $mainImage,
            $openingHours,
            $openingHoursExceptions,
            $socialLinks
        );
    }


    /**
     * @Factory(name="SetShopInput")
     *
     * @param int $shopId
     * @param ShopTypeEnum $type
     * @param string $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string $postcode
     * @param string $city
     * @param CountryEnum $country
     * @param string[]|null $emails
     * @param string[]|null $phoneNumbers
     * @param string|null $webpage
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $descriptionTranslations
     * @param bool $bookingAvailable
     * @param float $longitude
     * @param float $latitude
     * @param string|null $eshopURL
     * @param string|null $externalReservationURL
     * @param RegularOpeningHours|null $openingHours
     * @param OpeningHoursException[]|null $openingHoursExceptions
     * @param SocialLink[]|null $socialLinks
     *
     * @return SetShopInput
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     * @UseInputType(for="$descriptionTranslations", inputType="[TranslationInput!]")
     * @UseInputType(for="$openingHours", inputType="RegularOpeningHoursInput")
     * @UseInputType(for="$openingHoursExceptions", inputType="[OpeningHoursExceptionsInput!]")
     * @UseInputType(for="$socialLinks", inputType="[SocialLinkInput!]")
     *
     */
    public function setShop(
        int $shopId,
        ShopTypeEnum $type,
        string $street,
        string $descriptiveNumber = null,
        string $routingNumber = null,
        string $postcode,
        string $city,
        CountryEnum $country,
        array $emails = null,
        array $phoneNumbers = null,
        string $webpage = null,
        array $nameTranslations,
        array $descriptionTranslations = null,
        bool $bookingAvailable,
        float $longitude,
        float $latitude,
        string $eshopURL = null,
        string $externalReservationURL = null,
        RegularOpeningHours $openingHours = null,
        array $openingHoursExceptions = null,
        array $socialLinks = null
    ) : SetShopInput
    {
        return SetShopInput::create(
            $shopId,
            $type,
            $street,
            $descriptiveNumber,
            $routingNumber,
            $postcode,
            $city,
            $country,
            $emails,
            $phoneNumbers,
            $webpage,
            $nameTranslations,
            $descriptionTranslations,
            $bookingAvailable,
            $longitude,
            $latitude,
            $eshopURL,
            $externalReservationURL,
            $openingHours,
            $openingHoursExceptions,
            $socialLinks
        );
    }

    /**
     * @Factory(name="SetShopStateInput")
     *
     * @param int $shopId
     * @param EntityStateEnum $state
     *
     * @return SetShopStateInput
     */
    public function setShopState(
        int $shopId,
        EntityStateEnum $state
    ) : SetShopStateInput
    {
        return SetShopStateInput::create(
            $shopId,
            $state
        );
    }

    /**
     * @Factory(name="RegularOpeningHoursInput")
     *
     * @param OpeningHoursTimeInterval[]|null $monday
     * @param OpeningHoursTimeInterval[]|null $tuesday
     * @param OpeningHoursTimeInterval[]|null $wednesday
     * @param OpeningHoursTimeInterval[]|null $thursday
     * @param OpeningHoursTimeInterval[]|null $friday
     * @param OpeningHoursTimeInterval[]|null $saturday
     * @param OpeningHoursTimeInterval[]|null $sunday
     *
     * @UseInputType(for="$monday", inputType="[OpeningHoursTimeIntervalInput!]")
     * @UseInputType(for="$tuesday", inputType="[OpeningHoursTimeIntervalInput!]")
     * @UseInputType(for="$wednesday", inputType="[OpeningHoursTimeIntervalInput!]")
     * @UseInputType(for="$thursday", inputType="[OpeningHoursTimeIntervalInput!]")
     * @UseInputType(for="$friday", inputType="[OpeningHoursTimeIntervalInput!]")
     * @UseInputType(for="$saturday", inputType="[OpeningHoursTimeIntervalInput!]")
     * @UseInputType(for="$sunday", inputType="[OpeningHoursTimeIntervalInput!]")
     *
     * @return RegularOpeningHours
     */
    public function createRegularOpeningHours(
        ?array $monday,
        ?array $tuesday,
        ?array $wednesday,
        ?array $thursday,
        ?array $friday,
        ?array $saturday,
        ?array $sunday
    ) : RegularOpeningHours {
        return RegularOpeningHours::create(
            $monday,
            $tuesday,
            $wednesday,
            $thursday,
            $friday,
            $saturday,
            $sunday
        );
    }

    /**
     * @Factory(name="OpeningHoursTimeIntervalInput")
     *
     * @param OpeningHoursTime $fromTime
     * @param OpeningHoursTime $untilTime
     *
     * @UseInputType(for="$fromTime", inputType="OpeningHoursTimeInput")
     * @UseInputType(for="$untilTime", inputType="OpeningHoursTimeInput")
     *
     * @return OpeningHoursTimeInterval
     */
    public function createOpeningHoursTimeInterval(
        OpeningHoursTime $fromTime,
        OpeningHoursTime $untilTime
    ) : OpeningHoursTimeInterval {
        return OpeningHoursTimeInterval::create(
            $fromTime,
            $untilTime
        );
    }


    /**
     * @Factory(name="OpeningHoursTimeInput")
     *
     * @param int $hours
     * @param int $minutes
     *
     * @return OpeningHoursTime
     */
    public function createOpeningHoursTime(
        int $hours,
        int $minutes
    ) : OpeningHoursTime {
        return OpeningHoursTime::create(
            $hours,
            $minutes
        );
    }


    /**
     * @Factory(name="OpeningHoursExceptionsInput")
     *
     * @param DateTimeImmutable $date
     * @param string|null $text
     * @param OpeningHoursTimeInterval[]|null $openingHours
     *
     * @UseInputType(for="$openingHours", inputType="[OpeningHoursTimeIntervalInput!]")
     *
     * @return OpeningHoursException
     * @throws Exception
     */
    public function createOpeningHoursExceptions(
        DateTimeImmutable $date,
        ?string $text,
        ?array $openingHours
    ) : OpeningHoursException{

        return OpeningHoursException::create(
            $date,
            $text,
            $openingHours
        );
    }

    /**
     * @Factory(name="UpdateFavouriteShopsInput")
     *
     * @param int $shopId
     * @param bool $favourite
     * @param bool $mainShop
     *
     * @return UpdateFavouriteShopsInput
     */
    public function createUpdateFavouriteShopsInput(
        int $shopId,
        bool $favourite,
        bool $mainShop
    ) : UpdateFavouriteShopsInput {
        return UpdateFavouriteShopsInput::create(
            $shopId,
            $favourite,
            $mainShop
        );
    }

    /**
     * @Factory(name="UserShopRelationInput")
     *
     * @param string $uid
     * @param int $shopId
     *
     * @return UserShopRelationInput
     */
    public function createUserShopRelationInput(
        string $uid,
        int $shopId
    ) : UserShopRelationInput {
        return UserShopRelationInput::create(
            $uid,
            $shopId
        );
    }

    /**
     * @Factory(name="TranslationInput")
     *
     * @param string $languageCode
     * @param string $value
     *
     * @return TranslationInput
     */
    public function createTranslationInput(
        string $languageCode,
        string $value
    ) : TranslationInput {
        return TranslationInput::create(
            $languageCode,
            $value
        );
    }

    /**
     * @Factory(name="CreateCategoryInput")
     *
     * @param int $shopId
     * @param array $nameTranslations
     * @param array $descriptionTranslations
     * @param int|null $parentCategoryId
     *
     * @return CreateCategoryInput
     *
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     * @UseInputType(for="$descriptionTranslations", inputType="[TranslationInput!]")
     */
    public function CreateCategoryInput(
        int $shopId,
        array $nameTranslations,
        array $descriptionTranslations,
        ?int $parentCategoryId
    ) : CreateCategoryInput {
        return CreateCategoryInput::create(
            $shopId,
            $nameTranslations,
            $descriptionTranslations,
            $parentCategoryId
        );
    }

    /**
     * @Factory(name="SetCategoryInput")
     *
     * @param int $categoryId
     * @param array $nameTranslations
     * @param array $descriptionTranslations
     * @param int|null $parentCategoryId
     *
     * @return SetCategoryInput
     *
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     * @UseInputType(for="$descriptionTranslations", inputType="[TranslationInput!]")
     */
    public function SetCategoryInput(
        int $categoryId,
        array $nameTranslations,
        array $descriptionTranslations,
        ?int $parentCategoryId
    ) : SetCategoryInput {
        return SetCategoryInput::create(
            $categoryId,
            $nameTranslations,
            $descriptionTranslations,
            $parentCategoryId
        );
    }

    /**
     * @Factory(name="CreateProductInput")
     *
     * @param int $shopId
     * @param array $nameTranslations
     * @param array|null $descriptionTranslations
     * @param int|null $categoryId
     * @param CurrencyEnum $currency
     * @param float $price
     * @param ?string $image
     *
     * @return CreateProductInput
     *
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     * @UseInputType(for="$descriptionTranslations", inputType="[TranslationInput!]")
     */
    public function createProductInput(
        int $shopId,
        array $nameTranslations,
        ?array $descriptionTranslations,
        ?int $categoryId,
        CurrencyEnum $currency,
        float $price,
        ?string $image
    ) : CreateProductInput {
        return CreateProductInput::create(
            $shopId,
            $nameTranslations,
            $descriptionTranslations,
            $categoryId,
            $currency,
            $price,
            $image
        );
    }

    /**
     * @Factory(name="SetProductInput")
     *
     * @param int $productId
     * @param int $shopId
     * @param array $nameTranslations
     * @param array|null $descriptionTranslations
     * @param int|null $categoryId
     * @param CurrencyEnum $currency
     * @param float $price
     *
     * @return SetProductInput
     *
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     * @UseInputType(for="$descriptionTranslations", inputType="[TranslationInput!]")
     */
    public function setProductInput(
        int $productId,
        int $shopId,
        array $nameTranslations,
        ?array $descriptionTranslations,
        ?int $categoryId,
        CurrencyEnum $currency,
        float $price
    ) : SetProductInput {
        return SetProductInput::create(
            $productId,
            $shopId,
            $nameTranslations,
            $descriptionTranslations,
            $categoryId,
            $currency,
            $price
        );
    }

    /**
     * @Factory(name="SetEntityOrderingInput")
     *
     * @param string $entityId
     * @param int $ordering
     *
     * @return SetEntityOrderingInput
     */
    public function setEntityOrderingInput(string $entityId, int $ordering): SetEntityOrderingInput {
        return SetEntityOrderingInput::create($entityId, $ordering);
    }

    /**
     * @Factory(name="UpdateFavouriteProductsInput")
     *
     * @param int $productId
     * @param bool $favourite
     *
     * @return UpdateFavouriteProductsInput
     */
    public function createUpdateFavouriteProductsInput(
        int $productId,
        bool $favourite
    ) : UpdateFavouriteProductsInput {
        return UpdateFavouriteProductsInput::create(
            $productId,
            $favourite
        );
    }

    /**
     * @Factory(name="SocialLinkInput")
     *
     * @param string|null $userString
     * @param string|null $url
     * @param SocialsTypeEnum $type
     * @return SocialLink
     */
    public function createSocialLinksInput(
        ?string $userString,
        ?string $url,
        SocialsTypeEnum $type
    ) : SocialLink {
        return SocialLink::create(
            $userString,
            $url,
            $type
        );
    }

    /**
     * @Factory(name="CreateTastingProductInput")
     *
     * @param int $productId
     * @param int $tastingId
     * @param array $descriptionTranslations
     * @param int $ordering
     *
     * @return CreateTastingProductInput
     *
     * @UseInputType(for="$descriptionTranslations", inputType="[TranslationInput!]")
     */
    public function createTastingProductInput(
        int $productId,
        int $tastingId,
        array $descriptionTranslations,
        int $ordering
    ) : CreateTastingProductInput {
        return CreateTastingProductInput::create(
            $productId,
            $tastingId,
            $descriptionTranslations,
            $ordering
        );
    }


    /**
     * @Factory(name="SetTastingProductInput")
     *
     * @param int $tastingProductId
     * @param int $productId
     * @param int $tastingId
     * @param array $descriptionTranslations
     *
     * @return SetTastingProductInput
     *
     * @UseInputType(for="$descriptionTranslations", inputType="[TranslationInput!]")
     */
    public function setTastingProductInput(
        int $tastingProductId,
        int $productId,
        int $tastingId,
        array $descriptionTranslations
    ) : SetTastingProductInput {
        return SetTastingProductInput::create(
            $tastingProductId,
            $productId,
            $tastingId,
            $descriptionTranslations
        );
    }

    /**
     * @Factory(name="CreateTastingInput")
     *
     * @param int $shopId
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $publicDescriptionTranslations
     * @param TranslationInput[]|null $privateDescriptionTranslations
     * @param DateTimeImmutable $dateFrom
     * @param DateTimeImmutable $dateUntil
     * @param EntityStateEnum|null $state
     * @param string $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string $postcode
     * @param string $city
     * @param CountryEnum $country
     * @param float $longitude
     * @param float $latitude
     * @param bool $vipOnly
     *
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     * @UseInputType(for="$publicDescriptionTranslations", inputType="[TranslationInput!]")
     * @UseInputType(for="$privateDescriptionTranslations", inputType="[TranslationInput!]")
     *
     * @return CreateTastingInput
     */
    public function createTastingInput(
        int $shopId,
        array $nameTranslations,
        ?array $publicDescriptionTranslations,
        ?array $privateDescriptionTranslations,
        DateTimeImmutable $dateFrom,
        DateTimeImmutable $dateUntil,
        ?EntityStateEnum $state,
        string $street,
        ?string $descriptiveNumber,
        ?string $routingNumber,
        string $postcode,
        string $city,
        CountryEnum $country,
        float $longitude,
        float $latitude,
        bool $vipOnly
    ) : CreateTastingInput {
        return CreateTastingInput::create(
            $shopId,
            $nameTranslations,
            $publicDescriptionTranslations,
            $privateDescriptionTranslations,
            $dateFrom,
            $dateUntil,
            $state,
            $street,
            $descriptiveNumber,
            $routingNumber,
            $postcode,
            $city,
            $country,
            $longitude,
            $latitude,
            $vipOnly
        );
    }

    /**
     * @Factory(name="SetTastingInput")
     *
     * @param int $tastingId
     * @param int $shopId
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $publicDescriptionTranslations
     * @param TranslationInput[]|null $privateDescriptionTranslations
     * @param DateTimeImmutable $dateFrom
     * @param DateTimeImmutable $dateUntil
     * @param EventTypeEnum $eventType
     * @param EntityStateEnum|null $state
     * @param string $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string $postcode
     * @param string $city
     * @param CountryEnum $country
     * @param float $longitude
     * @param float $latitude
     * @param bool $vipOnly
     *
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     * @UseInputType(for="$publicDescriptionTranslations", inputType="[TranslationInput!]")
     * @UseInputType(for="$privateDescriptionTranslations", inputType="[TranslationInput!]")
     *
     * @return SetTastingInput
     */
    public function SetTastingInput(
        int $tastingId,
        int $shopId,
        array $nameTranslations,
        ?array $publicDescriptionTranslations,
        ?array $privateDescriptionTranslations,
        DateTimeImmutable $dateFrom,
        DateTimeImmutable $dateUntil,
        EventTypeEnum $eventType,
        ?EntityStateEnum $state,
        string $street,
        ?string $descriptiveNumber,
        ?string $routingNumber,
        string $postcode,
        string $city,
        CountryEnum $country,
        float $longitude,
        float $latitude,
        bool $vipOnly
    ) : SetTastingInput {
        return SetTastingInput::create(
            $tastingId,
            $shopId,
            $nameTranslations,
            $publicDescriptionTranslations,
            $privateDescriptionTranslations,
            $dateFrom,
            $dateUntil,
            $eventType,
            $state,
            $street,
            $descriptiveNumber,
            $routingNumber,
            $postcode,
            $city,
            $country,
            $longitude,
            $latitude,
            $vipOnly
        );
    }

    /**
     * @Factory(name="CreateAccessCodesInput")
     *
     * @param int $eventId
     * @param int $numOfCodes
     * @param DateTimeImmutable|null $expiryDate
     * @return CreateAccessCodesInput
     */
    public function CreateAccessCodesInput(
        int $eventId,
        int $numOfCodes,
        ?DateTimeImmutable $expiryDate
    ) : CreateAccessCodesInput {
        return CreateAccessCodesInput::create(
            $eventId,
            $numOfCodes,
            $expiryDate
        );
    }

    /**
     * @Factory(name="SetAccessCodeInput")
     *
     * @param int $accessCodeId
     * @param DateTimeImmutable|null $expiryDate
     * @param string|null $note
     * @return SetAccessCodeInput
     */
    public function SetAccessCodeInput(
        int $accessCodeId,
        ?DateTimeImmutable $expiryDate,
        ?string $note
    ) : SetAccessCodeInput {
        return SetAccessCodeInput::create(
            $accessCodeId,
            $expiryDate,
            $note
        );
    }

    /**
     * @Factory(name="AddUserToEventInput")
     *
     * @param string|null $uid
     * @param string|null $email
     * @param string|null $accessCode
     * @param int $eventId
     * @return AddUserToEventInput
     */
    public function AddUserToEventInput(
        ?string $uid,
        ?string $email,
        ?string $accessCode,
        int $eventId
    ) : AddUserToEventInput {
        return AddUserToEventInput::create(
            $uid,
            $email,
            $accessCode,
            $eventId
        );
    }

    /**
     * @Factory(name="RemoveUserFromEventInput")
     *
     * @param string $uid
     * @param int $eventId
     * @return RemoveUserFromEventInput
     */
    public function RemoveUserFromEventInput(
        string $uid,
        int $eventId
    ) : RemoveUserFromEventInput {
        return RemoveUserFromEventInput::create(
            $uid,
            $eventId
        );
    }

    /**
     * @Factory(name="UserTastingProductReviewInput")
     *
     * @param int $tastingProductId
     * @param string|null $publicNote
     * @param string|null $privateNote
     * @param float|null $rating
     * @param int[]|null $flavours
     * @return UserTastingProductReviewInput
     */
    public function UserTastingProductReviewInput(
        int $tastingProductId,
        ?string $publicNote,
        ?string $privateNote,
        ?float $rating,
        ?array $flavours
    ) : UserTastingProductReviewInput {
        return UserTastingProductReviewInput::create(
            $tastingProductId,
            $publicNote,
            $privateNote,
            $rating,
            $flavours
        );
    }
    /**
     * @Factory(name="SetProductCategoryInput")
     *
     * @param int $productId
     * @param int|null $categoryId
     * @return SetProductCategoryInput
     */
    public function createSetProductCategoryInput(
        int $productId,
        ?int $categoryId
    ) : SetProductCategoryInput {
        return SetProductCategoryInput::create(
            $productId,
            $categoryId
        );
    }

    /**
     * @Factory(name="SetParentEntityInput")
     *
     * @param int $entityId
     * @param int|null $parentEntityId
     * @return SetParentEntityInput
     */
    public function createSetParentEntityInput(
        int $entityId,
        ?int $parentEntityId
    ) : SetParentEntityInput {
        return SetParentEntityInput::create(
            $entityId,
            $parentEntityId
        );
    }

    /**
     * @Factory(name="CreateBonusInput")
     *
     * @param int $shopId
     * @param TranslationInput[] $nameTranslations
     * @param DateTimeImmutable|null $expiryDate
     * @param int $value
     *
     * @return CreateBonusInput
     *
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     */
    public function createBonusInput(
        int $shopId,
        array $nameTranslations,
        ?DateTimeImmutable $expiryDate,
        int $value
    ) : CreateBonusInput {
        return CreateBonusInput::create(
            $shopId,
            $nameTranslations,
            $expiryDate,
            $value
        );
    }

    /**
     * @Factory(name="SetBonusProgramDescriptionInput")
     *
     * @param int $shopId
     * @param TranslationInput[] $descriptionTranslations
     *
     * @return SetBonusProgramDescriptionInput
     *
     * @UseInputType(for="$descriptionTranslations", inputType="[TranslationInput!]!")
     */
    public function setBonusProgramDescriptionInput(
        int $shopId,
        array $descriptionTranslations
    ) : SetBonusProgramDescriptionInput {
        return SetBonusProgramDescriptionInput::create(
            $shopId,
            $descriptionTranslations
        );
    }

    /**
     * @Factory(name="SetBonusInput")
     *
     * @param int $bonusId
     * @param TranslationInput[] $nameTranslations
     * @param DateTimeImmutable|null $expiryDate
     * @param int $value
     *
     * @return SetBonusInput
     *
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     */
    public function setBonusInput(
        int $bonusId,
        array $nameTranslations,
        ?DateTimeImmutable $expiryDate,
        int $value
    ) : SetBonusInput {
        return SetBonusInput::create(
            $bonusId,
            $nameTranslations,
            $expiryDate,
            $value
        );
    }

    /**
     * @Factory(name="AddPointsToUserInput")
     *
     * @param string|null $uid
     * @param string|null $email
     * @param string|null $bonusUserCode
     * @param int $shopId
     * @param int $points
     *
     * @return AddPointsToUserInput
     */
    public function addPointsToUserInput(
        ?string $uid,
        ?string $email,
        ?string $bonusUserCode,
        int $shopId,
        int $points
    ) : AddPointsToUserInput
    {
        return AddPointsToUserInput::create(
            $uid,
            $email,
            $bonusUserCode,
            $shopId,
            $points
        );
    }


    /**
     * @Factory(name="CreateVipBenefitInput")
     *
     * @param int $shopId
     * @param array $nameTranslations
     * @param array|null $descriptionTranslations
     *
     * @return CreateVipBenefitInput
     *
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     * @UseInputType(for="$descriptionTranslations", inputType="[TranslationInput!]")
     */
    public function createVipBenefitInput(
        int $shopId,
        array $nameTranslations,
        ?array $descriptionTranslations
    ) : CreateVipBenefitInput {
        return CreateVipBenefitInput::create(
            $shopId,
            $nameTranslations,
            $descriptionTranslations
        );
    }

    /**
     * @Factory(name="SetVipBenefitInput")
     *
     * @param int $vipBenefitId
     * @param array $nameTranslations
     * @param array|null $descriptionTranslations
     *
     * @return SetVipBenefitInput
     *
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     * @UseInputType(for="$descriptionTranslations", inputType="[TranslationInput!]")
     */
    public function setVipBenefitInput(
        int $vipBenefitId,
        array $nameTranslations,
        ?array $descriptionTranslations
    ) : SetVipBenefitInput {
        return SetVipBenefitInput::create(
            $vipBenefitId,
            $nameTranslations,
            $descriptionTranslations
        );
    }

    /**
     * @Factory(name="UserVipBenefitRelationInput")
     *
     * @param string $uid
     * @param int $vipBenefitId
     *
     * @return UserVipBenefitRelationInput
     */
    public function createUserVipBenefitRelationInput(
        string $uid,
        int $vipBenefitId
    ) : UserVipBenefitRelationInput {
        return UserVipBenefitRelationInput::create(
            $uid,
            $vipBenefitId
        );
    }

    /**
     * @Factory(name="UserTastingSessionReviewInput")
     *
     * @param int $tastingId
     * @param UserTastingProductReviewInput[] $productReviews
     * @param string|null $publicNote
     * @param string|null $privateNote
     * @param float|null $rating
     *
     * @UseInputType(for="$productReviews", inputType="[UserTastingProductReviewInput!]")
     *
     * @return UserTastingSessionReviewInput
     */
    public function UserTastingSessionReviewInput(
        int $tastingId,
        ?array $productReviews,
        ?string $publicNote,
        ?string $privateNote,
        ?float $rating
    ) : UserTastingSessionReviewInput {
        return UserTastingSessionReviewInput::create(
            $tastingId,
            $productReviews,
            $publicNote,
            $privateNote,
            $rating
        );
    }

    /**
     * @Factory(name="CreateFlavourWheelInput")
     *
     * @param int $tastingId
     * @param string|null $name
     *
     * @return CreateFlavourWheelInput
     */
    public function createFlavourWheelInput(
        int $tastingId,
        ?string $name
    ) : CreateFlavourWheelInput
    {
        return CreateFlavourWheelInput::create(
            $tastingId,
            $name
        );
    }

    /**
     * @Factory(name="CreateFlavourInput")
     *
     * @param int $flavourWheelId
     * @param TranslationInput[] $nameTranslations
     * @param string $color
     * @param int $ordering
     * @param int|null $parentFlavourId
     *
     * @return CreateFlavourInput
     *
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     */
    public function createFlavourInput(
        int $flavourWheelId,
        array $nameTranslations,
        string $color,
        int $ordering,
        ?int $parentFlavourId
    ) : CreateFlavourInput {
        return CreateFlavourInput::create(
            $flavourWheelId,
            $nameTranslations,
            $color,
            $ordering,
            $parentFlavourId
        );
    }

    /**
     * @Factory(name="SetFlavourInput")
     *
     * @param int $flavourId
     * @param int $flavourWheelId
     * @param TranslationInput[] $nameTranslations
     * @param string $color
     * @return SetFlavourInput
     *
     * @UseInputType(for="$nameTranslations", inputType="[TranslationInput!]!")
     */
    public function setFlavourInput(
        int $flavourId,
        int $flavourWheelId,
        array $nameTranslations,
        string $color
    ) : SetFlavourInput {
        return SetFlavourInput::create(
            $flavourId,
            $flavourWheelId,
            $nameTranslations,
            $color
        );
    }

    /**
     * @Factory(name="CreateNotificationInput")
     *
     * @param int $shopId
     * @param string $title
     * @param string $text
     * @param string|null $image
     * @param string|null $name
     * @param NotificationGroupEnum[] $groups
     * @return CreateNotificationInput
     */
    public function createNotificationInput(
        int $shopId,
        string $title,
        string $text,
        ?string $image,
        ?string $name,
        array $groups
    ) : CreateNotificationInput {
        return CreateNotificationInput::create(
            $shopId,
            $title,
            $text,
            $image,
            $name,
            $groups
        );
    }

    /**
     * @Factory(name="SetNotificationInput")
     *
     * @param int $notificationId
     * @param int $shopId
     * @param string $title
     * @param string $text
     * @param string|null $name
     * @param NotificationGroupEnum[] $groups
     * @return SetNotificationInput
     */
    public function setNotificationInput(
        int $notificationId,
        int $shopId,
        string $title,
        string $text,
        ?string $name,
        array $groups
    ) : SetNotificationInput {
        return SetNotificationInput::create(
            $notificationId,
            $shopId,
            $title,
            $text,
            $name,
            $groups
        );
    }

    /**
     * @Factory(name="SetEntityImageInput")
     *
     * @param string $entityId
     * @param string|null $image
     * @return SetEntityImageInput
     */
    public function setEntityImageInput(string $entityId, ?string $image) : SetEntityImageInput {
        return SetEntityImageInput::create($entityId, $image);
    }
}