<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\UserHasEmailNotificationsEnabledEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\UserTypeEnum;

class CreateUserInput
{
    /**
     * @var string
     */
    private $uid;

    /**
     * @var UserTypeEnum
     */
    private $type;

    /**
     * @var EntityStateEnum|null //TODO: nebude null
     */
    private $state;

    /**
     * @var string|null
     */
    private $firstName;

    /**
     * @var string|null
     */
    private $lastName;

    /**
     * @var int|null
     */
    private $mainShop;

    /**
     * @var UserHasEmailNotificationsEnabledEnum|null
     */
    private $hasEmailNotificationsEnabled;

    /**
     * @var string|null
     */
    private $userImage;

    /**
     * @var int|null
     */
    private $birthYear;

    /**
     * @var GenderEnum|null
     */
    private $gender;


    public static function create(
        string $uid,
        ?EntityStateEnum $state,
        UserTypeEnum $type,
        ?string $firstName,
        ?string $lastName,
        ?int $birthYear,
        ?GenderEnum $gender,
        ?int $mainShop,
        ?UserHasEmailNotificationsEnabledEnum $hasEmailNotificationsEnabled,
        ?string $userImage
    ) : CreateUserInput
    {
        $instance = new self();

        $instance->uid = $uid;
        $instance->state = $state;
        $instance->type = $type;
        $instance->firstName = $firstName;
        $instance->lastName = $lastName;
        $instance->birthYear = $birthYear;
        $instance->gender = $gender;
        $instance->mainShop = $mainShop;
        $instance->hasEmailNotificationsEnabled = $hasEmailNotificationsEnabled;
        $instance->userImage = $userImage;

        return $instance;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return UserTypeEnum
     */
    public function getType(): UserTypeEnum
    {
        return $this->type;
    }

    /**
     * @param UserTypeEnum $type
     */
    public function setType(UserTypeEnum $type): void
    {
        $this->type = $type;
    }

    /**
     * @return EntityStateEnum|null
     */
    public function getState(): ?EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum|null $state
     */
    public function setState(?EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return int|null
     */
    public function getMainShop(): ?int
    {
        return $this->mainShop;
    }

    /**
     * @param int|null $mainShop
     */
    public function setMainShop(?int $mainShop): void
    {
        $this->mainShop = $mainShop;
    }

    /**
     * @return UserHasEmailNotificationsEnabledEnum|null
     */
    public function getHasEmailNotificationsEnabled(): ?UserHasEmailNotificationsEnabledEnum
    {
        return $this->hasEmailNotificationsEnabled;
    }

    /**
     * @param UserHasEmailNotificationsEnabledEnum|null $hasEmailNotificationsEnabled
     */
    public function setHasEmailNotificationsEnabled(?UserHasEmailNotificationsEnabledEnum $hasEmailNotificationsEnabled): void
    {
        $this->hasEmailNotificationsEnabled = $hasEmailNotificationsEnabled;
    }

    /**
     * @return string|null
     */
    public function getUserImage(): ?string
    {
        return $this->userImage;
    }

    /**
     * @param string|null $userImage
     */
    public function setUserImage(?string $userImage): void
    {
        $this->userImage = $userImage;
    }

    /**
     * @return int|null
     */
    public function getBirthYear(): ?int
    {
        return $this->birthYear;
    }

    /**
     * @param int|null $birthYear
     */
    public function setBirthYear(?int $birthYear): void
    {
        $this->birthYear = $birthYear;
    }

    /**
     * @return GenderEnum|null
     */
    public function getGender(): ?GenderEnum
    {
        return $this->gender;
    }

    /**
     * @param GenderEnum|null $gender
     */
    public function setGender(?GenderEnum $gender): void
    {
        $this->gender = $gender;
    }
}