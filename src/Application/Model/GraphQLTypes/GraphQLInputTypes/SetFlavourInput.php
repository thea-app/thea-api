<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class SetFlavourInput
{
    /**
     * @var int
     */
    private $flavourId;

    /**
     * @var int
     */
    private $flavourWheelId;

    /**
     * @var TranslationInput[]
     */
    private $nameTranslations;

    /**
     * @var string
     */
    private $color;

    /**
     * @param int $flavourId
     * @param int $flavourWheelId
     * @param TranslationInput[] $nameTranslations
     * @param string $color
     *
     * @return SetFlavourInput
     */
    public static function create(
        int $flavourId,
        int $flavourWheelId,
        array $nameTranslations,
        string $color): SetFlavourInput
    {
        $instance = new self();

        $instance->flavourId = $flavourId;
        $instance->flavourWheelId = $flavourWheelId;
        $instance->nameTranslations = $nameTranslations;
        $instance->color = $color;

        return $instance;
    }

    /**
     * @return int
     */
    public function getFlavourId(): int
    {
        return $this->flavourId;
    }

    /**
     * @param int $flavourId
     */
    public function setFlavourId(int $flavourId): void
    {
        $this->flavourId = $flavourId;
    }

    /**
     * @return int
     */
    public function getFlavourWheelId(): int
    {
        return $this->flavourWheelId;
    }

    /**
     * @param int $flavourWheelId
     */
    public function setFlavourWheelId(int $flavourWheelId): void
    {
        $this->flavourWheelId = $flavourWheelId;
    }

    /**
     * @return TranslationInput[]
     */
    public function getNameTranslations(): array
    {
        return $this->nameTranslations;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     */
    public function setNameTranslations(array $nameTranslations): void
    {
        $this->nameTranslations = $nameTranslations;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

}
