<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\UserHasEmailNotificationsEnabledEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\UserTypeEnum;

class UpdateUserInput
{
    /**
     * @var string|null
     */
    private $uid;

    /**
     * @var UserTypeEnum|null
     */
    private $type;

    /**
     * @var EntityStateEnum|null
     */
    private $state;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var string|null
     */
    private $firstName;

    /**
     * @var string|null
     */
    private $lastName;

    /**
     * @var int|null
     */
    private $birthYear;

    /**
     * @var GenderEnum|null
     */
    private $gender;

    /**
     * @var UserHasEmailNotificationsEnabledEnum|null
     */
    private $hasEmailNotificationsEnabled;

    /** Image in base64 format
     *
     * @var string|null
     */
    private ?string $userImage;


    public static function create (
        ?string $uid,
        ?EntityStateEnum $state,
        ?UserTypeEnum $type,
        ?string $email,
        ?string $firstName,
        ?string $lastName,
        ?int $birthYear,
        ?GenderEnum $gender,
        ?UserHasEmailNotificationsEnabledEnum $hasEmailNotificationsEnabled,
        ?string $userImage
    ): UpdateUserInput
    {
        $instance = new self();

        $instance->uid = $uid;
        $instance->state = $state;
        $instance->type = $type;
        $instance->email = $email;
        $instance->firstName = $firstName;
        $instance->lastName = $lastName;
        $instance->birthYear = $birthYear;
        $instance->gender = $gender;
        $instance->hasEmailNotificationsEnabled = $hasEmailNotificationsEnabled;
        $instance->userImage = $userImage;

        return $instance;
    }

    /**
     * @return string|null
     */
    public function getUid(): ?string
    {
        return $this->uid;
    }

    /**
     * @param string|null $uid
     */
    public function setUid(?string $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return UserTypeEnum|null
     */
    public function getType(): ?UserTypeEnum
    {
        return $this->type;
    }

    /**
     * @param UserTypeEnum|null $type
     */
    public function setType(?UserTypeEnum $type): void
    {
        $this->type = $type;
    }

    /**
     * @return EntityStateEnum|null
     */
    public function getState(): ?EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum|null $state
     */
    public function setState(?EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return UserHasEmailNotificationsEnabledEnum|null
     */
    public function getHasEmailNotificationsEnabled(): ?UserHasEmailNotificationsEnabledEnum
    {
        return $this->hasEmailNotificationsEnabled;
    }

    /**
     * @param UserHasEmailNotificationsEnabledEnum|null $hasEmailNotificationsEnabled
     */
    public function setHasEmailNotificationsEnabled(?UserHasEmailNotificationsEnabledEnum $hasEmailNotificationsEnabled): void
    {
        $this->hasEmailNotificationsEnabled = $hasEmailNotificationsEnabled;
    }

    /**
     * @return int|null
     */
    public function getBirthYear(): ?int
    {
        return $this->birthYear;
    }

    /**
     * @param int|null $birthYear
     */
    public function setBirthYear(?int $birthYear): void
    {
        $this->birthYear = $birthYear;
    }

    /**
     * @return GenderEnum|null
     */
    public function getGender(): ?GenderEnum
    {
        return $this->gender;
    }

    /**
     * @param GenderEnum|null $gender
     */
    public function setGender(?GenderEnum $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return string|null
     */
    public function getUserImage(): ?string
    {
        return $this->userImage;
    }

    /**
     * @param string|null $userImage
     */
    public function setUserImage(?string $userImage): void
    {
        $this->userImage = $userImage;
    }
}