<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class SetEntityOrderingInput
{

    /**
     * @var string
     */
    private string $entityId;

    /**
     * @var int
     */
    private int $ordering;

    public static function create (
        string $entityId,
        int $ordering
    ): SetEntityOrderingInput
    {
        $instance = new self();

        $instance->entityId = $entityId;
        $instance->ordering = $ordering;

        return $instance;
    }

    /**
     * @return string
     */
    public function getEntityId(): string
    {
        return $this->entityId;
    }

    /**
     * @param string $entityId
     */
    public function setEntityId(string $entityId): void
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string
     */
    public function getOrdering(): string
    {
        return $this->ordering;
    }

    /**
     * @param string $ordering
     */
    public function setOrdering(string $ordering): void
    {
        $this->ordering = $ordering;
    }

}