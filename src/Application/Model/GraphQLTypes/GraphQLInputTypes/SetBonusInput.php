<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use DateTimeImmutable;

class SetBonusInput
{
    /**
     * @var int
     */
    private $bonusId;

    /**
     * @var TranslationInput[]
     */
    private $nameTranslations;

    /**
     * @var DateTimeImmutable|null
     */
    private $expiryDate;

    /**
     * @var int
     */
    private $value;

    /**
     * @param int $bonusId
     * @param TranslationInput[] $nameTranslations
     * @param DateTimeImmutable|null $expiryDate
     * @param int $value
     * @return SetBonusInput
     */
    public static function create(
        int $bonusId,
        array $nameTranslations,
        ?DateTimeImmutable $expiryDate,
        int $value
    ): SetBonusInput
    {
        $instance = new self();

        $instance->bonusId = $bonusId;
        $instance->nameTranslations = $nameTranslations;
        $instance->expiryDate = $expiryDate;
        $instance->value = $value;

        return $instance;
    }

    /**
     * @return int
     */
    public function getBonusId(): int
    {
        return $this->bonusId;
    }

    /**
     * @return TranslationInput[]
     */
    public function getNameTranslations(): array
    {
        return $this->nameTranslations;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getExpiryDate(): ?DateTimeImmutable
    {
        return $this->expiryDate;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

}