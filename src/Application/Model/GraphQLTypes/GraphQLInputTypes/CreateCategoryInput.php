<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class CreateCategoryInput
{
    /**
     * @var int
     */
    private $shopId;

    /**
     * @var TranslationInput[]
     */
    private $nameTranslations;

    /**
     * @var TranslationInput[]
     */
    private $descriptionTranslations;

    /**
     * @var int|null
     */
    private $parentCategoryId;

    /**
     * @param int $shopId
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[] $descriptionTranslations
     * @param int|null $parentCategoryId
     *
     * @return CreateCategoryInput
     */
    public static function create(
        int $shopId,
        array $nameTranslations,
        array $descriptionTranslations,
        ?int $parentCategoryId
    )
    {
        $instance = new self();

        $instance->shopId = $shopId;
        $instance->nameTranslations = $nameTranslations;
        $instance->descriptionTranslations = $descriptionTranslations;
        $instance->parentCategoryId = $parentCategoryId;

        return $instance;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     */
    public function setShopId(int $shopId): void
    {
        $this->shopId = $shopId;
    }

    /**
     * @return TranslationInput[]
     */
    public function getNameTranslations(): array
    {
        return $this->nameTranslations;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     */
    public function setNameTranslations(array $nameTranslations): void
    {
        $this->nameTranslations = $nameTranslations;
    }

    /**
     * @return TranslationInput[]
     */
    public function getDescriptionTranslations(): array
    {
        return $this->descriptionTranslations;
    }

    /**
     * @param TranslationInput[] $descriptionTranslations
     */
    public function setDescriptionTranslations(array $descriptionTranslations): void
    {
        $this->descriptionTranslations = $descriptionTranslations;
    }

    /**
     * @return int|null
     */
    public function getParentCategoryId(): ?int
    {
        return $this->parentCategoryId;
    }

    /**
     * @param int|null $parentCategoryId
     */
    public function setParentCategoryId(?int $parentCategoryId): void
    {
        $this->parentCategoryId = $parentCategoryId;
    }

}