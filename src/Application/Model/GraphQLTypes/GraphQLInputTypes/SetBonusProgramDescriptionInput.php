<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;

class SetBonusProgramDescriptionInput
{
    /**
     * @var int
     */
    private $shopId;

    /**
     * @var TranslationInput[]
     */
    private $descriptionTranslations;

    /**
     * @param int $shopId
     * @param TranslationInput[] $descriptionTranslations
     * @return SetBonusProgramDescriptionInput
     */
    public static function create(
        int $shopId,
        array $descriptionTranslations
    ): SetBonusProgramDescriptionInput
    {
        $instance = new self();

        $instance->shopId = $shopId;
        $instance->descriptionTranslations = $descriptionTranslations;

        return $instance;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @return TranslationInput[]
     */
    public function getDescriptionTranslations(): array
    {
        return $this->descriptionTranslations;
    }

}