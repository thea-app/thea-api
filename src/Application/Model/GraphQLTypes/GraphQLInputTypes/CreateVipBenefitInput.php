<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class CreateVipBenefitInput
{
    /**
     * @var int
     */
    private $shopId;

    /**
     * @var TranslationInput[]
     */
    private $nameTranslations;

    /**
     * @var TranslationInput[]|null
     */
    private $descriptionTranslations;

    /**
     * @param int $shopId
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $descriptionTranslations
     * @return CreateVipBenefitInput
     */
    public static function create(
        int $shopId,
        array $nameTranslations,
        ?array $descriptionTranslations
    ): CreateVipBenefitInput
    {
        $instance = new self();

        $instance->shopId = $shopId;
        $instance->nameTranslations = $nameTranslations;
        $instance->descriptionTranslations = $descriptionTranslations;

        return $instance;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     */
    public function setShopId(int $shopId): void
    {
        $this->shopId = $shopId;
    }

    /**
     * @return TranslationInput[]
     */
    public function getNameTranslations(): array
    {
        return $this->nameTranslations;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     */
    public function setNameTranslations(array $nameTranslations): void
    {
        $this->nameTranslations = $nameTranslations;
    }

    /**
     * @return TranslationInput[]|null
     */
    public function getDescriptionTranslations(): ?array
    {
        return $this->descriptionTranslations;
    }

    /**
     * @param TranslationInput[]|null $descriptionTranslations
     */
    public function setDescriptionTranslations(?array $descriptionTranslations): void
    {
        $this->descriptionTranslations = $descriptionTranslations;
    }

}