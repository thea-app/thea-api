<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class SetCategoryInput
{
    /**
     * @var int
     */
    private $categoryId;

    /**
     * @var TranslationInput[]
     */
    private $nameTranslations;

    /**
     * @var TranslationInput[]
     */
    private $descriptionTranslations;

    /**
     * @var int|null
     */
    private $parentCategoryId;

    /**
     * @param int $categoryId
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[] $descriptionTranslations
     * @param int|null $parentCategoryId
     *
     * @return SetCategoryInput
     */
    public static function create(
        int $categoryId,
        array $nameTranslations,
        array $descriptionTranslations,
        ?int $parentCategoryId
    ): SetCategoryInput
    {
        $instance = new self();

        $instance->categoryId = $categoryId;
        $instance->nameTranslations = $nameTranslations;
        $instance->descriptionTranslations = $descriptionTranslations;
        $instance->parentCategoryId = $parentCategoryId;

        return $instance;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return TranslationInput[]
     */
    public function getNameTranslations(): array
    {
        return $this->nameTranslations;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     */
    public function setNameTranslations(array $nameTranslations): void
    {
        $this->nameTranslations = $nameTranslations;
    }

    /**
     * @return TranslationInput[]
     */
    public function getDescriptionTranslations(): array
    {
        return $this->descriptionTranslations;
    }

    /**
     * @param TranslationInput[] $descriptionTranslations
     */
    public function setDescriptionTranslations(array $descriptionTranslations): void
    {
        $this->descriptionTranslations = $descriptionTranslations;
    }

    /**
     * @return int|null
     */
    public function getParentCategoryId(): ?int
    {
        return $this->parentCategoryId;
    }

    /**
     * @param int|null $parentCategoryId
     */
    public function setParentCategoryId(?int $parentCategoryId): void
    {
        $this->parentCategoryId = $parentCategoryId;
    }

}