<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class Location
{
    private $latitude;
    private $longitude;

    public static function create(float $latitude, float $longitude)
    {
        $instance = new self();

        $instance->latitude = $latitude;
        $instance->longitude = $longitude;

        return $instance;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }
}