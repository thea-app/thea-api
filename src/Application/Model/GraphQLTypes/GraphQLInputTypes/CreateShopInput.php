<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Entities\OpeningHoursException;
use App\Application\Model\Entities\SocialLink;
use App\Application\Model\Enum\CountryEnum;
use App\Application\Model\Enum\ShopTypeEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\GraphQLTypes\RegularOpeningHours;

class CreateShopInput
{
    /**
     * @var EntityStateEnum
     */
    private $state; //zmenit typ state v entite shops.php

    /**
     * @var ShopTypeEnum
     */
    private $type;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string|null
     */
    private $descriptiveNumber;

    /**
     * @var string|null
     */
    private $routingNumber;

    /**
     * @var string
     */
    private $postcode;

    /**
     * @var string
     */
    private $city;

    /**
     * @var CountryEnum
     */
    private $country;

    /**
     * @var array|null
     */
    private $emails;

    /**
     * @var array|null
     */
    private $phoneNumbers;

    /**
     * @var string|null
     */
    private $webpage;

    /**
     * @var TranslationInput[]
     */
    private $nameTranslations;

    /**
     * @var TranslationInput[]|null
     */
    private $descriptionTranslations;

    /**
     * @var bool
     */
    private $bookingAvailable;

    /**
     * @var float
     */
    private $longitude;

    /**
     * @var float
     */
    private $latitude;

    /**
     * @var string|null
     */
    private $externalReservationURL;

    /**
     * @var string|null
     */
    private $eshopURL;

    /**
     * @var string|null
     */
    private $mainImage;

    /**
     * @var RegularOpeningHours|null
     */
    private $openingHours;

    /**
     * @var OpeningHoursException[]|null
     */
    private $openingHoursExceptions;

    /**
     * @var SocialLink[]|null
     */
    private $socialLinks;

    /**
     * @param EntityStateEnum $state
     * @param ShopTypeEnum $type
     * @param string $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string $postcode
     * @param string $city
     * @param CountryEnum $country
     * @param array|null $emails
     * @param array|null $phoneNumbers
     * @param string|null $webpage
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $descriptionTranslations
     * @param bool $bookingAvailable
     * @param float $longitude
     * @param float $latitude
     * @param string|null $eshopURL
     * @param string|null $externalReservationURL
     * @param string|null $mainImage
     * @param RegularOpeningHours|null $openingHours
     * @param OpeningHoursException[]|null $openingHoursExceptions
     * @param SocialLink[]|null $socialLinks
     * @return CreateShopInput
     */
    public static function create(
        EntityStateEnum $state,
        ShopTypeEnum $type,
        string $street,
        ?string $descriptiveNumber,
        ?string $routingNumber,
        string $postcode,
        string $city,
        CountryEnum $country,
        ?array $emails,
        ?array $phoneNumbers,
        ?string $webpage,
        array $nameTranslations,
        ?array $descriptionTranslations,
        bool $bookingAvailable,
        float $longitude,
        float $latitude,
        ?string $eshopURL,
        ?string $externalReservationURL,
        ?string $mainImage,
        ?RegularOpeningHours $openingHours,
        ?array $openingHoursExceptions,
        ?array $socialLinks
    ): CreateShopInput
    {
        $instance = new self();

        $instance->state = $state;
        $instance->type = $type;
        $instance->street = $street;
        $instance->descriptiveNumber = $descriptiveNumber;
        $instance->routingNumber = $routingNumber;
        $instance->postcode = $postcode;
        $instance->city = $city;
        $instance->country = $country;
        $instance->emails = $emails;
        $instance->phoneNumbers = $phoneNumbers;
        $instance->webpage = $webpage;
        $instance->nameTranslations = $nameTranslations;
        $instance->descriptionTranslations = $descriptionTranslations;
        $instance->bookingAvailable = $bookingAvailable;
        $instance->longitude = $longitude;
        $instance->latitude = $latitude;
        $instance->eshopURL = $eshopURL;
        $instance->externalReservationURL = $externalReservationURL;
        $instance->mainImage = $mainImage;
        $instance->openingHours = $openingHours;
        $instance->openingHoursExceptions = $openingHoursExceptions;
        $instance->socialLinks = $socialLinks;

        return $instance;
    }

    /**
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @return ShopTypeEnum
     */
    public function getType(): ShopTypeEnum
    {
        return $this->type;
    }

    /**
     * @param ShopTypeEnum $type
     */
    public function setType(ShopTypeEnum $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return string|null
     */
    public function getDescriptiveNumber(): ?string
    {
        return $this->descriptiveNumber;
    }

    /**
     * @param string|null $descriptiveNumber
     */
    public function setDescriptiveNumber(?string $descriptiveNumber): void
    {
        $this->descriptiveNumber = $descriptiveNumber;
    }

    /**
     * @return string|null
     */
    public function getRoutingNumber(): ?string
    {
        return $this->routingNumber;
    }

    /**
     * @param string|null $routingNumber
     */
    public function setRoutingNumber(?string $routingNumber): void
    {
        $this->routingNumber = $routingNumber;
    }

    /**
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode(string $postcode): void
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return CountryEnum
     */
    public function getCountry(): CountryEnum
    {
        return $this->country;
    }

    /**
     * @param CountryEnum $country
     */
    public function setCountry(CountryEnum $country): void
    {
        $this->country = $country;
    }

    /**
     * @return array|null
     */
    public function getEmails(): ?array
    {
        return $this->emails;
    }

    /**
     * @param array|null $emails
     */
    public function setEmails(?array $emails): void
    {
        $this->emails = $emails;
    }

    /**
     * @return array|null
     */
    public function getPhoneNumbers(): ?array
    {
        return $this->phoneNumbers;
    }

    /**
     * @param array|null $phoneNumbers
     */
    public function setPhoneNumbers(?array $phoneNumbers): void
    {
        $this->phoneNumbers = $phoneNumbers;
    }

    /**
     * @return string|null
     */
    public function getWebpage(): ?string
    {
        return $this->webpage;
    }

    /**
     * @param string|null $webpage
     */
    public function setWebpage(?string $webpage): void
    {
        $this->webpage = $webpage;
    }

    /**
     * @return TranslationInput[]
     */
    public function getNameTranslations(): array
    {
        return $this->nameTranslations;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     */
    public function setNameTranslations(array $nameTranslations): void
    {
        $this->nameTranslations = $nameTranslations;
    }

    /**
     * @return TranslationInput[]|null
     */
    public function getDescriptionTranslations(): ?array
    {
        return $this->descriptionTranslations;
    }

    /**
     * @param TranslationInput[]|null $descriptionTranslations
     */
    public function setDescriptionTranslations(?array $descriptionTranslations): void
    {
        $this->descriptionTranslations = $descriptionTranslations;
    }

    /**
     * @return bool
     */
    public function isBookingAvailable(): bool
    {
        return $this->bookingAvailable;
    }

    /**
     * @param bool $bookingAvailable
     */
    public function setBookingAvailable(bool $bookingAvailable): void
    {
        $this->bookingAvailable = $bookingAvailable;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string|null
     */
    public function getExternalReservationURL(): ?string
    {
        return $this->externalReservationURL;
    }

    /**
     * @param string|null $externalReservationURL
     */
    public function setExternalReservationURL(?string $externalReservationURL): void
    {
        $this->externalReservationURL = $externalReservationURL;
    }

    /**
     * @return string|null
     */
    public function getMainImage(): ?string
    {
        return $this->mainImage;
    }

    /**
     * @param string|null $mainImage
     */
    public function setMainImage(?string $mainImage): void
    {
        $this->mainImage = $mainImage;
    }

    /**
     * @return RegularOpeningHours|null
     */
    public function getOpeningHours(): ?RegularOpeningHours
    {
        return $this->openingHours;
    }

    /**
     * @param RegularOpeningHours|null $openingHours
     */
    public function setOpeningHours(?RegularOpeningHours $openingHours): void
    {
        $this->openingHours = $openingHours;
    }

    /**
     * @return array|null
     */
    public function getOpeningHoursExceptions(): ?array
    {
        return $this->openingHoursExceptions;
    }

    /**
     * @param array|null $openingHoursExceptions
     */
    public function setOpeningHoursExceptions(?array $openingHoursExceptions): void
    {
        $this->openingHoursExceptions = $openingHoursExceptions;
    }

    /**
     * @return SocialLink[]|null
     */
    public function getSocialLinks(): ?array
    {
        return $this->socialLinks;
    }

    /**
     * @param SocialLink[]|null $socialLinks
     */
    public function setSocialLinks(?array $socialLinks): void
    {
        $this->socialLinks = $socialLinks;
    }

    /**
     * @return string|null
     */
    public function getEshopURL(): ?string
    {
        return $this->eshopURL;
    }

    /**
     * @param string|null $eshopURL
     */
    public function setEshopURL(?string $eshopURL): void
    {
        $this->eshopURL = $eshopURL;
    }
}