<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class UserTastingSessionReviewInput
{
    /**
     * @var int
     */
    private $tastingId;

    /**
     * @var array|null
     */
    private $productReviews;

    /**
     * @var string|null
     */
    private $publicNote;

    /**
     * @var string|null
     */
    private $privateNote;

    /**
     * @var float|null
     */
    private $rating;

    /**
     * @param int $tastingId
     * @param UserTastingProductReviewInput[]|null $productReviews
     * @param string|null $publicNote
     * @param string|null $privateNote
     * @param float|null $rating
     *
     * @return UserTastingSessionReviewInput
     */
    public static function create(
        int $tastingId,
        ?array $productReviews,
        ?string $publicNote,
        ?string $privateNote,
        ?float $rating
    ): UserTastingSessionReviewInput
    {
        $instance = new self();

        $instance->tastingId = $tastingId;
        $instance->productReviews = $productReviews;
        $instance->publicNote = $publicNote;
        $instance->privateNote = $privateNote;
        $instance->rating = $rating;

        return $instance;
    }

    /**
     * @return int
     */
    public function getTastingId(): int
    {
        return $this->tastingId;
    }

    /**
     * @param int $tastingId
     */
    public function setTastingId(int $tastingId): void
    {
        $this->tastingId = $tastingId;
    }

    /**
     * @return array|null
     */
    public function getProductReviews(): ?array
    {
        return $this->productReviews;
    }

    /**
     * @param array|null $productReviews
     */
    public function setProductReviews(?array $productReviews): void
    {
        $this->productReviews = $productReviews;
    }

    /**
     * @return string|null
     */
    public function getPublicNote(): ?string
    {
        return $this->publicNote;
    }

    /**
     * @param string|null $publicNote
     */
    public function setPublicNote(?string $publicNote): void
    {
        $this->publicNote = $publicNote;
    }

    /**
     * @return string|null
     */
    public function getPrivateNote(): ?string
    {
        return $this->privateNote;
    }

    /**
     * @param string|null $privateNote
     */
    public function setPrivateNote(?string $privateNote): void
    {
        $this->privateNote = $privateNote;
    }

    /**
     * @return float|null
     */
    public function getRating(): ?float
    {
        return $this->rating;
    }

    /**
     * @param float|null $rating
     */
    public function setRating(?float $rating): void
    {
        $this->rating = $rating;
    }
}