<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class TranslationInput
{
    /**
     * @var string
     */
    private $languageCode;

    /**
     * @var string
     */
    private $value;

    public static function create(string $languageCode, string $value)
    {
        $instance = new self();

        $instance->languageCode = $languageCode;
        $instance->value = $value;

        return $instance;
    }

    /**
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

}