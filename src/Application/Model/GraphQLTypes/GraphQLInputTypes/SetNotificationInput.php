<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Enum\NotificationGroupEnum;

class SetNotificationInput
{
    /**
     * @var int
     */
    private $notificationId;

    /**
     * @var int
     */
    private $shopId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string|null
     *
     */
    private $name;

    /**
     * @var NotificationGroupEnum[]
     */
    private $groups;

    /**
     * @param int $notificationId
     * @param int $shopId
     * @param string $title
     * @param string $text
     * @param string|null $name
     * @param NotificationGroupEnum[] $groups
     * @return SetNotificationInput
     */
    public static function create(
        int $notificationId,
        int $shopId,
        string $title,
        string $text,
        ?string $name,
        array $groups
    )
    {
        $instance = new self();

        $instance->notificationId = $notificationId;
        $instance->shopId = $shopId;
        $instance->title = $title;
        $instance->text = $text;
        $instance->name = $name;
        $instance->groups = $groups;

        return $instance;
    }

    /**
     * @return int
     */
    public function getNotificationId(): int
    {
        return $this->notificationId;
    }

    /**
     * @param int $notificationId
     */
    public function setNotificationId(int $notificationId): void
    {
        $this->notificationId = $notificationId;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     */
    public function setShopId(int $shopId): void
    {
        $this->shopId = $shopId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return NotificationGroupEnum[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param NotificationGroupEnum[] $groups
     */
    public function setGroups(array $groups): void
    {
        $this->groups = $groups;
    }
}