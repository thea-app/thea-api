<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class UpdateFavouriteShopsInput
{
    /**
     * @var int
     */
    private $shopId;

    /**
     * @var bool
     */
    private $favourite;

    /**
     * @var bool
     */
    private $mainShop;

    /**
     * @param int $shopId
     * @param bool $favourite
     * @param bool $mainShop
     * @return UpdateFavouriteShopsInput
     */
    public static function create(int $shopId, bool $favourite, bool $mainShop): UpdateFavouriteShopsInput
    {
        $instance = new self();

        $instance->shopId = $shopId;
        $instance->favourite = $favourite;
        $instance->mainShop = $mainShop;

        return $instance;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @return bool
     */
    public function isFavourite(): bool
    {
        return $this->favourite;
    }

    /**
     * @return bool
     */
    public function isMainShop(): bool
    {
        return $this->mainShop;
    }
}