<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;

class SetVipBenefitInput
{
    /**
     * @var int
     */
    private $vipBenefitId;

    /**
     * @var TranslationInput[]
     */
    private $nameTranslations;

    /**
     * @var TranslationInput[]|null
     */
    private $descriptionTranslations;

    /**
     * @param int $vipBenefitId
     * @param TranslationInput[] $nameTranslations
     * @param TranslationInput[]|null $descriptionTranslations
     * @return SetVipBenefitInput
     */
    public static function create(
        int $vipBenefitId,
        array $nameTranslations,
        ?array $descriptionTranslations
    ): SetVipBenefitInput
    {
        $instance = new self();

        $instance->vipBenefitId = $vipBenefitId;
        $instance->nameTranslations = $nameTranslations;
        $instance->descriptionTranslations = $descriptionTranslations;

        return $instance;
    }

    /**
     * @return TranslationInput[]
     */
    public function getNameTranslations(): array
    {
        return $this->nameTranslations;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     */
    public function setNameTranslations(array $nameTranslations): void
    {
        $this->nameTranslations = $nameTranslations;
    }

    /**
     * @return TranslationInput[]|null
     */
    public function getDescriptionTranslations(): ?array
    {
        return $this->descriptionTranslations;
    }

    /**
     * @param TranslationInput[]|null $descriptionTranslations
     */
    public function setDescriptionTranslations(?array $descriptionTranslations): void
    {
        $this->descriptionTranslations = $descriptionTranslations;
    }

    /**
     * @return int
     */
    public function getVipBenefitId(): int
    {
        return $this->vipBenefitId;
    }

    /**
     * @param int $vipBenefitId
     */
    public function setVipBenefitId(int $vipBenefitId): void
    {
        $this->vipBenefitId = $vipBenefitId;
    }


}