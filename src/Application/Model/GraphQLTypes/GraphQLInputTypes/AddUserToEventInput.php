<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class AddUserToEventInput
{
    /**
     * @var string|null
     */
    private $uid;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var int
     */
    private $eventId;

    /**
     * @var string|null
     */
    private $accessCode;

    /**
     * @param string|null $uid
     * @param string|null $email
     * @param string|null $accessCode
     * @param int $eventId
     * @return AddUserToEventInput
     */
    public static function create(
        ?string $uid,
        ?string $email,
        ?string $accessCode,
        int $eventId
    ): AddUserToEventInput
    {
        $instance = new self();

        $instance->uid = $uid;
        $instance->email = $email;
        $instance->accessCode = $accessCode;
        $instance->eventId = $eventId;

        return $instance;
    }

    /**
     * @return string|null
     */
    public function getUid(): ?string
    {
        return $this->uid;
    }

    /**
     * @param string|null $uid
     */
    public function setUid(?string $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getEventId(): int
    {
        return $this->eventId;
    }

    /**
     * @param int $eventId
     */
    public function setEventId(int $eventId): void
    {
        $this->eventId = $eventId;
    }

    /**
     * @return string|null
     */
    public function getAccessCode(): ?string
    {
        return $this->accessCode;
    }

    /**
     * @param string|null $accessCode
     */
    public function setAccessCode(?string $accessCode): void
    {
        $this->accessCode = $accessCode;
    }
}