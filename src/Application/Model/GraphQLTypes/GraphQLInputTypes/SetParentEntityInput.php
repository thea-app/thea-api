<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class SetParentEntityInput
{
    /**
     * @var int
     */
    private $entityId;

    /**
     * @var int|null
     */
    private $parentEntityId;

    /**
     * @param int $entityId
     * @param int|null $parentEntityId
     *
     * @return SetParentEntityInput
     */
    public static function create(
        int $entityId,
        ?int $parentEntityId
    ): SetParentEntityInput
    {
        $instance = new self();

        $instance->entityId = $entityId;
        $instance->parentEntityId = $parentEntityId;

        return $instance;
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     */
    public function setEntityId(int $entityId): void
    {
        $this->entityId = $entityId;
    }

    /**
     * @return int|null
     */
    public function getParentEntityId(): ?int
    {
        return $this->parentEntityId;
    }

    /**
     * @param int|null $parentEntityId
     */
    public function setParentEntityId(?int $parentEntityId): void
    {
        $this->parentEntityId = $parentEntityId;
    }
}