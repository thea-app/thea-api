<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Enum\EntityStateEnum;

class SetShopStateInput
{
    /**
     * @var int
     */
    private $shopId;

    /**
     * @var EntityStateEnum
     */
    private $state;

    /**
     * @param int $shopId
     * @param EntityStateEnum $state
     * @return SetShopStateInput
     */
    public static function create (
        int $shopId,
        EntityStateEnum $state
    ): SetShopStateInput
    {
        $instance = new self();

        $instance->shopId = $shopId;
        $instance->state = $state;

        return $instance;
    }
    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     */
    public function setShopId(int $shopId): void
    {
        $this->shopId = $shopId;
    }

    /**
     * @return EntityStateEnum|null
     */
    public function getState(): ?EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum|null $state
     */
    public function setState(?EntityStateEnum $state): void
    {
        $this->state = $state;
    }

}