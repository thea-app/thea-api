<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;

use DateTimeImmutable;

class CreateAccessCodesInput
{

    /**
     * @var int
     */
    private $eventId;

    /**
     * @var int
     */
    private $numOfCodes;

    /**
     * @var DateTimeImmutable|null
     */
    private $expiryDate;

    /**
     * @param int $eventId
     * @param int $numOfCodes
     * @param DateTimeImmutable|null $expiryDate
     * @return CreateAccessCodesInput
     */
    public static function create(
        int $eventId,
        int $numOfCodes,
        ?DateTimeImmutable $expiryDate
    ): CreateAccessCodesInput
    {
        $instance = new self();

        $instance->eventId = $eventId;
        $instance->numOfCodes = $numOfCodes;
        $instance->expiryDate = $expiryDate;

        return $instance;
    }

    /**
     * @return int
     */
    public function getEventId(): int
    {
        return $this->eventId;
    }

    /**
     * @param int $eventId
     */
    public function setEventId(int $eventId): void
    {
        $this->eventId = $eventId;
    }

    /**
     * @return int
     */
    public function getNumOfCodes(): int
    {
        return $this->numOfCodes;
    }

    /**
     * @param int $numOfCodes
     */
    public function setNumOfCodes(int $numOfCodes): void
    {
        $this->numOfCodes = $numOfCodes;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getExpiryDate(): ?DateTimeImmutable
    {
        return $this->expiryDate;
    }

    /**
     * @param DateTimeImmutable|null $expiryDate
     */
    public function setExpiryDate(?DateTimeImmutable $expiryDate): void
    {
        $this->expiryDate = $expiryDate;
    }

}