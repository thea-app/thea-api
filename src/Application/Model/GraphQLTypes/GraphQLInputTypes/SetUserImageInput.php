<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class SetUserImageInput
{

    /**
     * @var string|null
     */
    private ?string $uid;

    /** Image in base64 format
     *
     * @var string|null
     */
    private ?string $userImage;

    public static function create (
        ?string $uid,
        ?string $userImage
    ): SetUserImageInput
    {
        $instance = new self();

        $instance->uid = $uid;
        $instance->userImage = $userImage;

        return $instance;
    }

    /**
     * @return string|null
     */
    public function getUid(): ?string
    {
        return $this->uid;
    }

    /**
     * @param string|null $uid
     */
    public function setUid(?string $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return string|null
     */
    public function getUserImage(): ?string
    {
        return $this->userImage;
    }

    /**
     * @param string|null $userImage
     */
    public function setUserImage(?string $userImage): void
    {
        $this->userImage = $userImage;
    }
}