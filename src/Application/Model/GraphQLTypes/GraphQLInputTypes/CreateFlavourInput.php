<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class CreateFlavourInput
{
    /**
     * @var int
     */
    private $flavourWheelId;

    /**
     * @var TranslationInput[]
     */
    private $nameTranslations;

    /**
     * @var string
     */
    private $color;

    /**
     * @var int
     */
    private $ordering;

    /**
     * @var int|null
     */
    private $parentFlavourId;

    /**
     * @param int $flavourWheelId
     * @param TranslationInput[] $nameTranslations
     * @param string $color
     * @param int $ordering
     * @param int|null $parentFlavourId
     *
     * @return CreateFlavourInput
     */
    public static function create(
        int $flavourWheelId,
        array $nameTranslations,
        string $color,
        int $ordering,
        ?int $parentFlavourId): CreateFlavourInput
    {
        $instance = new self();

        $instance->flavourWheelId = $flavourWheelId;
        $instance->nameTranslations = $nameTranslations;
        $instance->color = $color;
        $instance->ordering = $ordering;
        $instance->parentFlavourId = $parentFlavourId;

        return $instance;
    }

    /**
     * @return int
     */
    public function getFlavourWheelId(): int
    {
        return $this->flavourWheelId;
    }

    /**
     * @param int $flavourWheelId
     */
    public function setFlavourWheelId(int $flavourWheelId): void
    {
        $this->flavourWheelId = $flavourWheelId;
    }

    /**
     * @return TranslationInput[]
     */
    public function getNameTranslations(): array
    {
        return $this->nameTranslations;
    }

    /**
     * @param TranslationInput[] $nameTranslations
     */
    public function setNameTranslations(array $nameTranslations): void
    {
        $this->nameTranslations = $nameTranslations;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return int|null
     */
    public function getParentFlavourId(): ?int
    {
        return $this->parentFlavourId;
    }

    /**
     * @param int|null $parentFlavourId
     */
    public function setParentFlavourId(?int $parentFlavourId): void
    {
        $this->parentFlavourId = $parentFlavourId;
    }

    /**
     * @return int
     */
    public function getOrdering(): int
    {
        return $this->ordering;
    }

    /**
     * @param int $ordering
     */
    public function setOrdering(int $ordering): void
    {
        $this->ordering = $ordering;
    }

}