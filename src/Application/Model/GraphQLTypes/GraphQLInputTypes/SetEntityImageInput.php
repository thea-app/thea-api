<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class SetEntityImageInput
{
    /**
     * @var string
     */
    private string $entityId;

    /** Image in base64 format
     *
     * @var string|null
     */
    private ?string $image;

    public static function create (
        string $entityId,
        ?string $image
    ): SetEntityImageInput
    {
        $instance = new self();

        $instance->entityId = $entityId;
        $instance->image = $image;

        return $instance;
    }

    /**
     * @return string
     */
    public function getEntityId(): string
    {
        return $this->entityId;
    }

    /**
     * @param string $entityId
     */
    public function setEntityId(string $entityId): void
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string|null $image
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

}