<?php

namespace App\Application\Model\GraphQLTypes;

use TheCodingMachine\GraphQLite\Annotations\Type;
use TheCodingMachine\GraphQLite\Annotations\Field;

use App\Application\Model\Enum\SocialsTypeEnum;

/** GraphQLite annotations
 * @Type()
 *
 * Class SocialTypeStaticUrl
 * @package App\Application\Model\GraphQLTypes
 */
class SocialTypeStaticURL
{
    /**
     * @var SocialsTypeEnum
     */
    protected $type;

    /**
     * @var string
     */
    protected $staticURL;

    public static function create(SocialsTypeEnum $type, string $staticURL): SocialTypeStaticURL
    {
        $instance = new self();

        $instance->type = $type;
        $instance->staticURL = $staticURL;

        return $instance;
    }

    /**
     * @Field()
     *
     * @return SocialsTypeEnum
     */
    public function getType(): SocialsTypeEnum
    {
        return $this->type;
    }

    /**
     * @Field()
     *
     * @return string
     */
    public function getStaticURL(): string
    {
        return $this->staticURL;
    }


}