<?php


namespace App\Application\Model;



use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use Exception;


trait Timestampable
{
    /**
     * @var DateTimeImmutable|null
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    protected $created;

    /**
     * @var DateTimeImmutable|null
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    protected $modified;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @throws Exception
     */
    public function updateTimestamps()
    {
        $this->setModified(new DateTimeImmutable('now', new DateTimeZone('Europe/Prague')));

        if($this->getCreated() == null) {
            $this->setCreated(new DateTimeImmutable('now', new DateTimeZone('Europe/Prague')));
        }
    }

    /**
     * @param DateTimeImmutable $created
     */
    public function setCreated(DateTimeImmutable $created): void
    {
        $this->created = $created;
    }

    /**
     * @param DateTimeImmutable $modified
     */
    public function setModified(DateTimeImmutable $modified): void
    {
        $this->modified = $modified;
    }

    /**
     * @Field(outputType="DateTime")
     *
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function getCreated(): ?DateTimeImmutable
    {
        if ($this->created == null)
            return null;
        return new DateTimeImmutable($this->created->format(DateTimeImmutable::ATOM));
    }

    /**
     * @Field(outputType="DateTime")
     *
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function getModified(): ?DateTimeImmutable
    {
        if ($this->modified == null)
            return null;
        return new DateTimeImmutable($this->modified->format(DateTimeImmutable::ATOM));
    }

}