<?php


namespace App\Application\Model\Exceptions;


use Exception;

class InputNotValidException extends Exception
{
    public function __construct($message = "Input not valid.")
    {
        parent::__construct($message, 1);
    }
}