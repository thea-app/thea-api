<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class CountryEnum
 * @package App\Application\Model\Enum
 */
class CountryEnum extends Enum
{
    public const CZ = 'CZ';
    public const SK = 'SK';
}