<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class EventFilterEnum
 * @package App\Application\Model\Enum
 */
class EventFilterEnum extends Enum
{
    //date of end still not ended
    public const FUTURE = 'FUTURE';

    //date of end already ends in the past
    public const PAST = 'PAST';

    //ongoing event
    public const ONGOING = 'ONGOING';
}