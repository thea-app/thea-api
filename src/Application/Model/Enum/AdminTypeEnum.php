<?php


namespace App\Application\Model\Enum;


use MyCLabs\Enum\Enum;

class AdminTypeEnum extends Enum
{
    public const SUPERADMIN = 'SUPERADMIN';
    public const ADMIN = 'ADMIN';
}