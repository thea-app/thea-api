<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class CurrencyEnum
 * @package App\Application\Model\Enum
 */
class CurrencyEnum extends Enum
{
    public const CZK = 'CZK';
    public const EUR = 'EUR';
}