<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class UserTypeEnum
 * @package App\Application\Model\Enum
 */
class UserTypeEnum extends Enum
{
    public const SUPERUSER = 'SUPERUSER';
    public const CUSTOMER = 'CUSTOMER';
    public const SUPERADMIN = 'SUPERADMIN';
    public const ADMIN = 'ADMIN';
}