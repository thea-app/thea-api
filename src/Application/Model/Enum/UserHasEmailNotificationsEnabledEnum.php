<?php


namespace App\Application\Model\Enum;


use MyCLabs\Enum\Enum;

/**
 * Class UserHasEmailNotificationsEnabledEnum
 * @package App\Application\Model\Enum
 */
class UserHasEmailNotificationsEnabledEnum extends Enum
{
    public const ALLOWED_AND_ENABLED = 'ALLOWED_AND_ENABLED';
    public const ALLOWED_AND_DISABLED = 'ALLOWED_AND_DISABLED';
    public const NOT_ALLOWED = 'NOT_ALLOWED';
}