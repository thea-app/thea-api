<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

class ShopTypeEnum extends Enum
{
    public const TEA = 'TEA';
    public const COFFEE = 'COFFEE';
    public const WINE = 'WINE';
}