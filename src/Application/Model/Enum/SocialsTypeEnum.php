<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class SocialsTypeEnum
 * @package App\Application\Model\Enum
 */
class SocialsTypeEnum extends Enum
{
    public const FACEBOOK = 'FACEBOOK';
    public const TWITTER = 'TWITTER';
    public const YELP = 'YELP';
    public const ZOMATO = 'ZOMATO';
    public const TRIP_ADVISOR = 'TRIP_ADVISOR';
    public const OTHER = 'OTHER';

    /**
     * @param SocialsTypeEnum $e
     * @return String
     */
    public static function getLinkUrl(SocialsTypeEnum $e) : String {
        switch ($e) {
            case SocialsTypeEnum::FACEBOOK:
                return "https://www.facebook.com/";
            case SocialsTypeEnum::TWITTER:
                return "https://www.twitter.com/";
            case SocialsTypeEnum::YELP:
                return "https://www.yelp.com/";
            case SocialsTypeEnum::ZOMATO:
                return "https://www.zomato.com/";
            case SocialsTypeEnum::TRIP_ADVISOR:
                return "https://www.tripadvisor.com/";
            default:
                return "";
        }
    }
}