<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

class ShopFilterEnum extends Enum
{
    public const NO_FILTER = 'NO_FILTER';
//    public const TOP = 'TOP';
    public const NEAR = 'NEAR';
}