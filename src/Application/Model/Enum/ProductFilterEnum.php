<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

class ProductFilterEnum extends Enum
{
    public const NO_FILTER = 'NO_FILTER';
    public const MENU = 'MENU';
//    public const TASTING = 'TASTING'; todo: when tastings are implemented
}