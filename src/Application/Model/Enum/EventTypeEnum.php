<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class EventTypeEnum
 * @package App\Application\Model\Enum
 */
class EventTypeEnum extends Enum
{
    public const TASTING = 'TASTING';
}