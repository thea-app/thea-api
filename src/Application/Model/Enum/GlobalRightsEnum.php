<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class GlobalRightsEnum
 * @package App\Application\Model\Enum
 *
 * Enum of all global rights codes in GraphQLite Security annotation is_granted(), that can be given to specific user via UserRights db entity
 * For scope based rights, that are connected with scope, @see ScopeRightsEnum
 */
class GlobalRightsEnum extends Enum
{
    public const CAN_GET_USERS          = 'CAN_GET_USERS';
    public const CAN_DELETE_USER        = 'CAN_DELETE_USER';

    public const CAN_CREATE_SHOP        = 'CAN_CREATE_SHOP';
    public const CAN_UPDATE_SHOP_STATE  = 'CAN_UPDATE_SHOP_STATE';
    public const CAN_DELETE_SHOP        = 'CAN_DELETE_SHOP';
}