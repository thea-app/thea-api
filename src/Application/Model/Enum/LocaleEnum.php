<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class LocaleEnum
 * @package App\Application\Model\Enum
 */
class LocaleEnum extends Enum
{
    public const cs = 'cs';
    public const sk = 'sk';
    public const en = 'en';
}