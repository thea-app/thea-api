<?php


namespace App\Application\Model\Enum;


use MyCLabs\Enum\Enum;

class NotificationGroupEnum extends Enum
{
    public const VIP = 'VIP';
    public const FAVOURITE = 'FAVOURITE';
    public const MAIN = 'MAIN';
}