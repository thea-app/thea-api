<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class TastingStateEnum
 * @package App\Application\Model\Enum
 */
class TastingStateEnum extends Enum
{
    /** @var string user is not registered and tasting has not started yet */
    public const UNREGISTERED_NOT_STARTED = 'UNREGISTERED_NOT_STARTED';

    /** @var string user is not registered and tasting has already started */
    public const UNREGISTERED_STARTED = 'UNREGISTERED_STARTED';

    /** @var string user is not registered and tasting has ended already */
    public const UNREGISTERED_ENDED = 'UNREGISTERED_ENDED';

    /** @var string user is registered and tasting has not stared yet */
    public const REGISTERED_NOT_STARTED = 'REGISTERED_NOT_STARTED';

    /** @var string user is registered and tasting has already started and user still not finished the tasting*/
    public const REGISTERED_STARTED_NOT_FINISHED = 'REGISTERED_STARTED_NOT_FINISHED';

    /** @var string user is registered and tasting has already started and user already finished the tasting */
    public const REGISTERED_STARTED_FINISHED = 'REGISTERED_STARTED_FINISHED';

    /** @var string user is registered, tasting has already ended but user still not finished the tasting */
    public const REGISTERED_ENDED_NOT_FINISHED = 'REGISTERED_ENDED_NOT_FINISHED';

    /** @var string user is registered, tasting has already ended and user already finished the tasting */
    public const REGISTERED_ENDED_FINISHED  = 'REGISTERED_ENDED_FINISHED ';
}