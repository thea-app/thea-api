<?php


namespace App\Application\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class ScopeRightsEnum
 * @package App\Application\Model\Enum
 *
 * Enum of all scope based rights in GraphQLite Security annotation is_granted()
 * For global rights, that are not connected with scope, @see GlobalRightsEnum
 */
class ScopeRightsEnum extends Enum
{
    public const CAN_MANAGE_USER            = 'CAN_MANAGE_USER';
    public const CAN_GET_PRIVATE_USER_INFO  = 'CAN_GET_PRIVATE_USER_INFO';

    public const CAN_GET_PRIVATE_SHOP_INFO  = 'CAN_GET_PRIVATE_SHOP_INFO';
    public const CAN_MANAGE_SHOP            = 'CAN_MANAGE_SHOP';

    public const CAN_MANAGE_ACCESS_CODES    = 'CAN_MANAGE_ACCESS_CODES';
    public const CAN_MANAGE_BONUSES         = 'CAN_MANAGE_BONUSES';
    public const CAN_MANAGE_MENU            = 'CAN_MANAGE_MENU';
    public const CAN_MANAGE_EVENTS          = 'CAN_MANAGE_EVENTS';
    public const CAN_MANAGE_NOTIFICATIONS   = 'CAN_MANAGE_NOTIFICATIONS';
    public const CAN_MANAGE_VIP             = 'CAN_MANAGE_VIP';
    public const CAN_MANAGE_SHOP_ADMINS     = 'CAN_MANAGE_SHOP_ADMINS';
}