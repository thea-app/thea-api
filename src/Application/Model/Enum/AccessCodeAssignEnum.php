<?php


namespace App\Application\Model\Enum;


use MyCLabs\Enum\Enum;

class AccessCodeAssignEnum extends Enum
{
    public const ALL = 'ALL';
    public const ASSIGNED = 'ASSIGNED';
    public const NOT_ASSIGNED = 'NOT_ASSIGNED';
}