<?php


namespace App\Application\Model\Enum;


use MyCLabs\Enum\Enum;

/**
 * Class EntityStateEnum
 * @package App\Application\Model\Enum
 */
class EntityStateEnum extends Enum
{
    public const ACTIVE = 'ACTIVE';
    public const INACTIVE = 'INACTIVE';
    public const DELETED = 'DELETED';
}