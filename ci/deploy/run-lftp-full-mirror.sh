#!/bin/bash

lftp --user $FTP_USERNAME --env-password $FTP_SERVER <<LFTP_COMMANDS
cd $FTP_DIRECTORY;

set log:enabled yes;
set log:file ./ci/output/lftp.log;
set log:level 6;

mirror --delete --verbose --reverse --overwrite --upload-older --transfer-all --parallel=10 --no-umask --no-perms --log=./ci/output/lftp-mirror.log.sh --exclude-rx-from=./.ci/deploy/lftp-ignore.rx . .;

bye;
LFTP_COMMANDS
