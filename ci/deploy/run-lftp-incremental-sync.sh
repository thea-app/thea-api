#!/bin/bash

GREEN='\033[1;32m'
YELLOW='\033[0;33m'
RED='\033[1;31m'
BLUE='\033[1;36m'
UNDERLINE='\033[4m'
UNDERLINE_OFF='\033[24m'
NO_COLOR='\033[0m' # No Color

function countLines () {
	# Echo is just to add new line to end of file in case it is missing
	# If file does not end with new line, line counting doesnt work as expected
	# https://stackoverflow.com/a/1521498/1513087
	echo `{ cat $1; echo ''; } | sed '/^\s*$/d' | wc -l`
}

MAX_PARALLEL_JOBS=10

# -------------------------------------------------------------------------------------

CHANGED_FILES_PATH="ci/output/changedFiles.txt"
CHANGED_FILES_CHUNKS_PATH="ci/output/changedFilesChunks"
CHANGED_FILES_CHUNK_SIZE=500
CREATE_DIRS_PATH="ci/output/createDirs.optimized.txt"
REMOVE_DIRS_PATH="ci/output/removeDirs.optimized.txt"
REMOVE_FILES_PATH="ci/output/removeFiles.optimized.txt"

LFTP_MPUT_COMMAND=""
LFTP_CREATE_DIRS_QUEUE_COMMANDS=""
LFTP_REMOVE_DIRS_QUEUE_COMMANDS=""
LFTP_REMOVE_FILES_QUEUE_COMMANDS=""

if [[ -f $CHANGED_FILES_PATH ]]; then
	filesToUpload=`countLines $CHANGED_FILES_PATH`
	echo -e "${GREEN}There are ${UNDERLINE}${filesToUpload} files to upload${UNDERLINE_OFF} - listed in ${CHANGED_FILES_PATH}${NO_COLOR}"

	if [[ $filesToUpload -gt 0 ]]; then
		mkdir -p $CHANGED_FILES_CHUNKS_PATH
		split --lines=$CHANGED_FILES_CHUNK_SIZE --numeric-suffixes=1 --suffix-length=4 $CHANGED_FILES_PATH "$CHANGED_FILES_CHUNKS_PATH/chunk_"
		echo -e "${BLUE}All files to upload were splitted into ${UNDERLINE}`ls -1 $CHANGED_FILES_CHUNKS_PATH | wc -l` chunks by $CHANGED_FILES_CHUNK_SIZE files${UNDERLINE_OFF} into folder $CHANGED_FILES_CHUNKS_PATH${NO_COLOR}"

		for chunkFile in "$CHANGED_FILES_CHUNKS_PATH"/*; do
			chunkFilesToPut=`sed '/^\s*$/d' $chunkFile | sed -e "s/\(.*\)/\"\1\"/" | tr '\n' ' '`
			LFTP_MPUT_COMMAND="${LFTP_MPUT_COMMAND}!date +\"%F %T.%N %Z\""$'\n'"echo \"Uploading files in $chunkFile\""$'\n'"mput -d -P $MAX_PARALLEL_JOBS ${chunkFilesToPut}"$'\n'
		done
	fi
else
	echo -e "${RED}File '$CHANGED_FILES_PATH' does not exists${NO_COLOR}"
fi

if [[ -f $CREATE_DIRS_PATH ]]; then
	dirsToCreate=`countLines $CREATE_DIRS_PATH`
	echo -e "${GREEN}There are ${UNDERLINE}${dirsToCreate} directories to create${UNDERLINE_OFF} - listed in $CREATE_DIRS_PATH${NO_COLOR}"

	if [[ $dirsToCreate -gt 0 ]]; then
		LFTP_CREATE_DIRS_QUEUE_COMMANDS="# Start creating directories"$'\n'
		while IFS="" read -r dir || [ -n "$dir" ]; do
		  LFTP_CREATE_DIRS_QUEUE_COMMANDS="${LFTP_CREATE_DIRS_QUEUE_COMMANDS}queue mkdir -pf ${dir}"$'\n'
		done < $CREATE_DIRS_PATH
	fi
else
	echo -e "${RED}File '$CREATE_DIRS_PATH' does not exists${NO_COLOR}"
fi

if [[ -f $REMOVE_DIRS_PATH ]]; then
	dirsToRemove=`countLines $REMOVE_DIRS_PATH`
	echo -e "${YELLOW}There are ${UNDERLINE}${dirsToRemove} directories to remove${UNDERLINE_OFF} - listed in $REMOVE_DIRS_PATH${NO_COLOR}"

	if [[ $dirsToRemove -gt 0 ]]; then
		while IFS="" read -r dir || [ -n "$dir" ]; do
		  LFTP_REMOVE_DIRS_QUEUE_COMMANDS="${LFTP_REMOVE_DIRS_QUEUE_COMMANDS}queue echo \"Removing directory ${dir} recursively\""$'\n'"queue rm -rf ${dir}"$'\n'
		done < $REMOVE_DIRS_PATH
	fi
else
	echo -e "${RED}File '$REMOVE_DIRS_PATH' does not exists${NO_COLOR}"
fi

if [[ -f $REMOVE_FILES_PATH ]]; then
	filesToRemove=`countLines $REMOVE_FILES_PATH`
	echo -e "${YELLOW}There are ${UNDERLINE}${filesToRemove} files to remove${UNDERLINE_OFF} - listed in $REMOVE_FILES_PATH${NO_COLOR}"

	if [[ $filesToRemove -gt 0 ]]; then
		LFTP_REMOVE_FILES_QUEUE_COMMANDS="# Start removing files"$'\n'
		while IFS="" read -r file || [ -n "$file" ]; do
		  LFTP_REMOVE_FILES_QUEUE_COMMANDS="${LFTP_REMOVE_FILES_QUEUE_COMMANDS}queue rm -f ${file}"$'\n'
		done < $REMOVE_FILES_PATH
	fi
else
	echo -e "${RED}File '$REMOVE_FILES_PATH' does not exists${NO_COLOR}"
fi

LFTP_COMMANDS=$(cat <<LFTP_COMMANDS
cd $FTP_DIRECTORY;
set log:enabled yes;
set log:file ./ci/output/lftp.log;
set log:level 6;

$LFTP_MPUT_COMMAND

# queue echo && rm -rf somehow turn off parallel
set cmd:queue-parallel $MAX_PARALLEL_JOBS;
queue stop;

$LFTP_CREATE_DIRS_QUEUE_COMMANDS
$LFTP_REMOVE_DIRS_QUEUE_COMMANDS
$LFTP_REMOVE_FILES_QUEUE_COMMANDS

queue start;
wait all;

bye;
LFTP_COMMANDS
)

echo "$LFTP_COMMANDS" > ci/output/lftp_commands.sh

lftp --user $FTP_USERNAME --env-password $FTP_SERVER < ci/output/lftp_commands.sh
