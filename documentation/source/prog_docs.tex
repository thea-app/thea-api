\chapter{Programátorská dokumentace}

Tato kapitola přibližuje základní principy a strukturu projektu. Jsou zde popsány hlavní stavební prvky projektu spolu s nejčastěji používanými funkcionalitami napříč celou aplikací. 

\section{Struktura aplikace}

Tato podkapitola popisuje základní strukturu aplikace, rozdělení skriptů do jmenných prostorů a základní stavební bloky aplikace. 

\subsection{Základní struktura aplikace}

Jedním z důležitých cílů při vytváření aplikace byla přehlednost a strukturovanost kódu. Části aplikace jsou proto rozděleny do jmenných prostorů a samotné skripty jsou hierarchicky rozděleny do adresářů.

Mezi hlavní části aplikace na kořenové úrovni patří adresáře app, ci, docker, logs, public, src, userFiles, var a vendor. Na kořenové úrovni se rovněž nachází soubor .env, který obsahuje data pro přístup do databáze a nastavení vývojářského režimu, .gitignore, který obsahuje seznam souborů, které se nenahrávají na git, .htaccess a composer.json obsahující informace o projektu a závislosti a definice použitých externích knihoven.
\begin{itemize}
    \item Adresář \textbf{app} obsahuje skripty dependencies.php pro definování závislostí používaných napříč aplikací, middleware.php definující middleware, routes.php definující http cesty k jednotlivým službám v aplikaci a settings.php obsahující základní nastavení projektu.

    \item Adresář \textbf{ci} obsahuje potřebné informace k fungování CI (Continuous Integration) pro automatické nasazování skriptů na server.

    \item Adresář \textbf{logs} je určen pro logy volání API. Tyto soubory se neverzují, takže v repozitáři je tato složka prázdná.

    \item Adresář \textbf{public} obsahuje skript index.php, který je vstupním bodem do aplikace. V něm se inicializují služby, potřebné pro fungování aplikace.

    \item Adresář \textbf{source} obsahuje funkční kódy aplikace. Je dále členěn na podadresáře Application, DoctrineExtensions a Firebase. V podadresáři Application je logika aplikace, model dat, kontroléry a pomocné služby. V podadresáři DoctrineExtensions je pomocný skript, který rozšiřuje dotazovací jazyk DQL využíván v GraphQL

    \item Adresář \textbf{userFiles} obsahuje obrázky, které uživatelé nahráli na server prostřednictvím mobilní nebo webové aplikace. Každý obrázek je uložen v samostatném podadresáři, který má stejný název jaký je identifikátor obrázku v databázi. Do tohoto podadresáře se také ukládají různé velikosti a formáty  obrázku, aby se při opakovaných dotazech na stejný obrázek šetřila výpočetní kapacita serveru.

    \item Adresář \textbf{var} obsahuje doctrine cache.

\end{itemize}

\subsection{Entity a definice databáze}
Z počáteční analýzy zadání práce nám vzešly určité základní objekty, které reprezentují objekty z reálného světa a se kterými potřebujeme pracovat v aplikaci. Mezi takové objekty patří například uživatel, obchod nebo degustace. Takové objekty v našem projektu reprezentují právě entity. Ty jsou umístěny ve jmenném prostoru \textit{App\textbackslash Application\textbackslash Model\textbackslash Entities}. U objektů je navíc potřeba ukládat si o nich data do databáze. K tomu slouží ORM anotace v jednotlivých entitách, které definují databázi jako takovou.

Entity jsou ve své podstatě PHP třídy, které zpravidla obsahují název entity, privátní atributy, gettery a settery, konstruktor, který je realizován pomocí funkce create, pomocné metody, ORM anotace a GraphQL anotace.

Každá entita, která představuje nějakou tabulku v databázi, obsahuje anotaci \textit{ORM\textbackslash Table}. V parametrech této anotace je pak definován název tabulky a případně nějaký vysvětlující popis.

Entity navíc mohou definovat GraphQL typy. Pro tento účel slouží anotace Type, ve které je jako parametr nastaven konkrétní datový typ pro GraphQL.

Privátní atributy reprezentují data o jednotlivých entitách. Pokud atribut reprezentuje sloupec v tabulce, entita obsahuje o takového atributu anotaci \textit{ORM\textbackslash Column}. V parametrech této anotace jsou pak definovány vlastnosti sloupce jako název, datový typ, zda je povolena null hodnota, maximální povolená délka hodnoty a jiné. \footnote{Všechny možné atributy jsou uvedeny v dokumentaci doctrine na \url{https://www.doctrine-project.org/}} Atributy v entitách mohou být také definovány jako vazby mezi tabulkami. K tomuto účelu slouží anotace \textit{ORM\textbackslash OneToOne}, \textit{ORM\textbackslash OneToMany}, \textit{ORM\textbackslash ManyToOne} a \textit{ORM\textbackslash ManyToMany}.\footnote{Bližší informace k těmto anotacím jsou uvedeny na dokumentačních stránkách \url{https://www.doctrine-project.org/}}

Gettery a settery jsou metody, jejichž název začíná prefixem get nebo set. V entitách slouží na vracení nebo nastavování dat do instancí daných entit. Některé metody nevracejí přímo hodnotu získanou z databáze ale ji dynamicky počítají.

Getter a setter jsou velmi často používány ve spojení s GraphQL, kde definují pole ve schématu. K tomuto účelu je použita anotace Field. Takto označenou metodu rozpozná GraphQL a dokáže přistoupit k návratové hodnotě metody. Toto se pak používá při volání jednotlivých query a mutací.

Další důležitou anotací ve spojení s get metodami je GraphQL anotace \textit{Security}. Ta definuje, zda ten, kdo se dotazuje k danému poli, má na to oprávnění. Jsou použity dva způsoby, jak zjistit, zda dotazující se uživatel má příslušná oprávnění. Prvním je použití funkce is\_granted. Ta dostane ve svém parametru typ povolení. Ten samý typ musí být přiřazen k uživateli v tabulce user\_rights, jinak k poli nemůže přistoupit. Druhým způsobem, jak použít anotaci \textit{Security} je zavolat funkci, která rozhodne a vrátí, zda má volající user povolení. Na základě této bool hodnoty se pak poskytne uživateli přístup k poli. Výjimku v přístupu k jednotlivým polím má superuser, na kterého se práva nestahují a který může přistoupit k jakémukoliv poli.

Konstruktor každé entity zajišťuje metoda create. Ta vytvoří instanci dané třídy, nastaví počáteční hodnoty ze svých parametrů a vrátí instanci třídy.

V entitách jsou dále použity různé GraphQL a ORM anotace upřesňující požadované vlastnosti.

\subsection{Query kontroléry}
Query kontrolery jsou PHP třídy umístěny ve jmenném prostoru \textit{App\textbackslash Application\textbackslash Controllers\textbackslash QueryControllers}. Obsahují GraphQL query a mutace pro jednotlivé entity, které slouží pro dotazování mobilu a webu do API. Query jsou dotazy, které nemění podobu dat nebo stav (například detail obchodu) a mutace jsou dotazy, které mění data nebo stav (například přihlášení nebo upload obrázku).

Každý query kontrolér je potomkem třídy BaseController. BaseController obsahuje repozitáře pro jednotlivé entity, při kterých potřebujeme přistupovat do databáze. BaseController navíc obsahuje instance MediaService a AuthorizationService.

Query kontroléry jsou většinou pojmenovány podle entity, se kterou primárně pracují, zakončené suffixem Controller. Obsahují jednotlivé query a mutace, které se vztahují k práci s danou entitou. Někdy mohou obsahovat konstruktor a pomocné metody.

Query je metoda v kontroléru, která obsahuje anotaci Query, kde jako parametr je její název. Metoda může obsahovat parametry pro zpřesnění požadavků při volání.

Mutace je metoda v kontroléru, která obsahuje anotaci Mutation, kde jako parametr je její název. Dále obsahuje vstupní parametry. Pokud jde o několik vstupních parametrů, mohou být definovány v takzvaném input typu. Všechny input typy jsou umístěny v balíčku \textit{App\textbackslash Application\textbackslash Model\textbackslash GraphQLTypes\textbackslash GraphQLInputTypes}.

Každá query a mutace má návratový typ, který je zároveň GraphQL typem. Tímto návratovým typem se říká response typy a jsou umístěny ve jmenném prostoru \textit{App\textbackslash  Application\textbackslash  Model\textbackslash  GraphQLTypes\textbackslash  GraphQLResponseModels}. Každý návratový typ dědí od GraphQLResponseModel třídy, která definuje základní strukturu dat. 

\subsection{GraphQL typy}

K tomu, aby komunikace prostřednictvím GraphQL mohla fungovat, je nutné definovat takzvané GraphQL typy. Z nich je pak vyskládaná celá GraphQL schéma a ta definuje celé rozhraní API. Typy, které tvoří GraphQL schéma jsou input typy, response typy, typy entit a polí v entitách, query a mutace a uživatelsky definované výčty, neboli Enumy.

Input typy se používají pro vstupní parametry při volání query nebo mutací. Jsou umístěny ve jmenném prostoru \textit{App\textbackslash Application\textbackslash Model\textbackslash GraphQLTypes\textbackslash GraphQLInputTypes}. Každý input typ musí být navíc definován v třídě InputTypesFactory a označen anotací Factory. Z této třídy se berou informace o všech input typech do GraphQL schématu.

Response typy se používají jako návratové hodnoty při voláních query a mutací. Jsou označeny anotací Type a jsou umístěny ve jmenném prostoru \textit{App\textbackslash  Application\textbackslash  Model\textbackslash  GraphQLTypes\textbackslash  GraphQLResponseModels}. Každý response typ dědí od třídy GraphQLResponseModel, která definuje základní strukturu. Sem patří pole success, errorMessage, errorCode a timestamp. 

\begin{itemize}
    \item \textbf{Success} je bool hodnota, která hovoří o tom, zda dotaz na API proběhl v pořádku nebo zda nastala někde chyba. 

    \item \textbf{ErrorMessage} a \textbf{ErrorCode} potom uprestňujú v případě chyby popis a kód chyby.

    \item \textbf{Timestamp} je časový otisk dotazu
\end{itemize}


Některé response typy zpravidla ještě obsahují pole data. V případě, že dotaz proběhl v pořádku, pole data obsahuje samotná data, na která se uživatel dotazoval, jinak je vrácena null hodnota.

Další část GraphQL typů je definována v jednotlivých entitách v jmenném prostoru\textit{ App\textbackslash Application\textbackslash Model\textbackslash Entities}. Zde se využívá opět anotace Type s parametrem názvu typu. Součástí GraphQL schémata jsou také pole v rámci entit, na které se může uživatel dotazovat. Tato pole jsou metody v entitách s prefixem get nebo is a s anotací Field.

Uživatel při dotazech na API používá query a mutace. Všechny query a mutace jsou definovány v GraphQL kontrolérech v jmenném prostoru \textit{App\textbackslash Application\textbackslash Controllers\textbackslash QueryControllers}. Bližší informace o kontrolérech jsou popsány v sekci Query kontroléry.

Poslední součástí GraphQL schématu jsou uživatelsky definované Enum typy. Ty se nacházejí ve jmenném prostoru \textit{App\textbackslash Application\textbackslash Model\textbackslash Enum} a dědí od třídy Enum. Na to, aby GraphQL knihovna našla informace o definovaných Enumech, musí být zaregistrovány v souboru dependencies.php prostřednictvím metod registerEnumTypes a registerDoctrineTypeMapping.

\subsection{Ostatní části projektu}
Mezi další důležité části projektu patří media service a security service. Media service je služba, která umožnuje práci s obrázky. To, jak tato služba funguje je popsáno v sekci Média. Security service je služba, která definuje a hlídá přístupová práva ke query a mutacím a k jednotlivým polím entit. Bližší informace o této službě jsou popsány v sekci Entity a definice databázi.

\section{Vybrané funkcionality}

\subsection{Média}

Základní třídou, která zastřešuje práci s médii je třída MediaService. Na ukládání jsou povoleny obrázky ve formátu webp, png a jpeg. Tato třída poskytuje následující metody:
\begin{itemize}
    \item \textbf{importFromBase64} --- přeloží string ve formátu Base64 na soubor ve formátu definovaném konstantou BASE\_IMAGE\_FORMAT a v případě, že je výsledný soubor korektní, uloží ho na server a vrátí identifikátor, na jehož základě je k němu možné přistupovat
    
    \item \textbf{replaceFromBase64} --- přeloží string ve formátu Base64 na soubor ve formátu definovaném konstantou BASE\_IMAGE\_FORMAT a v případě, že je výsledný soubor korektní, tak: 
    \begin{enumerate}
        \item Odstraní starý soubor, který je identifikován přijatým identifikátorem
        \item Uloží nový soubor na server a vrátí identifikátor, na jehož základě je k němu možné přistupovat
    \end{enumerate}
    
    \item \textbf{getImageByIdentifier} --- na základě identifikátoru vrátí soubor ze serveru v požadovaném formátu a s požadovanými rozměry
    
    \item \textbf{deleteImage} --- odstraní soubor na základě identifikátoru
    
\end{itemize}

Úložištěm pro jednotlivé soubory, se kterými se pracuje v rámci MediaService, je složka userFiles nacházející se v kořeni projektu. Ten obsahuje složky, jejichž název odpovídá identifikátorům uložených souborů. Uvnitř se nachází soubor base.webp (formát souboru je konfigurovatelný pomocí konstanty BASE\_IMAGE\_FORMAT). Tento soubor představuje původní obrázek. Dále se do této složky při volání getImageByIdentifier ukládají soubory dle jmenných konvencí:
\begin{lstlisting}
    {width} + "_" + {height} + "." + {format}
\end{lstlisting}
Příklad:
\begin{lstlisting}
   500_500.webp
\end{lstlisting}
Takovým ukládáním se zrychluje práce s médii v případě, že se dotazy opakují. Není třeba vždy přizpůsobovat formát a velikost souboru, ale v případě, že soubor s požadovanými parametry již existuje, jen se vrátí.

Je zde zároveň implementována základní ochrana proti Denial of Service útokům založeným na snaze přehltit server dotazy na konverzi obrázků v různých velikostech. Je nastavena maximální velikost obrázku, a velikosti jsou před konverzí zaokrouhlovány nahoru tak, aby se při dotazech na podobné velikosti zamezilo zbytečným konverzím.

Na získávání souborů z klientských částí projektu je vystaven endpoint končící cestou /media. URL adresa sloužící k získání souboru má takovýto formát:
\begin{lstlisting}
    {URL serveru} + "/media/" + {width} + ":" + {height} + "/" + 
    {imageIdentifier} + "." + {format}
\end{lstlisting}
Příklad:
\begin{lstlisting}
    https://thea-api.jenda.dev/media/500:500/1e93a11b860ee47dd8f80f6d16e26688ada752fa.webp
\end{lstlisting}

\subsection{Firebase}

Firebase je služba, která poskytuje nástroje pro vytváření webových a mobilních aplikací. V tomto projektu jsou využity nástroje pro autorizaci uživatelů a push notifikace.

K přistupování k službám Firebase je použit balíček Kreait\textbackslash Firebase. Ten je importován v souboru composer.json ve verzi 5.20. Jednotlivé služby jsou definovány v souboru dependencies.php a jsou pojmenovány jako Auth a Messaging.

\begin{lstlisting}[caption={Služby Auth a Messaging},captionpos=b]
\Kreait\Firebase\Auth::class => function (ContainerInterface $container) {
    //get firebase credentials filename from settings
    $settings = $container->get('settings');
    $firebaseCredentials = $settings['firebaseCredentials'];

    $factory = (new Factory)->withServiceAccount(dirname(__DIR__, 1) . $firebaseCredentials);

    return $factory->createAuth();
},

\Kreait\Firebase\Messaging::class => function (ContainerInterface $container) {
    //get firebase credentials filename from settings
    $settings = $container->get('settings');
    $firebaseCredentials = $settings['firebaseCredentials'];

    $factory = (new Factory)->withServiceAccount(dirname(__DIR__, 1) . $firebaseCredentials);

    return $factory->createMessaging();
}
\end{lstlisting}
Služba Auth se využívá v některých query a mutacích v případě, že je potřeba získat data o uživateli z Firebase. Taková situace nastane například při vytváření nového uživatele mobilní aplikace, kdy se nejprve vytvoří uživatel ve Firebase a až po úspěšném vytvoření se dále vytváří do serverové databázi. Na to, zda je v momentě vytváření uživatele v serverové databázi již ten uživatel ve Firebase, se právě použije služba Auth.

Služba Messaging se využívá při práci s oznámeními. V případě, že administrátor podniku chce poslat notifikaci svým zákazníkům, interně na API se zavolá služba Firebase Cloud Messaging pomocí kterého se pošlou oznámení na zařízení zákazníků.

\subsection{Překlady}

Součástí řešení je i možnost vytvářet a prezentovat vícejazyčné texty. K tomuto slouží právě translation entity, umístěné v jmenném prostoru \textit{App\textbackslash Application\textbackslash Model\textbackslash Entities}. Každé entitě, při které to má smysl náleží jedna translation entity. Pojmenování nese po své přidružené entitě, které je ukončeno Translations suffixem.

Ke každé translation entitě existuje tabulka v databázi se stejným názvem. V ní jsou uloženy údaje o jazyku daného překladu, reference k jakému záznamu přidružené entity se daný překlad váže a všechna přeložitelná pole vztahující se k dané entitě (většinou se jedná o pole name a description, ale mohou se objevovat i jiná).

\subsection{Trait Timestampable}

V aplikaci existují pole, které se používají napříč všemi entitami. Jde o pole created a modified, vyjadřující datum a čas vytvoření, respektive modifikace záznamů v databázi. Tyto záznamy jsou vkládány do všech entit. Proto dávalo smysl vytvořit v projektu trait, který umožňuje znovupoužití kódu a který se stará o vytváření a nastavování těchto hodnot, čímž odpadá nutnost řešit tyto položky v jednotlivých entitách. V projektu se tento trait nazývá Timestampable a je umístěn v jmenném prostoru \textit{App\textbackslash Application\textbackslash Model}. 

Na to, aby trait Timestampable fungoval pro entitu, je potřebné, aby entita obsahovala anotaci \textit{ORM\textbackslash HasLifecycleCallbacks}, což zajistí fungování anotací \textit{ORM\textbackslash PrePersist} a \textit{ORM\textbackslash PreUpdate} uvnitř traitu.

Trait obsahuje zmíněné pole created a modified, get a set metody a metodu updateTimestamps. Tato metoda hlídá akce persist a update na příslušné entitě u které je tento trait aplikovaný, a těsně před jejich provedením nastavuje hodnoty created, pokud ještě není nastavena a modified dané entitě. Navíc metody get obsahují GraphQL anotaci Field. Tím se pole created a modified promítnou i do GraphQL schématu.

\section{Modelový příklad volání query}

V této podkapitole je popsán modelový příklad volání dotazu do API. Pro tento účel je vybrána mutace UpdateUser sloužící pro aktualizaci hodnot uživatele. Popis je rozdělen do jednotlivých bodů.

\subsection{Volání mutace klientem}
Na začátku uživatel (v tomto kontextu mobil nebo web) zavolá mutaci UpdateUser s parametry pro změnu jednotlivých polí entity User. Tento dotaz je zavolán na endpoint končící cestou /graphql. Tato cesta je definována v souboru routes.php. Na základě této definice se zavolá GraphQLController

\begin{lstlisting}[caption={Příklad input dat volání mutace UpdateUser},captionpos=b]
{
  "inputData": {
    "uid": "nMqDsIDr5ZR7Awf3TupvDI1B9xU2",
    "type": "ADMIN",
    "state": "ACTIVE",
    "email": "tomas.kuranko@gmail.com",
    "firstName": "Tomas",
    "lastName": "Kuranko",
    "birthYear": 1996,
    "gender": "MALE"
  }
}
\end{lstlisting}

\begin{lstlisting}[caption={Nastavení routy endpointu /graphql},captionpos=b]

    $app->get('/graphql', GraphQLController::class . ':index');
    $app->post('/graphql', GraphQLController::class . ':index');
\end{lstlisting}

\subsection{Přijetí dotazu od klienta, inicializace}
Po zavolání GraphQLController se vytvoří cache, SchemaFactory, AuthenticationService, AuthorizationService, samotná schéma a logger. Logger slouží pro debugovací účely při volání query a mutací. Následně se zavolá provádění samotné mutace prostřednictvím metody executeQuery na objektu GraphQL.

\begin{lstlisting}[caption={Vytvoření služeb a volání executeQuery v GraphQLController.php },captionpos=b]
$app = AppFactory::create();
$cache = new Cache();
$factory = new SchemaFactory($cache, $app->getContainer());
$factory->addRootTypeMapperFactory(new AnyScalarTypeMapperFactory());
$factory->addControllerNamespace('App\\Application\\Controllers\\QueryControllers')
    ->addTypeNamespace('App\\Application\\Model');
// Configure an authentication service (to resolve the @Logged annotations).
$authenticationService = new AuthenticationService();
$factory->setAuthenticationService($authenticationService);
// Configure an authorization service (to resolve the @Right annotations).
$authorizationService = AuthorizationService::getInstance();
$authorizationService->setAuthenticationService($authenticationService);
$factory->setAuthorizationService($authorizationService);

$schema = $factory->createSchema();

$input = $request->getParsedBody();
$this->logger->debug("GraphQL Request: " . json_encode($input));
$this->logger->debug("Session details: " . json_encode([$request->getCookieParams(), $_SESSION]));

$query = $input['query'] ?? '';

$variables = isset($input['variables']) ? $input['variables'] : null;

$context = [
    'db'      => $this->db,
    'logger'  => $this->logger,
    'auth'    => $this->auth
];

$result = GraphQL::executeQuery($schema, $query, null, $context, $variables);
\end{lstlisting}

\subsection{Autentikace a autorizace volajícího}
Dalším krokem je ověření přístupových práv volajícího uživatele k dané mutaci. Přístupová práva jsou definována v UserController.php v anotaci Security\footnote{Popis obecného použití anotace Security lze nalézt v kapitole Entity a definice databázi}. V parametru anotace je volání metody canManageUser s parametrem uid volajícího USERA. Tato metoda volá službu AuthorizationService, konkrétně metodu isAllowed, která vrátí, zda volající uživatel má nebo nemá přístup k mutaci.

\begin{lstlisting}[caption={Anotace Security v mutaci updateUser},captionpos=b]
* @Security("this.canManageUser(inputData.getUid())")
\end{lstlisting}

\subsection{Vyhodnocení dotazu}
\subsubsection{Získání uživatele, kterého hodnoty se mají aktualizovat}
Po udělení práva pro volání mutace UpdateUser nastane samotné volání a provádění této mutace v UserController.php. Mutaci UpdateUser je možné volat dvěma způsoby. Prvním je ten, že v parametrech volání nastavíme pole uid, které určuje pro jakého uživatele chceme aktualizovat data. Druhým způsobem je parametr uid nezadávat (hodnota bude null). V takovém případě se vytáhne uid uživatele ze session a aktualizují se data o tomtéž uživateli, který mutaci volá. 

\begin{lstlisting}[caption={Vytažení uid ze session},captionpos=b]
if (is_null($inputData->getUid()) && isset($_SESSION['uid'])) {
    $inputData->setUid($_SESSION['uid']);
}
\end{lstlisting}

\subsubsection{Ověření existence ve Firebase}
Po získání uid uživatele, jehož data chceme aktualizovat, se zkontroluje, zda uživatel existuje jednak ve Firebase a jednak v databázi. V případě, že někde nebude existovat, nastane chyba a vyhodí se výjimka.

\begin{lstlisting}[caption={Ověření existence uživatele ve Firebase a v databázi},captionpos=b]
//Test, if user does exist in Firebase
$this->firebase->getUser($inputData->getUid());

/** @var User|null $foundUser */
$foundUser = $this->userRepository->findOneBy((array('uid' => $inputData->getUid())));

if (is_null($foundUser)) {
    $result->setData(null);
    throw new Exception("Update failed: User does not exist in database");
}
\end{lstlisting}

\subsubsection{Aktualizace hodnot}
V případě, že uživatel se zadaným uid existuje ve Firebase i v databázi, nastane samotná aktualizace dat. Pokud hodnota některého pole při volání mutací chybí (její hodnota je null), zachovává se původní hodnota, která byla před voláním mutace. Následně se aktualizované hodnoty uloží na server a do Firebase.

\begin{lstlisting}[caption={Update nových hodnot a uložení do databáze a Firebase},captionpos=b]
//update new values
$newImage = $inputData->getUserImage();
if (!is_null($newImage)) {
    $oldImageIdentifier = $foundUser->getUserImageIdentifier();
    if (!is_null($oldImageIdentifier))
        $foundUser->setUserImageIdentifier($this->mediaService->replaceFromBase64($newImage, $oldImageIdentifier));
    else
        $foundUser->setUserImageIdentifier($this->mediaService->importFromBase64($newImage));
}

if(!is_null($inputData->getState())) $foundUser->setState($inputData->getState());
if(!is_null($inputData->getType())) $foundUser->setType($inputData->getType());
if(!is_null($inputData->getEmail())) $foundUser->setEmail($inputData->getEmail());
if(!is_null($inputData->getFirstName())) $foundUser->setFirstName($inputData->getFirstName());
if(!is_null($inputData->getLastName())) $foundUser->setLastName($inputData->getLastName());
if(!is_null($inputData->getBirthYear())) $foundUser->setBirthYear($inputData->getBirthYear());
if(!is_null($inputData->getGender())) $foundUser->setGender($inputData->getGender());
if(!is_null($inputData->getHasEmailNotificationsEnabled())) $foundUser->setHasEmailNotificationsEnabled($inputData->getHasEmailNotificationsEnabled());

//Update user in DB
$this->entityManager->flush();

//Update user in firebase
$this->firebase->updateUser($inputData->getUid(), [
    "email" => $foundUser->getEmail(),
    "displayName" => $foundUser->getFirstName() . ' ' . $foundUser->getLastName()
]);
\end{lstlisting}

\subsubsection{Vytvoření výsledku}
Posledním krokem v těle mutace je nastavení a vrácení Result dat. Result obsahuje pole data, success, errorMessage, errorCode a timestamp. V případě, že nikde nenastala během volání a provádění mutace chyba, položka data obsahuje samotná data, v tomto případě objekt uživatele. V případě, že chyba někde nastala, data budou obsahovat null hodnotu. Položka success obsahuje bool hodnotu a mluví o tom, zda dotaz proběhl v pořádku, nebo někde nastala během volání chyba. Položka errorMessage je vyplněna v případě, že někde nasatala chyba a obsahuje popis této chyby. Položka timestamp označuje datum a čas odpovědi na dotaz a položka ErrorCode je zatím defaultně nastavena na null. Do budoucna bude sloužit pro snadnější identifikaci chyby, která při volání nastala.

\begin{lstlisting}[caption={Výsledek volání mutace},captionpos=b]
{
    "data": {
        "updateUser": {
            "success": true,
            "errorMessage": "",
            "timestamp": "2021-05-20T12:44:24+02:00",
            "errorCode": null,
            "data": {
                "id": 35,
                "uid": "nMqDsIDr5ZR7Awf3TupvDI1B9xU2",
                "state": "ACTIVE",
                "type": "ADMIN",
                "email": "tomas.kuranko@gmail.com",
                "firstName": "a",
                "lastName": "a",                
                "hasEmailNotificationsEnabled": "ALLOWED_AND_DISABLED",
                "userImageURL": "",
                "isFullyRegistered": true,
                "birthYear": 0,
                "age": 2021,
                "gender": "MALE",                
                "notifications": [],
                "notificationsEnabled": null,
                "created": "2020-08-31T19:26:59+02:00",
                "modified": "2021-05-20T12:44:24+02:00"
            }
        }
    }
}
\end{lstlisting}

\subsection{Vrácení odpovědi klientovi}
Poté, co skončí vykonávání mutace prostřednictvím volání metody executeQuery na objektu GraphQL v souboru GraphQLController.php, se zapíše výsledek volání do resultu a také se zaloguje kvůli zjištění případných chyb během volání mutace. Posledním krokem je poslání výsledku pomocí GraphQL uživateli, který vznesl dotaz.

\section{Dokumentace GraphQL schématu}
GraphQL schéma je generováno pomocí GraphQLite knihovny na základě odpovídajících anotací v jednotlivých entitách a GraphQL typech. Bližší popis celého schématu lze získat pomocí introspekce dle následujícího návodu:

\begin{enumerate}
\item Instalace nástroje fetch-graphql-schema pomocí správce balíčků npm \footnote{\url{https://www.npmjs.com/}}
\begin{lstlisting}
npm install -g fetch-graphql-schema
\end{lstlisting}

\item  Získání schematu
\begin{lstlisting}
fetch-graphql-schema https://thea-api.jenda.dev/graphql -r -o schema.graphql
\end{lstlisting}
\end{enumerate}

Výsledné schéma s celkovým popisem je získáno z produkčního serveru a je uložena v souboru schema.graphql.

\section{Dokumentace databáze}
Databáze je generována z entit za pomoci frameworku doctrine. Je tvořena z tabulek reprezentujících jednotlivé entity. Nad každou tabulkou je definován primární klíč zpravidla označený jako id. Mezi tabulkami jsou definovány vztahy a integritní omezení. Bližší popis jednotlivých tabulek, sloupců tabulek a integritních omezení obsahuje \autoref{chap:appendice_db}.

\chapter{Instalační manuál}

Aplikace očekává poměrně standardní konfiguraci webového serveru. K úspěšnému spuštění aplikace je potřeba mít k dispozici Apache server s PHP 7.4, s povolenou konfigurací hlaviček přes soubory .htaccess a s rozšířením Imagick\footnote{\url{https://www.php.net/manual/en/book.imagick.php}} nakonfigurovaným tak, aby podporoval obrázky ve formátu Jpeg, Png a Webp.

\section{Příprava pro produkční použití}

Aplikaci není potřeba kompilovat, ale je potřeba nainstalovat všechny potřebné závislosti. K tomu slouží následující příkaz:

\begin{verbatim}
composer install --optimize-autoloader --prefer-dist --no-dev
\end{verbatim}

\section{Nasazení na produkční server}

Po nainstalování závislostí stačí aplikaci nasadit nahráním kompletního obsahu adresáře s aplikací na server.